<?php
/**
 * Created by PhpStorm.
 * User: iyahe@qq.com (天明)
 * Date: 2018/1/24
 * Time: 16:42
 */

$json = file_get_contents(__DIR__.'/rune.json');
$jsonArr = json_decode($json,true);
file_put_contents('rune.json',json_encode($jsonArr,JSON_UNESCAPED_UNICODE));