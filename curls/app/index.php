<?php
/**
 * Created by PhpStorm.
 * User: iyahe@qq.com (天明)
 * Date: 2018/1/24
 * Time: 15:42
 */

require_once dirname(__DIR__).'/vendor/autoload.php';
use QL\QueryList;

$html = file_get_contents(__DIR__.'/html/index.html');

$rules = array(
    'text' => array('li.mod-iconitem a','text'),
    'img' => array('li.mod-iconitem a img','data-original'),
    'id' => array('li.mod-iconitem','data-id'),
);
$data = QueryList::Query($html,$rules)->data;
print_r($data);

file_put_contents('user.json',json_encode($data,JSON_UNESCAPED_UNICODE));

