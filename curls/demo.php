<?php
/**
 * Created by PhpStorm.
 * User: iyahe@qq.com (天明)
 * Date: 2018/2/8
 * Time: 10:19
 */

$az = range('A','Z');
$count = 26;
$arr = [];
$all = 100;
for ($i = 0; $i < floor($all/$count); $i++)
{
    for ($j = 0; $j < $count; $j++)
    {
        if($i == 0){
            $arr[] = $az[$j];
        }else{
            $arr[] = $az[$i-1].$az[$j];
        }
    }
}