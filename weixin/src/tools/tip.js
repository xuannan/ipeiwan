/**
 * 提示与加载工具类
 */
export default class Tips {
    constructor() {
        this.isLoading = false
    }

    /**
     * 弹出提示框
     */

    static success(title, duration = 3000, fn) {
        wx.showToast({
            title: title,
            icon: 'success',
            mask: true,
            duration: duration
        });
        if (duration > 0) {
            setTimeout(() => {
                fn && fn();
            }, duration);
        }
    }

    /**
     * 弹出确认窗口
     */
    static confirm(text, fn, title = '提示') {
        return new Promise((resolve, reject) => {
            wx.showModal({
                title: title,
                content: text,
                showCancel: true,
                success: res => {
                    if (res.confirm) {
                        fn && fn(true)
                    } else if (res.cancel) {
                        fn && fn(false)
                    }
                },
                fail: res => {
                    reject(payload)
                }
            })
        })
    }

    static toast(title, onHide, icon = 'success') {
        wx.showToast({
            title: title,
            icon: icon,
            mask: true,
            duration: 500
        })
        // 隐藏结束回调
        if (onHide) {
            setTimeout(() => {
                onHide()
            }, 500)
        }
    }

    /**
     * 警告框
     */
    static alert(title,time=2500) {
        wx.showToast({
            title: title,
            image: 'http://img.he29.com/play/2d5a107009d6e4890d102582dfa57557fdf2766f.png',
            mask: true,
            duration: 2500
        })
    }

    /**
     * 错误框
     */

    static error(title,time=500,fn) {
        wx.showToast({
            title: title,
            image: 'http://img.he29.com/play/a51fa5be6a9969d284a70f683f77fed2c77c1450.png',
            mask: true,
            duration: 1000
        });
        // 隐藏结束回调
        setTimeout(() => {
            fn && fn()
        }, time)
    }

    /**
     * 弹出加载提示
     */
    static loading(title = '加载中') {
        if (Tips.isLoading) {
            return
        }
        Tips.isLoading = true
        wx.showLoading({
            title: title,
            mask: true
        })
    }

    /**
     * 加载完毕
     */
    static loaded() {
        if (Tips.isLoading) {
            Tips.isLoading = false
            wx.hideLoading()
        }
    }


    static share(title, url, desc) {
        return {
            title: title,
            path: url,
            desc: desc,
            success: function (res) {
                Tips.toast('分享成功')
            }
        }
    }
}

/**
 * 静态变量，是否加载中
 */
Tips.isLoading = false;
