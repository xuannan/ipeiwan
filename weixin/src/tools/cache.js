module.exports = {
    set:function (key,data,fn) {
        if(typeof data != 'string'){
            data = JSON.stringify(data);
        }
        wx.setStorageSync(key,data);
    },
    get:function (key,def='') {
        let data = this.getData(key);
        if(data) return data;
        return def;
    },
    getData:function (key) {
        let res = wx.getStorageSync(key);
        try{
            return JSON.parse(res);
        }catch(e){
            return res;
        }
    },
    del:function (key) {
        wx.removeStorageSync(key)
    }
};
