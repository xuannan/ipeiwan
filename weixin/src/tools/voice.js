const options = {
    duration: 10000,
    sampleRate: 44100,
    numberOfChannels: 1,
    encodeBitRate: 192000,
    format: 'aac',
    frameSize: 50
};

const voice = {
    config:options,
    start:function () {
        const recorderManager = wx.getRecorderManager();
        recorderManager.start(options);
        console.log('开始录音...');
    },
    stop:function (fn) {
        recorderManager.onStop((res) => {
            const { tempFilePath } = res;
            fn && fn (tempFilePath);
        });
    }
};


module.exports = voice
