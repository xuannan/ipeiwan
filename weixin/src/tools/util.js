function getCurrentTime() {
    var keep = '';
    var date = new Date();
    var y = date.getFullYear();
    var m = date.getMonth() + 1;
    m = m < 10 ? '0' + m : m;
    var d = date.getDate() < 10 ? '0' + date.getDate() : date.getDate();
    var h = date.getHours() < 10 ? '0' + date.getHours() : date.getHours();
    var f = date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes();
    var s = date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds();
    var rand = Math.round(Math.random() * 899 + 100);
    keep = y + '' + m + '' + d + '' + h + '' + f + '' + s;
    return keep; //20160614134947
}

function objLength(input) {
    var type = toString(input);
    var length = 0;
    if (type != "[object Object]") {
        //throw "输入必须为对象{}！"
    } else {
        for (var key in input) {
            if (key != "number") {
                length++;
            }

        }
    }
    return length;
}
//浮点型除法
function div(a, b) {
    var c, d, e = 0,
        f = 0;
    try {
        e = a.toString().split(".")[1].length;
    } catch (g) {
    }
    try {
        f = b.toString().split(".")[1].length;
    } catch (g) {
    }
    return c = Number(a.toString().replace(".", "")), d = Number(b.toString().replace(".", "")), mul(c / d, Math.pow(10, f - e));
}
//浮点型加法函数   
function accAdd(arg1, arg2) {
    var r1, r2, m;
    try {
        r1 = arg1.toString().split(".")[1].length;
    } catch (e) {
        r1 = 0;
    }
    try {
        r2 = arg2.toString().split(".")[1].length;
    } catch (e) {
        r2 = 0;
    }
    m = Math.pow(10, Math.max(r1, r2));
    return ((arg1 * m + arg2 * m) / m).toFixed(2);
}
//浮点型乘法
function mul(a, b) {
    var c = 0,
        d = a.toString(),
        e = b.toString();
    try {
        c += d.split(".")[1].length;
    } catch (f) {
    }
    try {
        c += e.split(".")[1].length;
    } catch (f) {
    }
    return Number(d.replace(".", "")) * Number(e.replace(".", "")) / Math.pow(10, c);
}

// 遍历对象属性和值
function displayProp(obj) {
    var names = "";
    for (var name in obj) {
        names += name + obj[name];
    }
    return names;
}
// 去除字符串所有空格
function sTrim(text) {
    return text.replace(/\s/ig, '')
}
//去除所有:
function replaceMaohao(txt) {
    return txt.replace(/\:/ig, '')
}
//转换星星分数
function convertStarArray(score) {
    //1 全星,0 空星,2半星
    var arr = []
    for (var i = 1; i <= 5; i++) {
        if (score >= i) {
            arr.push(1)
        } else if (score > i - 1 && score < i + 1) {
            arr.push(2)
        } else {
            arr.push(0)
        }
    }
    return arr
}
//获取当前data
function getData($data,$value) {
    try{
        let pat = $data.currentTarget;
        if($value){
            return pat.dataset[$value];
        }else{
            return pat.dataset;
        }
    }catch(e){
        return false;
    }
}

function goto(url) {
    wx.navigateTo({
        url: url
    })
}

function href(url) {
    wx.navigateTo({
        url: '/pages/'+url
    })
}

function getDateDiff(dateTimeStamp,frg=false) {
    if(!frg){
        dateTimeStamp = new Date(dateTimeStamp).getTime();
    }
    let minute = 1000 * 60;
    let hour = minute * 60;
    let day = hour * 24;
    let halfamonth = day * 15;
    let month = day * 30;
    let now = new Date().getTime();
    let diffValue = now - dateTimeStamp;
    if (diffValue < 0) {
        console.log("结束日期不能小于开始日期！");
    }
    let monthC = diffValue / month;
    let weekC = diffValue / (7 * day);
    let dayC = diffValue / day;
    let hourC = diffValue / hour;
    let minC = diffValue / minute;
    let result = '';
    if (monthC >= 1) {
        result = "" + parseInt(monthC) + "个月前";
    } else if (weekC >= 1) {
        result = "" + parseInt(weekC) + "周前";
    } else if (dayC >= 1) {
        result = "" + parseInt(dayC) + "天前";
    } else if (hourC >= 1) {
        result = "" + parseInt(hourC) + "小时前";
    } else if (minC >= 1) {
        result = "" + parseInt(minC) + "分钟前";
    } else {
        result = "刚刚";
    }
    return result;
}
function cutString(str, len) {
    if (str == null) return '这人好懒啊,什么介绍都没写.';
    if (str.length * 2 <= len) {
        return str;
    }
    let strlen = 0;
    let s = "";
    for (let i = 0; i < str.length; i++) {
        s = s + str.charAt(i);
        if (str.charCodeAt(i) > 128) {
            strlen = strlen + 2;
            if (strlen >= len) {
                return s.substring(0, s.length - 1) + "...";
            }
        } else {
            strlen = strlen + 1;
            if (strlen >= len) {
                return s.substring(0, s.length - 2) + "...";
            }
        }
    }
    return s;
}


function title(tmpTitle) {
    wx.setNavigationBarTitle({
        title: tmpTitle ,
        success:function(){},
        complete:function(){}
    });
}

function SystemInfo(model='') {
    let res = wx.getSystemInfoSync();
    if(model) res = res[model];
    return res;
}

function AddData(obj,obj2) {
    obj2.forEach((value)=>{
        obj.push(value)
    });
    return obj;
}

function goback(page=1) {
    wx.navigateBack({
        delta: page
    })
}

function getFulltime(str){
    let myDate = new Date(str);
    let leftTime = (myDate - new Date()); //计算剩余的毫秒数
    let days = parseInt(leftTime / 1000 / 60 / 60 / 24 , 10); //计算剩余的天数
    let hours = parseInt(leftTime / 1000 / 60 / 60 % 24 , 10); //计算剩余的小时
    let minutes = parseInt(leftTime / 1000 / 60 % 60, 10);//计算剩余的分钟
    let seconds = parseInt(leftTime / 1000 % 60, 10);//计算剩余的秒数

    days = checkTime(days);
    hours = checkTime(hours);
    minutes = checkTime(minutes);
    seconds = checkTime(seconds);
    let timder = days+"天" + hours+"小时" + minutes+"分";
    if(days <= 0){
        timder = hours+"小时" + minutes+"分";
    }
    if(hours <= 0){
        timder =  minutes+"分";
    }
    if(minutes <= 0){
        timder = seconds+"秒";
    }
    return {
        time:days+"天" + hours+"小时" + minutes+"分",
        timder:timder,
        days:days,
        hours:hours,
        minutes:minutes,
        seconds:seconds
    }
}
function checkTime(i){
    if(i<10) {
        i = "0" + i;
    }
    return i;
}


function nowTime() {
    var date = new Date();
    var seperator1 = "-";
    var seperator2 = ":";
    var month = date.getMonth() + 1;
    var strDate = date.getDate();
    if (month >= 1 && month <= 9) {
        month = "0" + month;
    }
    if (strDate >= 0 && strDate <= 9) {
        strDate = "0" + strDate;
    }

    return {
        s:date.getHours() + seperator2 + date.getMinutes()
        + seperator2 + date.getSeconds(),
        y:date.getFullYear() + seperator1 + month + seperator1 + strDate,
        t:date.getFullYear() + seperator1 + month + seperator1 + strDate
        + " " + date.getHours() + seperator2 + date.getMinutes()
        + seperator2 + date.getSeconds()
    }
}

function getRandomColor(){
    return '#'+Math.floor(Math.random()*16777215).toString(16);
}


function getColor (num){
    let all = [];
    for (let i = 0; i < num; i++) {
        all.push(this.getRandomColor())
    }
    return all;
}



module.exports = {
    getCurrentTime: getCurrentTime,
    objLength: objLength,
    displayProp: displayProp,
    sTrim: sTrim,
    replaceMaohao: replaceMaohao,
    div: div,
    mul: mul,
    accAdd: accAdd,
    convertStarArray: convertStarArray,
    get:getData,
    goto:goto,
    href:href,
    title:title,
    getDateDiff:getDateDiff,
    cutString:cutString,
    sysInfo:SystemInfo,
    AddData:AddData,
    goback:goback,
    getFulltime:getFulltime,
    nowTime:nowTime,
    getRandomColor:getRandomColor,
    getColor:getColor
}