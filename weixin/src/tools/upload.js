import {fileUpload} from '../api/request'
function uploadFile(fn) {
    wx.chooseImage({
        sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
        sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
        success: function (res) {
            // 返回选定照片的本地文件路径列表，tempFilePath可以作为img标签的src属性显示图片
            let file  = res.tempFilePaths[0];
            fileUpload('Upload/formData',file,{},function (res) {
                if(res.code > 0) {
                    fn && fn(res.info.url);
                }
            })
        }
    })
}
module.exports = {
    uploadFile
}
