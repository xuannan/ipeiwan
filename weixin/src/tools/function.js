import {href,get} from './util'
function goHome(url,event) {
    let uid = get(event,'uid');
    href(url+'?uid='+uid)
}
module.exports = {
    goHome:goHome
}