
function setTop(background,frontColor='#ffffff') {
    wx.setNavigationBarColor({
        frontColor:frontColor,
        backgroundColor: background,
        animation: {
            duration: 400,
            timingFunc: 'easeIn'
        }
    });
}

module.exports = {
    setTop:setTop
}
