import {sys} from '../config/index'
import {requrst} from '../api/request'
import config from '../config/global'
import wepy from 'wepy'
const login = function (fn) {
    wx.login({
        success: function(res) {
            if (res.code) {
                let url = `https://api.weixin.qq.com/sns/jscode2session?appid=`+sys.appid+`&secret=`+sys.secret+`&js_code=`+res.code+`&grant_type=authorization_code`;
                requrst('config/login',{
                    url:url,
                    type:config.webType
                },(res)=>{
                    console.log(res.info);
                    fn && fn(res.info,true);
                });
            }else{
                fn && fn(res,false);
            }
        }
    });
};

const isLogin = function (fn) {
    wx.checkSession({
        success: function(){
            fn && fn(true)
        },
        fail: function(){
            fn && fn(false)
        }
    })
};


const userInfo = function (fn,su,er) {
    wx.getUserInfo({
        lang:'zh_CN',
        success: function(res) {
            fn && fn(res);
        },
        fail:function (res) {
            su && su();
        },
        complete:function (res) {
            er && er(res);
        }
    })
}

const getUserInfo = function(cb){
    console.log('getUserInfo')
    wepy.getUserInfo({
        lang:'zh_CN',
        success (res) {
            //that.globalData.userInfo = res.userInfo;
            console.log(res)
            cb && cb(res.userInfo)
        },
        fail:function (res) {
            console.log(res)
            cb && cb(false)
        }
    })
}




module.exports = {
    login:login,
    isLogin:isLogin,
    userInfo:userInfo,
    getUserInfo:getUserInfo
};
