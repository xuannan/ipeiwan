import {wxRequest,fileRequest} from '../tools/wxRequest';
let env = "-test";
const apiMall = 'http://play.he29.com/index.php?s=/api/';
const indexBannerList = (params,fn) => wxRequest(params, apiMall + 'config/banner',(res)=>{
    fn && fn(res);
});
const indexBannerTab = (params,fn) => wxRequest(params, apiMall + 'config/tab4',(res)=>{
    fn && fn(res);
});

const requrst = (name,params,fn,loading=true) => wxRequest(params, apiMall + name,(res)=>{
    fn && fn(res);
},loading);

const fileUpload = (name,path,params,fn) =>fileRequest(params, path ,apiMall + name,(res)=>{
    if(res.data){
        res = JSON.parse(res.data);
    }
    fn && fn(res);
});

module.exports = {
    indexBannerList,
    indexBannerTab,
    requrst,
    fileUpload
};
