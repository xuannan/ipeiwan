/*v0.6vv_20170214_fbi*/
window.__wcc_version__='v0.6vv_20170214_fbi'
window.__wxml_transpiler_version__='v0.1'
var $gwxc
var $gaic={}
$gwx=function(path,global){
function _(a,b){b&&a.children.push(b);}
function _v(k){if(typeof(k)!='undefined')return {tag:'virtual','wxKey':k,children:[]};return {tag:'virtual',children:[]};}
function _n(tag){$gwxc++;if($gwxc>=16000){throw 'Dom limit exceeded, please check if there\'s any mistake you\'ve made.'};return {tag:tag.substr(0,3)=='wx-'?tag:'wx-'+tag,attr:{},children:[],n:[]}}
function _p(a,b){b&&a.properities.push(b);}
function _s(scope,env,key){return typeof(scope[key])!='undefined'?scope[key]:env[key]}function _wl(tname,prefix){console.warn('WXMLRT:'+prefix+':-1:-1:-1: Template `' + tname + '` is being called recursively, will be stop.')}$gwn=console.warn;
$gwl=console.log;
function $gwh()
{
function x(){}
x.prototype =
{
hn: function( obj )
{
if( typeof(obj) == 'object' )
{
var cnt=0;
var any=false;
for(var x in obj)
{
any|=x==='__value__';
cnt++;
if(cnt>2)break;
}
return cnt == 2 && any && obj.hasOwnProperty('__wxspec__') ? "h" : "n";
}
return "n";
},
nh: function( obj, special )
{
return { __value__: obj, __wxspec__: special ? special : true }
},
rv: function( obj )
{
return this.hn(obj)==='n'?obj:this.rv(obj.__value__);
}
}
return new x;
}
wh=$gwh();
function $gwrt( should_pass_type_info )
{
function ArithmeticEv( ops, e, s, g, o )
{
var rop = ops[0][1];
var _a,_b,_c,_d, _aa, _bb;
switch( rop )
{
case '?:':
_a = rev( ops[1], e, s, g, o );
_c = should_pass_type_info && ( wh.hn(_a) === 'h' );
_d = wh.rv( _a ) ? rev( ops[2], e, s, g, o ) : rev( ops[3], e, s, g, o );
_d = _c && wh.hn( _d ) === 'n' ? wh.nh( _d, 'c' ) : _d;
return _d;
break;
case '&&':
_a = rev( ops[1], e, s, g, o );
_c = should_pass_type_info && ( wh.hn(_a) === 'h' );
_d = wh.rv( _a ) ? rev( ops[2], e, s, g, o ) : wh.rv( _a );
_d = _c && wh.hn( _d ) === 'n' ? wh.nh( _d, 'c' ) : _d;
return _d;
break;
case '||':
_a = rev( ops[1], e, s, g, o );
_c = should_pass_type_info && ( wh.hn(_a) === 'h' );
_d = wh.rv( _a ) ? wh.rv(_a) : rev( ops[2], e, s, g, o );
_d = _c && wh.hn( _d ) === 'n' ? wh.nh( _d, 'c' ) : _d;
return _d;
break;
case '+':
case '*':
case '/':
case '%':
case '|':
case '^':
case '&':
case '===':
case '==':
case '!=':
case '!==':
case '>=':
case '<=':
case '>':
case '<':
case '<<':
case '>>':
_a = rev( ops[1], e, s, g, o );
_b = rev( ops[2], e, s, g, o );
_c = should_pass_type_info && (wh.hn( _a ) === 'h' || wh.hn( _b ) === 'h');
switch( rop )
{
case '+':
_d = wh.rv( _a ) + wh.rv( _b );
break;
case '*':
_d = wh.rv( _a ) * wh.rv( _b );
break;
case '/':
_d = wh.rv( _a ) / wh.rv( _b );
break;
case '%':
_d = wh.rv( _a ) % wh.rv( _b );
break;
case '|':
_d = wh.rv( _a ) | wh.rv( _b );
break;
case '^':
_d = wh.rv( _a ) ^ wh.rv( _b );
break;
case '&':
_d = wh.rv( _a ) & wh.rv( _b );
break;
case '===':
_d = wh.rv( _a ) === wh.rv( _b );
break;
case '==':
_d = wh.rv( _a ) == wh.rv( _b );
break;
case '!=':
_d = wh.rv( _a ) != wh.rv( _b );
break;
case '!==':
_d = wh.rv( _a ) !== wh.rv( _b );
break;
case '>=':
_d = wh.rv( _a ) >= wh.rv( _b );
break;
case '<=':
_d = wh.rv( _a ) <= wh.rv( _b );
break;
case '>':
_d = wh.rv( _a ) > wh.rv( _b );
break;
case '<':
_d = wh.rv( _a ) < wh.rv( _b );
break;
case '<<':
_d = wh.rv( _a ) << wh.rv( _b );
break;
case '>>':
_d = wh.rv( _a ) >> wh.rv( _b );
break;
default:
break;
}
return _c ? wh.nh( _d, "c" ) : _d;
break;
case '-':
_a = ops.length === 3 ? rev( ops[1], e, s, g, o ) : 0;
_b = ops.length === 3 ? rev( ops[2], e, s, g, o ) : rev( ops[1], e, s, g, o );
_c = should_pass_type_info && (wh.hn( _a ) === 'h' || wh.hn( _b ) === 'h');
_d = _c ? wh.rv( _a ) - wh.rv( _b ) : _a - _b;
return _c ? wh.nh( _d, "c" ) : _d;
break;
case '!':
_a = rev( ops[1], e, s, g, o );
_c = should_pass_type_info && (wh.hn( _a ) == 'h');
_d = !wh.rv(_a);
return _c ? wh.nh( _d, "c" ) : _d;
case '~':
_a = rev( ops[1], e, s, g, o );
_c = should_pass_type_info && (wh.hn( _a ) == 'h');
_d = ~wh.rv(_a);
return _c ? wh.nh( _d, "c" ) : _d;
default:
$gwn('unrecognized op' + rop );
}
}
function rev( ops, e, s, g, o )
{
var op = ops[0];
if( typeof(op)==='object' )
{
var vop=op[0];
var _a, _aa, _b, _bb, _c, _d, _s, _e, _ta, _tb, _td;
switch(vop)
{
case 2:
return ArithmeticEv(ops,e,s,g,o);
break;
case 4:
return rev( ops[1], e, s, g, o );
break;
case 5:
switch( ops.length )
{
case 2:
return should_pass_type_info ?
[rev(ops[1],e,s,g,o)] :
[wh.rv(rev(ops[1],e,s,g,o))];
break;
case 1:
return [];
break;
default:
_a = rev( ops[1],e,s,g,o );
_a.push(
should_pass_type_info ?
rev( ops[2],e,s,g,o ) :
wh.rv( rev(ops[2],e,s,g,o) )
);
return _a;
break;
}
break;
case 6:
_a = rev(ops[1],e,s,g,o);
_ta = wh.hn(_a)==='h';
_aa = _ta ? wh.rv(_a) : _a;
o.is_affected |= _ta;
if( should_pass_type_info )
{
if( _aa===null || typeof(_aa) === 'undefined' )
{
return _ta ? wh.nh(undefined, 'e') : undefined;
}
_b = rev(ops[2],e,s,g,o);
_tb = wh.hn(_b) === 'h';
_bb = _tb ? wh.rv(_b) : _b;
o.is_affected |= _tb;
if( _bb===null || typeof(_bb) === 'undefined' )
{
return (_ta || _tb) ? wh.nh(undefined, 'e') : undefined;
}
_d = _aa[_bb];
_td = wh.hn(_d)==='h';
o.is_affected |= _td;
return (_ta || _tb) ? (_td ? _d : wh.nh(_d, 'e')) : _d;
}
else
{
if( _aa===null || typeof(_aa) === 'undefined' )
{
return undefined;
}
_b = rev(ops[2],e,s,g,o);
_tb = wh.hn(_b) === 'h';
_bb = _tb ? wh.rv(_b) : _b;
o.is_affected |= _tb;
if( _bb===null || typeof(_bb) === 'undefined' )
{
return undefined;
}
_d = _aa[_bb];
_td = wh.hn(_d)==='h';
o.is_affected |= _td;
return _td ? wh.rv(_d) : _d;
}
case 7:
switch(ops[1][0])
{
case 11:
o.is_affected |= wh.hn(g)==='h';
return g;
case 3:
_s = wh.rv( s );
_e = wh.rv( e );
_b = ops[1][1];
_a = _s && _s.hasOwnProperty(_b) ?
s : _e && ( _e.hasOwnProperty(_b) ? e : undefined );
if( should_pass_type_info )
{
if( _a )
{
_ta = wh.hn(_a) === 'h';
_aa = _ta ? wh.rv( _a ) : _a;
_d = _aa[_b];
_td = wh.hn(_d) === 'h';
o.is_affected |= _ta || _td;
_d = _ta && !_td ? wh.nh(_d,'e') : _d;
return _d;
}
}
else
{
if( _a )
{
_ta = wh.hn(_a) === 'h';
_aa = _ta ? wh.rv( _a ) : _a;
_d = _aa[_b];
_td = wh.hn(_d) === 'h';
o.is_affected |= _ta || _td;
return wh.rv(_d);
}
}
return undefined;
}
break;
case 8:
_a = {};
_a[ops[1]] = rev(ops[2],e,s,g,o);
return _a;
break;
case 9:
_a = rev(ops[1],e,s,g,o);
_b = rev(ops[2],e,s,g,o);
function merge( _a, _b, _ow )
{
_ta = wh.hn(_a)==='h';
_tb = wh.hn(_b)==='h';
_aa = wh.rv(_a);
_bb = wh.rv(_b);
if( should_pass_type_info )
{
if( _tb )
{
for(var k in _bb)
{
if ( _ow || !_aa.hasOwnProperty(k) )
_aa[k]=wh.nh(_bb[k],'e');
}
}
else
{
for(var k in _bb)
{
if ( _ow || !_aa.hasOwnProperty(k) )
_aa[k]=_bb[k];
}
}
}
else
{
for(var k in _bb)
{
if ( _ow || _aa.hasOwnProperty(k) )
_aa[k]=wh.rv(_bb[k]);
}
}
return _a;
}
var _c = _a
var _ow = true
if ( typeof(ops[1][0]) === "object" && ops[1][0][0] === 10 ) {
_a = _b
_b = _c
_ow = false
}
if ( typeof(ops[1][0]) === "object" && ops[1][0][0] === 10 ) {
var _r = {}
return merge( merge( _r, _a, _ow ), _b, _ow );
}
else
return merge( _a, _b, _ow );
break;
case 10:
return should_pass_type_info ? rev(ops[1],e,s,g,o) : wh.rv(rev(ops[1],e,s,g,o));
}
}
else
{
if( op === 3 || op === 1 ) return ops[1];
else if( op === 11 )
{
var _a='';
for( var i = 1 ; i < ops.length ; i++ )
{
var xp = wh.rv(rev(ops[i],e,s,g,o));
_a += typeof(xp) === 'undefined' ? '' : xp;
}
return _a;
}
}
}
return rev;
}
gra=$gwrt(true);
grb=$gwrt(false);
function TestTest( expr, ops, e,s,g, expect_a, expect_b, expect_affected )
{
{
var o = {is_affected:false};
var a = gra( ops, e,s,g, o );
if( JSON.stringify(a) != JSON.stringify( expect_a )
|| o.is_affected != expect_affected )
{
console.warn( "A. " + expr + " get result " + JSON.stringify(a) + ", " + o.is_affected + ", but " + JSON.stringify( expect_a ) + ", " + expect_affected + " is expected" );
}
}
{
var o = {is_affected:false};
var a = grb( ops, e,s,g, o );
if( JSON.stringify(a) != JSON.stringify( expect_b )
|| o.is_affected != expect_affected )
{
console.warn( "B. " + expr + " get result " + JSON.stringify(a) + ", " + o.is_affected + ", but " + JSON.stringify( expect_b ) + ", " + expect_affected + " is expected" );
}
}
}

function wfor( to_iter, func, env, _s, global, father, itemname, indexname, keyname )
{
var _n = wh.hn( to_iter ) === 'n';
var scope = wh.rv( _s );
var has_old_item = scope.hasOwnProperty(itemname);
var has_old_index = scope.hasOwnProperty(indexname);
var old_item = scope[itemname];
var old_index = scope[indexname];
var full = Object.prototype.toString.call(wh.rv(to_iter));
var type = full[8];
if( type === 'N' && full[10] === 'l' ) type = 'X';
var _y;
if( _n )
{
if( type === 'A' )
{
for( var i = 0 ; i < to_iter.length ; i++ )
{
scope[itemname] = to_iter[i];
scope[indexname] = wh.nh(i, 'h');
_y = keyname ? (keyname==="*this" ? _v(wh.rv(to_iter[i])) : _v(wh.rv(wh.rv(to_iter[i])[keyname]))) : _v();
_(father,_y);
func( env, scope, _y, global );
}
}
else if( type === 'O' )
{
for( var k in to_iter )
{
scope[itemname] = to_iter[k];
scope[indexname] = wh.nh(k, 'h');
_y = keyname ? (keyname==="*this" ? _v(wh.rv(to_iter[k])) : _v(wh.rv(wh.rv(to_iter[k])[keyname]))) : _v();
_(father,_y);
func( env,scope,_y,global );
}
}
else if( type === 'S' )
{
for( var i = 0 ; i < to_iter.length ; i++ )
{
scope[itemname] = to_iter[i];
scope[indexname] = wh.nh(i, 'h');
_y = _v( to_iter[i] + i );
_(father,_y);
func( env, scope, _y, global );
}
}
else if( type === 'N' )
{
for( var i = 0 ; i < to_iter ; i++ )
{
scope[itemname] = i;
scope[indexname] = wh.nh(i, 'h');
_y = _v( i );
_(father,_y);
func(env,scope,_y,global);
}
}
else
{
}
}
else
{
var r_to_iter = wh.rv(to_iter);
var r_iter_item, iter_item;
if( type === 'A' )
{
for( var i = 0 ; i < r_to_iter.length ; i++ )
{
iter_item = r_to_iter[i];
iter_item = wh.hn(iter_item)==='n' ? wh.nh(iter_item,'h') : iter_item;
r_iter_item = wh.rv( iter_item );
scope[itemname] = iter_item
scope[indexname] = wh.nh(i, 'h');
_y = keyname ? (keyname==="*this" ? _v(r_iter_item) : _v(wh.rv(r_iter_item[keyname]))) : _v();
_(father,_y);
func( env, scope, _y, global );
}
}
else if( type === 'O' )
{
for( var k in r_to_iter )
{
iter_item = r_to_iter[k];
iter_item = wh.hn(iter_item)==='n'? wh.nh(iter_item,'h') : iter_item;
r_iter_item = wh.rv( iter_item );
scope[itemname] = iter_item;
scope[indexname] = wh.nh(k, 'h');
_y = keyname ? (keyname==="*this" ? _v(r_iter_item) : _v(wh.rv(r_iter_item[keyname]))) : _v();
_(father,_y);
func( env, scope, _y, global );
}
}
else if( type === 'S' )
{
for( var i = 0 ; i < r_to_iter.length ; i++ )
{
scope[itemname] = wh.nh(r_to_iter[i],'h');
scope[indexname] = wh.nh(i, 'h');
_y = _v( to_iter[i] + i );
_(father,_y);
func( env, scope, _y, global );
}
}
else if( type === 'N' )
{
for( var i = 0 ; i < r_to_iter ; i++ )
{
scope[itemname] = wh.nh(i,'h');
scope[indexname]= wh.nh(i,'h');
_y = _v( i );
_(father,_y);
func(env,scope,_y,global);
}
}
else
{
}
}
if(has_old_item)
{
scope[itemname]=old_item;
}
else
{
delete scope[itemname];
}
if(has_old_index)
{
scope[indexname]=old_index;
}
else{delete scope[indexname];}}


function _r( node, attrname, opindex, env, scope, global )
{
var o = {};
var a = grb( z[opindex], env, scope, global, o );
node.attr[attrname] = a;
if( o.is_affected ) node.n.push( attrname );
}
function _o( opindex, env, scope, global )
{
var nothing = {};
return grb( z[opindex], env, scope, global, nothing );
}
function _1( opindex, env, scope, global )
{
var nothing = {};
return gra( z[opindex], env, scope, global, nothing );
}
function _2( opindex, func, env, scope, global, father, itemname, indexname, keyname )
{
var to_iter = _1( opindex, env, scope, global, father, itemname, indexname, keyname );
wfor( to_iter, func, env, scope, global, father, itemname, indexname, keyname );
}
function _gv( )
{
if( typeof(window.__webview_engine_version__) == 'undefined' ) return 0.0;
return window.__webview_engine_version__;
}
function _m(tag,attrs,env,scope,global)
{
var tmp=_n(tag);
var base=0;
for(var i = 0 ; i < attrs.length ; i+=2 )
{
if(attrs[i+1]<0)
{tmp.attr[attrs[i]]=true;}else{_r(tmp,attrs[i],base+attrs[i+1],env,scope,global);
if(base===0)base=attrs[i+1];}}return tmp;};function _ai(i,p,e,me,r,c){var x=_grp(p,e,me);if(x)i.push(x);else{i.push('');console.warn('WXMLRT:'+me+':import:'+r+':'+c+': Path `'+p+'` not found from `'+me+'`.')}}function _grp(p,e,me){if(p[0]!='/'){var mepart=me.split('/');mepart.pop();var ppart=p.split('/');for(var i=0;i<ppart.length;i++){if(ppart[i]=='..')mepart.pop();else if(ppart[i]=='.' || !ppart[i])continue;else mepart.push(ppart[i]);}p=mepart.join('/');}if(me[0]=='.'&&p[0]=='/')p='.'+p;if(e[p])return p;if(e[p+'.wxml'])return p+'.wxml';}function _gd(p,c,e,d){if(!c)return;if(d[p][c])return d[p][c];for(var x=e[p].i.length-1;x>=0;x--){if(e[p].i[x]&&d[e[p].i[x]][c])return d[e[p].i[x]][c]};for(var x=e[p].ti.length-1;x>=0;x--){var q=_grp(e[p].ti[x],e,p);if(q&&d[q][c])return d[q][c]}
var ii=_gapi(e,p);for(var x=0;x<ii.length;x++){if(ii[x]&&d[ii[x]][c])return d[ii[x]][c]}for(var k=e[p].j.length-1;k>=0;k--)if(e[p].j[k]){for(var q=e[e[p].j[k]].ti.length-1;q>=0;q--){var pp=_grp(e[e[p].j[k]].ti[q],e,p);if(pp&&d[pp][c]){return d[pp][c]}}}}function _gapi(e,p){if(!p)return [];if($gaic[p]){return $gaic[p]};var ret=[],q=[],h=0,t=0,put={},visited={};q.push(p);visited[p]=true;t++;while(h<t){var a=q[h++];for(var i=0;i<e[a].ic.length;i++){var nd=e[a].ic[i];var np=_grp(nd,e,a);if(np&&!visited[np]){visited[np]=true;q.push(np);t++;}}for(var i=0;a!=p&&i<e[a].ti.length;i++){var ni=e[a].ti[i];var nm=_grp(ni,e,a);if(nm&&!put[nm]){put[nm]=true;ret.push(nm);}}}$gaic[p]=ret;return ret;}var $ixc={};function _ic(p,ent,me,e,s,r,gg){var x=_grp(p,ent,me);ent[me].j.push(x);if(x){if($ixc[x]){console.warn('WXMLRT:-1:include:-1:-1: `'+p+'` is being included in a loop, will be stop.');return;}$ixc[x]=true;try{ent[x].f(e,s,r,gg)}catch(e){}$ixc[x]=false;}else{console.warn('WXMLRT:'+me+':include:-1:-1: Included path `'+p+'` not found from `'+me+'`.')}}function _w(tn,f,line,c){console.warn('WXMLRT:'+f+':template:'+line+':'+c+': Template `'+tn+'` not found.');}function _ev(dom){var changed=false;delete dom.properities;delete dom.n;if(dom.children){do{changed=false;var newch = [];for(var i=0;i<dom.children.length;i++){var ch=dom.children[i];if( ch.tag=='virtual'){changed=true;for(var j=0;ch.children&&j<ch.children.length;j++){newch.push(ch.children[j]);}}else { newch.push(ch); } } dom.children = newch; }while(changed);for(var i=0;i<dom.children.length;i++){_ev(dom.children[i]);}} return dom; }var e_={}
if(global&&typeof(global.entrys)=='object')e_=global.entrys
var d_={}
if(global&&typeof(global.defines)=='object')d_=global.defines
var p_={}
var z = [];
  (function(z){
    var a = 11;
    function Z(ops){z.push(ops)};
    Z([3, 'chat']);Z([3, 'j_page']);Z([[2, "*"], [[7],[3, "windowHeight"]], [1, 10]]);Z([3, 'true']);Z([3, 'msg-list']);Z([[7],[3, "chatList"]]);Z([3, 'key']);Z([3, 'list']);Z([[7],[3, "index"]]);Z([[6],[[7],[3, "chatList"]],[3, "length"]]);Z([[2, "||"],[[2, "=="], [[7],[3, "index"]], [[2, "-"], [[6],[[7],[3, "chatList"]],[3, "length"]], [1, 1]]],[[2, "=="], [[7],[3, "index"]], [1, 0]]]);Z([3, 'time']);Z([a, [3, '\r\n                        '],[[6],[[7],[3, "item"]],[3, "chat_update"]],[3, '\r\n                    ']]);Z([[2, "!="], [[6],[[7],[3, "item"]],[3, "left"]], [1, 0]]);Z([3, 'receiver winu-flex-box-horizontal']);Z([3, 'user-pic']);Z([[6],[[7],[3, "item"]],[3, "header"]]);Z([3, 'area winu-flex-item']);Z([3, 'content']);Z([3, 'receiver-jt']);Z([a, [3, '\r\n                                '],[[6],[[7],[3, "item"]],[3, "content"]],[3, '\r\n                            ']]);Z([3, 'sender winu-flex-box-horizontal']);Z([3, 'sender-jt']);Z([3, 'weui-search-bar']);Z([3, 'z-index:999;background:#dddddd']);Z([3, 'weui-search-bar__form']);Z([3, 'weui-search-bar__box']);Z([3, 'text-align:left;padding-left:10px;']);Z([1, false]);Z([3, 'weui-icon-search_in-box']);Z([3, '14']);Z([3, 'top:11px']);Z([3, 'success']);Z([3, 'inputTyping']);Z([3, 'weui-search-bar__input']);Z([[7],[3, "inputShowed"]]);Z([3, '请输入聊天内容']);Z([3, 'height:35px;line-height:35px;']);Z([3, 'text']);Z([[7],[3, "inputVal"]]);Z([[2, ">"], [[6],[[7],[3, "inputVal"]],[3, "length"]], [1, 0]]);Z([3, 'clearInput']);Z([3, 'weui-icon-clear']);Z([3, 'clearText']);Z([3, 'clear']);Z([3, 'sednMessage']);Z([3, 'sendBtns weui-search-bar__cancel-btn']);Z([[2, "!"], [[7],[3, "inputShowed"]]]);Z([3, 'margin-top:5rpx;margin-left:8px;']);Z([3, ' 发 送 ']);
  })(z);d_["./pages/chat/index.wxml"] = {};
  var m0=function(e,s,r,gg){
    var oEj = _m( "view", ["class", 0,"id", 0], e, s, gg);var oFj = _m( "view", ["scrollY", -1,"id", 1,"scrollTop", 1,"scrollWithAnimation", 2], e, s, gg);var oGj = _n("view");_r(oGj, 'class', 4, e, s, gg);var oHj = _v();var oIj = function(oMj,oLj,oKj,gg){var oJj = _m( "view", ["class", 7,"data-index", 1,"data-len", 2], oMj, oLj, gg);var oOj = _v();
      if (_o(10, oMj, oLj, gg)) {
        oOj.wxVkey = 1;var oPj = _n("view");_r(oPj, 'class', 11, oMj, oLj, gg);var oRj = _o(12, oMj, oLj, gg);_(oPj,oRj);_(oOj, oPj);
      } _(oJj,oOj);var oSj = _v();
      if (_o(13, oMj, oLj, gg)) {
        oSj.wxVkey = 1;var oTj = _n("view");_r(oTj, 'class', 14, oMj, oLj, gg);var oVj = _m( "image", ["class", 15,"src", 1], oMj, oLj, gg);_(oTj,oVj);var oWj = _n("view");_r(oWj, 'class', 17, oMj, oLj, gg);var oXj = _n("view");_r(oXj, 'class', 18, oMj, oLj, gg);var oYj = _n("text");_r(oYj, 'class', 19, oMj, oLj, gg);_(oXj,oYj);var oZj = _o(20, oMj, oLj, gg);_(oXj,oZj);_(oWj,oXj);_(oTj,oWj);_(oSj, oTj);
      }else {
        oSj.wxVkey = 2;var oaj = _n("view");_r(oaj, 'class', 21, e, s, gg);var ocj = _n("view");_r(ocj, 'class', 17, e, s, gg);var odj = _n("view");_r(odj, 'class', 18, e, s, gg);var oej = _n("text");_r(oej, 'class', 22, e, s, gg);_(odj,oej);var ofj = _o(20, e, s, gg);_(odj,ofj);_(ocj,odj);_(oaj,ocj);var ogj = _m( "image", ["class", 15,"src", 1], e, s, gg);_(oaj,ogj);_(oSj, oaj);
      }_(oJj,oSj);_(oKj, oJj);return oKj;};_2(5, oIj, e, s, gg, oHj, "item", "index", 'key');_(oGj,oHj);_(oFj,oGj);_(oEj,oFj);var ohj = _m( "view", ["class", 23,"style", 1], e, s, gg);var oij = _n("view");_r(oij, 'class', 25, e, s, gg);var ojj = _m( "view", ["class", 26,"style", 1], e, s, gg);var okj = _v();
      if (_o(28, e, s, gg)) {
        okj.wxVkey = 1;var olj = _m( "icon", ["class", 29,"size", 1,"style", 2,"type", 3], e, s, gg);_(okj, olj);
      } _(ojj,okj);var onj = _m( "input", ["autoFocus", -1,"bindinput", 33,"class", 1,"focus", 2,"placeholder", 3,"style", 4,"type", 5,"value", 6], e, s, gg);_(ojj,onj);var ooj = _v();
      if (_o(40, e, s, gg)) {
        ooj.wxVkey = 1;var opj = _m( "view", ["bindtap", 41,"class", 1], e, s, gg);var orj = _m( "icon", ["size", 30,"bindtap", 13,"type", 14], e, s, gg);_(opj,orj);_(ooj, opj);
      } _(ojj,ooj);_(oij,ojj);_(ohj,oij);var osj = _m( "view", ["bindtap", 45,"class", 1,"hidden", 2,"style", 3], e, s, gg);var otj = _o(49, e, s, gg);_(osj,otj);_(ohj,osj);_(oEj,ohj);_(r,oEj);
    return r;
  };
        e_["./pages/chat/index.wxml"]={f:m0,j:[],i:[],ti:[],ic:[]};
if(path&&e_[path]){
window.__wxml_comp_version__=0.02
return function(env,dd,global){$gwxc=0;var root={"tag":"wx-page"};root.children=[]
var main=e_[path].f
if(typeof(window.__webview_engine_version__)!='undefined'&&window.__webview_engine_version__+1e-6>=0.02+1e-6&&window.__mergeData__)
{env=window.__mergeData__(env,dd);}
try{
main(env,{},root,global);
if(typeof(window.__webview_engine_version__)=='undefined'||window.__webview_engine_version__+1e-6<0.01+1e-6){return _ev(root);}}catch(err){console.log(err)}return root;}}}@code-separator-line:#j_page{margin-bottom:%%?100rpx?%%}.sendBtns{background:rgba(67,133,246,.83);color:#fff;padding:%%?5rpx?%% %%?20rpx?%%;border-radius:5px}.weui-search-bar{background:#;border:none;position:fixed;bottom:0;width:100%}.weui-search-bar .weui-search-bar__form{border:none}.weui-search-bar .weui-search-bar__form .weui-icon-search_in-box{color:#e6e8f7}.weui-search-bar .weui-search-bar__form .weui-search-bar__input{color:#333}.winu-flex-box-horizontal{display:-webkit-box;display:-webkit-flex;display:flex;-webkit-box-orient:horizontal;-webkit-flex-flow:row;flex-flow:row;width:100%;box-sizing:border-box}.winu-flex-item{-webkit-box-flex:1;-webkit-flex:1;flex:1}.msg-list{padding:8px 5px 15px 0;min-height:0;height:auto}.receiver,.sender{padding:10px;clear:both}.user-pic{width:42px;height:42px;border-radius:50%;-webkit-border-radius:50%}.status{width:24px;display:inline-block;height:24px;line-height:24px;text-align:center;vertical-align:bottom;position:absolute;top:9px;left:0;background:#e3e7ec;border-radius:50%;-webkit-border-radius:50%}.status wx-i{color:#556893}.status.red{background:red}.status.red wx-i{color:#fff;font-size:14px}.status.rotate{animation:1s linear 0s normal none infinite rotate;-webkit-animation:1s linear 0s normal none infinite rotate}.area{margin:0 10px;position:relative;padding-right:42px}.sender .area{padding-right:0;padding-left:42px}.content{position:relative;background:#4385f6;color:#fff;padding:10px;font-size:14px;line-height:24px;border-radius:10px;-webkit-border-radius:10px;margin-left:6px;display:inline-block}.content .user-pic,.content wx-image{max-width:240px!important;max-height:142px!important;overflow:hidden}.receiver-jt,.sender-jt{font-style:normal;display:block;height:0;width:0;border-width:7px;border-style:solid;border-color:transparent #4385f6 transparent transparent;position:absolute;top:10px;left:-14px;z-index:12}.sender .content{background:#e3e7ec;color:#556893;margin-right:6px;margin-left:0;float:right}.sender-jt{border-color:transparent transparent transparent #e3e7ec;right:-14px;left:auto}.voice{text-align:left;padding-right:10px}.voice wx-label{padding-left:10px}.sender .voice{text-align:right;padding-left:10px}.sender .voice wx-label{padding-left:0;padding-right:10px}.voice wx-i.icon-shengyin2{font-size:20px;vertical-align:middle}.time{font-size:12px;text-align:center;height:24px;line-height:24px;color:#9daeca}.userinfo{padding:0 0 7px 5px;font-size:12px;color:#666;text-align:left}.level{display:inline-block;vertical-align:middle;font-size:9px;color:#fff;background:#1d9dd5;height:15px;line-height:15px;padding:0 4px;border-radius:2px;-webkit-border-radius:2px;margin-left:10px}.sender .level{margin-left:0;margin-right:10px}.sender .userinfo{padding:0 5px 7px 0;text-align:right}@keyframes rotate{wx-from{-webkit-transform:rotate(0)}wx-to{-webkit-transform:rotate(360deg)}}@-webkit-keyframes rotate{wx-from{-webkit-transform:rotate(0)}wx-to{-webkit-transform:rotate(360deg)}}@code-separator-line:__wxRoute = "pages/chat/index";__wxRouteBegin = true;
define("pages/chat/index.js", function(require, module, exports, window,document,frames,self,location,navigator,localStorage,history,Caches,screen,alert,confirm,prompt,fetch,XMLHttpRequest,WebSocket,webkit,WeixinJSCore,ServiceJSBridge,Reporter){
"use strict";function _interopRequireDefault(e){return e&&e.__esModule?e:{default:e}}function _asyncToGenerator(e){return function(){var t=e.apply(this,arguments);return new Promise(function(e,r){function n(a,i){try{var u=t[a](i),o=u.value}catch(e){return void r(e)}if(!u.done)return Promise.resolve(o).then(function(e){n("next",e)},function(e){n("throw",e)});e(o)}return n("next")})}}function _classCallCheck(e,t){if(!(e instanceof t))throw new TypeError("Cannot call a class as a function")}function _possibleConstructorReturn(e,t){if(!e)throw new ReferenceError("this hasn't been initialised - super() hasn't been called");return!t||"object"!=typeof t&&"function"!=typeof t?e:t}function _inherits(e,t){if("function"!=typeof t&&null!==t)throw new TypeError("Super expression must either be null or a function, not "+typeof t);e.prototype=Object.create(t&&t.prototype,{constructor:{value:e,enumerable:!1,writable:!0,configurable:!0}}),t&&(Object.setPrototypeOf?Object.setPrototypeOf(e,t):e.__proto__=t)}Object.defineProperty(exports,"__esModule",{value:!0});var _createClass=function(){function e(e,t){for(var r=0;r<t.length;r++){var n=t[r];n.enumerable=n.enumerable||!1,n.configurable=!0,"value"in n&&(n.writable=!0),Object.defineProperty(e,n.key,n)}}return function(t,r,n){return r&&e(t.prototype,r),n&&e(t,n),t}}(),_wepy=require("./../../npm/wepy/lib/wepy.js"),_wepy2=_interopRequireDefault(_wepy),_request=require("./../../api/request.js"),_util=require("./../../tools/util.js"),_util2=_interopRequireDefault(_util),_webSocket=require("./../../tools/webSocket.js"),_webSocket2=_interopRequireDefault(_webSocket),_cache=require("./../../tools/cache.js"),_cache2=_interopRequireDefault(_cache),_tip=require("./../../tools/tip.js"),_tip2=_interopRequireDefault(_tip),chatIndex=function(e){function t(){var e,r,n,a;_classCallCheck(this,t);for(var i=arguments.length,u=Array(i),o=0;o<i;o++)u[o]=arguments[o];return r=n=_possibleConstructorReturn(this,(e=t.__proto__||Object.getPrototypeOf(t)).call.apply(e,[this].concat(u))),n.config={navigationBarTitleText:"聊天"},n.components={},n.data={uuid:"",inputShowed:!0,inputVal:"",chatList:[],meHeader:"",youHeader:"",firstChat:1,windowHeight:""},n.computed={},n.methods={inputTyping:function(e){this.inputVal=e.detail.value,this.$apply()},clearText:function(){this.inputVal="",this.$apply()}},n.events={},a=r,_possibleConstructorReturn(n,a)}return _inherits(t,e),_createClass(t,[{key:"getUserInfo",value:function(){function e(){return t.apply(this,arguments)}var t=_asyncToGenerator(regeneratorRuntime.mark(function e(){var t=this;return regeneratorRuntime.wrap(function(e){for(;;)switch(e.prev=e.next){case 0:(0,_request.requrst)("user/getUserInfo",{uuid:this.uuid},function(e){_util2.default.title("和 "+e.info.user_nickname+" 的聊天"),t.youHeader=e.info.user_avatar});case 1:case"end":return e.stop()}},e,this)}));return e}()},{key:"socketStart",value:function(){function e(){return t.apply(this,arguments)}var t=_asyncToGenerator(regeneratorRuntime.mark(function e(){var t=this;return regeneratorRuntime.wrap(function(e){for(;;)switch(e.prev=e.next){case 0:this.$parent.socketMessage(function(e){var r=t.youHeader;300==e.code&&(r=e.info.header);var n={uuid:e.info.uuid,content:e.msg,left:1,header:r};t.chatList.push(n),t.appendChat(),wx.vibrateLong(),t.$apply()});case 1:case"end":return e.stop()}},e,this)}));return e}()},{key:"sednMessage",value:function(){function e(e){return t.apply(this,arguments)}var t=_asyncToGenerator(regeneratorRuntime.mark(function e(t){var r,n=this;return regeneratorRuntime.wrap(function(e){for(;;)switch(e.prev=e.next){case 0:r={uuid:this.uuid,content:this.inputVal,left:0,header:this.meHeader,firstChat:this.firstChat},this.inputVal&&(this.firstChat=0,_webSocket2.default.isOpen?(_webSocket2.default.send("chat",r,function(e){n.chatList.push(r),n.$apply()},1),this.inputVal="",this.appendChat()):_tip2.default.alert("聊天服务器连接失败,请退出重新进入聊天界面"));case 2:case"end":return e.stop()}},e,this)}));return e}()},{key:"getChatLog",value:function(){function e(){return t.apply(this,arguments)}var t=_asyncToGenerator(regeneratorRuntime.mark(function e(){var t=this;return regeneratorRuntime.wrap(function(e){for(;;)switch(e.prev=e.next){case 0:(0,_request.requrst)("chat/history",{uuid:this.uuid,type:0},function(e){var r=e.info.myinfo,n=e.info.userinfo;e.info.list.data.forEach(function(e,a){var i=0,u=_cache2.default.get("user_uid"),o=r.user_avatar;e.chat_uid!=u&&(i=1,o=n.user_avatar);var s={uuid:e.chat_send,content:e.chat_content,left:i,header:o,chat_update:e.chat_update};t.chatList.push(s)}),t.appendChat(),t.$apply()});case 1:case"end":return e.stop()}},e,this)}));return e}()},{key:"appendChat",value:function(){function e(){return t.apply(this,arguments)}var t=_asyncToGenerator(regeneratorRuntime.mark(function e(){return regeneratorRuntime.wrap(function(e){for(;;)switch(e.prev=e.next){case 0:wx.createSelectorQuery().select("#chat").boundingClientRect(function(e){console.log(e.bottom),wx.pageScrollTo({scrollTop:100*e.bottom})}).exec();case 1:case"end":return e.stop()}},e,this)}));return e}()},{key:"onLoad",value:function(e){this.uuid=e.uid,this.getUserInfo(),this.getChatLog();var t=_cache2.default.get("user_info");this.meHeader=t.user_avatar,this.firstChat=1,this.windowHeight=_util2.default.sysInfo("screenHeight")-100,this.socketStart()}}]),t}(_wepy2.default.page);Page(require("./../../npm/wepy/lib/wepy.js").default.$createPage(chatIndex,"pages/chat/index"));
});require("pages/chat/index.js")@code-separator-line:["div","view","image","text","icon","input"]