/*v0.6vv_20170214_fbi*/
window.__wcc_version__='v0.6vv_20170214_fbi'
window.__wxml_transpiler_version__='v0.1'
var $gwxc
var $gaic={}
$gwx=function(path,global){
function _(a,b){b&&a.children.push(b);}
function _v(k){if(typeof(k)!='undefined')return {tag:'virtual','wxKey':k,children:[]};return {tag:'virtual',children:[]};}
function _n(tag){$gwxc++;if($gwxc>=16000){throw 'Dom limit exceeded, please check if there\'s any mistake you\'ve made.'};return {tag:tag.substr(0,3)=='wx-'?tag:'wx-'+tag,attr:{},children:[],n:[]}}
function _p(a,b){b&&a.properities.push(b);}
function _s(scope,env,key){return typeof(scope[key])!='undefined'?scope[key]:env[key]}function _wl(tname,prefix){console.warn('WXMLRT:'+prefix+':-1:-1:-1: Template `' + tname + '` is being called recursively, will be stop.')}$gwn=console.warn;
$gwl=console.log;
function $gwh()
{
function x(){}
x.prototype =
{
hn: function( obj )
{
if( typeof(obj) == 'object' )
{
var cnt=0;
var any=false;
for(var x in obj)
{
any|=x==='__value__';
cnt++;
if(cnt>2)break;
}
return cnt == 2 && any && obj.hasOwnProperty('__wxspec__') ? "h" : "n";
}
return "n";
},
nh: function( obj, special )
{
return { __value__: obj, __wxspec__: special ? special : true }
},
rv: function( obj )
{
return this.hn(obj)==='n'?obj:this.rv(obj.__value__);
}
}
return new x;
}
wh=$gwh();
function $gwrt( should_pass_type_info )
{
function ArithmeticEv( ops, e, s, g, o )
{
var rop = ops[0][1];
var _a,_b,_c,_d, _aa, _bb;
switch( rop )
{
case '?:':
_a = rev( ops[1], e, s, g, o );
_c = should_pass_type_info && ( wh.hn(_a) === 'h' );
_d = wh.rv( _a ) ? rev( ops[2], e, s, g, o ) : rev( ops[3], e, s, g, o );
_d = _c && wh.hn( _d ) === 'n' ? wh.nh( _d, 'c' ) : _d;
return _d;
break;
case '&&':
_a = rev( ops[1], e, s, g, o );
_c = should_pass_type_info && ( wh.hn(_a) === 'h' );
_d = wh.rv( _a ) ? rev( ops[2], e, s, g, o ) : wh.rv( _a );
_d = _c && wh.hn( _d ) === 'n' ? wh.nh( _d, 'c' ) : _d;
return _d;
break;
case '||':
_a = rev( ops[1], e, s, g, o );
_c = should_pass_type_info && ( wh.hn(_a) === 'h' );
_d = wh.rv( _a ) ? wh.rv(_a) : rev( ops[2], e, s, g, o );
_d = _c && wh.hn( _d ) === 'n' ? wh.nh( _d, 'c' ) : _d;
return _d;
break;
case '+':
case '*':
case '/':
case '%':
case '|':
case '^':
case '&':
case '===':
case '==':
case '!=':
case '!==':
case '>=':
case '<=':
case '>':
case '<':
case '<<':
case '>>':
_a = rev( ops[1], e, s, g, o );
_b = rev( ops[2], e, s, g, o );
_c = should_pass_type_info && (wh.hn( _a ) === 'h' || wh.hn( _b ) === 'h');
switch( rop )
{
case '+':
_d = wh.rv( _a ) + wh.rv( _b );
break;
case '*':
_d = wh.rv( _a ) * wh.rv( _b );
break;
case '/':
_d = wh.rv( _a ) / wh.rv( _b );
break;
case '%':
_d = wh.rv( _a ) % wh.rv( _b );
break;
case '|':
_d = wh.rv( _a ) | wh.rv( _b );
break;
case '^':
_d = wh.rv( _a ) ^ wh.rv( _b );
break;
case '&':
_d = wh.rv( _a ) & wh.rv( _b );
break;
case '===':
_d = wh.rv( _a ) === wh.rv( _b );
break;
case '==':
_d = wh.rv( _a ) == wh.rv( _b );
break;
case '!=':
_d = wh.rv( _a ) != wh.rv( _b );
break;
case '!==':
_d = wh.rv( _a ) !== wh.rv( _b );
break;
case '>=':
_d = wh.rv( _a ) >= wh.rv( _b );
break;
case '<=':
_d = wh.rv( _a ) <= wh.rv( _b );
break;
case '>':
_d = wh.rv( _a ) > wh.rv( _b );
break;
case '<':
_d = wh.rv( _a ) < wh.rv( _b );
break;
case '<<':
_d = wh.rv( _a ) << wh.rv( _b );
break;
case '>>':
_d = wh.rv( _a ) >> wh.rv( _b );
break;
default:
break;
}
return _c ? wh.nh( _d, "c" ) : _d;
break;
case '-':
_a = ops.length === 3 ? rev( ops[1], e, s, g, o ) : 0;
_b = ops.length === 3 ? rev( ops[2], e, s, g, o ) : rev( ops[1], e, s, g, o );
_c = should_pass_type_info && (wh.hn( _a ) === 'h' || wh.hn( _b ) === 'h');
_d = _c ? wh.rv( _a ) - wh.rv( _b ) : _a - _b;
return _c ? wh.nh( _d, "c" ) : _d;
break;
case '!':
_a = rev( ops[1], e, s, g, o );
_c = should_pass_type_info && (wh.hn( _a ) == 'h');
_d = !wh.rv(_a);
return _c ? wh.nh( _d, "c" ) : _d;
case '~':
_a = rev( ops[1], e, s, g, o );
_c = should_pass_type_info && (wh.hn( _a ) == 'h');
_d = ~wh.rv(_a);
return _c ? wh.nh( _d, "c" ) : _d;
default:
$gwn('unrecognized op' + rop );
}
}
function rev( ops, e, s, g, o )
{
var op = ops[0];
if( typeof(op)==='object' )
{
var vop=op[0];
var _a, _aa, _b, _bb, _c, _d, _s, _e, _ta, _tb, _td;
switch(vop)
{
case 2:
return ArithmeticEv(ops,e,s,g,o);
break;
case 4:
return rev( ops[1], e, s, g, o );
break;
case 5:
switch( ops.length )
{
case 2:
return should_pass_type_info ?
[rev(ops[1],e,s,g,o)] :
[wh.rv(rev(ops[1],e,s,g,o))];
break;
case 1:
return [];
break;
default:
_a = rev( ops[1],e,s,g,o );
_a.push(
should_pass_type_info ?
rev( ops[2],e,s,g,o ) :
wh.rv( rev(ops[2],e,s,g,o) )
);
return _a;
break;
}
break;
case 6:
_a = rev(ops[1],e,s,g,o);
_ta = wh.hn(_a)==='h';
_aa = _ta ? wh.rv(_a) : _a;
o.is_affected |= _ta;
if( should_pass_type_info )
{
if( _aa===null || typeof(_aa) === 'undefined' )
{
return _ta ? wh.nh(undefined, 'e') : undefined;
}
_b = rev(ops[2],e,s,g,o);
_tb = wh.hn(_b) === 'h';
_bb = _tb ? wh.rv(_b) : _b;
o.is_affected |= _tb;
if( _bb===null || typeof(_bb) === 'undefined' )
{
return (_ta || _tb) ? wh.nh(undefined, 'e') : undefined;
}
_d = _aa[_bb];
_td = wh.hn(_d)==='h';
o.is_affected |= _td;
return (_ta || _tb) ? (_td ? _d : wh.nh(_d, 'e')) : _d;
}
else
{
if( _aa===null || typeof(_aa) === 'undefined' )
{
return undefined;
}
_b = rev(ops[2],e,s,g,o);
_tb = wh.hn(_b) === 'h';
_bb = _tb ? wh.rv(_b) : _b;
o.is_affected |= _tb;
if( _bb===null || typeof(_bb) === 'undefined' )
{
return undefined;
}
_d = _aa[_bb];
_td = wh.hn(_d)==='h';
o.is_affected |= _td;
return _td ? wh.rv(_d) : _d;
}
case 7:
switch(ops[1][0])
{
case 11:
o.is_affected |= wh.hn(g)==='h';
return g;
case 3:
_s = wh.rv( s );
_e = wh.rv( e );
_b = ops[1][1];
_a = _s && _s.hasOwnProperty(_b) ?
s : _e && ( _e.hasOwnProperty(_b) ? e : undefined );
if( should_pass_type_info )
{
if( _a )
{
_ta = wh.hn(_a) === 'h';
_aa = _ta ? wh.rv( _a ) : _a;
_d = _aa[_b];
_td = wh.hn(_d) === 'h';
o.is_affected |= _ta || _td;
_d = _ta && !_td ? wh.nh(_d,'e') : _d;
return _d;
}
}
else
{
if( _a )
{
_ta = wh.hn(_a) === 'h';
_aa = _ta ? wh.rv( _a ) : _a;
_d = _aa[_b];
_td = wh.hn(_d) === 'h';
o.is_affected |= _ta || _td;
return wh.rv(_d);
}
}
return undefined;
}
break;
case 8:
_a = {};
_a[ops[1]] = rev(ops[2],e,s,g,o);
return _a;
break;
case 9:
_a = rev(ops[1],e,s,g,o);
_b = rev(ops[2],e,s,g,o);
function merge( _a, _b, _ow )
{
_ta = wh.hn(_a)==='h';
_tb = wh.hn(_b)==='h';
_aa = wh.rv(_a);
_bb = wh.rv(_b);
if( should_pass_type_info )
{
if( _tb )
{
for(var k in _bb)
{
if ( _ow || !_aa.hasOwnProperty(k) )
_aa[k]=wh.nh(_bb[k],'e');
}
}
else
{
for(var k in _bb)
{
if ( _ow || !_aa.hasOwnProperty(k) )
_aa[k]=_bb[k];
}
}
}
else
{
for(var k in _bb)
{
if ( _ow || _aa.hasOwnProperty(k) )
_aa[k]=wh.rv(_bb[k]);
}
}
return _a;
}
var _c = _a
var _ow = true
if ( typeof(ops[1][0]) === "object" && ops[1][0][0] === 10 ) {
_a = _b
_b = _c
_ow = false
}
if ( typeof(ops[1][0]) === "object" && ops[1][0][0] === 10 ) {
var _r = {}
return merge( merge( _r, _a, _ow ), _b, _ow );
}
else
return merge( _a, _b, _ow );
break;
case 10:
return should_pass_type_info ? rev(ops[1],e,s,g,o) : wh.rv(rev(ops[1],e,s,g,o));
}
}
else
{
if( op === 3 || op === 1 ) return ops[1];
else if( op === 11 )
{
var _a='';
for( var i = 1 ; i < ops.length ; i++ )
{
var xp = wh.rv(rev(ops[i],e,s,g,o));
_a += typeof(xp) === 'undefined' ? '' : xp;
}
return _a;
}
}
}
return rev;
}
gra=$gwrt(true);
grb=$gwrt(false);
function TestTest( expr, ops, e,s,g, expect_a, expect_b, expect_affected )
{
{
var o = {is_affected:false};
var a = gra( ops, e,s,g, o );
if( JSON.stringify(a) != JSON.stringify( expect_a )
|| o.is_affected != expect_affected )
{
console.warn( "A. " + expr + " get result " + JSON.stringify(a) + ", " + o.is_affected + ", but " + JSON.stringify( expect_a ) + ", " + expect_affected + " is expected" );
}
}
{
var o = {is_affected:false};
var a = grb( ops, e,s,g, o );
if( JSON.stringify(a) != JSON.stringify( expect_b )
|| o.is_affected != expect_affected )
{
console.warn( "B. " + expr + " get result " + JSON.stringify(a) + ", " + o.is_affected + ", but " + JSON.stringify( expect_b ) + ", " + expect_affected + " is expected" );
}
}
}

function wfor( to_iter, func, env, _s, global, father, itemname, indexname, keyname )
{
var _n = wh.hn( to_iter ) === 'n';
var scope = wh.rv( _s );
var has_old_item = scope.hasOwnProperty(itemname);
var has_old_index = scope.hasOwnProperty(indexname);
var old_item = scope[itemname];
var old_index = scope[indexname];
var full = Object.prototype.toString.call(wh.rv(to_iter));
var type = full[8];
if( type === 'N' && full[10] === 'l' ) type = 'X';
var _y;
if( _n )
{
if( type === 'A' )
{
for( var i = 0 ; i < to_iter.length ; i++ )
{
scope[itemname] = to_iter[i];
scope[indexname] = wh.nh(i, 'h');
_y = keyname ? (keyname==="*this" ? _v(wh.rv(to_iter[i])) : _v(wh.rv(wh.rv(to_iter[i])[keyname]))) : _v();
_(father,_y);
func( env, scope, _y, global );
}
}
else if( type === 'O' )
{
for( var k in to_iter )
{
scope[itemname] = to_iter[k];
scope[indexname] = wh.nh(k, 'h');
_y = keyname ? (keyname==="*this" ? _v(wh.rv(to_iter[k])) : _v(wh.rv(wh.rv(to_iter[k])[keyname]))) : _v();
_(father,_y);
func( env,scope,_y,global );
}
}
else if( type === 'S' )
{
for( var i = 0 ; i < to_iter.length ; i++ )
{
scope[itemname] = to_iter[i];
scope[indexname] = wh.nh(i, 'h');
_y = _v( to_iter[i] + i );
_(father,_y);
func( env, scope, _y, global );
}
}
else if( type === 'N' )
{
for( var i = 0 ; i < to_iter ; i++ )
{
scope[itemname] = i;
scope[indexname] = wh.nh(i, 'h');
_y = _v( i );
_(father,_y);
func(env,scope,_y,global);
}
}
else
{
}
}
else
{
var r_to_iter = wh.rv(to_iter);
var r_iter_item, iter_item;
if( type === 'A' )
{
for( var i = 0 ; i < r_to_iter.length ; i++ )
{
iter_item = r_to_iter[i];
iter_item = wh.hn(iter_item)==='n' ? wh.nh(iter_item,'h') : iter_item;
r_iter_item = wh.rv( iter_item );
scope[itemname] = iter_item
scope[indexname] = wh.nh(i, 'h');
_y = keyname ? (keyname==="*this" ? _v(r_iter_item) : _v(wh.rv(r_iter_item[keyname]))) : _v();
_(father,_y);
func( env, scope, _y, global );
}
}
else if( type === 'O' )
{
for( var k in r_to_iter )
{
iter_item = r_to_iter[k];
iter_item = wh.hn(iter_item)==='n'? wh.nh(iter_item,'h') : iter_item;
r_iter_item = wh.rv( iter_item );
scope[itemname] = iter_item;
scope[indexname] = wh.nh(k, 'h');
_y = keyname ? (keyname==="*this" ? _v(r_iter_item) : _v(wh.rv(r_iter_item[keyname]))) : _v();
_(father,_y);
func( env, scope, _y, global );
}
}
else if( type === 'S' )
{
for( var i = 0 ; i < r_to_iter.length ; i++ )
{
scope[itemname] = wh.nh(r_to_iter[i],'h');
scope[indexname] = wh.nh(i, 'h');
_y = _v( to_iter[i] + i );
_(father,_y);
func( env, scope, _y, global );
}
}
else if( type === 'N' )
{
for( var i = 0 ; i < r_to_iter ; i++ )
{
scope[itemname] = wh.nh(i,'h');
scope[indexname]= wh.nh(i,'h');
_y = _v( i );
_(father,_y);
func(env,scope,_y,global);
}
}
else
{
}
}
if(has_old_item)
{
scope[itemname]=old_item;
}
else
{
delete scope[itemname];
}
if(has_old_index)
{
scope[indexname]=old_index;
}
else{delete scope[indexname];}}


function _r( node, attrname, opindex, env, scope, global )
{
var o = {};
var a = grb( z[opindex], env, scope, global, o );
node.attr[attrname] = a;
if( o.is_affected ) node.n.push( attrname );
}
function _o( opindex, env, scope, global )
{
var nothing = {};
return grb( z[opindex], env, scope, global, nothing );
}
function _1( opindex, env, scope, global )
{
var nothing = {};
return gra( z[opindex], env, scope, global, nothing );
}
function _2( opindex, func, env, scope, global, father, itemname, indexname, keyname )
{
var to_iter = _1( opindex, env, scope, global, father, itemname, indexname, keyname );
wfor( to_iter, func, env, scope, global, father, itemname, indexname, keyname );
}
function _gv( )
{
if( typeof(window.__webview_engine_version__) == 'undefined' ) return 0.0;
return window.__webview_engine_version__;
}
function _m(tag,attrs,env,scope,global)
{
var tmp=_n(tag);
var base=0;
for(var i = 0 ; i < attrs.length ; i+=2 )
{
if(attrs[i+1]<0)
{tmp.attr[attrs[i]]=true;}else{_r(tmp,attrs[i],base+attrs[i+1],env,scope,global);
if(base===0)base=attrs[i+1];}}return tmp;};function _ai(i,p,e,me,r,c){var x=_grp(p,e,me);if(x)i.push(x);else{i.push('');console.warn('WXMLRT:'+me+':import:'+r+':'+c+': Path `'+p+'` not found from `'+me+'`.')}}function _grp(p,e,me){if(p[0]!='/'){var mepart=me.split('/');mepart.pop();var ppart=p.split('/');for(var i=0;i<ppart.length;i++){if(ppart[i]=='..')mepart.pop();else if(ppart[i]=='.' || !ppart[i])continue;else mepart.push(ppart[i]);}p=mepart.join('/');}if(me[0]=='.'&&p[0]=='/')p='.'+p;if(e[p])return p;if(e[p+'.wxml'])return p+'.wxml';}function _gd(p,c,e,d){if(!c)return;if(d[p][c])return d[p][c];for(var x=e[p].i.length-1;x>=0;x--){if(e[p].i[x]&&d[e[p].i[x]][c])return d[e[p].i[x]][c]};for(var x=e[p].ti.length-1;x>=0;x--){var q=_grp(e[p].ti[x],e,p);if(q&&d[q][c])return d[q][c]}
var ii=_gapi(e,p);for(var x=0;x<ii.length;x++){if(ii[x]&&d[ii[x]][c])return d[ii[x]][c]}for(var k=e[p].j.length-1;k>=0;k--)if(e[p].j[k]){for(var q=e[e[p].j[k]].ti.length-1;q>=0;q--){var pp=_grp(e[e[p].j[k]].ti[q],e,p);if(pp&&d[pp][c]){return d[pp][c]}}}}function _gapi(e,p){if(!p)return [];if($gaic[p]){return $gaic[p]};var ret=[],q=[],h=0,t=0,put={},visited={};q.push(p);visited[p]=true;t++;while(h<t){var a=q[h++];for(var i=0;i<e[a].ic.length;i++){var nd=e[a].ic[i];var np=_grp(nd,e,a);if(np&&!visited[np]){visited[np]=true;q.push(np);t++;}}for(var i=0;a!=p&&i<e[a].ti.length;i++){var ni=e[a].ti[i];var nm=_grp(ni,e,a);if(nm&&!put[nm]){put[nm]=true;ret.push(nm);}}}$gaic[p]=ret;return ret;}var $ixc={};function _ic(p,ent,me,e,s,r,gg){var x=_grp(p,ent,me);ent[me].j.push(x);if(x){if($ixc[x]){console.warn('WXMLRT:-1:include:-1:-1: `'+p+'` is being included in a loop, will be stop.');return;}$ixc[x]=true;try{ent[x].f(e,s,r,gg)}catch(e){}$ixc[x]=false;}else{console.warn('WXMLRT:'+me+':include:-1:-1: Included path `'+p+'` not found from `'+me+'`.')}}function _w(tn,f,line,c){console.warn('WXMLRT:'+f+':template:'+line+':'+c+': Template `'+tn+'` not found.');}function _ev(dom){var changed=false;delete dom.properities;delete dom.n;if(dom.children){do{changed=false;var newch = [];for(var i=0;i<dom.children.length;i++){var ch=dom.children[i];if( ch.tag=='virtual'){changed=true;for(var j=0;ch.children&&j<ch.children.length;j++){newch.push(ch.children[j]);}}else { newch.push(ch); } } dom.children = newch; }while(changed);for(var i=0;i<dom.children.length;i++){_ev(dom.children[i]);}} return dom; }var e_={}
if(global&&typeof(global.entrys)=='object')e_=global.entrys
var d_={}
if(global&&typeof(global.defines)=='object')d_=global.defines
var p_={}
var z = [];
  (function(z){
    var a = 11;
    function Z(ops){z.push(ops)};
    Z([3, 'scrollTolower']);Z([3, '2']);Z([3, 'true']);Z([a, [3, 'height:'],[[7],[3, "sysHeight"]],[3, 'px']]);Z([3, 'square']);Z([3, 'weui-search-bar']);Z([3, 'weui-search-bar__form']);Z([3, 'weui-search-bar__box']);Z([3, 'text-align:left']);Z([3, 'weui-icon-search_in-box']);Z([3, '14']);Z([3, 'search']);Z([3, 'inputTyping']);Z([3, 'weui-search-bar__input']);Z([[7],[3, "inputShowed"]]);Z([3, '搜索']);Z([3, 'text']);Z([[7],[3, "inputVal"]]);Z([[2, ">"], [[6],[[7],[3, "inputVal"]],[3, "length"]], [1, 0]]);Z([3, 'clearInput']);Z([3, 'weui-icon-clear']);Z([3, 'clear']);Z([3, 'hideInput']);Z([3, 'weui-search-bar__cancel-btn']);Z([[2, "!"], [[7],[3, "inputShowed"]]]);Z([3, '取消']);Z([3, 'navlist']);Z([[7],[3, "tagList"]]);Z([3, 'key']);Z([3, 'tagClick']);Z([a, [3, 'list '],[[2,'?:'],[[6],[[7],[3, "item"]],[3, "current"]],[1, "current"],[1, ""]]]);Z([[7],[3, "index"]]);Z([[6],[[7],[3, "item"]],[3, "tag_id"]]);Z([a, [[6],[[7],[3, "item"]],[3, "tag_name"]]]);Z([[2, ">="], [[7],[3, "flag"]], [1, 0]]);Z([3, 'userBox']);Z([[7],[3, "$squarea$list"]]);Z([3, 'boxlist']);Z([3, 'left']);Z([a, [3, 'online '],[[2,'?:'],[[2, "=="], [[6],[[7],[3, "item"]],[3, "user_online"]], [1, ""]],[1, "outline"],[1, ""]]]);Z([[2, "=="], [[6],[[7],[3, "item"]],[3, "user_online"]], [1, ""]]);Z([3, '离线']);Z([[6],[[7],[3, "item"]],[3, "user_online"]]);Z([3, '在线']);Z([3, '$squarea$moreThan']);Z([3, 'bgc']);Z([[6],[[7],[3, "item"]],[3, "user_id"]]);Z([3, 'aspectFill']);Z([[6],[[7],[3, "item"]],[3, "user_avatar"]]);Z([[7],[3, "$squarea$false"]]);Z([3, 'shadow']);Z([3, '14.65']);Z([3, 'right']);Z([3, '1234']);Z([3, 'title']);Z([a, [[2, "||"],[[6],[[6],[[7],[3, "item"]],[3, "set"]],[3, "set_note"]],[1, "快来选我啊"]]]);Z([3, 'username']);Z([a, [[6],[[7],[3, "item"]],[3, "user_nickname"]]]);Z([a, [[6],[[7],[3, "item"]],[3, "time2"]]]);Z([3, 'photolist']);Z([3, 'pshow']);Z([[2, ">"], [[6],[[6],[[7],[3, "item"]],[3, "photo"]],[3, "length"]], [1, 0]]);Z([[6],[[7],[3, "item"]],[3, "photo"]]);Z([3, 'itemName']);Z([3, '$squarea$PrvImg']);Z([3, 'showMe']);Z([[6],[[7],[3, "itemName"]],[3, "wall_img"]]);Z([[2, "-"], [1, 4], [[6],[[6],[[7],[3, "item"]],[3, "photo"]],[3, "length"]]]);Z([[7],[3, "$squarea$noPic"]]);Z([3, 'moreThan']);Z([3, 'http://img.he29.com/play/f4fe7786635ee901a46590b7f1926bc08d628bdf.png']);Z([3, '$squarea$rightTop']);Z([3, 'rightTop']);Z([[2, "=="], [[7],[3, "$squarea$use"]], [1, 1]]);Z([3, '$squarea$choseUse']);Z([3, 'choseTop comm']);Z([[6],[[7],[3, "item"]],[3, "set_gid"]]);Z([3, '\r\n                            选择他\r\n                        ']);Z([3, '$squarea$choseUseDet']);Z([3, 'choseUse comm']);Z([3, '他的评价']);Z([3, 'http://img.he29.com/play/0eab4673f7e92d46c311688f1421bb53ec5673ab.jpeg']);Z([[7],[3, "$squarea$watch$open"]]);Z([3, 'watchs']);Z([3, 'watch animated fadeIn']);Z([3, 'message']);Z([a, [[7],[3, "$squarea$watch$msgStr"]]]);Z([[2, "||"],[[7],[3, "$squarea$watch$imgs"]],[1, "http://img.he29.com/play/58c358435cad9d985da5236e9d497f2bc2ebcb0d.jpeg"]]);Z([3, 'btns']);Z([3, '$squarea$watch$waitRun']);Z([3, 'flex']);Z([a, [[2, "||"],[[7],[3, "$squarea$watch$left"]],[1, "稍后进入"]]]);Z([3, '$squarea$watch$nowRun']);Z([a, [[2, "||"],[[7],[3, "$squarea$watch$right"]],[1, "马上进入"]]]);Z([3, 'loading']);Z([3, 'empty']);Z([3, 'abnor']);Z([3, 'abnor__box']);Z([[7],[3, "$empty$image"]]);Z([3, 'abnor__image']);Z([3, 'widthFix']);Z([[7],[3, "$empty$title"]]);Z([3, 'abnor__text']);Z([[7],[3, "$empty$tip"]]);Z([3, 'abnor__tip']);Z([[7],[3, "$empty$button"]]);Z([3, '$empty$emitAbnorTap']);Z([3, 'abnor__btn']);Z([3, 'loadingMore']);Z([[2, "!="], [[7],[3, "$loadMore$flag"]], [1, 0]]);Z([[2, "=="], [[7],[3, "$loadMore$flag"]], [1, 1]]);Z([3, 'weui-loadmore']);Z([3, 'weui-loading']);Z([3, 'weui-loadmore__tips']);Z([3, '正在加载']);Z([[2, "<"], [[7],[3, "$loadMore$flag"]], [1, 0]]);Z([3, 'weui-loadmore weui-loadmore_line']);Z([3, 'weui-loadmore__tips weui-loadmore__tips_in-line']);Z([3, '加载完成']);Z([[2, "=="], [[7],[3, "$loadMore$flag"]], [1, 2]]);Z([3, '滑动加载更多']);
  })(z);d_["./pages/game/square.wxml"] = {};
  var m0=function(e,s,r,gg){
    var oTR = _m( "scroll-view", ["bindscrolltolower", 0,"lowerThreshold", 1,"scrollY", 1,"style", 2], e, s, gg);var oUR = _n("view");_r(oUR, 'class', 4, e, s, gg);var oVR = _n("view");_r(oVR, 'class', 5, e, s, gg);var oWR = _n("view");_r(oWR, 'class', 6, e, s, gg);var oXR = _m( "view", ["class", 7,"style", 1], e, s, gg);var oYR = _m( "icon", ["class", 9,"size", 1,"type", 2], e, s, gg);_(oXR,oYR);var oZR = _m( "input", ["bindinput", 12,"class", 1,"focus", 2,"placeholder", 3,"type", 4,"value", 5], e, s, gg);_(oXR,oZR);var oaR = _v();
      if (_o(18, e, s, gg)) {
        oaR.wxVkey = 1;var obR = _m( "view", ["bindtap", 19,"class", 1], e, s, gg);var odR = _m( "icon", ["size", 10,"type", 11], e, s, gg);_(obR,odR);_(oaR, obR);
      } _(oXR,oaR);_(oWR,oXR);_(oVR,oWR);var oeR = _m( "view", ["bindtap", 22,"class", 1,"hidden", 2], e, s, gg);var ofR = _o(25, e, s, gg);_(oeR,ofR);_(oVR,oeR);_(oUR,oVR);var ogR = _n("view");_r(ogR, 'class', 26, e, s, gg);var ohR = _n("scroll-view");_r(ohR, 'scrollX', 2, e, s, gg);var oiR = _v();var ojR = function(onR,omR,olR,gg){var okR = _m( "view", ["bindtap", 29,"class", 1,"data-id", 2,"data-ids", 3], onR, omR, gg);var opR = _o(33, onR, omR, gg);_(okR,opR);_(olR, okR);return olR;};_2(27, ojR, e, s, gg, oiR, "item", "index", 'key');_(ohR,oiR);_(ogR,ohR);_(oUR,ogR);var oqR = _v();
      if (_o(34, e, s, gg)) {
        oqR.wxVkey = 1;var orR = _n("view");_r(orR, 'class', 4, e, s, gg);var otR = _n("view");_r(otR, 'class', 35, e, s, gg);var ouR = _v();var ovR = function(ozR,oyR,oxR,gg){var owR = _n("view");_r(owR, 'class', 37, ozR, oyR, gg);var oAS = _n("view");_r(oAS, 'class', 38, ozR, oyR, gg);var oBS = _n("view");_r(oBS, 'class', 39, ozR, oyR, gg);var oCS = _v();
      if (_o(40, ozR, oyR, gg)) {
        oCS.wxVkey = 1;var oDS = _n("text");var oFS = _o(41, ozR, oyR, gg);_(oDS,oFS);_(oCS, oDS);
      }else {
        oCS.wxVkey = 2;var oGS = _n("text");_r(oGS, 'alt', 42, e, s, gg);var oIS = _o(43, e, s, gg);_(oGS,oIS);_(oCS, oGS);
      }_(oBS,oCS);_(oAS,oBS);var oJS = _m( "image", ["bindtap", 44,"class", 1,"data-uid", 2,"mode", 3,"src", 4], ozR, oyR, gg);_(oAS,oJS);var oKS = _v();
      if (_o(49, ozR, oyR, gg)) {
        oKS.wxVkey = 1;var oLS = _n("view");_r(oLS, 'class', 50, ozR, oyR, gg);var oNS = _n("view");_r(oNS, 'class', 38, ozR, oyR, gg);var oOS = _o(51, ozR, oyR, gg);_(oNS,oOS);_(oLS,oNS);var oPS = _n("view");_r(oPS, 'class', 52, ozR, oyR, gg);var oQS = _o(53, ozR, oyR, gg);_(oPS,oQS);_(oLS,oPS);_(oKS, oLS);
      } _(oAS,oKS);_(owR,oAS);var oRS = _n("view");_r(oRS, 'class', 52, ozR, oyR, gg);var oSS = _n("view");_r(oSS, 'class', 54, ozR, oyR, gg);var oTS = _o(55, ozR, oyR, gg);_(oSS,oTS);_(oRS,oSS);var oUS = _n("view");_r(oUS, 'class', 56, ozR, oyR, gg);var oVS = _n("text");_r(oVS, 'class', 38, ozR, oyR, gg);var oWS = _o(57, ozR, oyR, gg);_(oVS,oWS);_(oUS,oVS);var oXS = _n("text");_r(oXS, 'class', 52, ozR, oyR, gg);var oYS = _o(58, ozR, oyR, gg);_(oXS,oYS);_(oUS,oXS);_(oRS,oUS);var oZS = _n("view");_r(oZS, 'class', 59, ozR, oyR, gg);var oaS = _n("view");_r(oaS, 'class', 60, ozR, oyR, gg);var obS = _v();
      if (_o(61, ozR, oyR, gg)) {
        obS.wxVkey = 1;var ocS = _n("view");_r(ocS, 'class', 60, ozR, oyR, gg);var oeS = _v();var ofS = function(ojS,oiS,ohS,gg){var ogS = _m( "image", ["mode", 47,"bindtap", 17,"class", 18,"data-img", 19,"src", 19], ojS, oiS, gg);_(ohS, ogS);return ohS;};_2(62, ofS, ozR, oyR, gg, oeS, "itemName", "index", 'key');_(ocS,oeS);var olS = _v();var omS = function(oqS,opS,ooS,gg){var onS = _m( "image", ["class", 65,"src", 3], oqS, opS, gg);_(ooS, onS);return ooS;};_2(67, omS, ozR, oyR, gg, olS, "item", "index", 'key');_(ocS,olS);_(obS, ocS);
      }else {
        obS.wxVkey = 2;var osS = _n("view");_r(osS, 'class', 60, e, s, gg);var ouS = _m( "image", ["class", 65,"src", 3], e, s, gg);_(osS,ouS);var ovS = _m( "image", ["class", 65,"src", 3], e, s, gg);_(osS,ovS);var owS = _m( "image", ["class", 65,"src", 3], e, s, gg);_(osS,owS);var oxS = _m( "image", ["class", 65,"src", 3], e, s, gg);_(osS,oxS);_(obS, osS);
      }_(oaS,obS);_(oZS,oaS);var oyS = _m( "view", ["bindtap", 44,"data-uid", 2,"class", 25], ozR, oyR, gg);var ozS = _n("image");_r(ozS, 'src', 70, ozR, oyR, gg);_(oyS,ozS);_(oZS,oyS);_(oRS,oZS);var o_S = _m( "view", ["bindtap", 71,"class", 1], ozR, oyR, gg);var oAT = _v();
      if (_o(73, ozR, oyR, gg)) {
        oAT.wxVkey = 1;var oBT = _m( "view", ["data-uid", 46,"data-name", 11,"bindtap", 28,"class", 29,"data-gid", 30], ozR, oyR, gg);var oDT = _o(77, ozR, oyR, gg);_(oBT,oDT);_(oAT, oBT);
      } _(o_S,oAT);var oET = _m( "text", ["data-uid", 46,"data-name", 11,"data-gid", 30,"bindtap", 32,"class", 33], ozR, oyR, gg);var oFT = _o(80, ozR, oyR, gg);_(oET,oFT);_(o_S,oET);var oGT = _v();
      if (_o(49, ozR, oyR, gg)) {
        oGT.wxVkey = 1;var oHT = _n("image");_r(oHT, 'src', 81, ozR, oyR, gg);_(oGT, oHT);
      } _(o_S,oGT);_(oRS,o_S);_(owR,oRS);_(oxR, owR);return oxR;};_2(36, ovR, e, s, gg, ouR, "item", "index", 'key');_(otR,ouR);_(orR,otR);var oJT = _v();
      if (_o(82, e, s, gg)) {
        oJT.wxVkey = 1;var oKT = _n("view");_r(oKT, 'class', 83, e, s, gg);var oMT = _n("view");_r(oMT, 'class', 84, e, s, gg);var oNT = _n("text");_r(oNT, 'class', 85, e, s, gg);var oOT = _o(86, e, s, gg);_(oNT,oOT);_(oMT,oNT);var oPT = _n("image");_r(oPT, 'src', 87, e, s, gg);_(oMT,oPT);var oQT = _n("view");_r(oQT, 'class', 88, e, s, gg);var oRT = _m( "text", ["bindtap", 89,"class", 1], e, s, gg);var oST = _o(91, e, s, gg);_(oRT,oST);_(oQT,oRT);var oTT = _m( "text", ["class", 90,"bindtap", 2], e, s, gg);var oUT = _o(93, e, s, gg);_(oTT,oUT);_(oQT,oTT);_(oMT,oQT);_(oKT,oMT);var oVT = _n("view");_r(oVT, 'class', 94, e, s, gg);_(oKT,oVT);_(oJT, oKT);
      } _(orR,oJT);_(oqR, orR);
      }else {
        oqR.wxVkey = 2;var oWT = _n("view");_r(oWT, 'class', 95, e, s, gg);var oYT = _n("view");_r(oYT, 'class', 96, e, s, gg);var oZT = _n("view");_r(oZT, 'class', 97, e, s, gg);var oaT = _v();
      if (_o(98, e, s, gg)) {
        oaT.wxVkey = 1;var obT = _m( "image", ["src", 98,"class", 1,"mode", 2], e, s, gg);_(oaT, obT);
      } _(oZT,oaT);var odT = _v();
      if (_o(101, e, s, gg)) {
        odT.wxVkey = 1;var oeT = _n("view");_r(oeT, 'class', 102, e, s, gg);var ogT = _o(101, e, s, gg);_(oeT,ogT);_(odT, oeT);
      } _(oZT,odT);var ohT = _v();
      if (_o(103, e, s, gg)) {
        ohT.wxVkey = 1;var oiT = _n("view");_r(oiT, 'class', 104, e, s, gg);var okT = _o(103, e, s, gg);_(oiT,okT);_(ohT, oiT);
      } _(oZT,ohT);var olT = _v();
      if (_o(105, e, s, gg)) {
        olT.wxVkey = 1;var omT = _m( "view", ["bindtap", 106,"class", 1], e, s, gg);var ooT = _o(105, e, s, gg);_(omT,ooT);_(olT, omT);
      } _(oZT,olT);_(oYT,oZT);_(oWT,oYT);_(oqR, oWT);
      }_(oUR,oqR);_(oTR,oUR);var opT = _n("view");_r(opT, 'class', 108, e, s, gg);var oqT = _v();
      if (_o(109, e, s, gg)) {
        oqT.wxVkey = 1;var orT = _n("view");_r(orT, 'class', 108, e, s, gg);var otT = _v();
      if (_o(110, e, s, gg)) {
        otT.wxVkey = 1;var ouT = _n("view");_r(ouT, 'class', 111, e, s, gg);var owT = _n("view");_r(owT, 'class', 112, e, s, gg);_(ouT,owT);var oxT = _n("view");_r(oxT, 'class', 113, e, s, gg);var oyT = _o(114, e, s, gg);_(oxT,oyT);_(ouT,oxT);_(otT, ouT);
      } _(orT,otT);var ozT = _v();
      if (_o(115, e, s, gg)) {
        ozT.wxVkey = 1;var o_T = _n("view");_r(o_T, 'class', 116, e, s, gg);var oBU = _n("view");_r(oBU, 'class', 117, e, s, gg);var oCU = _o(118, e, s, gg);_(oBU,oCU);_(o_T,oBU);_(ozT, o_T);
      } _(orT,ozT);var oDU = _v();
      if (_o(119, e, s, gg)) {
        oDU.wxVkey = 1;var oEU = _n("view");_r(oEU, 'class', 116, e, s, gg);var oGU = _n("view");_r(oGU, 'class', 117, e, s, gg);var oHU = _o(120, e, s, gg);_(oGU,oHU);_(oEU,oGU);_(oDU, oEU);
      } _(orT,oDU);_(oqT, orT);
      } _(opT,oqT);_(oTR,opT);_(r,oTR);
    return r;
  };
        e_["./pages/game/square.wxml"]={f:m0,j:[],i:[],ti:[],ic:[]};
if(path&&e_[path]){
window.__wxml_comp_version__=0.02
return function(env,dd,global){$gwxc=0;var root={"tag":"wx-page"};root.children=[]
var main=e_[path].f
if(typeof(window.__webview_engine_version__)!='undefined'&&window.__webview_engine_version__+1e-6>=0.02+1e-6&&window.__mergeData__)
{env=window.__mergeData__(env,dd);}
try{
main(env,{},root,global);
if(typeof(window.__webview_engine_version__)=='undefined'||window.__webview_engine_version__+1e-6<0.01+1e-6){return _ev(root);}}catch(err){console.log(err)}return root;}}}@code-separator-line:.empty .abnor{position:relative;display:block;width:100%;height:0;padding-bottom:100%;overflow:hidden}.empty .abnor__box{position:absolute;display:flex;top:0;bottom:0;left:0;right:0;flex-direction:column;justify-content:center;align-items:center}.empty .abnor__btn{min-width:%%?228rpx?%%;height:%%?66rpx?%%;margin-top:%%?30rpx?%%;padding:0 %%?30rpx?%%;background-color:#3e9aff;border:0 none;border-radius:%%?2rpx?%%;color:#fff;font-size:%%?28rpx?%%;text-align:center;overflow:hidden;line-height:%%?66rpx?%%}.empty .abnor__btn:active{background-color:#3e9aff}.empty .abnor__image{width:%%?514rpx?%%;background:transparent no-repeat;background-size:cover}.empty .abnor__text{margin-top:%%?30rpx?%%;color:#333;font-size:%%?28rpx?%%}.empty .abnor__tip{margin-top:%%?20rpx?%%;color:#666;font-size:%%?24rpx?%%}.watch{text-align:center;position:fixed;margin:auto;left:0;right:0;top:42px;bottom:0;z-index:100;border-radius:10px;width:%%?480rpx?%%;height:%%?320rpx?%%}.watch wx-image{width:100%;height:%%?300rpx?%%;border-top-left-radius:10px;border-top-right-radius:10px;opacity:.9}.watch .btns{display:flex;background:#fff;z-index:100;font-size:%%?32rpx?%%;color:#969696;position:absolute;bottom:0;width:100%;height:%%?80rpx?%%;line-height:%%?80rpx?%%;text-align:center;border-bottom-left-radius:10px;border-bottom-right-radius:10px}.watch .btns .flex{flex:1}.watch .btns .flex:last-child{color:#0c9}.watch .message{position:absolute;top:%%?20rpx?%%;font-size:%%?32rpx?%%;color:#fff;padding:%%?10rpx?%%;margin:%%?10rpx?%%;z-index:99;text-shadow:0 0 2px #f1f1f1}.watchs .loading{position:absolute;top:0;left:0;width:100%;height:100%;z-index:1;background:rgba(0,0,0,.5)}.square .weui-search-bar{background:0 0;border:none}.square .weui-search-bar .weui-search-bar__form{border:none}.square .weui-search-bar .weui-search-bar__form .weui-icon-search_in-box{color:#e6e8f7}.square .weui-search-bar .weui-search-bar__form .weui-search-bar__input{color:#e6e8f7}.square .navlist{height:43px;background:#3e9aff;color:#3e9aff}.square .navlist wx-scroll-view{height:100%;width:1000px;overflow:hidden}.square .navlist .current{border-bottom:2px solid #fff}.square .navlist .list{float:left;padding:10px 20px;color:#fff;font-size:%%?24rpx?%%}.square .userBox{margin-top:%%?15rpx?%%}.square .userBox .boxlist{height:130px;width:98%;margin:0 auto;background:#fff;display:flex;border-radius:%%?5rpx?%%;margin-bottom:%%?10rpx?%%;box-shadow:0 0 2px #fff;padding:%%?10rpx?%%;box-sizing:border-box}.square .userBox .boxlist .left{float:left;position:relative;flex:1}.square .userBox .boxlist .left .online{position:absolute;top:0;left:0;font-size:14px;color:#f1f0ec;text-shadow:0 0 2px #ccc;background:#3e9aff;padding:%%?1rpx?%% %%?10rpx?%%;border-radius:%%?5rpx?%%}.square .userBox .boxlist .left .online .outline{background:#fb8404}.square .userBox .boxlist .left .online .onlines{background:#3e9aff}.square .userBox .boxlist .left .outline{background:#fb8404}.square .userBox .boxlist .left .bgc{width:120px;height:120px;border-radius:%%?10rpx?%%}.square .userBox .boxlist .left .shadow{position:absolute;bottom:0;height:30px;display:flex;font-size:15px;line-height:30px;color:#fff;width:100%;text-shadow:0 0 2px #ccc}.square .userBox .boxlist .left .shadow wx-view{flex:1;text-align:center}.square .userBox .boxlist .right{float:left;font-size:%%?24rpx?%%;position:relative;padding-left:%%?22rpx?%%;flex:3}.square .userBox .boxlist .right .title{height:25px;line-height:25px;color:#33373a;margin-top:7px}.square .userBox .boxlist .right .username{color:#3e9aff;height:22px;line-height:22px;display:flex;margin-bottom:5px}.square .userBox .boxlist .right .username wx-text{flex:1}.square .userBox .boxlist .right .username .left{text-align:left}.square .userBox .boxlist .right .username .right{text-align:right;color:rgba(101,107,111,.44);font-size:%%?20rpx?%%;margin-right:%%?15rpx?%%}.square .userBox .boxlist .right .photolist .pshow{display:flex;width:92%}.square .userBox .boxlist .right .photolist .pshow .showMe{flex:1;width:24%;height:%%?95rpx?%%;margin-right:%%?5rpx?%%;background:rgba(245,246,248,.9);padding:%%?2rpx?%%;box-sizing:border-box}.square .userBox .boxlist .right .photolist .moreThan{opacity:.8;width:%%?60rpx?%%;height:%%?60rpx?%%;position:absolute;right:0;bottom:18px}.square .userBox .boxlist .right .photolist .moreThan wx-image{width:100%;height:100%}.square .userBox .boxlist .right .rightTop{position:absolute;top:%%?1rpx?%%;right:%%?10rpx?%%;width:%%?150rpx?%%;height:%%?80rpx?%%}.square .userBox .boxlist .right .rightTop .comm{text-align:center;color:#fff;border-radius:%%?5rpx?%%;font-size:%%?24rpx?%%;padding:0 %%?20rpx?%%;display:block;margin-top:%%?5rpx?%%;height:24px;line-height:24px;overflow:hidden}.square .userBox .boxlist .right .rightTop .choseUse{background:#3e9aff}.square .userBox .boxlist .right .rightTop wx-image{width:%%?60rpx?%%;height:%%?35rpx?%%}.square .userBox .boxlist .right .rightTop .choseTop{background:#cc752a;height:24px;line-height:24px;overflow:hidden}.square .formSubmit{position:absolute;top:0;height:25px;opacity:0}.square .formHide{position:absolute;opacity:0}.loadingMore{clear:both;display:block}body .square .weui-search-bar{background:0 0;border:none}body .square .weui-search-bar .weui-search-bar__form{border:none}body .square .weui-search-bar .weui-search-bar__form .weui-icon-search_in-box{color:#e6e8f7}body .square .weui-search-bar .weui-search-bar__form .weui-search-bar__input{color:#e6e8f7}@code-separator-line:__wxRoute = "pages/game/square";__wxRouteBegin = true;
define("pages/game/square.js", function(require, module, exports, window,document,frames,self,location,navigator,localStorage,history,Caches,screen,alert,confirm,prompt,fetch,XMLHttpRequest,WebSocket,webkit,WeixinJSCore,ServiceJSBridge,Reporter){
"use strict";function _interopRequireDefault(e){return e&&e.__esModule?e:{default:e}}function _asyncToGenerator(e){return function(){var t=e.apply(this,arguments);return new Promise(function(e,r){function n(a,i){try{var o=t[a](i),u=o.value}catch(e){return void r(e)}if(!o.done)return Promise.resolve(u).then(function(e){n("next",e)},function(e){n("throw",e)});e(u)}return n("next")})}}function _classCallCheck(e,t){if(!(e instanceof t))throw new TypeError("Cannot call a class as a function")}function _possibleConstructorReturn(e,t){if(!e)throw new ReferenceError("this hasn't been initialised - super() hasn't been called");return!t||"object"!=typeof t&&"function"!=typeof t?e:t}function _inherits(e,t){if("function"!=typeof t&&null!==t)throw new TypeError("Super expression must either be null or a function, not "+typeof t);e.prototype=Object.create(t&&t.prototype,{constructor:{value:e,enumerable:!1,writable:!0,configurable:!0}}),t&&(Object.setPrototypeOf?Object.setPrototypeOf(e,t):e.__proto__=t)}Object.defineProperty(exports,"__esModule",{value:!0});var _createClass=function(){function e(e,t){for(var r=0;r<t.length;r++){var n=t[r];n.enumerable=n.enumerable||!1,n.configurable=!0,"value"in n&&(n.writable=!0),Object.defineProperty(e,n.key,n)}}return function(t,r,n){return r&&e(t.prototype,r),n&&e(t,n),t}}(),_wepy=require("./../../npm/wepy/lib/wepy.js"),_wepy2=_interopRequireDefault(_wepy),_util=require("./../../tools/util.js"),_request=require("./../../api/request.js"),_loadMore=require("./../../base/tips/loadMore.js"),_loadMore2=_interopRequireDefault(_loadMore),_square=require("./../../base/square.js"),_square2=_interopRequireDefault(_square),_empty=require("./../../base/loading/empty.js"),_empty2=_interopRequireDefault(_empty),GameSquare=function(e){function t(){var e,r,n,a;_classCallCheck(this,t);for(var i=arguments.length,o=Array(i),u=0;u<i;u++)o[u]=arguments[u];return r=n=_possibleConstructorReturn(this,(e=t.__proto__||Object.getPrototypeOf(t)).call.apply(e,[this].concat(o))),n.config={navigationBarTitleText:"玩伴广场"},n.$props={squarea:{use:"0"},empty:{}},n.$events={},n.components={loadMore:_loadMore2.default,squarea:_square2.default,empty:_empty2.default},n.data={list:[],currid:0,tag:1,screen:{},gameInfo:{},tagList:{},sysHeight:"",page:1,flag:0,noPic:"http://img.he29.com/play/41b2112a216b192f553d12e34c93deafae45fb00.png"},n.computed={},n.methods={PrvImg:function(e){var t=(0,_util.get)(e,"img");console.log(t)},tagClick:function(e){var t=(0,_util.get)(e,"id"),r=(0,_util.get)(e,"ids");this.list=[],this.makeTagFun(t,r)},moreThan:function(e){var t=(0,_util.get)(e,"uid");(0,_util.href)("home/index?uid="+t)},rightTop:function(){}},n.events={},a=r,_possibleConstructorReturn(n,a)}return _inherits(t,e),_createClass(t,[{key:"makeTagFun",value:function(e,t){for(var r in this.tagList)this.tagList[r].current=!1;this.tag=t,this.tagList[e].current=!0,this.page=1,this.flag=1,this.getGameList(),this.$apply()}},{key:"makeGetGameList",value:function(e){for(var t in e)if(e[t].user_online){var r=new Date(e[t].user_update).getTime();e[t].time2=(0,_util.getDateDiff)(r)}return e}},{key:"getAllTag",value:function(){function e(){return t.apply(this,arguments)}var t=_asyncToGenerator(regeneratorRuntime.mark(function e(){var t=this;return regeneratorRuntime.wrap(function(e){for(;;)switch(e.prev=e.next){case 0:(0,_request.requrst)("tag/getAllTag",{},function(e){t.tagList=e.info,t.makeTagFun(0,1)});case 1:case"end":return e.stop()}},e,this)}));return e}()},{key:"getGameList",value:function(){function e(){return t.apply(this,arguments)}var t=_asyncToGenerator(regeneratorRuntime.mark(function e(){var t=this;return regeneratorRuntime.wrap(function(e){for(;;)switch(e.prev=e.next){case 0:if(!(this.flag<0)){e.next=2;break}return e.abrupt("return",!1);case 2:this.flag=1,(0,_request.requrst)("Square/getData",{gid:this.currid,tag:this.tag,page:this.page++},function(e){if(0==e.info.total)t.flag=-3,t.$broadcast("LoadingEmptyFun","http://img.he29.com/play/cc775c9f2bd163e94c19ec09ae3dcb3d0a9016be.png","当前标签下没有小伙伴哦","...","换个试试吧"),t.$apply();else{if(e.info.data.length>0){t.flag=2;var r=t.makeGetGameList(e.info.data);t.list=(0,_util.AddData)(t.list,r),t.$apply()}else t.flag=-1,t.list=[];t.$broadcast("squareList",t.list,0),t.$apply()}});case 4:case"end":return e.stop()}},e,this)}));return e}()},{key:"getGameInfo",value:function(){function e(){return t.apply(this,arguments)}var t=_asyncToGenerator(regeneratorRuntime.mark(function e(){var t=this;return regeneratorRuntime.wrap(function(e){for(;;)switch(e.prev=e.next){case 0:(0,_request.requrst)("game/info",{gid:this.currid},function(e){t.gameInfo=e.info,(0,_util.title)(t.gameInfo.game_name+" - 玩伴广场"),t.$apply()});case 1:case"end":return e.stop()}},e,this)}));return e}()},{key:"scrollTolower",value:function(){function e(e){return t.apply(this,arguments)}var t=_asyncToGenerator(regeneratorRuntime.mark(function e(t){return regeneratorRuntime.wrap(function(e){for(;;)switch(e.prev=e.next){case 0:this.getGameList();case 1:case"end":return e.stop()}},e,this)}));return e}()},{key:"onLoad",value:function(e){this.sysHeight=(0,_util.sysInfo)("windowHeight"),this.currid=e.gid,this.getGameInfo(),this.getAllTag();this.$apply()}}]),t}(_wepy2.default.page);Page(require("./../../npm/wepy/lib/wepy.js").default.$createPage(GameSquare,"pages/game/square"));
});require("pages/game/square.js")@code-separator-line:["div","scroll-view","view","icon","input","text","image"]