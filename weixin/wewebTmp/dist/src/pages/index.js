/*v0.6vv_20170214_fbi*/
window.__wcc_version__='v0.6vv_20170214_fbi'
window.__wxml_transpiler_version__='v0.1'
var $gwxc
var $gaic={}
$gwx=function(path,global){
function _(a,b){b&&a.children.push(b);}
function _v(k){if(typeof(k)!='undefined')return {tag:'virtual','wxKey':k,children:[]};return {tag:'virtual',children:[]};}
function _n(tag){$gwxc++;if($gwxc>=16000){throw 'Dom limit exceeded, please check if there\'s any mistake you\'ve made.'};return {tag:tag.substr(0,3)=='wx-'?tag:'wx-'+tag,attr:{},children:[],n:[]}}
function _p(a,b){b&&a.properities.push(b);}
function _s(scope,env,key){return typeof(scope[key])!='undefined'?scope[key]:env[key]}function _wl(tname,prefix){console.warn('WXMLRT:'+prefix+':-1:-1:-1: Template `' + tname + '` is being called recursively, will be stop.')}$gwn=console.warn;
$gwl=console.log;
function $gwh()
{
function x(){}
x.prototype =
{
hn: function( obj )
{
if( typeof(obj) == 'object' )
{
var cnt=0;
var any=false;
for(var x in obj)
{
any|=x==='__value__';
cnt++;
if(cnt>2)break;
}
return cnt == 2 && any && obj.hasOwnProperty('__wxspec__') ? "h" : "n";
}
return "n";
},
nh: function( obj, special )
{
return { __value__: obj, __wxspec__: special ? special : true }
},
rv: function( obj )
{
return this.hn(obj)==='n'?obj:this.rv(obj.__value__);
}
}
return new x;
}
wh=$gwh();
function $gwrt( should_pass_type_info )
{
function ArithmeticEv( ops, e, s, g, o )
{
var rop = ops[0][1];
var _a,_b,_c,_d, _aa, _bb;
switch( rop )
{
case '?:':
_a = rev( ops[1], e, s, g, o );
_c = should_pass_type_info && ( wh.hn(_a) === 'h' );
_d = wh.rv( _a ) ? rev( ops[2], e, s, g, o ) : rev( ops[3], e, s, g, o );
_d = _c && wh.hn( _d ) === 'n' ? wh.nh( _d, 'c' ) : _d;
return _d;
break;
case '&&':
_a = rev( ops[1], e, s, g, o );
_c = should_pass_type_info && ( wh.hn(_a) === 'h' );
_d = wh.rv( _a ) ? rev( ops[2], e, s, g, o ) : wh.rv( _a );
_d = _c && wh.hn( _d ) === 'n' ? wh.nh( _d, 'c' ) : _d;
return _d;
break;
case '||':
_a = rev( ops[1], e, s, g, o );
_c = should_pass_type_info && ( wh.hn(_a) === 'h' );
_d = wh.rv( _a ) ? wh.rv(_a) : rev( ops[2], e, s, g, o );
_d = _c && wh.hn( _d ) === 'n' ? wh.nh( _d, 'c' ) : _d;
return _d;
break;
case '+':
case '*':
case '/':
case '%':
case '|':
case '^':
case '&':
case '===':
case '==':
case '!=':
case '!==':
case '>=':
case '<=':
case '>':
case '<':
case '<<':
case '>>':
_a = rev( ops[1], e, s, g, o );
_b = rev( ops[2], e, s, g, o );
_c = should_pass_type_info && (wh.hn( _a ) === 'h' || wh.hn( _b ) === 'h');
switch( rop )
{
case '+':
_d = wh.rv( _a ) + wh.rv( _b );
break;
case '*':
_d = wh.rv( _a ) * wh.rv( _b );
break;
case '/':
_d = wh.rv( _a ) / wh.rv( _b );
break;
case '%':
_d = wh.rv( _a ) % wh.rv( _b );
break;
case '|':
_d = wh.rv( _a ) | wh.rv( _b );
break;
case '^':
_d = wh.rv( _a ) ^ wh.rv( _b );
break;
case '&':
_d = wh.rv( _a ) & wh.rv( _b );
break;
case '===':
_d = wh.rv( _a ) === wh.rv( _b );
break;
case '==':
_d = wh.rv( _a ) == wh.rv( _b );
break;
case '!=':
_d = wh.rv( _a ) != wh.rv( _b );
break;
case '!==':
_d = wh.rv( _a ) !== wh.rv( _b );
break;
case '>=':
_d = wh.rv( _a ) >= wh.rv( _b );
break;
case '<=':
_d = wh.rv( _a ) <= wh.rv( _b );
break;
case '>':
_d = wh.rv( _a ) > wh.rv( _b );
break;
case '<':
_d = wh.rv( _a ) < wh.rv( _b );
break;
case '<<':
_d = wh.rv( _a ) << wh.rv( _b );
break;
case '>>':
_d = wh.rv( _a ) >> wh.rv( _b );
break;
default:
break;
}
return _c ? wh.nh( _d, "c" ) : _d;
break;
case '-':
_a = ops.length === 3 ? rev( ops[1], e, s, g, o ) : 0;
_b = ops.length === 3 ? rev( ops[2], e, s, g, o ) : rev( ops[1], e, s, g, o );
_c = should_pass_type_info && (wh.hn( _a ) === 'h' || wh.hn( _b ) === 'h');
_d = _c ? wh.rv( _a ) - wh.rv( _b ) : _a - _b;
return _c ? wh.nh( _d, "c" ) : _d;
break;
case '!':
_a = rev( ops[1], e, s, g, o );
_c = should_pass_type_info && (wh.hn( _a ) == 'h');
_d = !wh.rv(_a);
return _c ? wh.nh( _d, "c" ) : _d;
case '~':
_a = rev( ops[1], e, s, g, o );
_c = should_pass_type_info && (wh.hn( _a ) == 'h');
_d = ~wh.rv(_a);
return _c ? wh.nh( _d, "c" ) : _d;
default:
$gwn('unrecognized op' + rop );
}
}
function rev( ops, e, s, g, o )
{
var op = ops[0];
if( typeof(op)==='object' )
{
var vop=op[0];
var _a, _aa, _b, _bb, _c, _d, _s, _e, _ta, _tb, _td;
switch(vop)
{
case 2:
return ArithmeticEv(ops,e,s,g,o);
break;
case 4:
return rev( ops[1], e, s, g, o );
break;
case 5:
switch( ops.length )
{
case 2:
return should_pass_type_info ?
[rev(ops[1],e,s,g,o)] :
[wh.rv(rev(ops[1],e,s,g,o))];
break;
case 1:
return [];
break;
default:
_a = rev( ops[1],e,s,g,o );
_a.push(
should_pass_type_info ?
rev( ops[2],e,s,g,o ) :
wh.rv( rev(ops[2],e,s,g,o) )
);
return _a;
break;
}
break;
case 6:
_a = rev(ops[1],e,s,g,o);
_ta = wh.hn(_a)==='h';
_aa = _ta ? wh.rv(_a) : _a;
o.is_affected |= _ta;
if( should_pass_type_info )
{
if( _aa===null || typeof(_aa) === 'undefined' )
{
return _ta ? wh.nh(undefined, 'e') : undefined;
}
_b = rev(ops[2],e,s,g,o);
_tb = wh.hn(_b) === 'h';
_bb = _tb ? wh.rv(_b) : _b;
o.is_affected |= _tb;
if( _bb===null || typeof(_bb) === 'undefined' )
{
return (_ta || _tb) ? wh.nh(undefined, 'e') : undefined;
}
_d = _aa[_bb];
_td = wh.hn(_d)==='h';
o.is_affected |= _td;
return (_ta || _tb) ? (_td ? _d : wh.nh(_d, 'e')) : _d;
}
else
{
if( _aa===null || typeof(_aa) === 'undefined' )
{
return undefined;
}
_b = rev(ops[2],e,s,g,o);
_tb = wh.hn(_b) === 'h';
_bb = _tb ? wh.rv(_b) : _b;
o.is_affected |= _tb;
if( _bb===null || typeof(_bb) === 'undefined' )
{
return undefined;
}
_d = _aa[_bb];
_td = wh.hn(_d)==='h';
o.is_affected |= _td;
return _td ? wh.rv(_d) : _d;
}
case 7:
switch(ops[1][0])
{
case 11:
o.is_affected |= wh.hn(g)==='h';
return g;
case 3:
_s = wh.rv( s );
_e = wh.rv( e );
_b = ops[1][1];
_a = _s && _s.hasOwnProperty(_b) ?
s : _e && ( _e.hasOwnProperty(_b) ? e : undefined );
if( should_pass_type_info )
{
if( _a )
{
_ta = wh.hn(_a) === 'h';
_aa = _ta ? wh.rv( _a ) : _a;
_d = _aa[_b];
_td = wh.hn(_d) === 'h';
o.is_affected |= _ta || _td;
_d = _ta && !_td ? wh.nh(_d,'e') : _d;
return _d;
}
}
else
{
if( _a )
{
_ta = wh.hn(_a) === 'h';
_aa = _ta ? wh.rv( _a ) : _a;
_d = _aa[_b];
_td = wh.hn(_d) === 'h';
o.is_affected |= _ta || _td;
return wh.rv(_d);
}
}
return undefined;
}
break;
case 8:
_a = {};
_a[ops[1]] = rev(ops[2],e,s,g,o);
return _a;
break;
case 9:
_a = rev(ops[1],e,s,g,o);
_b = rev(ops[2],e,s,g,o);
function merge( _a, _b, _ow )
{
_ta = wh.hn(_a)==='h';
_tb = wh.hn(_b)==='h';
_aa = wh.rv(_a);
_bb = wh.rv(_b);
if( should_pass_type_info )
{
if( _tb )
{
for(var k in _bb)
{
if ( _ow || !_aa.hasOwnProperty(k) )
_aa[k]=wh.nh(_bb[k],'e');
}
}
else
{
for(var k in _bb)
{
if ( _ow || !_aa.hasOwnProperty(k) )
_aa[k]=_bb[k];
}
}
}
else
{
for(var k in _bb)
{
if ( _ow || _aa.hasOwnProperty(k) )
_aa[k]=wh.rv(_bb[k]);
}
}
return _a;
}
var _c = _a
var _ow = true
if ( typeof(ops[1][0]) === "object" && ops[1][0][0] === 10 ) {
_a = _b
_b = _c
_ow = false
}
if ( typeof(ops[1][0]) === "object" && ops[1][0][0] === 10 ) {
var _r = {}
return merge( merge( _r, _a, _ow ), _b, _ow );
}
else
return merge( _a, _b, _ow );
break;
case 10:
return should_pass_type_info ? rev(ops[1],e,s,g,o) : wh.rv(rev(ops[1],e,s,g,o));
}
}
else
{
if( op === 3 || op === 1 ) return ops[1];
else if( op === 11 )
{
var _a='';
for( var i = 1 ; i < ops.length ; i++ )
{
var xp = wh.rv(rev(ops[i],e,s,g,o));
_a += typeof(xp) === 'undefined' ? '' : xp;
}
return _a;
}
}
}
return rev;
}
gra=$gwrt(true);
grb=$gwrt(false);
function TestTest( expr, ops, e,s,g, expect_a, expect_b, expect_affected )
{
{
var o = {is_affected:false};
var a = gra( ops, e,s,g, o );
if( JSON.stringify(a) != JSON.stringify( expect_a )
|| o.is_affected != expect_affected )
{
console.warn( "A. " + expr + " get result " + JSON.stringify(a) + ", " + o.is_affected + ", but " + JSON.stringify( expect_a ) + ", " + expect_affected + " is expected" );
}
}
{
var o = {is_affected:false};
var a = grb( ops, e,s,g, o );
if( JSON.stringify(a) != JSON.stringify( expect_b )
|| o.is_affected != expect_affected )
{
console.warn( "B. " + expr + " get result " + JSON.stringify(a) + ", " + o.is_affected + ", but " + JSON.stringify( expect_b ) + ", " + expect_affected + " is expected" );
}
}
}

function wfor( to_iter, func, env, _s, global, father, itemname, indexname, keyname )
{
var _n = wh.hn( to_iter ) === 'n';
var scope = wh.rv( _s );
var has_old_item = scope.hasOwnProperty(itemname);
var has_old_index = scope.hasOwnProperty(indexname);
var old_item = scope[itemname];
var old_index = scope[indexname];
var full = Object.prototype.toString.call(wh.rv(to_iter));
var type = full[8];
if( type === 'N' && full[10] === 'l' ) type = 'X';
var _y;
if( _n )
{
if( type === 'A' )
{
for( var i = 0 ; i < to_iter.length ; i++ )
{
scope[itemname] = to_iter[i];
scope[indexname] = wh.nh(i, 'h');
_y = keyname ? (keyname==="*this" ? _v(wh.rv(to_iter[i])) : _v(wh.rv(wh.rv(to_iter[i])[keyname]))) : _v();
_(father,_y);
func( env, scope, _y, global );
}
}
else if( type === 'O' )
{
for( var k in to_iter )
{
scope[itemname] = to_iter[k];
scope[indexname] = wh.nh(k, 'h');
_y = keyname ? (keyname==="*this" ? _v(wh.rv(to_iter[k])) : _v(wh.rv(wh.rv(to_iter[k])[keyname]))) : _v();
_(father,_y);
func( env,scope,_y,global );
}
}
else if( type === 'S' )
{
for( var i = 0 ; i < to_iter.length ; i++ )
{
scope[itemname] = to_iter[i];
scope[indexname] = wh.nh(i, 'h');
_y = _v( to_iter[i] + i );
_(father,_y);
func( env, scope, _y, global );
}
}
else if( type === 'N' )
{
for( var i = 0 ; i < to_iter ; i++ )
{
scope[itemname] = i;
scope[indexname] = wh.nh(i, 'h');
_y = _v( i );
_(father,_y);
func(env,scope,_y,global);
}
}
else
{
}
}
else
{
var r_to_iter = wh.rv(to_iter);
var r_iter_item, iter_item;
if( type === 'A' )
{
for( var i = 0 ; i < r_to_iter.length ; i++ )
{
iter_item = r_to_iter[i];
iter_item = wh.hn(iter_item)==='n' ? wh.nh(iter_item,'h') : iter_item;
r_iter_item = wh.rv( iter_item );
scope[itemname] = iter_item
scope[indexname] = wh.nh(i, 'h');
_y = keyname ? (keyname==="*this" ? _v(r_iter_item) : _v(wh.rv(r_iter_item[keyname]))) : _v();
_(father,_y);
func( env, scope, _y, global );
}
}
else if( type === 'O' )
{
for( var k in r_to_iter )
{
iter_item = r_to_iter[k];
iter_item = wh.hn(iter_item)==='n'? wh.nh(iter_item,'h') : iter_item;
r_iter_item = wh.rv( iter_item );
scope[itemname] = iter_item;
scope[indexname] = wh.nh(k, 'h');
_y = keyname ? (keyname==="*this" ? _v(r_iter_item) : _v(wh.rv(r_iter_item[keyname]))) : _v();
_(father,_y);
func( env, scope, _y, global );
}
}
else if( type === 'S' )
{
for( var i = 0 ; i < r_to_iter.length ; i++ )
{
scope[itemname] = wh.nh(r_to_iter[i],'h');
scope[indexname] = wh.nh(i, 'h');
_y = _v( to_iter[i] + i );
_(father,_y);
func( env, scope, _y, global );
}
}
else if( type === 'N' )
{
for( var i = 0 ; i < r_to_iter ; i++ )
{
scope[itemname] = wh.nh(i,'h');
scope[indexname]= wh.nh(i,'h');
_y = _v( i );
_(father,_y);
func(env,scope,_y,global);
}
}
else
{
}
}
if(has_old_item)
{
scope[itemname]=old_item;
}
else
{
delete scope[itemname];
}
if(has_old_index)
{
scope[indexname]=old_index;
}
else{delete scope[indexname];}}


function _r( node, attrname, opindex, env, scope, global )
{
var o = {};
var a = grb( z[opindex], env, scope, global, o );
node.attr[attrname] = a;
if( o.is_affected ) node.n.push( attrname );
}
function _o( opindex, env, scope, global )
{
var nothing = {};
return grb( z[opindex], env, scope, global, nothing );
}
function _1( opindex, env, scope, global )
{
var nothing = {};
return gra( z[opindex], env, scope, global, nothing );
}
function _2( opindex, func, env, scope, global, father, itemname, indexname, keyname )
{
var to_iter = _1( opindex, env, scope, global, father, itemname, indexname, keyname );
wfor( to_iter, func, env, scope, global, father, itemname, indexname, keyname );
}
function _gv( )
{
if( typeof(window.__webview_engine_version__) == 'undefined' ) return 0.0;
return window.__webview_engine_version__;
}
function _m(tag,attrs,env,scope,global)
{
var tmp=_n(tag);
var base=0;
for(var i = 0 ; i < attrs.length ; i+=2 )
{
if(attrs[i+1]<0)
{tmp.attr[attrs[i]]=true;}else{_r(tmp,attrs[i],base+attrs[i+1],env,scope,global);
if(base===0)base=attrs[i+1];}}return tmp;};function _ai(i,p,e,me,r,c){var x=_grp(p,e,me);if(x)i.push(x);else{i.push('');console.warn('WXMLRT:'+me+':import:'+r+':'+c+': Path `'+p+'` not found from `'+me+'`.')}}function _grp(p,e,me){if(p[0]!='/'){var mepart=me.split('/');mepart.pop();var ppart=p.split('/');for(var i=0;i<ppart.length;i++){if(ppart[i]=='..')mepart.pop();else if(ppart[i]=='.' || !ppart[i])continue;else mepart.push(ppart[i]);}p=mepart.join('/');}if(me[0]=='.'&&p[0]=='/')p='.'+p;if(e[p])return p;if(e[p+'.wxml'])return p+'.wxml';}function _gd(p,c,e,d){if(!c)return;if(d[p][c])return d[p][c];for(var x=e[p].i.length-1;x>=0;x--){if(e[p].i[x]&&d[e[p].i[x]][c])return d[e[p].i[x]][c]};for(var x=e[p].ti.length-1;x>=0;x--){var q=_grp(e[p].ti[x],e,p);if(q&&d[q][c])return d[q][c]}
var ii=_gapi(e,p);for(var x=0;x<ii.length;x++){if(ii[x]&&d[ii[x]][c])return d[ii[x]][c]}for(var k=e[p].j.length-1;k>=0;k--)if(e[p].j[k]){for(var q=e[e[p].j[k]].ti.length-1;q>=0;q--){var pp=_grp(e[e[p].j[k]].ti[q],e,p);if(pp&&d[pp][c]){return d[pp][c]}}}}function _gapi(e,p){if(!p)return [];if($gaic[p]){return $gaic[p]};var ret=[],q=[],h=0,t=0,put={},visited={};q.push(p);visited[p]=true;t++;while(h<t){var a=q[h++];for(var i=0;i<e[a].ic.length;i++){var nd=e[a].ic[i];var np=_grp(nd,e,a);if(np&&!visited[np]){visited[np]=true;q.push(np);t++;}}for(var i=0;a!=p&&i<e[a].ti.length;i++){var ni=e[a].ti[i];var nm=_grp(ni,e,a);if(nm&&!put[nm]){put[nm]=true;ret.push(nm);}}}$gaic[p]=ret;return ret;}var $ixc={};function _ic(p,ent,me,e,s,r,gg){var x=_grp(p,ent,me);ent[me].j.push(x);if(x){if($ixc[x]){console.warn('WXMLRT:-1:include:-1:-1: `'+p+'` is being included in a loop, will be stop.');return;}$ixc[x]=true;try{ent[x].f(e,s,r,gg)}catch(e){}$ixc[x]=false;}else{console.warn('WXMLRT:'+me+':include:-1:-1: Included path `'+p+'` not found from `'+me+'`.')}}function _w(tn,f,line,c){console.warn('WXMLRT:'+f+':template:'+line+':'+c+': Template `'+tn+'` not found.');}function _ev(dom){var changed=false;delete dom.properities;delete dom.n;if(dom.children){do{changed=false;var newch = [];for(var i=0;i<dom.children.length;i++){var ch=dom.children[i];if( ch.tag=='virtual'){changed=true;for(var j=0;ch.children&&j<ch.children.length;j++){newch.push(ch.children[j]);}}else { newch.push(ch); } } dom.children = newch; }while(changed);for(var i=0;i<dom.children.length;i++){_ev(dom.children[i]);}} return dom; }var e_={}
if(global&&typeof(global.entrys)=='object')e_=global.entrys
var d_={}
if(global&&typeof(global.defines)=='object')d_=global.defines
var p_={}
var z = [];
  (function(z){
    var a = 11;
    function Z(ops){z.push(ops)};
    Z([3, 'app']);Z([3, 'winTab']);Z([[6],[[7],[3, "tabIndex"]],[1, 0]]);Z([a, [3, 'app animated '],[[7],[3, "$indexTab$change"]]]);Z([[7],[3, "$indexTab$swiper$autoplay"]]);Z([3, 'swiper']);Z([[7],[3, "$indexTab$swiper$duration"]]);Z([[7],[3, "$indexTab$swiper$indicatorActiveColor"]]);Z([[7],[3, "$indexTab$swiper$indicatorDots"]]);Z([[7],[3, "$indexTab$swiper$interval"]]);Z([[7],[3, "$indexTab$swiper$adList"]]);Z([3, 'key']);Z([3, '$indexTab$swiper$goToAdvert']);Z([3, 'slide-image']);Z([[6],[[7],[3, "item"]],[3, "url"]]);Z([3, 'aspectFill']);Z([[6],[[7],[3, "item"]],[3, "img"]]);Z([a, [3, 'navcates '],[[2,'?:'],[[2, "=="], [[7],[3, "$indexTab$navcate$leftColor"]], [1, 0]],[1, "noborder"],[1, ""]]]);Z([3, 'left']);Z([3, 'icon']);Z([[7],[3, "$indexTab$navcate$icon"]]);Z([3, 'name']);Z([a, [[7],[3, "$indexTab$navcate$title"]]]);Z([3, '$indexTab$navcate$NavcateRight']);Z([3, 'right']);Z([3, 'rtext']);Z([a, [[7],[3, "$indexTab$navcate$right"]]]);Z([3, 'http://img.he29.com/play/f4fe7786635ee901a46590b7f1926bc08d628bdf.png']);Z([3, 'rows']);Z([[7],[3, "$indexTab$rows$autoplay"]]);Z([[7],[3, "$indexTab$rows$circular"]]);Z([[7],[3, "$indexTab$rows$duration"]]);Z([[7],[3, "$indexTab$rows$indicatorActiveColor"]]);Z([[7],[3, "$indexTab$rows$indicatorColor"]]);Z([[7],[3, "$indexTab$rows$indicatorDots"]]);Z([[7],[3, "$indexTab$rows$interval"]]);Z([a, [3, 'height:'],[[2, "+"], [[2, "*"], [[7],[3, "$indexTab$rows$width"]], [1, 2]], [1, 10]],[3, 'px']]);Z([[7],[3, "$indexTab$rows$vertical"]]);Z([[7],[3, "$indexTab$rows$adList"]]);Z([3, 'itemName']);Z([3, 'idx']);Z([3, 'content']);Z([[6],[[7],[3, "itemName"]],[3, "info"]]);Z([3, '$indexTab$rows$run']);Z([3, 'rowList']);Z([[6],[[7],[3, "item"]],[3, "bill_uid"]]);Z([a, [3, 'width:'],[[7],[3, "$indexTab$rows$width"]],[3, 'px;height:'],[[7],[3, "$indexTab$rows$width"]],[3, 'px']]);Z([3, 'need']);Z([[6],[[7],[3, "item"]],[3, "user_avatar"]]);Z([3, 'tagbum']);Z([[6],[[7],[3, "item"]],[3, "icon"]]);Z([3, 'nameline']);Z([a, [[6],[[7],[3, "item"]],[3, "user_nickname"]]]);Z([3, 'opa5']);Z([a, [3, 'height:'],[[7],[3, "$indexTab$rows$width"]],[3, 'px;']]);Z([3, '\r\n                                    榜上无人\r\n                                ']);Z([3, 'opt']);Z([[6],[[7],[3, "itemName"]],[3, "nobodoy"]]);Z([a, [3, 'navcates '],[[2,'?:'],[[2, "=="], [[7],[3, "$indexTab$navcate2$leftColor"]], [1, 0]],[1, "noborder"],[1, ""]]]);Z([[7],[3, "$indexTab$navcate2$icon"]]);Z([a, [[7],[3, "$indexTab$navcate2$title"]]]);Z([3, '$indexTab$navcate2$NavcateRight']);Z([a, [[7],[3, "$indexTab$navcate2$right"]]]);Z([3, 'plist']);Z([a, [3, 'plist animated '],[[7],[3, "$indexTab$plist$amz"]]]);Z([[7],[3, "$indexTab$plist$list"]]);Z([3, '$indexTab$plist$goChose']);Z([3, 'box']);Z([[6],[[7],[3, "item"]],[3, "game_id"]]);Z([[6],[[7],[3, "item"]],[3, "about"]]);Z([[6],[[7],[3, "item"]],[3, "game_thumb"]]);Z([3, 'leftbox']);Z([[6],[[7],[3, "item"]],[3, "type_color"]]);Z([3, 'top']);Z([a, [[6],[[7],[3, "item"]],[3, "game_name"]]]);Z([3, 'center']);Z([a, [[6],[[7],[3, "item"]],[3, "game_auth"]]]);Z([3, 'fot']);Z([a, [3, '\r\n                        '],[[6],[[7],[3, "item"]],[3, "about_number"]],[3, '\r\n                        ']]);Z([3, 'orgin']);Z([3, '人关注']);Z([3, 'rightbox']);Z([3, 'leftTop']);Z([[6],[[7],[3, "item"]],[3, "type_icon"]]);Z([3, 'bgc']);Z([[2, "=="], [[6],[[7],[3, "item"]],[3, "about"]], [1, 1]]);Z([3, 'choseUse']);Z([a, [[7],[3, "$indexTab$plist$title"]]]);Z([3, '关注游戏']);Z([3, 'h50']);Z([3, 'h20']);Z([3, 'container']);Z([[7],[3, "$indexTab$tips$open"]]);Z([3, 'loadmsg animated fadeIn']);Z([3, 'msg']);Z([3, 'layui-m-layercont']);Z([a, [[7],[3, "$indexTab$tips$msg"]]]);Z([[7],[3, "$indexTab$watcher$open"]]);Z([3, 'watchs']);Z([3, 'watch animated fadeIn']);Z([3, 'message']);Z([a, [[7],[3, "$indexTab$watcher$msgStr"]]]);Z([[2, "||"],[[7],[3, "$indexTab$watcher$imgs"]],[1, "http://img.he29.com/play/58c358435cad9d985da5236e9d497f2bc2ebcb0d.jpeg"]]);Z([3, 'btns']);Z([3, '$indexTab$watcher$waitRun']);Z([3, 'flex']);Z([a, [[2, "||"],[[7],[3, "$indexTab$watcher$left"]],[1, "稍后进入"]]]);Z([3, '$indexTab$watcher$nowRun']);Z([a, [[2, "||"],[[7],[3, "$indexTab$watcher$right"]],[1, "马上进入"]]]);Z([3, 'loading']);Z([[6],[[7],[3, "tabIndex"]],[1, 1]]);Z([a, [3, 'animated '],[[7],[3, "$gameTab$change"]]]);Z([a, [3, 'plist animated '],[[7],[3, "$gameTab$plist$amz"]]]);Z([[7],[3, "$gameTab$plist$list"]]);Z([3, '$gameTab$plist$goChose']);Z([a, [[7],[3, "$gameTab$plist$title"]]]);Z([[6],[[7],[3, "tabIndex"]],[1, 2]]);Z([a, [3, 'status animated '],[[7],[3, "$orderTab$change"]]]);Z([3, 'navlistd']);Z([[7],[3, "$orderTab$navset"]]);Z([3, '$orderTab$tab9Click']);Z([3, 'lisDox']);Z([3, 'leftText']);Z([3, 'icon54']);Z([3, 'title']);Z([a, [[6],[[7],[3, "item"]],[3, "name"]]]);Z([3, 'rightText']);Z([[2, "!="], [[6],[[7],[3, "item"]],[3, "rightNum"]], [1, 0]]);Z([[6],[[7],[3, "item"]],[3, "rightNum"]]);Z([3, 'aui-badge']);Z([3, 'aui-badge aui-badge-none']);Z([[6],[[7],[3, "tabIndex"]],[1, 3]]);Z([a, [3, 'container animated '],[[7],[3, "$userTab$change"]]]);Z([3, 'userHome']);Z([3, 'http://img.he29.com/play/a3706862203a9c247cf6fab2ff80b9a00828a093.jpeg']);Z([3, 'header']);Z([3, 'headerImg']);Z([[6],[[7],[3, "$userTab$userInfo"]],[3, "user_avatar"]]);Z([3, 'userInfo']);Z([3, 'username']);Z([a, [[6],[[7],[3, "$userTab$userInfo"]],[3, "user_nickname"]]]);Z([[7],[3, "$userTab$false"]]);Z([3, 'tag']);Z([3, 'LV3']);Z([3, '$userTab$tab9Click']);Z([3, 'rightBot']);Z([3, '/pages/index/sign']);Z([3, 'textContent']);Z([3, 'fa fa-user']);Z([3, '签到有礼\r\n                ']);Z([3, 'leftBorder']);Z([3, 'hdfot']);Z([3, 'textLine']);Z([a, [3, 'navcates '],[[2,'?:'],[[2, "=="], [[7],[3, "$userTab$navcate$leftColor"]], [1, 0]],[1, "noborder"],[1, ""]]]);Z([[7],[3, "$userTab$navcate$icon"]]);Z([a, [[7],[3, "$userTab$navcate$title"]]]);Z([3, '$userTab$navcate$NavcateRight']);Z([a, [[7],[3, "$userTab$navcate$right"]]]);Z([3, 'nav9']);Z([[6],[[7],[3, "$userTab$navset"]],[3, "navcate"]]);Z([3, 'navList']);Z([3, 'navlist']);Z([[6],[[7],[3, "$userTab$navset"]],[3, "navfot"]]);Z([[2, ">"], [[6],[[7],[3, "item"]],[3, "disable"]], [1, 0]]);Z([3, 'foot']);Z([3, 'footer']);Z([3, 'h90']);Z([3, 'groupitem']);Z([[7],[3, "$footer$false"]]);Z([3, 'sendBtn']);Z([3, 'https://s1.ax1x.com/2017/12/01/4BuMF.th.png']);Z([[7],[3, "$footer$list"]]);Z([3, '$footer$item']);Z([3, '$footer$index']);Z([3, '$footer$key']);Z([3, '$footer$advTice']);Z([a, [3, 'my '],[[2,'?:'],[[2, "=="], [[6],[[7],[3, "$footer$item"]],[3, "current"]], [1, 1]],[1, "cur"],[1, "nocur"]]]);Z([3, 'WeChat']);Z([[7],[3, "$footer$index"]]);Z([[6],[[7],[3, "$footer$item"]],[3, "url"]]);Z([3, 'tapTest']);Z([3, 'fimg']);Z([[2, "=="], [[6],[[7],[3, "$footer$item"]],[3, "current"]], [1, 1]]);Z([3, 'footIcon']);Z([[6],[[7],[3, "$footer$item"]],[3, "curricon"]]);Z([[6],[[7],[3, "$footer$item"]],[3, "icon"]]);Z([3, 'fname']);Z([a, [3, 'color:'],[[2,'?:'],[[2, "=="], [[6],[[7],[3, "$footer$item"]],[3, "current"]], [1, 1]],[[6],[[7],[3, "$footer$footer"]],[3, "current"]],[[6],[[7],[3, "$footer$footer"]],[3, "defaultColor"]]]]);Z([a, [[6],[[7],[3, "$footer$item"]],[3, "name"]]]);Z([3, 'guess']);Z([[7],[3, "$footer$rightData"]]);Z([3, 'tylist']);Z([3, '$footer$rightClick']);Z([3, 'tyimg']);Z([[6],[[7],[3, "onstart"]],[3, "show"]]);Z([3, 'adv']);Z([[7],[3, "$advtice$open"]]);Z([3, 'panal']);Z([3, 'contentText']);Z([a, [3, 'height:'],[[7],[3, "$advtice$height"]],[3, 'px;margin-top:'],[[7],[3, "$advtice$top"]],[3, 'px;']]);Z([3, '$advtice$goUrl']);Z([3, 'father']);Z([[7],[3, "$advtice$url"]]);Z([3, 'background']);Z([[6],[[7],[3, "onstart"]],[3, "background"]]);Z([3, 'weui-article__section']);Z([3, 'weui-article__h3']);Z([3, 'text-align:center']);Z([a, [[6],[[7],[3, "onstart"]],[3, "title"]]]);Z([3, 'PanContent']);Z([a, [[6],[[7],[3, "onstart"]],[3, "content"]]]);Z([3, 'weui-article__p']);Z([3, 'line']);Z([3, '$advtice$close']);Z([3, 'close']);Z([3, 'X']);
  })(z);d_["./pages/index.wxml"] = {};
  var m0=function(e,s,r,gg){
    var oOC = _n("view");_r(oOC, 'class', 0, e, s, gg);var oPC = _n("view");_r(oPC, 'class', 1, e, s, gg);var oQC = _v();
      if (_o(2, e, s, gg)) {
        oQC.wxVkey = 1;var oRC = _n("view");_r(oRC, 'class', 3, e, s, gg);var oTC = _n("view");var oUC = _m( "swiper", ["autoplay", 4,"class", 1,"duration", 2,"indicatorActiveColor", 3,"indicatorDots", 4,"interval", 5], e, s, gg);var oVC = _v();var oWC = function(oaC,oZC,oYC,gg){var ocC = _n("swiper-item");var odC = _m( "image", ["bindtap", 12,"class", 1,"data-id", 2,"mode", 3,"src", 4], oaC, oZC, gg);_(ocC,odC);_(oYC,ocC);return oYC;};_2(10, oWC, e, s, gg, oVC, "item", "index", 'key');_(oUC,oVC);_(oTC,oUC);_(oRC,oTC);var oeC = _n("view");_r(oeC, 'class', 17, e, s, gg);var ofC = _n("view");_r(ofC, 'class', 18, e, s, gg);var ogC = _n("view");_r(ogC, 'class', 19, e, s, gg);var ohC = _v();
      if (_o(20, e, s, gg)) {
        ohC.wxVkey = 1;var oiC = _n("image");_r(oiC, 'src', 20, e, s, gg);_(ohC, oiC);
      } _(ogC,ohC);_(ofC,ogC);var okC = _n("view");_r(okC, 'class', 21, e, s, gg);var olC = _o(22, e, s, gg);_(okC,olC);_(ofC,okC);_(oeC,ofC);var omC = _m( "view", ["bindtap", 23,"class", 1], e, s, gg);var onC = _n("text");_r(onC, 'class', 25, e, s, gg);var ooC = _o(26, e, s, gg);_(onC,ooC);_(omC,onC);var opC = _n("image");_r(opC, 'src', 27, e, s, gg);_(omC,opC);_(oeC,omC);_(oRC,oeC);var oqC = _n("view");_r(oqC, 'class', 28, e, s, gg);var orC = _m( "swiper", ["class", 5,"autoplay", 24,"circular", 25,"duration", 26,"indicatorActiveColor", 27,"indicatorColor", 28,"indicatorDots", 29,"interval", 30,"style", 31,"vertical", 32], e, s, gg);var osC = _v();var otC = function(oxC,owC,ovC,gg){var ouC = _n("swiper-item");var ozC = _n("view");_r(ozC, 'class', 41, oxC, owC, gg);var o_C = _v();var oAD = function(oED,oDD,oCD,gg){var oGD = _m( "view", ["bindtap", 43,"class", 1,"data-uid", 2,"style", 3], oED, oDD, gg);var oHD = _v();
      if (_o(45, oED, oDD, gg)) {
        oHD.wxVkey = 1;var oID = _n("view");_r(oID, 'class', 47, oED, oDD, gg);var oKD = _m( "image", ["mode", 15,"src", 33], oED, oDD, gg);_(oID,oKD);var oLD = _m( "image", ["mode", 15,"class", 34,"src", 35], oED, oDD, gg);_(oID,oLD);var oMD = _n("text");_r(oMD, 'class', 51, oED, oDD, gg);var oND = _o(52, oED, oDD, gg);_(oMD,oND);_(oID,oMD);_(oHD, oID);
      }else {
        oHD.wxVkey = 2;var oOD = _n("view");_r(oOD, 'class', 53, e, s, gg);var oQD = _m( "view", ["class", 47,"style", 7], e, s, gg);var oRD = _o(55, e, s, gg);_(oQD,oRD);_(oOD,oQD);var oSD = _n("view");_r(oSD, 'class', 56, e, s, gg);_(oOD,oSD);var oTD = _m( "image", ["mode", 15,"src", 42], e, s, gg);_(oOD,oTD);_(oHD, oOD);
      }_(oGD,oHD);_(oCD,oGD);return oCD;};_2(42, oAD, oxC, owC, gg, o_C, "item", "index", 'key');_(ozC,o_C);_(ouC,ozC);_(ovC, ouC);return ovC;};_2(38, otC, e, s, gg, osC, "itemName", "idx", 'key');_(orC,osC);_(oqC,orC);_(oRC,oqC);var oUD = _n("view");_r(oUD, 'class', 58, e, s, gg);var oVD = _n("view");_r(oVD, 'class', 18, e, s, gg);var oWD = _n("view");_r(oWD, 'class', 19, e, s, gg);var oXD = _v();
      if (_o(59, e, s, gg)) {
        oXD.wxVkey = 1;var oYD = _n("image");_r(oYD, 'src', 59, e, s, gg);_(oXD, oYD);
      } _(oWD,oXD);_(oVD,oWD);var oaD = _n("view");_r(oaD, 'class', 21, e, s, gg);var obD = _o(60, e, s, gg);_(oaD,obD);_(oVD,oaD);_(oUD,oVD);var ocD = _m( "view", ["class", 24,"bindtap", 37], e, s, gg);var odD = _n("text");_r(odD, 'class', 25, e, s, gg);var oeD = _o(62, e, s, gg);_(odD,oeD);_(ocD,odD);var ofD = _n("image");_r(ofD, 'src', 27, e, s, gg);_(ocD,ofD);_(oUD,ocD);_(oRC,oUD);var ogD = _n("view");_r(ogD, 'class', 63, e, s, gg);var ohD = _n("view");_r(ohD, 'class', 64, e, s, gg);var oiD = _v();var ojD = function(onD,omD,olD,gg){var okD = _m( "view", ["bindtap", 66,"class", 1,"data-id", 2,"data-ids", 3,"data-img", 4], onD, omD, gg);var opD = _m( "view", ["class", 71,"style", 1], onD, omD, gg);var oqD = _n("view");_r(oqD, 'class', 73, onD, omD, gg);var orD = _o(74, onD, omD, gg);_(oqD,orD);_(opD,oqD);var osD = _n("view");_r(osD, 'class', 75, onD, omD, gg);var otD = _o(76, onD, omD, gg);_(osD,otD);_(opD,osD);var ouD = _n("view");_r(ouD, 'class', 77, onD, omD, gg);var ovD = _o(78, onD, omD, gg);_(ouD,ovD);var owD = _n("text");_r(owD, 'class', 79, onD, omD, gg);var oxD = _o(80, onD, omD, gg);_(owD,oxD);_(ouD,owD);_(opD,ouD);_(okD,opD);var oyD = _n("view");_r(oyD, 'class', 81, onD, omD, gg);_(okD,oyD);var ozD = _m( "image", ["class", 82,"src", 1], onD, omD, gg);_(okD,ozD);var o_D = _m( "image", ["mode", 15,"src", 55,"class", 69], onD, omD, gg);_(okD,o_D);var oAE = _m( "view", ["data-id", 68,"data-ids", 1,"data-img", 2], onD, omD, gg);var oBE = _v();
      if (_o(85, onD, omD, gg)) {
        oBE.wxVkey = 1;var oCE = _m( "text", ["style", 72,"class", 14], onD, omD, gg);var oEE = _o(87, onD, omD, gg);_(oCE,oEE);_(oBE, oCE);
      }else {
        oBE.wxVkey = 2;var oFE = _n("text");_r(oFE, 'class', 86, e, s, gg);var oHE = _o(88, e, s, gg);_(oFE,oHE);_(oBE, oFE);
      }_(oAE,oBE);_(okD,oAE);_(olD, okD);return olD;};_2(65, ojD, e, s, gg, oiD, "item", "index", 'key');_(ohD,oiD);_(ogD,ohD);var oIE = _n("view");_r(oIE, 'class', 89, e, s, gg);_(ogD,oIE);_(oRC,ogD);var oJE = _n("view");_r(oJE, 'class', 90, e, s, gg);_(oRC,oJE);var oKE = _n("view");_r(oKE, 'class', 91, e, s, gg);var oLE = _v();
      if (_o(92, e, s, gg)) {
        oLE.wxVkey = 1;var oME = _n("view");_r(oME, 'class', 93, e, s, gg);var oOE = _n("view");_r(oOE, 'class', 94, e, s, gg);var oPE = _n("text");_r(oPE, 'class', 95, e, s, gg);var oQE = _o(96, e, s, gg);_(oPE,oQE);_(oOE,oPE);_(oME,oOE);_(oLE, oME);
      } _(oKE,oLE);_(oRC,oKE);var oRE = _v();
      if (_o(97, e, s, gg)) {
        oRE.wxVkey = 1;var oSE = _n("view");_r(oSE, 'class', 98, e, s, gg);var oUE = _n("view");_r(oUE, 'class', 99, e, s, gg);var oVE = _n("text");_r(oVE, 'class', 100, e, s, gg);var oWE = _o(101, e, s, gg);_(oVE,oWE);_(oUE,oVE);var oXE = _n("image");_r(oXE, 'src', 102, e, s, gg);_(oUE,oXE);var oYE = _n("view");_r(oYE, 'class', 103, e, s, gg);var oZE = _m( "text", ["bindtap", 104,"class", 1], e, s, gg);var oaE = _o(106, e, s, gg);_(oZE,oaE);_(oYE,oZE);var obE = _m( "text", ["class", 105,"bindtap", 2], e, s, gg);var ocE = _o(108, e, s, gg);_(obE,ocE);_(oYE,obE);_(oUE,oYE);_(oSE,oUE);var odE = _n("view");_r(odE, 'class', 109, e, s, gg);_(oSE,odE);_(oRE, oSE);
      } _(oRC,oRE);_(oQC, oRC);
      } _(oPC,oQC);var oeE = _v();
      if (_o(110, e, s, gg)) {
        oeE.wxVkey = 1;var ofE = _n("view");_r(ofE, 'class', 111, e, s, gg);var ohE = _n("view");_r(ohE, 'class', 63, e, s, gg);var oiE = _n("view");_r(oiE, 'class', 112, e, s, gg);var ojE = _v();var okE = function(ooE,onE,omE,gg){var olE = _m( "view", ["class", 67,"data-id", 1,"data-ids", 2,"data-img", 3,"bindtap", 47], ooE, onE, gg);var oqE = _m( "view", ["class", 71,"style", 1], ooE, onE, gg);var orE = _n("view");_r(orE, 'class', 73, ooE, onE, gg);var osE = _o(74, ooE, onE, gg);_(orE,osE);_(oqE,orE);var otE = _n("view");_r(otE, 'class', 75, ooE, onE, gg);var ouE = _o(76, ooE, onE, gg);_(otE,ouE);_(oqE,otE);var ovE = _n("view");_r(ovE, 'class', 77, ooE, onE, gg);var owE = _o(78, ooE, onE, gg);_(ovE,owE);var oxE = _n("text");_r(oxE, 'class', 79, ooE, onE, gg);var oyE = _o(80, ooE, onE, gg);_(oxE,oyE);_(ovE,oxE);_(oqE,ovE);_(olE,oqE);var ozE = _n("view");_r(ozE, 'class', 81, ooE, onE, gg);_(olE,ozE);var o_E = _m( "image", ["class", 82,"src", 1], ooE, onE, gg);_(olE,o_E);var oAF = _m( "image", ["mode", 15,"src", 55,"class", 69], ooE, onE, gg);_(olE,oAF);var oBF = _m( "view", ["data-id", 68,"data-ids", 1,"data-img", 2], ooE, onE, gg);var oCF = _v();
      if (_o(85, ooE, onE, gg)) {
        oCF.wxVkey = 1;var oDF = _m( "text", ["style", 72,"class", 14], ooE, onE, gg);var oFF = _o(115, ooE, onE, gg);_(oDF,oFF);_(oCF, oDF);
      }else {
        oCF.wxVkey = 2;var oGF = _n("text");_r(oGF, 'class', 86, e, s, gg);var oIF = _o(88, e, s, gg);_(oGF,oIF);_(oCF, oGF);
      }_(oBF,oCF);_(olE,oBF);_(omE, olE);return omE;};_2(113, okE, e, s, gg, ojE, "item", "index", 'key');_(oiE,ojE);_(ohE,oiE);var oJF = _n("view");_r(oJF, 'class', 89, e, s, gg);_(ohE,oJF);_(ofE,ohE);_(oeE, ofE);
      } _(oPC,oeE);var oKF = _v();
      if (_o(116, e, s, gg)) {
        oKF.wxVkey = 1;var oLF = _n("view");_r(oLF, 'class', 117, e, s, gg);var oNF = _n("view");_r(oNF, 'class', 118, e, s, gg);var oOF = _v();var oPF = function(oTF,oSF,oRF,gg){var oQF = _m( "view", ["data-url", 14,"bindtap", 106,"class", 107], oTF, oSF, gg);var oVF = _n("view");_r(oVF, 'class', 122, oTF, oSF, gg);var oWF = _m( "image", ["mode", 15,"src", 35,"class", 108], oTF, oSF, gg);_(oVF,oWF);var oXF = _n("text");_r(oXF, 'class', 124, oTF, oSF, gg);var oYF = _o(125, oTF, oSF, gg);_(oXF,oYF);_(oVF,oXF);_(oQF,oVF);var oZF = _n("view");_r(oZF, 'class', 126, oTF, oSF, gg);var oaF = _v();
      if (_o(127, oTF, oSF, gg)) {
        oaF.wxVkey = 1;var obF = _n("view");var odF = _v();
      if (_o(128, oTF, oSF, gg)) {
        odF.wxVkey = 1;var oeF = _n("text");_r(oeF, 'class', 129, oTF, oSF, gg);var ogF = _o(128, oTF, oSF, gg);_(oeF,ogF);_(odF, oeF);
      }else {
        odF.wxVkey = 2;var ohF = _n("text");_r(ohF, 'class', 130, e, s, gg);_(odF, ohF);
      }_(obF,odF);_(oaF, obF);
      } _(oZF,oaF);var ojF = _n("image");_r(ojF, 'src', 27, oTF, oSF, gg);_(oZF,ojF);_(oQF,oZF);_(oRF, oQF);return oRF;};_2(119, oPF, e, s, gg, oOF, "item", "index", 'key');_(oNF,oOF);_(oLF,oNF);_(oKF, oLF);
      } _(oPC,oKF);var okF = _v();
      if (_o(131, e, s, gg)) {
        okF.wxVkey = 1;var olF = _n("view");_r(olF, 'class', 132, e, s, gg);var onF = _n("view");_r(onF, 'class', 133, e, s, gg);var ooF = _m( "image", ["class", 84,"src", 50], e, s, gg);_(onF,ooF);var opF = _n("view");_r(opF, 'class', 135, e, s, gg);var oqF = _m( "image", ["class", 136,"src", 1], e, s, gg);_(opF,oqF);_(onF,opF);var orF = _n("view");_r(orF, 'class', 138, e, s, gg);var osF = _n("view");_r(osF, 'class', 139, e, s, gg);var otF = _o(140, e, s, gg);_(osF,otF);_(orF,osF);var ouF = _v();
      if (_o(141, e, s, gg)) {
        ouF.wxVkey = 1;var ovF = _n("view");_r(ovF, 'class', 142, e, s, gg);var oxF = _o(143, e, s, gg);_(ovF,oxF);_(ouF, ovF);
      } _(orF,ouF);_(onF,orF);var oyF = _m( "view", ["bindtap", 144,"class", 1,"data-url", 2], e, s, gg);var ozF = _n("view");_r(ozF, 'class', 147, e, s, gg);var o_F = _n("text");_r(o_F, 'class', 148, e, s, gg);_(ozF,o_F);var oAG = _o(149, e, s, gg);_(ozF,oAG);_(oyF,ozF);var oBG = _v();
      if (_o(141, e, s, gg)) {
        oBG.wxVkey = 1;var oCG = _n("view");_r(oCG, 'class', 150, e, s, gg);_(oBG, oCG);
      } _(oyF,oBG);_(onF,oyF);var oEG = _n("view");_r(oEG, 'class', 151, e, s, gg);var oFG = _n("image");_(oEG,oFG);var oGG = _n("text");_r(oGG, 'class', 152, e, s, gg);_(oEG,oGG);_(onF,oEG);_(olF,onF);var oHG = _n("view");_r(oHG, 'class', 153, e, s, gg);var oIG = _n("view");_r(oIG, 'class', 18, e, s, gg);var oJG = _n("view");_r(oJG, 'class', 19, e, s, gg);var oKG = _v();
      if (_o(154, e, s, gg)) {
        oKG.wxVkey = 1;var oLG = _n("image");_r(oLG, 'src', 154, e, s, gg);_(oKG, oLG);
      } _(oJG,oKG);_(oIG,oJG);var oNG = _n("view");_r(oNG, 'class', 21, e, s, gg);var oOG = _o(155, e, s, gg);_(oNG,oOG);_(oIG,oNG);_(oHG,oIG);var oPG = _m( "view", ["class", 24,"bindtap", 132], e, s, gg);var oQG = _n("text");_r(oQG, 'class', 25, e, s, gg);var oRG = _o(157, e, s, gg);_(oQG,oRG);_(oPG,oQG);var oSG = _n("image");_r(oSG, 'src', 27, e, s, gg);_(oPG,oSG);_(oHG,oPG);_(olF,oHG);var oTG = _n("view");_r(oTG, 'class', 158, e, s, gg);var oUG = _v();var oVG = function(oZG,oYG,oXG,gg){var oWG = _m( "view", ["data-url", 14,"bindtap", 130,"class", 146], oZG, oYG, gg);var obG = _n("image");_r(obG, 'src', 50, oZG, oYG, gg);_(oWG,obG);var ocG = _n("text");_r(ocG, 'class', 21, oZG, oYG, gg);var odG = _o(125, oZG, oYG, gg);_(ocG,odG);_(oWG,ocG);_(oXG, oWG);return oXG;};_2(159, oVG, e, s, gg, oUG, "item", "index", 'key');_(oTG,oUG);_(olF,oTG);var oeG = _n("view");_r(oeG, 'class', 161, e, s, gg);var ofG = _v();var ogG = function(okG,ojG,oiG,gg){var ohG = _v();
      if (_o(163, okG, ojG, gg)) {
        ohG.wxVkey = 1;var omG = _m( "view", ["data-url", 14,"class", 107,"bindtap", 130], okG, ojG, gg);var ooG = _n("view");_r(ooG, 'class', 122, okG, ojG, gg);var opG = _n("image");_r(opG, 'src', 50, okG, ojG, gg);_(ooG,opG);var oqG = _n("text");_r(oqG, 'class', 124, okG, ojG, gg);var orG = _o(125, okG, ojG, gg);_(oqG,orG);_(ooG,oqG);_(omG,ooG);var osG = _n("view");_r(osG, 'class', 126, okG, ojG, gg);var otG = _n("image");_r(otG, 'src', 27, okG, ojG, gg);_(osG,otG);_(omG,osG);_(ohG, omG);
      } _(oiG, ohG);return oiG;};_2(162, ogG, e, s, gg, ofG, "item", "index", 'key');_(oeG,ofG);_(olF,oeG);_(okF, olF);
      } _(oPC,okF);_(oOC,oPC);var ouG = _n("view");_r(ouG, 'class', 164, e, s, gg);var ovG = _n("view");_r(ovG, 'class', 165, e, s, gg);var owG = _n("view");_r(owG, 'class', 166, e, s, gg);_(ovG,owG);var oxG = _n("view");_r(oxG, 'class', 167, e, s, gg);var oyG = _v();
      if (_o(168, e, s, gg)) {
        oyG.wxVkey = 1;var ozG = _n("view");_r(ozG, 'class', 169, e, s, gg);var oAH = _n("image");_r(oAH, 'src', 170, e, s, gg);_(ozG,oAH);_(oyG, ozG);
      } _(oxG,oyG);var oBH = _v();var oCH = function(oGH,oFH,oEH,gg){var oIH = _m( "view", ["bindtap", 175,"class", 1,"data-hi", 2,"data-index", 3,"data-url", 4,"id", 5], oGH, oFH, gg);var oJH = _n("view");_r(oJH, 'class', 181, oGH, oFH, gg);var oKH = _v();
      if (_o(182, oGH, oFH, gg)) {
        oKH.wxVkey = 1;var oLH = _m( "image", ["class", 183,"src", 1], oGH, oFH, gg);_(oKH, oLH);
      }else {
        oKH.wxVkey = 2;var oNH = _m( "image", ["class", 183,"src", 2], e, s, gg);_(oKH, oNH);
      }_(oJH,oKH);_(oIH,oJH);var oPH = _m( "text", ["class", 186,"style", 1], oGH, oFH, gg);var oQH = _o(188, oGH, oFH, gg);_(oPH,oQH);_(oIH,oPH);_(oEH,oIH);return oEH;};_2(171, oCH, e, s, gg, oBH, "$footer$item", "$footer$index", '$footer$key');_(oxG,oBH);_(ovG,oxG);var oRH = _n("view");_r(oRH, 'class', 189, e, s, gg);var oSH = _v();var oTH = function(oXH,oWH,oVH,gg){var oUH = _n("view");_r(oUH, 'class', 191, oXH, oWH, gg);var oZH = _m( "image", ["data-url", 14,"src", 36,"alt", 111,"bindtap", 178,"class", 179], oXH, oWH, gg);_(oUH,oZH);_(oVH, oUH);return oVH;};_2(190, oTH, e, s, gg, oSH, "item", "index", 'key');_(oRH,oSH);_(ovG,oRH);_(ouG,ovG);_(oOC,ouG);var oaH = _v();
      if (_o(194, e, s, gg)) {
        oaH.wxVkey = 1;var obH = _n("view");_r(obH, 'class', 195, e, s, gg);var odH = _v();
      if (_o(196, e, s, gg)) {
        odH.wxVkey = 1;var oeH = _n("view");_r(oeH, 'class', 197, e, s, gg);var ogH = _m( "view", ["class", 198,"style", 1], e, s, gg);var ohH = _m( "view", ["bindtap", 200,"class", 1,"data-url", 2], e, s, gg);var oiH = _n("view");_r(oiH, 'class', 203, e, s, gg);var ojH = _n("image");_r(ojH, 'src', 204, e, s, gg);_(oiH,ojH);_(ohH,oiH);var okH = _n("view");_r(okH, 'class', 41, e, s, gg);var olH = _n("view");_r(olH, 'class', 205, e, s, gg);var omH = _n("view");_r(omH, 'class', 206, e, s, gg);var onH = _n("view");_r(onH, 'style', 207, e, s, gg);var ooH = _o(208, e, s, gg);_(onH,ooH);_(omH,onH);_(olH,omH);var opH = _n("view");_r(opH, 'class', 209, e, s, gg);var oqH = _o(210, e, s, gg);_(opH,oqH);_(olH,opH);var orH = _n("view");_r(orH, 'class', 211, e, s, gg);_(olH,orH);_(okH,olH);_(ohH,okH);_(ogH,ohH);var osH = _n("view");_r(osH, 'class', 212, e, s, gg);_(ogH,osH);var otH = _m( "view", ["bindtap", 213,"class", 1], e, s, gg);var ouH = _o(215, e, s, gg);_(otH,ouH);_(ogH,otH);_(oeH,ogH);var ovH = _n("view");_r(ovH, 'class', 109, e, s, gg);_(oeH,ovH);_(odH, oeH);
      } _(obH,odH);_(oaH, obH);
      } _(oOC,oaH);_(r,oOC);
    return r;
  };
        e_["./pages/index.wxml"]={f:m0,j:[],i:[],ti:[],ic:[]};
if(path&&e_[path]){
window.__wxml_comp_version__=0.02
return function(env,dd,global){$gwxc=0;var root={"tag":"wx-page"};root.children=[]
var main=e_[path].f
if(typeof(window.__webview_engine_version__)!='undefined'&&window.__webview_engine_version__+1e-6>=0.02+1e-6&&window.__mergeData__)
{env=window.__mergeData__(env,dd);}
try{
main(env,{},root,global);
if(typeof(window.__webview_engine_version__)=='undefined'||window.__webview_engine_version__+1e-6<0.01+1e-6){return _ev(root);}}catch(err){console.log(err)}return root;}}}@code-separator-line:.panal{z-index:999;position:relative}.panal .contentText{position:fixed;left:0;top:42px;right:0;bottom:0;width:70%;z-index:99;margin:0 auto;display:flex;justify-content:center;background:#fff;height:auto;font-size:%%?26rpx?%%}.panal .contentText .close{position:absolute;display:flex;justify-content:center;width:%%?50rpx?%%;height:%%?50rpx?%%;border-radius:50%;background:rgba(0,0,0,.5);bottom:%%?-120rpx?%%;color:rgba(255,255,255,.55);line-height:%%?50rpx?%%;text-align:center;border:1px solid rgba(255,255,255,.5)}.panal .contentText .line{position:absolute;display:flex;justify-content:center;height:%%?120rpx?%%;width:1px;background:rgba(255,255,255,.47);bottom:%%?-120rpx?%%}.panal .contentText .content{width:100%;display:block;z-index:999}.panal .contentText .father{width:100%;height:100%;font-size:%%?24rpx?%%}.panal .loading{position:fixed;width:100%;height:100%;background:rgba(0,0,0,.5);top:42px;left:0;z-index:10}.panal .weui-article__h3{font-size:%%?28rpx?%%;color:#656b6f;padding:%%?20rpx?%%;border-bottom:1px solid #f1f1f1}.panal .background{width:100%;height:100%;position:absolute;top:0;left:0;z-index:-1;overflow:hidden;border-radius:%%?20rpx?%%}.panal .background wx-image{height:100%;width:100%}.navcates{width:100%;height:%%?75rpx?%%;line-height:%%?75rpx?%%;display:flex;background-color:#fff;margin:%%?10rpx?%% 0;padding:%%?5rpx?%% %%?12rpx?%%;font-size:%%?28rpx?%%;color:#4b4b4b;border-left:4px solid #3a97fe;box-shadow:0 0 2px #fff;overflow:hidden;box-sizing:border-box}.navcates wx-text{flex:1}.navcates .left{flex:4;text-align:left;letter-spacing:%%?3rpx?%%}.navcates .left .name{display:inline-block;position:relative;top:%%?-1rpx?%%;left:%%?5rpx?%%;width:100%}.navcates .left .icon{display:inline-block;position:relative;top:%%?7rpx?%%}.navcates .left .icon wx-image{width:%%?40rpx?%%;height:%%?40rpx?%%}.navcates .right{flex:2;text-align:right;line-height:37px}.navcates .right .rtext{position:relative;top:%%?-12rpx?%%}.navcates .right wx-image{height:%%?45rpx?%%;width:%%?45rpx?%%;padding-top:%%?17.5rpx?%%}.noborder{border-left:none}.navlistd{margin-top:0}.navlistd .lisDox{height:%%?90rpx?%%;line-height:%%?90rpx?%%;background:#fff;border-bottom:1px solid #f1f1f1;color:#666;padding-left:%%?25rpx?%%;display:flex}.navlistd .lisDox .leftText{flex:4}.navlistd .lisDox .leftText .icon54{width:%%?50rpx?%%;height:%%?50rpx?%%;position:relative;top:%%?12rpx?%%}.navlistd .lisDox .leftText .title{padding-left:%%?10rpx?%%}.navlistd .lisDox .rightText{flex:1;text-align:right;padding-right:%%?20rpx?%%;padding-top:%%?17rpx?%%;position:relative}.navlistd .lisDox .rightText wx-image{width:%%?45rpx?%%;height:%%?45rpx?%%}.navlistd .top20{margin-top:20 rpx}.aui-badge{display:inline-block;width:auto;text-align:center;min-height:%%?29rpx?%%;min-width:%%?29rpx?%%;line-height:%%?29rpx?%%;padding:%%?3rpx?%% %%?6rpx?%%;font-size:%%?24rpx?%%;color:#fff;background-color:#ff2600;border-radius:%%?30rpx?%%;position:absolute;top:%%?36rpx?%%;left:41%;z-index:99}.aui-badge-none{min-width:%%?25rpx?%%;height:%%?25rpx?%%}.navcates{width:100%;height:%%?75rpx?%%;line-height:%%?75rpx?%%;display:flex;background-color:#fff;margin:%%?10rpx?%% 0;padding:%%?5rpx?%% %%?12rpx?%%;font-size:%%?28rpx?%%;color:#4b4b4b;border-left:4px solid #3a97fe;box-shadow:0 0 2px #fff;overflow:hidden;box-sizing:border-box}.navcates wx-text{flex:1}.navcates .left{flex:4;text-align:left;letter-spacing:%%?3rpx?%%}.navcates .left .name{display:inline-block;position:relative;top:%%?-1rpx?%%;left:%%?5rpx?%%;width:100%}.navcates .left .icon{display:inline-block;position:relative;top:%%?7rpx?%%}.navcates .left .icon wx-image{width:%%?40rpx?%%;height:%%?40rpx?%%}.navcates .right{flex:2;text-align:right;line-height:37px}.navcates .right .rtext{position:relative;top:%%?-12rpx?%%}.navcates .right wx-image{height:%%?45rpx?%%;width:%%?45rpx?%%;padding-top:%%?17.5rpx?%%}.noborder{border-left:none}.tag{display:flex;justify-content:center}.tag wx-text{color:#fff;background:#58c4ff;border-radius:5%;box-shadow:0 0 1px #fff;float:left;margin-right:%%?15rpx?%%;padding:%%?5rpx?%% %%?10rpx?%%;font-size:%%?24rpx?%%}.ulist .li:first-child{border-top:1px solid #eee;margin-top:5px}.ulist .li{height:%%?80rpx?%%;line-height:%%?80rpx?%%;background:#fff;border-bottom:1px solid #f1f1f1}.ulist .li wx-image{width:%%?55rpx?%%;height:%%?55rpx?%%;float:left}.ulist .li wx-text{float:left;letter-spacing:%%?5rpx?%%;font-size:%%?32rpx?%%;color:#333}.ulist .li .icon{margin-top:%%?11rpx?%%}.ulist .li .rightIcon{float:right}body{background:#eee}body .userHome{position:relative;margin:0 auto;display:block;height:220px;padding-top:60px;box-sizing:border-box;color:#fff;font-size:%%?32rpx?%%}body .userHome .header{margin:0 auto;display:flex;justify-content:center}body .userHome .header .headerImg{width:%%?120rpx?%%;height:%%?120rpx?%%;border-radius:50%;border:3px solid #fff}body .userHome .userInfo{display:flex;justify-content:center;height:%%?60rpx?%%;line-height:%%?60rpx?%%}body .userHome .userInfo .tag{width:%%?81rpx?%%;height:%%?40rpx?%%;line-height:%%?40rpx?%%;background:red;border-radius:5px;margin:%%?10rpx?%%;font-size:%%?24rpx?%%;box-sizing:border-box}body .userHome .bgc{position:absolute;top:0;left:0;width:100%;height:100%;z-index:-1}body .userHome .hdfot{height:%%?60rpx?%%;line-height:%%?60rpx?%%;background:rgba(0,0,0,.5);position:absolute;bottom:0;color:#fff;overflow:hidden}body .userHome .rightBot{position:absolute;bottom:%%?120rpx?%%;right:0;width:%%?165rpx?%%;height:%%?60rpx?%%;line-height:%%?60rpx?%%;color:#fff;padding:%%?10rpx?%%;padding-left:%%?30rpx?%%;box-sizing:border-box;background:rgba(0,0,0,.5);border-bottom-left-radius:%%?30rpx?%% 50%;border-top-left-radius:%%?30rpx?%% 50%}body .userHome .rightBot .leftBorder{height:%%?60rpx?%%;width:%%?60rpx?%%;background:rgba(0,0,0,.5);border-radius:50%;position:absolute;top:0;left:%%?-30rpx?%%;z-index:-1}body .userHome .rightBot .textContent{height:%%?40rpx?%%;line-height:%%?40rpx?%%;z-index:10;color:#fff;font-size:%%?28rpx?%%}body .nav9{background:#fff;position:relative;height:%%?220rpx?%%;overflow:hidden}body .nav9 .navList{width:20%;float:left;text-align:center;color:#575757;font-size:%%?24rpx?%%;border:1px solid #f1f1f1;box-sizing:border-box;padding:%%?15rpx?%% %%?20rpx?%%;position:relative;top:-2px;left:-2px;border-bottom:none;border-right:none}body .nav9 .navList wx-image{width:%%?42rpx?%%;height:%%?42rpx?%%;display:block;text-align:center;margin:0 auto}body .nav9 .navList .name{position:relative;top:%%?6rpx?%%;overflow:hidden;text-overflow:ellipsis;white-space:nowrap}body .navlist{margin-top:%%?20rpx?%%}body .navlist .lisDox{height:%%?90rpx?%%;line-height:%%?90rpx?%%;background:#fff;border-bottom:1px solid #f1f1f1;color:#3c3c3c;padding-left:%%?25rpx?%%;display:flex}body .navlist .lisDox .leftText{flex:4}body .navlist .lisDox .leftText wx-image{width:%%?42rpx?%%;height:%%?42rpx?%%;position:relative;top:%%?7rpx?%%}body .navlist .lisDox .leftText .title{padding-left:%%?15rpx?%%}body .navlist .lisDox .rightText{flex:1;text-align:right;padding-right:%%?20rpx?%%;padding-top:%%?17rpx?%%}body .navlist .lisDox .rightText wx-image{width:%%?45rpx?%%;height:%%?45rpx?%%}body .navlist .top20{margin-top:%%?20rpx?%%}.box{height:140px;width:98%;overflow:hidden;position:relative;border-radius:5px;box-sizing:border-box;margin:%%?10rpx?%% auto}.box .bgc{position:absolute;top:0;left:0;width:100%;height:100%;z-index:-1}.box .leftTop{position:absolute;top:0;left:0;width:40px;height:40px}.box .leftbox{float:left;width:140px;height:140px;background:#68b4b2;z-index:10;font-size:12px;color:#fff;padding-left:5%;padding-top:18px}.box .leftbox .top{font-size:%%?40rpx?%%}.box .leftbox .center{height:35px;line-height:30px}.box .leftbox .fot{height:30px;line-height:30px;font-size:%%?40rpx?%%}.box .leftbox .fot .orgin{color:orange;font-size:%%?28rpx?%%}.box .rightbox{float:right}.box .choseUse{position:absolute;bottom:%%?10rpx?%%;right:%%?10rpx?%%;line-height:%%?40rpx?%%;background:#0c9;text-align:center;color:#fff;border-radius:%%?5rpx?%%;font-size:%%?24rpx?%%;padding:%%?2rpx?%% %%?20rpx?%%}.msg{width:auto;max-width:80%;margin:0 auto;background-color:rgba(0,0,0,.7);color:#fff;display:inline-block;font-size:16px;border-radius:5px;box-shadow:0 0 8px rgba(0,0,0,.1);pointer-events:auto;-webkit-overflow-scrolling:touch;-webkit-animation-fill-mode:both;animation-fill-mode:both;-webkit-animation-duration:.2s;animation-duration:.2s;box-sizing:content-box;position:fixed;bottom:20%;z-index:999;text-align:center;padding:5px 10px}.layui-m-layercont{line-height:22px;text-align:center}.loadmsg{display:flex;justify-content:center;margin:0 auto;text-align:center}.watch{text-align:center;position:fixed;margin:auto;left:0;right:0;top:42px;bottom:0;z-index:100;border-radius:10px;width:%%?480rpx?%%;height:%%?320rpx?%%}.watch wx-image{width:100%;height:%%?300rpx?%%;border-top-left-radius:10px;border-top-right-radius:10px;opacity:.9}.watch .btns{display:flex;background:#fff;z-index:100;font-size:%%?32rpx?%%;color:#969696;position:absolute;bottom:0;width:100%;height:%%?80rpx?%%;line-height:%%?80rpx?%%;text-align:center;border-bottom-left-radius:10px;border-bottom-right-radius:10px}.watch .btns .flex{flex:1}.watch .btns .flex:last-child{color:#0c9}.watch .message{position:absolute;top:%%?20rpx?%%;font-size:%%?32rpx?%%;color:#fff;padding:%%?10rpx?%%;margin:%%?10rpx?%%;z-index:99;text-shadow:0 0 2px #f1f1f1}.watchs .loading{position:absolute;top:0;left:0;width:100%;height:100%;z-index:1;background:rgba(0,0,0,.5)}.navcates{width:100%;height:%%?75rpx?%%;line-height:%%?75rpx?%%;display:flex;background-color:#fff;margin:%%?10rpx?%% 0;padding:%%?5rpx?%% %%?12rpx?%%;font-size:%%?28rpx?%%;color:#4b4b4b;border-left:4px solid #3a97fe;box-shadow:0 0 2px #fff;overflow:hidden;box-sizing:border-box}.navcates wx-text{flex:1}.navcates .left{flex:4;text-align:left;letter-spacing:%%?3rpx?%%}.navcates .left .name{display:inline-block;position:relative;top:%%?-1rpx?%%;left:%%?5rpx?%%;width:100%}.navcates .left .icon{display:inline-block;position:relative;top:%%?7rpx?%%}.navcates .left .icon wx-image{width:%%?40rpx?%%;height:%%?40rpx?%%}.navcates .right{flex:2;text-align:right;line-height:37px}.navcates .right .rtext{position:relative;top:%%?-12rpx?%%}.navcates .right wx-image{height:%%?45rpx?%%;width:%%?45rpx?%%;padding-top:%%?17.5rpx?%%}.noborder{border-left:none}.navcates{width:100%;height:%%?75rpx?%%;line-height:%%?75rpx?%%;display:flex;background-color:#fff;margin:%%?10rpx?%% 0;padding:%%?5rpx?%% %%?12rpx?%%;font-size:%%?28rpx?%%;color:#4b4b4b;border-left:4px solid #3a97fe;box-shadow:0 0 2px #fff;overflow:hidden;box-sizing:border-box}.navcates wx-text{flex:1}.navcates .left{flex:4;text-align:left;letter-spacing:%%?3rpx?%%}.navcates .left .name{display:inline-block;position:relative;top:%%?-1rpx?%%;left:%%?5rpx?%%;width:100%}.navcates .left .icon{display:inline-block;position:relative;top:%%?7rpx?%%}.navcates .left .icon wx-image{width:%%?40rpx?%%;height:%%?40rpx?%%}.navcates .right{flex:2;text-align:right;line-height:37px}.navcates .right .rtext{position:relative;top:%%?-12rpx?%%}.navcates .right wx-image{height:%%?45rpx?%%;width:%%?45rpx?%%;padding-top:%%?17.5rpx?%%}.noborder{border-left:none}.content{display:flex;flex-wrap:wrap;align-content:space-between}.content .rowList{float:left;width:24%;box-sizing:border-box;height:95px;position:relative;overflow:hidden;margin:%%?5rpx?%%}.content .rowList .need{width:100%;height:100%}.content .rowList wx-image{width:100%;height:100%}.content .rowList .nameline{height:%%?45rpx?%%;line-height:%%?45rpx?%%;background:rgba(0,0,0,.6);color:#ddd9d0;font-size:%%?22rpx?%%;position:absolute;bottom:0;width:92%;padding:0 %%?10rpx?%%;text-align:center;left:0}.content .rowList .tagbum{position:absolute;top:%%?-5rpx?%%;right:%%?10rpx?%%;width:%%?45rpx?%%;height:%%?60rpx?%%}.content .rowList .opa5{background:rgba(0,0,0,.5);position:absolute;width:100%;height:100%;top:0;left:0;text-align:center;color:#f2f2f2;font-size:%%?28rpx?%%}.content .rowList .opa5 wx-image{top:0;left:0}.content .rowList .opa5 .opt{background:rgba(0,0,0,.5);position:absolute;width:100%;height:100%;top:0;left:0}.content .rowList .opa5 .need{position:absolute;height:95px;text-align:center;display:flex;justify-content:center;align-items:center;width:100%;text-shadow:0 0 2px #000;z-index:100}.content .rowList .weui-progress{position:absolute;bottom:%%?2rpx?%%;z-index:999;width:100%}.swiper{height:190px}.swiper .slide-image{width:100%;height:100%}.box{height:140px;width:98%;overflow:hidden;position:relative;border-radius:5px;box-sizing:border-box;margin:%%?10rpx?%% auto}.box .bgc{position:absolute;top:0;left:0;width:100%;height:100%;z-index:-1}.box .leftTop{position:absolute;top:0;left:0;width:40px;height:40px}.box .leftbox{float:left;width:140px;height:140px;background:#68b4b2;z-index:10;font-size:12px;color:#fff;padding-left:5%;padding-top:18px}.box .leftbox .top{font-size:%%?40rpx?%%}.box .leftbox .center{height:35px;line-height:30px}.box .leftbox .fot{height:30px;line-height:30px;font-size:%%?40rpx?%%}.box .leftbox .fot .orgin{color:orange;font-size:%%?28rpx?%%}.box .rightbox{float:right}.box .choseUse{position:absolute;bottom:%%?10rpx?%%;right:%%?10rpx?%%;line-height:%%?40rpx?%%;background:#0c9;text-align:center;color:#fff;border-radius:%%?5rpx?%%;font-size:%%?24rpx?%%;padding:%%?2rpx?%% %%?20rpx?%%}.swiper{height:180px}.swiper .slide-image{width:100%;height:100%}.groupitem{display:flex;width:100%;text-align:center;position:fixed;bottom:0;height:%%?100rpx?%%;line-height:%%?100rpx?%%;background:#fff;box-shadow:0 0 %%?2rpx?%% #ccc;font-size:%%?24rpx?%%;color:#666;cursor:pointer}.groupitem .my{flex:1;position:relative;overflow:hidden}.groupitem .cur{color:#3a97fe}.groupitem .fimg{width:100%}.groupitem .fimg .footIcon{width:%%?42rpx?%%;height:%%?42rpx?%%}.groupitem .fname{width:100%;position:relative;top:%%?-70rpx?%%}.groupitem .sendBtn{width:%%?90rpx?%%;height:%%?90rpx?%%;border:3px solid #fff;border-radius:50%;position:absolute;background:#fff;padding:%%?5rpx?%%;top:-25px;left:43%;box-shadow:0 -3px #f5f5f5}.groupitem .sendBtn wx-image{width:100%;height:100%;margin:0 auto}.view100{height:%%?130rpx?%%}.guess{position:fixed;bottom:60px;right:0;width:40px;padding:10px}.guess .tylist{width:%%?40rpx?%%;height:%%?40rpx?%%;background:#fff;border-radius:50%;margin-top:5px;padding:5px;overflow:hidden;box-shadow:0 0 3px #ccc}.guess .tylist .tyimg{width:100%;height:100%}@code-separator-line:__wxRoute = "pages/index";__wxRouteBegin = true;
define("pages/index.js", function(require, module, exports, window,document,frames,self,location,navigator,localStorage,history,Caches,screen,alert,confirm,prompt,fetch,XMLHttpRequest,WebSocket,webkit,WeixinJSCore,ServiceJSBridge,Reporter){
"use strict";function _interopRequireDefault(e){return e&&e.__esModule?e:{default:e}}function _asyncToGenerator(e){return function(){var t=e.apply(this,arguments);return new Promise(function(e,r){function n(a,o){try{var i=t[a](o),u=i.value}catch(e){return void r(e)}if(!i.done)return Promise.resolve(u).then(function(e){n("next",e)},function(e){n("throw",e)});e(u)}return n("next")})}}function _classCallCheck(e,t){if(!(e instanceof t))throw new TypeError("Cannot call a class as a function")}function _possibleConstructorReturn(e,t){if(!e)throw new ReferenceError("this hasn't been initialised - super() hasn't been called");return!t||"object"!=typeof t&&"function"!=typeof t?e:t}function _inherits(e,t){if("function"!=typeof t&&null!==t)throw new TypeError("Super expression must either be null or a function, not "+typeof t);e.prototype=Object.create(t&&t.prototype,{constructor:{value:e,enumerable:!1,writable:!0,configurable:!0}}),t&&(Object.setPrototypeOf?Object.setPrototypeOf(e,t):e.__proto__=t)}Object.defineProperty(exports,"__esModule",{value:!0});var _createClass=function(){function e(e,t){for(var r=0;r<t.length;r++){var n=t[r];n.enumerable=n.enumerable||!1,n.configurable=!0,"value"in n&&(n.writable=!0),Object.defineProperty(e,n.key,n)}}return function(t,r,n){return r&&e(t.prototype,r),n&&e(t,n),t}}(),_wepy=require("./../npm/wepy/lib/wepy.js"),_wepy2=_interopRequireDefault(_wepy),_footer=require("./../base/footer.js"),_footer2=_interopRequireDefault(_footer),_index=require("./index/index.js"),_index2=_interopRequireDefault(_index),_index3=require("./user/index.js"),_index4=_interopRequireDefault(_index3),_index5=require("./game/index.js"),_index6=_interopRequireDefault(_index5),_status=require("./order/status.js"),_status2=_interopRequireDefault(_status),_panal=require("./../base/panal.js"),_panal2=_interopRequireDefault(_panal),_request=require("./../api/request.js"),_util=require("./../tools/util.js"),_cache=require("./../tools/cache.js"),_cache2=_interopRequireDefault(_cache),_md=require("./../tools/md5.js"),Index=function(e){function t(){var e,r,n,a;_classCallCheck(this,t);for(var o=arguments.length,i=Array(o),u=0;u<o;u++)i[u]=arguments[u];return r=n=_possibleConstructorReturn(this,(e=t.__proto__||Object.getPrototypeOf(t)).call.apply(e,[this].concat(i))),n.config={navigationBarTitleText:"来和我玩嘛"},n.methods={showToast:function(){var e=this.$wxpage.selectComponent(".J_toast");e&&e.show()},goto:function(e){var t=(0,_util.get)(e,"url");this.onstart.show=!1,(0,_util.goto)(t),this.$apply()}},n.data={tabIndex:[!0,!1,!1,!1],option:{},onstart:{},cacheName:"advert_click"},n.events={currrntTab:function(e){this.currrntTabFun(e)},BasePnalOCallBack:function(e){var t=_cache2.default.get(this.cacheName),r="";r=t?t+"-"+(0,_md.hex_md5)(e):(0,_md.hex_md5)(e),_cache2.default.set(this.cacheName,r)}},n.$props={indexTab:{},gameTab:{},userTab:{},orderTab:{}},n.$events={},n.components={footer:_footer2.default,indexTab:_index2.default,gameTab:_index6.default,userTab:_index4.default,orderTab:_status2.default,advtice:_panal2.default},a=r,_possibleConstructorReturn(n,a)}return _inherits(t,e),_createClass(t,[{key:"currrntTabFun",value:function(){function e(e){return t.apply(this,arguments)}var t=_asyncToGenerator(regeneratorRuntime.mark(function e(t){var r;return regeneratorRuntime.wrap(function(e){for(;;)switch(e.prev=e.next){case 0:for(r in this.tabIndex)this.tabIndex[r]=!1;this.tabIndex[t]=!0,this.$broadcast("sonListen",t),this.$apply();case 4:case"end":return e.stop()}},e,this)}));return e}()},{key:"startAdvtice",value:function(){function e(){return t.apply(this,arguments)}var t=_asyncToGenerator(regeneratorRuntime.mark(function e(){var t,r,n=this;return regeneratorRuntime.wrap(function(e){for(;;)switch(e.prev=e.next){case 0:t=_cache2.default.get(this.cacheName),r=t.split("-"),(0,_request.requrst)("advert/onstart",{cache:JSON.stringify(r)},function(e){e.code>0&&(r.includes(e.info.md5)||(n.onstart=e.info,n.onstart.show=!0,n.$broadcast("BasePnalHeight",360,!0,n.onstart.url),n.$apply()))});case 3:case"end":return e.stop()}},e,this)}));return e}()},{key:"onShareAppMessage",value:function(e){return{objdata:{title:"天明小哥哥想和你一起玩游戏,你来嘛~",path:"pages/index",success:function(e){console.log(e)},fail:function(e){console.log(e)}}}}},{key:"onShow",value:function(){this.startAdvtice()}},{key:"onLoad",value:function(e){this.option=e,this.option.tab&&this.currrntTabFun(e.tab),this.$apply()}}]),t}(_wepy2.default.page);Page(require("./../npm/wepy/lib/wepy.js").default.$createPage(Index,"pages/index"));
});require("pages/index.js")@code-separator-line:["div","view","swiper","block","swiper-item","image","text"]