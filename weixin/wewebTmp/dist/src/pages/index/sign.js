/*v0.6vv_20170214_fbi*/
window.__wcc_version__='v0.6vv_20170214_fbi'
window.__wxml_transpiler_version__='v0.1'
var $gwxc
var $gaic={}
$gwx=function(path,global){
function _(a,b){b&&a.children.push(b);}
function _v(k){if(typeof(k)!='undefined')return {tag:'virtual','wxKey':k,children:[]};return {tag:'virtual',children:[]};}
function _n(tag){$gwxc++;if($gwxc>=16000){throw 'Dom limit exceeded, please check if there\'s any mistake you\'ve made.'};return {tag:tag.substr(0,3)=='wx-'?tag:'wx-'+tag,attr:{},children:[],n:[]}}
function _p(a,b){b&&a.properities.push(b);}
function _s(scope,env,key){return typeof(scope[key])!='undefined'?scope[key]:env[key]}function _wl(tname,prefix){console.warn('WXMLRT:'+prefix+':-1:-1:-1: Template `' + tname + '` is being called recursively, will be stop.')}$gwn=console.warn;
$gwl=console.log;
function $gwh()
{
function x(){}
x.prototype =
{
hn: function( obj )
{
if( typeof(obj) == 'object' )
{
var cnt=0;
var any=false;
for(var x in obj)
{
any|=x==='__value__';
cnt++;
if(cnt>2)break;
}
return cnt == 2 && any && obj.hasOwnProperty('__wxspec__') ? "h" : "n";
}
return "n";
},
nh: function( obj, special )
{
return { __value__: obj, __wxspec__: special ? special : true }
},
rv: function( obj )
{
return this.hn(obj)==='n'?obj:this.rv(obj.__value__);
}
}
return new x;
}
wh=$gwh();
function $gwrt( should_pass_type_info )
{
function ArithmeticEv( ops, e, s, g, o )
{
var rop = ops[0][1];
var _a,_b,_c,_d, _aa, _bb;
switch( rop )
{
case '?:':
_a = rev( ops[1], e, s, g, o );
_c = should_pass_type_info && ( wh.hn(_a) === 'h' );
_d = wh.rv( _a ) ? rev( ops[2], e, s, g, o ) : rev( ops[3], e, s, g, o );
_d = _c && wh.hn( _d ) === 'n' ? wh.nh( _d, 'c' ) : _d;
return _d;
break;
case '&&':
_a = rev( ops[1], e, s, g, o );
_c = should_pass_type_info && ( wh.hn(_a) === 'h' );
_d = wh.rv( _a ) ? rev( ops[2], e, s, g, o ) : wh.rv( _a );
_d = _c && wh.hn( _d ) === 'n' ? wh.nh( _d, 'c' ) : _d;
return _d;
break;
case '||':
_a = rev( ops[1], e, s, g, o );
_c = should_pass_type_info && ( wh.hn(_a) === 'h' );
_d = wh.rv( _a ) ? wh.rv(_a) : rev( ops[2], e, s, g, o );
_d = _c && wh.hn( _d ) === 'n' ? wh.nh( _d, 'c' ) : _d;
return _d;
break;
case '+':
case '*':
case '/':
case '%':
case '|':
case '^':
case '&':
case '===':
case '==':
case '!=':
case '!==':
case '>=':
case '<=':
case '>':
case '<':
case '<<':
case '>>':
_a = rev( ops[1], e, s, g, o );
_b = rev( ops[2], e, s, g, o );
_c = should_pass_type_info && (wh.hn( _a ) === 'h' || wh.hn( _b ) === 'h');
switch( rop )
{
case '+':
_d = wh.rv( _a ) + wh.rv( _b );
break;
case '*':
_d = wh.rv( _a ) * wh.rv( _b );
break;
case '/':
_d = wh.rv( _a ) / wh.rv( _b );
break;
case '%':
_d = wh.rv( _a ) % wh.rv( _b );
break;
case '|':
_d = wh.rv( _a ) | wh.rv( _b );
break;
case '^':
_d = wh.rv( _a ) ^ wh.rv( _b );
break;
case '&':
_d = wh.rv( _a ) & wh.rv( _b );
break;
case '===':
_d = wh.rv( _a ) === wh.rv( _b );
break;
case '==':
_d = wh.rv( _a ) == wh.rv( _b );
break;
case '!=':
_d = wh.rv( _a ) != wh.rv( _b );
break;
case '!==':
_d = wh.rv( _a ) !== wh.rv( _b );
break;
case '>=':
_d = wh.rv( _a ) >= wh.rv( _b );
break;
case '<=':
_d = wh.rv( _a ) <= wh.rv( _b );
break;
case '>':
_d = wh.rv( _a ) > wh.rv( _b );
break;
case '<':
_d = wh.rv( _a ) < wh.rv( _b );
break;
case '<<':
_d = wh.rv( _a ) << wh.rv( _b );
break;
case '>>':
_d = wh.rv( _a ) >> wh.rv( _b );
break;
default:
break;
}
return _c ? wh.nh( _d, "c" ) : _d;
break;
case '-':
_a = ops.length === 3 ? rev( ops[1], e, s, g, o ) : 0;
_b = ops.length === 3 ? rev( ops[2], e, s, g, o ) : rev( ops[1], e, s, g, o );
_c = should_pass_type_info && (wh.hn( _a ) === 'h' || wh.hn( _b ) === 'h');
_d = _c ? wh.rv( _a ) - wh.rv( _b ) : _a - _b;
return _c ? wh.nh( _d, "c" ) : _d;
break;
case '!':
_a = rev( ops[1], e, s, g, o );
_c = should_pass_type_info && (wh.hn( _a ) == 'h');
_d = !wh.rv(_a);
return _c ? wh.nh( _d, "c" ) : _d;
case '~':
_a = rev( ops[1], e, s, g, o );
_c = should_pass_type_info && (wh.hn( _a ) == 'h');
_d = ~wh.rv(_a);
return _c ? wh.nh( _d, "c" ) : _d;
default:
$gwn('unrecognized op' + rop );
}
}
function rev( ops, e, s, g, o )
{
var op = ops[0];
if( typeof(op)==='object' )
{
var vop=op[0];
var _a, _aa, _b, _bb, _c, _d, _s, _e, _ta, _tb, _td;
switch(vop)
{
case 2:
return ArithmeticEv(ops,e,s,g,o);
break;
case 4:
return rev( ops[1], e, s, g, o );
break;
case 5:
switch( ops.length )
{
case 2:
return should_pass_type_info ?
[rev(ops[1],e,s,g,o)] :
[wh.rv(rev(ops[1],e,s,g,o))];
break;
case 1:
return [];
break;
default:
_a = rev( ops[1],e,s,g,o );
_a.push(
should_pass_type_info ?
rev( ops[2],e,s,g,o ) :
wh.rv( rev(ops[2],e,s,g,o) )
);
return _a;
break;
}
break;
case 6:
_a = rev(ops[1],e,s,g,o);
_ta = wh.hn(_a)==='h';
_aa = _ta ? wh.rv(_a) : _a;
o.is_affected |= _ta;
if( should_pass_type_info )
{
if( _aa===null || typeof(_aa) === 'undefined' )
{
return _ta ? wh.nh(undefined, 'e') : undefined;
}
_b = rev(ops[2],e,s,g,o);
_tb = wh.hn(_b) === 'h';
_bb = _tb ? wh.rv(_b) : _b;
o.is_affected |= _tb;
if( _bb===null || typeof(_bb) === 'undefined' )
{
return (_ta || _tb) ? wh.nh(undefined, 'e') : undefined;
}
_d = _aa[_bb];
_td = wh.hn(_d)==='h';
o.is_affected |= _td;
return (_ta || _tb) ? (_td ? _d : wh.nh(_d, 'e')) : _d;
}
else
{
if( _aa===null || typeof(_aa) === 'undefined' )
{
return undefined;
}
_b = rev(ops[2],e,s,g,o);
_tb = wh.hn(_b) === 'h';
_bb = _tb ? wh.rv(_b) : _b;
o.is_affected |= _tb;
if( _bb===null || typeof(_bb) === 'undefined' )
{
return undefined;
}
_d = _aa[_bb];
_td = wh.hn(_d)==='h';
o.is_affected |= _td;
return _td ? wh.rv(_d) : _d;
}
case 7:
switch(ops[1][0])
{
case 11:
o.is_affected |= wh.hn(g)==='h';
return g;
case 3:
_s = wh.rv( s );
_e = wh.rv( e );
_b = ops[1][1];
_a = _s && _s.hasOwnProperty(_b) ?
s : _e && ( _e.hasOwnProperty(_b) ? e : undefined );
if( should_pass_type_info )
{
if( _a )
{
_ta = wh.hn(_a) === 'h';
_aa = _ta ? wh.rv( _a ) : _a;
_d = _aa[_b];
_td = wh.hn(_d) === 'h';
o.is_affected |= _ta || _td;
_d = _ta && !_td ? wh.nh(_d,'e') : _d;
return _d;
}
}
else
{
if( _a )
{
_ta = wh.hn(_a) === 'h';
_aa = _ta ? wh.rv( _a ) : _a;
_d = _aa[_b];
_td = wh.hn(_d) === 'h';
o.is_affected |= _ta || _td;
return wh.rv(_d);
}
}
return undefined;
}
break;
case 8:
_a = {};
_a[ops[1]] = rev(ops[2],e,s,g,o);
return _a;
break;
case 9:
_a = rev(ops[1],e,s,g,o);
_b = rev(ops[2],e,s,g,o);
function merge( _a, _b, _ow )
{
_ta = wh.hn(_a)==='h';
_tb = wh.hn(_b)==='h';
_aa = wh.rv(_a);
_bb = wh.rv(_b);
if( should_pass_type_info )
{
if( _tb )
{
for(var k in _bb)
{
if ( _ow || !_aa.hasOwnProperty(k) )
_aa[k]=wh.nh(_bb[k],'e');
}
}
else
{
for(var k in _bb)
{
if ( _ow || !_aa.hasOwnProperty(k) )
_aa[k]=_bb[k];
}
}
}
else
{
for(var k in _bb)
{
if ( _ow || _aa.hasOwnProperty(k) )
_aa[k]=wh.rv(_bb[k]);
}
}
return _a;
}
var _c = _a
var _ow = true
if ( typeof(ops[1][0]) === "object" && ops[1][0][0] === 10 ) {
_a = _b
_b = _c
_ow = false
}
if ( typeof(ops[1][0]) === "object" && ops[1][0][0] === 10 ) {
var _r = {}
return merge( merge( _r, _a, _ow ), _b, _ow );
}
else
return merge( _a, _b, _ow );
break;
case 10:
return should_pass_type_info ? rev(ops[1],e,s,g,o) : wh.rv(rev(ops[1],e,s,g,o));
}
}
else
{
if( op === 3 || op === 1 ) return ops[1];
else if( op === 11 )
{
var _a='';
for( var i = 1 ; i < ops.length ; i++ )
{
var xp = wh.rv(rev(ops[i],e,s,g,o));
_a += typeof(xp) === 'undefined' ? '' : xp;
}
return _a;
}
}
}
return rev;
}
gra=$gwrt(true);
grb=$gwrt(false);
function TestTest( expr, ops, e,s,g, expect_a, expect_b, expect_affected )
{
{
var o = {is_affected:false};
var a = gra( ops, e,s,g, o );
if( JSON.stringify(a) != JSON.stringify( expect_a )
|| o.is_affected != expect_affected )
{
console.warn( "A. " + expr + " get result " + JSON.stringify(a) + ", " + o.is_affected + ", but " + JSON.stringify( expect_a ) + ", " + expect_affected + " is expected" );
}
}
{
var o = {is_affected:false};
var a = grb( ops, e,s,g, o );
if( JSON.stringify(a) != JSON.stringify( expect_b )
|| o.is_affected != expect_affected )
{
console.warn( "B. " + expr + " get result " + JSON.stringify(a) + ", " + o.is_affected + ", but " + JSON.stringify( expect_b ) + ", " + expect_affected + " is expected" );
}
}
}

function wfor( to_iter, func, env, _s, global, father, itemname, indexname, keyname )
{
var _n = wh.hn( to_iter ) === 'n';
var scope = wh.rv( _s );
var has_old_item = scope.hasOwnProperty(itemname);
var has_old_index = scope.hasOwnProperty(indexname);
var old_item = scope[itemname];
var old_index = scope[indexname];
var full = Object.prototype.toString.call(wh.rv(to_iter));
var type = full[8];
if( type === 'N' && full[10] === 'l' ) type = 'X';
var _y;
if( _n )
{
if( type === 'A' )
{
for( var i = 0 ; i < to_iter.length ; i++ )
{
scope[itemname] = to_iter[i];
scope[indexname] = wh.nh(i, 'h');
_y = keyname ? (keyname==="*this" ? _v(wh.rv(to_iter[i])) : _v(wh.rv(wh.rv(to_iter[i])[keyname]))) : _v();
_(father,_y);
func( env, scope, _y, global );
}
}
else if( type === 'O' )
{
for( var k in to_iter )
{
scope[itemname] = to_iter[k];
scope[indexname] = wh.nh(k, 'h');
_y = keyname ? (keyname==="*this" ? _v(wh.rv(to_iter[k])) : _v(wh.rv(wh.rv(to_iter[k])[keyname]))) : _v();
_(father,_y);
func( env,scope,_y,global );
}
}
else if( type === 'S' )
{
for( var i = 0 ; i < to_iter.length ; i++ )
{
scope[itemname] = to_iter[i];
scope[indexname] = wh.nh(i, 'h');
_y = _v( to_iter[i] + i );
_(father,_y);
func( env, scope, _y, global );
}
}
else if( type === 'N' )
{
for( var i = 0 ; i < to_iter ; i++ )
{
scope[itemname] = i;
scope[indexname] = wh.nh(i, 'h');
_y = _v( i );
_(father,_y);
func(env,scope,_y,global);
}
}
else
{
}
}
else
{
var r_to_iter = wh.rv(to_iter);
var r_iter_item, iter_item;
if( type === 'A' )
{
for( var i = 0 ; i < r_to_iter.length ; i++ )
{
iter_item = r_to_iter[i];
iter_item = wh.hn(iter_item)==='n' ? wh.nh(iter_item,'h') : iter_item;
r_iter_item = wh.rv( iter_item );
scope[itemname] = iter_item
scope[indexname] = wh.nh(i, 'h');
_y = keyname ? (keyname==="*this" ? _v(r_iter_item) : _v(wh.rv(r_iter_item[keyname]))) : _v();
_(father,_y);
func( env, scope, _y, global );
}
}
else if( type === 'O' )
{
for( var k in r_to_iter )
{
iter_item = r_to_iter[k];
iter_item = wh.hn(iter_item)==='n'? wh.nh(iter_item,'h') : iter_item;
r_iter_item = wh.rv( iter_item );
scope[itemname] = iter_item;
scope[indexname] = wh.nh(k, 'h');
_y = keyname ? (keyname==="*this" ? _v(r_iter_item) : _v(wh.rv(r_iter_item[keyname]))) : _v();
_(father,_y);
func( env, scope, _y, global );
}
}
else if( type === 'S' )
{
for( var i = 0 ; i < r_to_iter.length ; i++ )
{
scope[itemname] = wh.nh(r_to_iter[i],'h');
scope[indexname] = wh.nh(i, 'h');
_y = _v( to_iter[i] + i );
_(father,_y);
func( env, scope, _y, global );
}
}
else if( type === 'N' )
{
for( var i = 0 ; i < r_to_iter ; i++ )
{
scope[itemname] = wh.nh(i,'h');
scope[indexname]= wh.nh(i,'h');
_y = _v( i );
_(father,_y);
func(env,scope,_y,global);
}
}
else
{
}
}
if(has_old_item)
{
scope[itemname]=old_item;
}
else
{
delete scope[itemname];
}
if(has_old_index)
{
scope[indexname]=old_index;
}
else{delete scope[indexname];}}


function _r( node, attrname, opindex, env, scope, global )
{
var o = {};
var a = grb( z[opindex], env, scope, global, o );
node.attr[attrname] = a;
if( o.is_affected ) node.n.push( attrname );
}
function _o( opindex, env, scope, global )
{
var nothing = {};
return grb( z[opindex], env, scope, global, nothing );
}
function _1( opindex, env, scope, global )
{
var nothing = {};
return gra( z[opindex], env, scope, global, nothing );
}
function _2( opindex, func, env, scope, global, father, itemname, indexname, keyname )
{
var to_iter = _1( opindex, env, scope, global, father, itemname, indexname, keyname );
wfor( to_iter, func, env, scope, global, father, itemname, indexname, keyname );
}
function _gv( )
{
if( typeof(window.__webview_engine_version__) == 'undefined' ) return 0.0;
return window.__webview_engine_version__;
}
function _m(tag,attrs,env,scope,global)
{
var tmp=_n(tag);
var base=0;
for(var i = 0 ; i < attrs.length ; i+=2 )
{
if(attrs[i+1]<0)
{tmp.attr[attrs[i]]=true;}else{_r(tmp,attrs[i],base+attrs[i+1],env,scope,global);
if(base===0)base=attrs[i+1];}}return tmp;};function _ai(i,p,e,me,r,c){var x=_grp(p,e,me);if(x)i.push(x);else{i.push('');console.warn('WXMLRT:'+me+':import:'+r+':'+c+': Path `'+p+'` not found from `'+me+'`.')}}function _grp(p,e,me){if(p[0]!='/'){var mepart=me.split('/');mepart.pop();var ppart=p.split('/');for(var i=0;i<ppart.length;i++){if(ppart[i]=='..')mepart.pop();else if(ppart[i]=='.' || !ppart[i])continue;else mepart.push(ppart[i]);}p=mepart.join('/');}if(me[0]=='.'&&p[0]=='/')p='.'+p;if(e[p])return p;if(e[p+'.wxml'])return p+'.wxml';}function _gd(p,c,e,d){if(!c)return;if(d[p][c])return d[p][c];for(var x=e[p].i.length-1;x>=0;x--){if(e[p].i[x]&&d[e[p].i[x]][c])return d[e[p].i[x]][c]};for(var x=e[p].ti.length-1;x>=0;x--){var q=_grp(e[p].ti[x],e,p);if(q&&d[q][c])return d[q][c]}
var ii=_gapi(e,p);for(var x=0;x<ii.length;x++){if(ii[x]&&d[ii[x]][c])return d[ii[x]][c]}for(var k=e[p].j.length-1;k>=0;k--)if(e[p].j[k]){for(var q=e[e[p].j[k]].ti.length-1;q>=0;q--){var pp=_grp(e[e[p].j[k]].ti[q],e,p);if(pp&&d[pp][c]){return d[pp][c]}}}}function _gapi(e,p){if(!p)return [];if($gaic[p]){return $gaic[p]};var ret=[],q=[],h=0,t=0,put={},visited={};q.push(p);visited[p]=true;t++;while(h<t){var a=q[h++];for(var i=0;i<e[a].ic.length;i++){var nd=e[a].ic[i];var np=_grp(nd,e,a);if(np&&!visited[np]){visited[np]=true;q.push(np);t++;}}for(var i=0;a!=p&&i<e[a].ti.length;i++){var ni=e[a].ti[i];var nm=_grp(ni,e,a);if(nm&&!put[nm]){put[nm]=true;ret.push(nm);}}}$gaic[p]=ret;return ret;}var $ixc={};function _ic(p,ent,me,e,s,r,gg){var x=_grp(p,ent,me);ent[me].j.push(x);if(x){if($ixc[x]){console.warn('WXMLRT:-1:include:-1:-1: `'+p+'` is being included in a loop, will be stop.');return;}$ixc[x]=true;try{ent[x].f(e,s,r,gg)}catch(e){}$ixc[x]=false;}else{console.warn('WXMLRT:'+me+':include:-1:-1: Included path `'+p+'` not found from `'+me+'`.')}}function _w(tn,f,line,c){console.warn('WXMLRT:'+f+':template:'+line+':'+c+': Template `'+tn+'` not found.');}function _ev(dom){var changed=false;delete dom.properities;delete dom.n;if(dom.children){do{changed=false;var newch = [];for(var i=0;i<dom.children.length;i++){var ch=dom.children[i];if( ch.tag=='virtual'){changed=true;for(var j=0;ch.children&&j<ch.children.length;j++){newch.push(ch.children[j]);}}else { newch.push(ch); } } dom.children = newch; }while(changed);for(var i=0;i<dom.children.length;i++){_ev(dom.children[i]);}} return dom; }var e_={}
if(global&&typeof(global.entrys)=='object')e_=global.entrys
var d_={}
if(global&&typeof(global.defines)=='object')d_=global.defines
var p_={}
var z = [];
  (function(z){
    var a = 11;
    function Z(ops){z.push(ops)};
    Z([3, 'sign']);Z([3, 'header']);Z([3, 'flex']);Z([3, 'flex1 text-left']);Z([a, [[6],[[7],[3, "signData"]],[3, "allMoney"]],[3, '金币']]);Z([3, 'flex1 text-right']);Z([3, 'shop']);Z([3, 'btnc']);Z([3, '积分商城']);Z([3, 'gbc']);Z([3, 'http://img.he29.com/play/a7aec88954259677db385da828d11fb7edc51a98.jpeg']);Z([3, 'rand']);Z([3, 'bgf']);Z([3, 'startSign']);Z([3, 'border']);Z([[6],[[7],[3, "signData"]],[3, "today"]]);Z([3, '已签到']);Z([3, '未签到']);Z([3, 'line']);Z([3, 'onlined']);Z([a, [3, '\r\n                           连续'],[[6],[[6],[[7],[3, "signData"]],[3, "today"]],[3, "sign_count"]],[3, '天\r\n                       ']]);Z([a, [3, '\r\n                           连续'],[[6],[[7],[3, "signData"]],[3, "sign_count"]],[3, '天\r\n                       ']]);Z([3, 'tipsText']);Z([a, [3, '今日已签到,获得 '],[[6],[[7],[3, "signData"]],[3, "money"]],[3, ' 积分']]);Z([a, [3, '今日还未签到,签到将获得 '],[[6],[[7],[3, "signData"]],[3, "money"]],[3, ' 积分']]);Z([3, 'clear']);Z([a, [3, 'navcates '],[[2,'?:'],[[2, "=="], [[7],[3, "$navcate$leftColor"]], [1, 0]],[1, "noborder"],[1, ""]]]);Z([3, 'left']);Z([3, 'icon']);Z([[7],[3, "$navcate$icon"]]);Z([3, 'name']);Z([a, [[7],[3, "$navcate$title"]]]);Z([3, '$navcate$NavcateRight']);Z([3, 'right']);Z([3, 'rtext']);Z([a, [[7],[3, "$navcate$right"]]]);Z([3, 'http://img.he29.com/play/f4fe7786635ee901a46590b7f1926bc08d628bdf.png']);Z([3, 'dateTree']);Z([3, 'we_header']);Z([[7],[3, "moonk"]]);Z([3, 'key']);Z([3, 'width']);Z([3, 'value']);Z([a, [[7],[3, "item"]]]);Z([3, 'we_content']);Z([[7],[3, "m_allDay"]]);Z([a, [3, 'value '],[[2,'?:'],[[6],[[7],[3, "item"]],[3, "today"]],[1, "today"],[1, ""]],[3, ' '],[[2,'?:'],[[6],[[7],[3, "item"]],[3, "curr"]],[1, "curr"],[1, ""]]]);Z([a, [[6],[[7],[3, "item"]],[3, "day"]]]);Z([a, [3, 'navcates '],[[2,'?:'],[[2, "=="], [[7],[3, "$navcate2$leftColor"]], [1, 0]],[1, "noborder"],[1, ""]]]);Z([[7],[3, "$navcate2$icon"]]);Z([a, [[7],[3, "$navcate2$title"]]]);Z([3, '$navcate2$NavcateRight']);Z([a, [[7],[3, "$navcate2$right"]]]);Z([3, 'signDesc']);Z([3, 'title']);Z([3, 'slist']);Z([3, '1,获取上一个月时，']);Z([3, '2,获取上一个月时，到1月份需注意']);Z([3, '3,获取上一个月时，到1月份需注意；获取下一个月时']);Z([3, '4,获取上一个月时，到1月份需注意；获取下一个月时，到12月份时要注意。']);Z([3, '5,获取上一个月时，到1月份需注意。']);Z([3, '6,获取上一个月时。']);Z([3, '7,获取上一个月时到12月份时要注意。']);Z([[7],[3, "$msg$open"]]);Z([3, 'loadmsg animated fadeIn']);Z([3, 'msg']);Z([3, 'layui-m-layercont']);Z([a, [[7],[3, "$msg$msg"]]]);
  })(z);d_["./pages/index/sign.wxml"] = {};
  var m0=function(e,s,r,gg){
    var oJU = _n("view");_r(oJU, 'class', 0, e, s, gg);var oKU = _n("view");_r(oKU, 'class', 1, e, s, gg);var oLU = _n("view");_r(oLU, 'class', 2, e, s, gg);var oMU = _n("view");_r(oMU, 'class', 3, e, s, gg);var oNU = _o(4, e, s, gg);_(oMU,oNU);_(oLU,oMU);var oOU = _n("view");_r(oOU, 'class', 5, e, s, gg);var oPU = _m( "text", ["bindtap", 6,"class", 1], e, s, gg);var oQU = _o(8, e, s, gg);_(oPU,oQU);_(oOU,oPU);_(oLU,oOU);_(oKU,oLU);var oRU = _m( "image", ["class", 9,"src", 1], e, s, gg);_(oKU,oRU);var oSU = _n("view");_r(oSU, 'class', 11, e, s, gg);var oTU = _n("view");_r(oTU, 'class', 12, e, s, gg);var oUU = _m( "view", ["bindtap", 13,"class", 1], e, s, gg);var oVU = _n("view");_r(oVU, 'class', 0, e, s, gg);var oWU = _v();
      if (_o(15, e, s, gg)) {
        oWU.wxVkey = 1;var oXU = _n("text");var oZU = _o(16, e, s, gg);_(oXU,oZU);_(oWU, oXU);
      }else {
        oWU.wxVkey = 2;var oaU = _n("text");var ocU = _o(17, e, s, gg);_(oaU,ocU);_(oWU, oaU);
      }_(oVU,oWU);_(oUU,oVU);var odU = _n("text");_r(odU, 'class', 18, e, s, gg);_(oUU,odU);var oeU = _v();
      if (_o(15, e, s, gg)) {
        oeU.wxVkey = 1;var ofU = _n("text");_r(ofU, 'class', 19, e, s, gg);var ohU = _o(20, e, s, gg);_(ofU,ohU);_(oeU, ofU);
      }else {
        oeU.wxVkey = 2;var oiU = _n("text");_r(oiU, 'class', 19, e, s, gg);var okU = _o(21, e, s, gg);_(oiU,okU);_(oeU, oiU);
      }_(oUU,oeU);_(oTU,oUU);_(oSU,oTU);_(oKU,oSU);var olU = _n("view");_r(olU, 'class', 22, e, s, gg);var omU = _v();
      if (_o(15, e, s, gg)) {
        omU.wxVkey = 1;var onU = _n("text");var opU = _o(23, e, s, gg);_(onU,opU);_(omU, onU);
      }else {
        omU.wxVkey = 2;var oqU = _n("text");var osU = _o(24, e, s, gg);_(oqU,osU);_(omU, oqU);
      }_(olU,omU);_(oKU,olU);_(oJU,oKU);var otU = _n("view");_r(otU, 'class', 25, e, s, gg);_(oJU,otU);var ouU = _n("view");_r(ouU, 'class', 26, e, s, gg);var ovU = _n("view");_r(ovU, 'class', 27, e, s, gg);var owU = _n("view");_r(owU, 'class', 28, e, s, gg);var oxU = _v();
      if (_o(29, e, s, gg)) {
        oxU.wxVkey = 1;var oyU = _n("image");_r(oyU, 'src', 29, e, s, gg);_(oxU, oyU);
      } _(owU,oxU);_(ovU,owU);var o_U = _n("view");_r(o_U, 'class', 30, e, s, gg);var oAV = _o(31, e, s, gg);_(o_U,oAV);_(ovU,o_U);_(ouU,ovU);var oBV = _m( "view", ["bindtap", 32,"class", 1], e, s, gg);var oCV = _n("text");_r(oCV, 'class', 34, e, s, gg);var oDV = _o(35, e, s, gg);_(oCV,oDV);_(oBV,oCV);var oEV = _n("image");_r(oEV, 'src', 36, e, s, gg);_(oBV,oEV);_(ouU,oBV);_(oJU,ouU);var oFV = _n("view");_r(oFV, 'class', 37, e, s, gg);var oGV = _n("view");_r(oGV, 'class', 38, e, s, gg);var oHV = _v();var oIV = function(oMV,oLV,oKV,gg){var oJV = _n("view");_r(oJV, 'class', 41, oMV, oLV, gg);var oOV = _n("text");_r(oOV, 'class', 42, oMV, oLV, gg);var oPV = _o(43, oMV, oLV, gg);_(oOV,oPV);_(oJV,oOV);_(oKV, oJV);return oKV;};_2(39, oIV, e, s, gg, oHV, "item", "index", 'key');_(oGV,oHV);_(oFV,oGV);var oQV = _n("view");_r(oQV, 'class', 44, e, s, gg);var oRV = _v();var oSV = function(oWV,oVV,oUV,gg){var oTV = _n("view");_r(oTV, 'class', 41, oWV, oVV, gg);var oYV = _n("text");_r(oYV, 'class', 46, oWV, oVV, gg);var oZV = _o(47, oWV, oVV, gg);_(oYV,oZV);_(oTV,oYV);_(oUV, oTV);return oUV;};_2(45, oSV, e, s, gg, oRV, "item", "index", 'key');_(oQV,oRV);_(oFV,oQV);_(oJU,oFV);var oaV = _n("view");_r(oaV, 'class', 48, e, s, gg);var obV = _n("view");_r(obV, 'class', 27, e, s, gg);var ocV = _n("view");_r(ocV, 'class', 28, e, s, gg);var odV = _v();
      if (_o(49, e, s, gg)) {
        odV.wxVkey = 1;var oeV = _n("image");_r(oeV, 'src', 49, e, s, gg);_(odV, oeV);
      } _(ocV,odV);_(obV,ocV);var ogV = _n("view");_r(ogV, 'class', 30, e, s, gg);var ohV = _o(50, e, s, gg);_(ogV,ohV);_(obV,ogV);_(oaV,obV);var oiV = _m( "view", ["class", 33,"bindtap", 18], e, s, gg);var ojV = _n("text");_r(ojV, 'class', 34, e, s, gg);var okV = _o(52, e, s, gg);_(ojV,okV);_(oiV,ojV);var olV = _n("image");_r(olV, 'src', 36, e, s, gg);_(oiV,olV);_(oaV,oiV);_(oJU,oaV);var omV = _n("view");_r(omV, 'class', 53, e, s, gg);var onV = _n("view");_r(onV, 'class', 54, e, s, gg);_(omV,onV);var ooV = _n("view");_r(ooV, 'class', 55, e, s, gg);var opV = _o(56, e, s, gg);_(ooV,opV);_(omV,ooV);var oqV = _n("view");_r(oqV, 'class', 55, e, s, gg);var orV = _o(57, e, s, gg);_(oqV,orV);_(omV,oqV);var osV = _n("view");_r(osV, 'class', 55, e, s, gg);var otV = _o(58, e, s, gg);_(osV,otV);_(omV,osV);var ouV = _n("view");_r(ouV, 'class', 55, e, s, gg);var ovV = _o(59, e, s, gg);_(ouV,ovV);_(omV,ouV);var owV = _n("view");_r(owV, 'class', 55, e, s, gg);var oxV = _o(60, e, s, gg);_(owV,oxV);_(omV,owV);var oyV = _n("view");_r(oyV, 'class', 55, e, s, gg);var ozV = _o(61, e, s, gg);_(oyV,ozV);_(omV,oyV);var o_V = _n("view");_r(o_V, 'class', 55, e, s, gg);var oAW = _o(62, e, s, gg);_(o_V,oAW);_(omV,o_V);_(oJU,omV);var oBW = _v();
      if (_o(63, e, s, gg)) {
        oBW.wxVkey = 1;var oCW = _n("view");_r(oCW, 'class', 64, e, s, gg);var oEW = _n("view");_r(oEW, 'class', 65, e, s, gg);var oFW = _n("text");_r(oFW, 'class', 66, e, s, gg);var oGW = _o(67, e, s, gg);_(oFW,oGW);_(oEW,oFW);_(oCW,oEW);_(oBW, oCW);
      } _(oJU,oBW);_(r,oJU);
    return r;
  };
        e_["./pages/index/sign.wxml"]={f:m0,j:[],i:[],ti:[],ic:[]};
if(path&&e_[path]){
window.__wxml_comp_version__=0.02
return function(env,dd,global){$gwxc=0;var root={"tag":"wx-page"};root.children=[]
var main=e_[path].f
if(typeof(window.__webview_engine_version__)!='undefined'&&window.__webview_engine_version__+1e-6>=0.02+1e-6&&window.__mergeData__)
{env=window.__mergeData__(env,dd);}
try{
main(env,{},root,global);
if(typeof(window.__webview_engine_version__)=='undefined'||window.__webview_engine_version__+1e-6<0.01+1e-6){return _ev(root);}}catch(err){console.log(err)}return root;}}}@code-separator-line:.msg{width:auto;max-width:80%;margin:0 auto;background-color:rgba(0,0,0,.7);color:#fff;display:inline-block;font-size:16px;border-radius:5px;box-shadow:0 0 8px rgba(0,0,0,.1);pointer-events:auto;-webkit-overflow-scrolling:touch;-webkit-animation-fill-mode:both;animation-fill-mode:both;-webkit-animation-duration:.2s;animation-duration:.2s;box-sizing:content-box;position:fixed;bottom:20%;z-index:999;text-align:center;padding:5px 10px}.layui-m-layercont{line-height:22px;text-align:center}.loadmsg{display:flex;justify-content:center;margin:0 auto;text-align:center}.navcates{width:100%;height:%%?75rpx?%%;line-height:%%?75rpx?%%;display:flex;background-color:#fff;margin:%%?10rpx?%% 0;padding:%%?5rpx?%% %%?12rpx?%%;font-size:%%?28rpx?%%;color:#4b4b4b;border-left:4px solid #3a97fe;box-shadow:0 0 2px #fff;overflow:hidden;box-sizing:border-box}.navcates wx-text{flex:1}.navcates .left{flex:4;text-align:left;letter-spacing:%%?3rpx?%%}.navcates .left .name{display:inline-block;position:relative;top:%%?-1rpx?%%;left:%%?5rpx?%%;width:100%}.navcates .left .icon{display:inline-block;position:relative;top:%%?7rpx?%%}.navcates .left .icon wx-image{width:%%?40rpx?%%;height:%%?40rpx?%%}.navcates .right{flex:2;text-align:right;line-height:37px}.navcates .right .rtext{position:relative;top:%%?-12rpx?%%}.navcates .right wx-image{height:%%?45rpx?%%;width:%%?45rpx?%%;padding-top:%%?17.5rpx?%%}.noborder{border-left:none}.navcates{width:100%;height:%%?75rpx?%%;line-height:%%?75rpx?%%;display:flex;background-color:#fff;margin:%%?10rpx?%% 0;padding:%%?5rpx?%% %%?12rpx?%%;font-size:%%?28rpx?%%;color:#4b4b4b;border-left:4px solid #3a97fe;box-shadow:0 0 2px #fff;overflow:hidden;box-sizing:border-box}.navcates wx-text{flex:1}.navcates .left{flex:4;text-align:left;letter-spacing:%%?3rpx?%%}.navcates .left .name{display:inline-block;position:relative;top:%%?-1rpx?%%;left:%%?5rpx?%%;width:100%}.navcates .left .icon{display:inline-block;position:relative;top:%%?7rpx?%%}.navcates .left .icon wx-image{width:%%?40rpx?%%;height:%%?40rpx?%%}.navcates .right{flex:2;text-align:right;line-height:37px}.navcates .right .rtext{position:relative;top:%%?-12rpx?%%}.navcates .right wx-image{height:%%?45rpx?%%;width:%%?45rpx?%%;padding-top:%%?17.5rpx?%%}.noborder{border-left:none}.sign .header{position:relative;width:100%;height:220px;overflow:hidden}.sign .header .gbc{width:100%;height:220px}.sign .header .flex{position:absolute;top:0;display:flex;width:100%;color:#fff;padding:%%?15rpx?%% %%?25rpx?%%;margin:%%?10rpx?%% auto;font-size:%%?24rpx?%%;box-sizing:border-box}.sign .header .flex .btnc{width:%%?60rpx?%%;height:%%?25rpx?%%;border:1px solid #fff;border-radius:5px;line-height:%%?25rpx?%%;text-align:center;padding:%%?5rpx?%% %%?10rpx?%%}.sign .header .rand{position:absolute;width:100%;display:flex;justify-content:center;box-sizing:border-box;top:8%}.sign .header .rand .bgf{margin:0 auto;text-align:center;display:block;width:%%?260rpx?%%;height:%%?260rpx?%%;border-radius:50%;background:#fff;position:relative;border:10px solid #3696ff;box-sizing:border-box}.sign .header .rand .bgf .border{margin:10px;padding:10px;border-radius:50%;border:1px solid #7fbcff;width:%%?190rpx?%%;height:%%?190rpx?%%;box-sizing:border-box;position:relative}.sign .header .rand .bgf .border .sign{color:#007aff;font-size:%%?30rpx?%%;font-weight:600;margin-top:%%?27rpx?%%}.sign .header .rand .bgf .border .line{background:#656b6f;height:2px;width:100%;margin-top:%%?20rpx?%%}.sign .header .rand .bgf .border .onlined{color:#007aff;height:%%?30rpx?%%;font-size:%%?36rpx?%%;margin-bottom:%%?10rpx?%%;position:absolute;bottom:%%?120rpx?%%;width:100%;left:0}.sign .header .tipsText{height:%%?50rpx?%%;margin:0 auto;display:flex;justify-content:center;position:absolute;bottom:%%?40rpx?%%;color:#fff;font-size:%%?32rpx?%%;width:100%}.sign .dateTree{background:#fff;display:block;position:relative;height:%%?420rpx?%%}.sign .dateTree .we_header{display:block}.sign .dateTree .we_header .width{color:#666;font-size:%%?32rpx?%%}.sign .dateTree .we_content .width{color:#999ea2;font-size:%%?28rpx?%%}.sign .dateTree .we_content .value{padding:%%?10rpx?%%;border-radius:50%}.sign .dateTree .we_content .today{background:#fe6906;color:#fff}.sign .dateTree .we_content .curr{background:#007aff;color:#fff}.sign .signDesc{font-size:%%?24rpx?%%;color:#999ea2;padding:%%?20rpx?%% %%?40rpx?%%;background:#fff;margin-top:%%?10rpx?%%}.sign .signDesc .slist{display:block;line-height:%%?40rpx?%%}.sign .signDesc .title{font-size:%%?36rpx?%%}.width{width:13.8%;height:%%?70rpx?%%;text-align:center;float:left;line-height:%%?70rpx?%%}@code-separator-line:__wxRoute = "pages/index/sign";__wxRouteBegin = true;
define("pages/index/sign.js", function(require, module, exports, window,document,frames,self,location,navigator,localStorage,history,Caches,screen,alert,confirm,prompt,fetch,XMLHttpRequest,WebSocket,webkit,WeixinJSCore,ServiceJSBridge,Reporter){
"use strict";function _interopRequireDefault(e){return e&&e.__esModule?e:{default:e}}function _asyncToGenerator(e){return function(){var t=e.apply(this,arguments);return new Promise(function(e,r){function n(a,i){try{var o=t[a](i),s=o.value}catch(e){return void r(e)}if(!o.done)return Promise.resolve(s).then(function(e){n("next",e)},function(e){n("throw",e)});e(s)}return n("next")})}}function _classCallCheck(e,t){if(!(e instanceof t))throw new TypeError("Cannot call a class as a function")}function _possibleConstructorReturn(e,t){if(!e)throw new ReferenceError("this hasn't been initialised - super() hasn't been called");return!t||"object"!=typeof t&&"function"!=typeof t?e:t}function _inherits(e,t){if("function"!=typeof t&&null!==t)throw new TypeError("Super expression must either be null or a function, not "+typeof t);e.prototype=Object.create(t&&t.prototype,{constructor:{value:e,enumerable:!1,writable:!0,configurable:!0}}),t&&(Object.setPrototypeOf?Object.setPrototypeOf(e,t):e.__proto__=t)}Object.defineProperty(exports,"__esModule",{value:!0});var _createClass=function(){function e(e,t){for(var r=0;r<t.length;r++){var n=t[r];n.enumerable=n.enumerable||!1,n.configurable=!0,"value"in n&&(n.writable=!0),Object.defineProperty(e,n.key,n)}}return function(t,r,n){return r&&e(t.prototype,r),n&&e(t,n),t}}(),_wepy=require("./../../npm/wepy/lib/wepy.js"),_wepy2=_interopRequireDefault(_wepy),_navcate=require("./../../base/navcate.js"),_navcate2=_interopRequireDefault(_navcate),_request=require("./../../api/request.js"),_msg=require("./../../base/tips/msg.js"),_msg2=_interopRequireDefault(_msg),IndexSign=function(e){function t(){var e,r,n,a;_classCallCheck(this,t);for(var i=arguments.length,o=Array(i),s=0;s<i;s++)o[s]=arguments[s];return r=n=_possibleConstructorReturn(this,(e=t.__proto__||Object.getPrototypeOf(t)).call.apply(e,[this].concat(o))),n.config={navigationBarTitleText:"签到",backgroundTextStyle:"light",navigationBarBackgroundColor:"#007aff"},n.$props={navcate:{title:"签到日历"},navcate2:{title:"签到说明"}},n.$events={},n.components={navcate:_navcate2.default,navcate2:_navcate2.default,msg:_msg2.default},n.data={hasEmptyGrid:!1,showPicker:!1,moonk:["日","一","二","三","四","五","六"],m_days:[],m_currYear:"",m_currMonth:"",m_currDate:"",m_allDay:[],signData:{},nakedataFinish:!1},n.computed={},n.methods={shop:function(){this.$broadcast("layerMsg","暂未开放")},startSign:function(){var e=this;(0,_request.requrst)("Sign/startSign",{},function(t){e.$broadcast("layerMsg",t.msg),t.code>0&&(e.SignHistory(),e.$apply())})}},n.events={},a=r,_possibleConstructorReturn(n,a)}return _inherits(t,e),_createClass(t,[{key:"SignHistory",value:function(){function e(){return t.apply(this,arguments)}var t=_asyncToGenerator(regeneratorRuntime.mark(function e(){var t=this;return regeneratorRuntime.wrap(function(e){for(;;)switch(e.prev=e.next){case 0:(0,_request.requrst)("sign/SignHistory",{},function(e){t.signData=e.info,t.makeTree(),t.$apply()});case 1:case"end":return e.stop()}},e,this)}));return e}()},{key:"makeTree",value:function(){if(this.nakedataFinish)return!1;var e=new Date,t=e.getFullYear();this.m_currMonth=e.getMonth(),this.m_currDate=e.getDate(),this.m_currYear=t,this.m_days=new Array(31,28+function(e){return e%100==0?e%400==0?1:0:e%4==0?1:0}(t),31,30,31,31,30,31,30,31,30,31);var r=this.signData.list,n=[];for(var a in r){var i=new Date(r[a].sign_lasttime);n.push(i.getDate())}for(var o=0;o<this.m_days[this.m_currMonth];o++){var s={day:o,curr:n.includes(o),sign:"",today:this.m_currDate==o};this.m_allDay.push(s)}this.nakedataFinish=!0,this.$apply()}},{key:"onLoad",value:function(){this.SignHistory()}}]),t}(_wepy2.default.page);Page(require("./../../npm/wepy/lib/wepy.js").default.$createPage(IndexSign,"pages/index/sign"));
});require("pages/index/sign.js")@code-separator-line:["div","view","text","image"]