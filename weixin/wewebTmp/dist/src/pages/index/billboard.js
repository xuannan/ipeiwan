/*v0.6vv_20170214_fbi*/
window.__wcc_version__='v0.6vv_20170214_fbi'
window.__wxml_transpiler_version__='v0.1'
var $gwxc
var $gaic={}
$gwx=function(path,global){
function _(a,b){b&&a.children.push(b);}
function _v(k){if(typeof(k)!='undefined')return {tag:'virtual','wxKey':k,children:[]};return {tag:'virtual',children:[]};}
function _n(tag){$gwxc++;if($gwxc>=16000){throw 'Dom limit exceeded, please check if there\'s any mistake you\'ve made.'};return {tag:tag.substr(0,3)=='wx-'?tag:'wx-'+tag,attr:{},children:[],n:[]}}
function _p(a,b){b&&a.properities.push(b);}
function _s(scope,env,key){return typeof(scope[key])!='undefined'?scope[key]:env[key]}function _wl(tname,prefix){console.warn('WXMLRT:'+prefix+':-1:-1:-1: Template `' + tname + '` is being called recursively, will be stop.')}$gwn=console.warn;
$gwl=console.log;
function $gwh()
{
function x(){}
x.prototype =
{
hn: function( obj )
{
if( typeof(obj) == 'object' )
{
var cnt=0;
var any=false;
for(var x in obj)
{
any|=x==='__value__';
cnt++;
if(cnt>2)break;
}
return cnt == 2 && any && obj.hasOwnProperty('__wxspec__') ? "h" : "n";
}
return "n";
},
nh: function( obj, special )
{
return { __value__: obj, __wxspec__: special ? special : true }
},
rv: function( obj )
{
return this.hn(obj)==='n'?obj:this.rv(obj.__value__);
}
}
return new x;
}
wh=$gwh();
function $gwrt( should_pass_type_info )
{
function ArithmeticEv( ops, e, s, g, o )
{
var rop = ops[0][1];
var _a,_b,_c,_d, _aa, _bb;
switch( rop )
{
case '?:':
_a = rev( ops[1], e, s, g, o );
_c = should_pass_type_info && ( wh.hn(_a) === 'h' );
_d = wh.rv( _a ) ? rev( ops[2], e, s, g, o ) : rev( ops[3], e, s, g, o );
_d = _c && wh.hn( _d ) === 'n' ? wh.nh( _d, 'c' ) : _d;
return _d;
break;
case '&&':
_a = rev( ops[1], e, s, g, o );
_c = should_pass_type_info && ( wh.hn(_a) === 'h' );
_d = wh.rv( _a ) ? rev( ops[2], e, s, g, o ) : wh.rv( _a );
_d = _c && wh.hn( _d ) === 'n' ? wh.nh( _d, 'c' ) : _d;
return _d;
break;
case '||':
_a = rev( ops[1], e, s, g, o );
_c = should_pass_type_info && ( wh.hn(_a) === 'h' );
_d = wh.rv( _a ) ? wh.rv(_a) : rev( ops[2], e, s, g, o );
_d = _c && wh.hn( _d ) === 'n' ? wh.nh( _d, 'c' ) : _d;
return _d;
break;
case '+':
case '*':
case '/':
case '%':
case '|':
case '^':
case '&':
case '===':
case '==':
case '!=':
case '!==':
case '>=':
case '<=':
case '>':
case '<':
case '<<':
case '>>':
_a = rev( ops[1], e, s, g, o );
_b = rev( ops[2], e, s, g, o );
_c = should_pass_type_info && (wh.hn( _a ) === 'h' || wh.hn( _b ) === 'h');
switch( rop )
{
case '+':
_d = wh.rv( _a ) + wh.rv( _b );
break;
case '*':
_d = wh.rv( _a ) * wh.rv( _b );
break;
case '/':
_d = wh.rv( _a ) / wh.rv( _b );
break;
case '%':
_d = wh.rv( _a ) % wh.rv( _b );
break;
case '|':
_d = wh.rv( _a ) | wh.rv( _b );
break;
case '^':
_d = wh.rv( _a ) ^ wh.rv( _b );
break;
case '&':
_d = wh.rv( _a ) & wh.rv( _b );
break;
case '===':
_d = wh.rv( _a ) === wh.rv( _b );
break;
case '==':
_d = wh.rv( _a ) == wh.rv( _b );
break;
case '!=':
_d = wh.rv( _a ) != wh.rv( _b );
break;
case '!==':
_d = wh.rv( _a ) !== wh.rv( _b );
break;
case '>=':
_d = wh.rv( _a ) >= wh.rv( _b );
break;
case '<=':
_d = wh.rv( _a ) <= wh.rv( _b );
break;
case '>':
_d = wh.rv( _a ) > wh.rv( _b );
break;
case '<':
_d = wh.rv( _a ) < wh.rv( _b );
break;
case '<<':
_d = wh.rv( _a ) << wh.rv( _b );
break;
case '>>':
_d = wh.rv( _a ) >> wh.rv( _b );
break;
default:
break;
}
return _c ? wh.nh( _d, "c" ) : _d;
break;
case '-':
_a = ops.length === 3 ? rev( ops[1], e, s, g, o ) : 0;
_b = ops.length === 3 ? rev( ops[2], e, s, g, o ) : rev( ops[1], e, s, g, o );
_c = should_pass_type_info && (wh.hn( _a ) === 'h' || wh.hn( _b ) === 'h');
_d = _c ? wh.rv( _a ) - wh.rv( _b ) : _a - _b;
return _c ? wh.nh( _d, "c" ) : _d;
break;
case '!':
_a = rev( ops[1], e, s, g, o );
_c = should_pass_type_info && (wh.hn( _a ) == 'h');
_d = !wh.rv(_a);
return _c ? wh.nh( _d, "c" ) : _d;
case '~':
_a = rev( ops[1], e, s, g, o );
_c = should_pass_type_info && (wh.hn( _a ) == 'h');
_d = ~wh.rv(_a);
return _c ? wh.nh( _d, "c" ) : _d;
default:
$gwn('unrecognized op' + rop );
}
}
function rev( ops, e, s, g, o )
{
var op = ops[0];
if( typeof(op)==='object' )
{
var vop=op[0];
var _a, _aa, _b, _bb, _c, _d, _s, _e, _ta, _tb, _td;
switch(vop)
{
case 2:
return ArithmeticEv(ops,e,s,g,o);
break;
case 4:
return rev( ops[1], e, s, g, o );
break;
case 5:
switch( ops.length )
{
case 2:
return should_pass_type_info ?
[rev(ops[1],e,s,g,o)] :
[wh.rv(rev(ops[1],e,s,g,o))];
break;
case 1:
return [];
break;
default:
_a = rev( ops[1],e,s,g,o );
_a.push(
should_pass_type_info ?
rev( ops[2],e,s,g,o ) :
wh.rv( rev(ops[2],e,s,g,o) )
);
return _a;
break;
}
break;
case 6:
_a = rev(ops[1],e,s,g,o);
_ta = wh.hn(_a)==='h';
_aa = _ta ? wh.rv(_a) : _a;
o.is_affected |= _ta;
if( should_pass_type_info )
{
if( _aa===null || typeof(_aa) === 'undefined' )
{
return _ta ? wh.nh(undefined, 'e') : undefined;
}
_b = rev(ops[2],e,s,g,o);
_tb = wh.hn(_b) === 'h';
_bb = _tb ? wh.rv(_b) : _b;
o.is_affected |= _tb;
if( _bb===null || typeof(_bb) === 'undefined' )
{
return (_ta || _tb) ? wh.nh(undefined, 'e') : undefined;
}
_d = _aa[_bb];
_td = wh.hn(_d)==='h';
o.is_affected |= _td;
return (_ta || _tb) ? (_td ? _d : wh.nh(_d, 'e')) : _d;
}
else
{
if( _aa===null || typeof(_aa) === 'undefined' )
{
return undefined;
}
_b = rev(ops[2],e,s,g,o);
_tb = wh.hn(_b) === 'h';
_bb = _tb ? wh.rv(_b) : _b;
o.is_affected |= _tb;
if( _bb===null || typeof(_bb) === 'undefined' )
{
return undefined;
}
_d = _aa[_bb];
_td = wh.hn(_d)==='h';
o.is_affected |= _td;
return _td ? wh.rv(_d) : _d;
}
case 7:
switch(ops[1][0])
{
case 11:
o.is_affected |= wh.hn(g)==='h';
return g;
case 3:
_s = wh.rv( s );
_e = wh.rv( e );
_b = ops[1][1];
_a = _s && _s.hasOwnProperty(_b) ?
s : _e && ( _e.hasOwnProperty(_b) ? e : undefined );
if( should_pass_type_info )
{
if( _a )
{
_ta = wh.hn(_a) === 'h';
_aa = _ta ? wh.rv( _a ) : _a;
_d = _aa[_b];
_td = wh.hn(_d) === 'h';
o.is_affected |= _ta || _td;
_d = _ta && !_td ? wh.nh(_d,'e') : _d;
return _d;
}
}
else
{
if( _a )
{
_ta = wh.hn(_a) === 'h';
_aa = _ta ? wh.rv( _a ) : _a;
_d = _aa[_b];
_td = wh.hn(_d) === 'h';
o.is_affected |= _ta || _td;
return wh.rv(_d);
}
}
return undefined;
}
break;
case 8:
_a = {};
_a[ops[1]] = rev(ops[2],e,s,g,o);
return _a;
break;
case 9:
_a = rev(ops[1],e,s,g,o);
_b = rev(ops[2],e,s,g,o);
function merge( _a, _b, _ow )
{
_ta = wh.hn(_a)==='h';
_tb = wh.hn(_b)==='h';
_aa = wh.rv(_a);
_bb = wh.rv(_b);
if( should_pass_type_info )
{
if( _tb )
{
for(var k in _bb)
{
if ( _ow || !_aa.hasOwnProperty(k) )
_aa[k]=wh.nh(_bb[k],'e');
}
}
else
{
for(var k in _bb)
{
if ( _ow || !_aa.hasOwnProperty(k) )
_aa[k]=_bb[k];
}
}
}
else
{
for(var k in _bb)
{
if ( _ow || _aa.hasOwnProperty(k) )
_aa[k]=wh.rv(_bb[k]);
}
}
return _a;
}
var _c = _a
var _ow = true
if ( typeof(ops[1][0]) === "object" && ops[1][0][0] === 10 ) {
_a = _b
_b = _c
_ow = false
}
if ( typeof(ops[1][0]) === "object" && ops[1][0][0] === 10 ) {
var _r = {}
return merge( merge( _r, _a, _ow ), _b, _ow );
}
else
return merge( _a, _b, _ow );
break;
case 10:
return should_pass_type_info ? rev(ops[1],e,s,g,o) : wh.rv(rev(ops[1],e,s,g,o));
}
}
else
{
if( op === 3 || op === 1 ) return ops[1];
else if( op === 11 )
{
var _a='';
for( var i = 1 ; i < ops.length ; i++ )
{
var xp = wh.rv(rev(ops[i],e,s,g,o));
_a += typeof(xp) === 'undefined' ? '' : xp;
}
return _a;
}
}
}
return rev;
}
gra=$gwrt(true);
grb=$gwrt(false);
function TestTest( expr, ops, e,s,g, expect_a, expect_b, expect_affected )
{
{
var o = {is_affected:false};
var a = gra( ops, e,s,g, o );
if( JSON.stringify(a) != JSON.stringify( expect_a )
|| o.is_affected != expect_affected )
{
console.warn( "A. " + expr + " get result " + JSON.stringify(a) + ", " + o.is_affected + ", but " + JSON.stringify( expect_a ) + ", " + expect_affected + " is expected" );
}
}
{
var o = {is_affected:false};
var a = grb( ops, e,s,g, o );
if( JSON.stringify(a) != JSON.stringify( expect_b )
|| o.is_affected != expect_affected )
{
console.warn( "B. " + expr + " get result " + JSON.stringify(a) + ", " + o.is_affected + ", but " + JSON.stringify( expect_b ) + ", " + expect_affected + " is expected" );
}
}
}

function wfor( to_iter, func, env, _s, global, father, itemname, indexname, keyname )
{
var _n = wh.hn( to_iter ) === 'n';
var scope = wh.rv( _s );
var has_old_item = scope.hasOwnProperty(itemname);
var has_old_index = scope.hasOwnProperty(indexname);
var old_item = scope[itemname];
var old_index = scope[indexname];
var full = Object.prototype.toString.call(wh.rv(to_iter));
var type = full[8];
if( type === 'N' && full[10] === 'l' ) type = 'X';
var _y;
if( _n )
{
if( type === 'A' )
{
for( var i = 0 ; i < to_iter.length ; i++ )
{
scope[itemname] = to_iter[i];
scope[indexname] = wh.nh(i, 'h');
_y = keyname ? (keyname==="*this" ? _v(wh.rv(to_iter[i])) : _v(wh.rv(wh.rv(to_iter[i])[keyname]))) : _v();
_(father,_y);
func( env, scope, _y, global );
}
}
else if( type === 'O' )
{
for( var k in to_iter )
{
scope[itemname] = to_iter[k];
scope[indexname] = wh.nh(k, 'h');
_y = keyname ? (keyname==="*this" ? _v(wh.rv(to_iter[k])) : _v(wh.rv(wh.rv(to_iter[k])[keyname]))) : _v();
_(father,_y);
func( env,scope,_y,global );
}
}
else if( type === 'S' )
{
for( var i = 0 ; i < to_iter.length ; i++ )
{
scope[itemname] = to_iter[i];
scope[indexname] = wh.nh(i, 'h');
_y = _v( to_iter[i] + i );
_(father,_y);
func( env, scope, _y, global );
}
}
else if( type === 'N' )
{
for( var i = 0 ; i < to_iter ; i++ )
{
scope[itemname] = i;
scope[indexname] = wh.nh(i, 'h');
_y = _v( i );
_(father,_y);
func(env,scope,_y,global);
}
}
else
{
}
}
else
{
var r_to_iter = wh.rv(to_iter);
var r_iter_item, iter_item;
if( type === 'A' )
{
for( var i = 0 ; i < r_to_iter.length ; i++ )
{
iter_item = r_to_iter[i];
iter_item = wh.hn(iter_item)==='n' ? wh.nh(iter_item,'h') : iter_item;
r_iter_item = wh.rv( iter_item );
scope[itemname] = iter_item
scope[indexname] = wh.nh(i, 'h');
_y = keyname ? (keyname==="*this" ? _v(r_iter_item) : _v(wh.rv(r_iter_item[keyname]))) : _v();
_(father,_y);
func( env, scope, _y, global );
}
}
else if( type === 'O' )
{
for( var k in r_to_iter )
{
iter_item = r_to_iter[k];
iter_item = wh.hn(iter_item)==='n'? wh.nh(iter_item,'h') : iter_item;
r_iter_item = wh.rv( iter_item );
scope[itemname] = iter_item;
scope[indexname] = wh.nh(k, 'h');
_y = keyname ? (keyname==="*this" ? _v(r_iter_item) : _v(wh.rv(r_iter_item[keyname]))) : _v();
_(father,_y);
func( env, scope, _y, global );
}
}
else if( type === 'S' )
{
for( var i = 0 ; i < r_to_iter.length ; i++ )
{
scope[itemname] = wh.nh(r_to_iter[i],'h');
scope[indexname] = wh.nh(i, 'h');
_y = _v( to_iter[i] + i );
_(father,_y);
func( env, scope, _y, global );
}
}
else if( type === 'N' )
{
for( var i = 0 ; i < r_to_iter ; i++ )
{
scope[itemname] = wh.nh(i,'h');
scope[indexname]= wh.nh(i,'h');
_y = _v( i );
_(father,_y);
func(env,scope,_y,global);
}
}
else
{
}
}
if(has_old_item)
{
scope[itemname]=old_item;
}
else
{
delete scope[itemname];
}
if(has_old_index)
{
scope[indexname]=old_index;
}
else{delete scope[indexname];}}


function _r( node, attrname, opindex, env, scope, global )
{
var o = {};
var a = grb( z[opindex], env, scope, global, o );
node.attr[attrname] = a;
if( o.is_affected ) node.n.push( attrname );
}
function _o( opindex, env, scope, global )
{
var nothing = {};
return grb( z[opindex], env, scope, global, nothing );
}
function _1( opindex, env, scope, global )
{
var nothing = {};
return gra( z[opindex], env, scope, global, nothing );
}
function _2( opindex, func, env, scope, global, father, itemname, indexname, keyname )
{
var to_iter = _1( opindex, env, scope, global, father, itemname, indexname, keyname );
wfor( to_iter, func, env, scope, global, father, itemname, indexname, keyname );
}
function _gv( )
{
if( typeof(window.__webview_engine_version__) == 'undefined' ) return 0.0;
return window.__webview_engine_version__;
}
function _m(tag,attrs,env,scope,global)
{
var tmp=_n(tag);
var base=0;
for(var i = 0 ; i < attrs.length ; i+=2 )
{
if(attrs[i+1]<0)
{tmp.attr[attrs[i]]=true;}else{_r(tmp,attrs[i],base+attrs[i+1],env,scope,global);
if(base===0)base=attrs[i+1];}}return tmp;};function _ai(i,p,e,me,r,c){var x=_grp(p,e,me);if(x)i.push(x);else{i.push('');console.warn('WXMLRT:'+me+':import:'+r+':'+c+': Path `'+p+'` not found from `'+me+'`.')}}function _grp(p,e,me){if(p[0]!='/'){var mepart=me.split('/');mepart.pop();var ppart=p.split('/');for(var i=0;i<ppart.length;i++){if(ppart[i]=='..')mepart.pop();else if(ppart[i]=='.' || !ppart[i])continue;else mepart.push(ppart[i]);}p=mepart.join('/');}if(me[0]=='.'&&p[0]=='/')p='.'+p;if(e[p])return p;if(e[p+'.wxml'])return p+'.wxml';}function _gd(p,c,e,d){if(!c)return;if(d[p][c])return d[p][c];for(var x=e[p].i.length-1;x>=0;x--){if(e[p].i[x]&&d[e[p].i[x]][c])return d[e[p].i[x]][c]};for(var x=e[p].ti.length-1;x>=0;x--){var q=_grp(e[p].ti[x],e,p);if(q&&d[q][c])return d[q][c]}
var ii=_gapi(e,p);for(var x=0;x<ii.length;x++){if(ii[x]&&d[ii[x]][c])return d[ii[x]][c]}for(var k=e[p].j.length-1;k>=0;k--)if(e[p].j[k]){for(var q=e[e[p].j[k]].ti.length-1;q>=0;q--){var pp=_grp(e[e[p].j[k]].ti[q],e,p);if(pp&&d[pp][c]){return d[pp][c]}}}}function _gapi(e,p){if(!p)return [];if($gaic[p]){return $gaic[p]};var ret=[],q=[],h=0,t=0,put={},visited={};q.push(p);visited[p]=true;t++;while(h<t){var a=q[h++];for(var i=0;i<e[a].ic.length;i++){var nd=e[a].ic[i];var np=_grp(nd,e,a);if(np&&!visited[np]){visited[np]=true;q.push(np);t++;}}for(var i=0;a!=p&&i<e[a].ti.length;i++){var ni=e[a].ti[i];var nm=_grp(ni,e,a);if(nm&&!put[nm]){put[nm]=true;ret.push(nm);}}}$gaic[p]=ret;return ret;}var $ixc={};function _ic(p,ent,me,e,s,r,gg){var x=_grp(p,ent,me);ent[me].j.push(x);if(x){if($ixc[x]){console.warn('WXMLRT:-1:include:-1:-1: `'+p+'` is being included in a loop, will be stop.');return;}$ixc[x]=true;try{ent[x].f(e,s,r,gg)}catch(e){}$ixc[x]=false;}else{console.warn('WXMLRT:'+me+':include:-1:-1: Included path `'+p+'` not found from `'+me+'`.')}}function _w(tn,f,line,c){console.warn('WXMLRT:'+f+':template:'+line+':'+c+': Template `'+tn+'` not found.');}function _ev(dom){var changed=false;delete dom.properities;delete dom.n;if(dom.children){do{changed=false;var newch = [];for(var i=0;i<dom.children.length;i++){var ch=dom.children[i];if( ch.tag=='virtual'){changed=true;for(var j=0;ch.children&&j<ch.children.length;j++){newch.push(ch.children[j]);}}else { newch.push(ch); } } dom.children = newch; }while(changed);for(var i=0;i<dom.children.length;i++){_ev(dom.children[i]);}} return dom; }var e_={}
if(global&&typeof(global.entrys)=='object')e_=global.entrys
var d_={}
if(global&&typeof(global.defines)=='object')d_=global.defines
var p_={}
var z = [];
  (function(z){
    var a = 11;
    function Z(ops){z.push(ops)};
    Z([3, 'billboard']);Z([3, 'topImg']);Z([[6],[[7],[3, "config"]],[3, "topImg"]]);Z([a, [3, 'width:'],[[6],[[7],[3, "sysInfo"]],[3, "windowWidth"]],[3, 'px;height:'],[[2, "*"], [[6],[[7],[3, "sysInfo"]],[3, "windowWidth"]], [[6],[[7],[3, "config"]],[3, "height"]]],[3, 'px']]);Z([3, 'content']);Z([[7],[3, "bdlist"]]);Z([3, 'itemName']);Z([3, 'idx']);Z([3, 'key']);Z([3, 'item']);Z([3, 'navcates noborder']);Z([3, 'left']);Z([3, 'name']);Z([a, [[6],[[7],[3, "itemName"]],[3, "name"]]]);Z([3, 'right']);Z([3, 'NavcateRightClick']);Z([3, 'rtext']);Z([3, '我要上榜']);Z([3, 'http://img.he29.com/play/f4fe7786635ee901a46590b7f1926bc08d628bdf.png']);Z([3, 'tablist']);Z([[6],[[7],[3, "itemName"]],[3, "info"]]);Z([3, 'listItem']);Z([3, 'height:100%']);Z([[6],[[7],[3, "item"]],[3, "bill_uid"]]);Z([3, 'GoHome']);Z([3, 'itemshow']);Z([3, 'index tag']);Z([3, 'aspectFill']);Z([[6],[[7],[3, "item"]],[3, "icon"]]);Z([3, 'nameline']);Z([a, [[6],[[7],[3, "item"]],[3, "user_nickname"]]]);Z([3, 'zan']);Z([a, [3, '👍'],[[2, "||"],[[6],[[7],[3, "item"]],[3, "start"]],[1, 0]]]);Z([3, 'img left']);Z([3, 'hd']);Z([[6],[[7],[3, "item"]],[3, "user_avatar"]]);Z([1, false]);Z([3, 'icon']);Z([3, 'http://img.he29.com/play/b482af58ce460bde91d63c68c11d892e7b4449e8.png']);Z([3, 'http://img.he29.com/play/51f33432a1296d687811daf1c81172ff2235a342.png']);Z([3, 'http://img.he29.com/play/d49724a0c30871551404ed2dd21126fe7879233a.png']);Z([3, 'weui-progress']);Z([3, 'weui-progress__bar']);Z([[6],[[7],[3, "item"]],[3, "precolor"]]);Z([[6],[[7],[3, "item"]],[3, "progress"]]);Z([3, '3']);Z([3, 'pretext']);Z([a, [[6],[[7],[3, "item"]],[3, "prTime"]],[3, 'h']]);Z([3, 'itemshow empty']);Z([3, 'noBodoy']);Z([[6],[[7],[3, "item"]],[3, "bill_id"]]);Z([[6],[[7],[3, "item"]],[3, "bill_index"]]);Z([[6],[[7],[3, "itemName"]],[3, "money"]]);Z([3, 'opa5']);Z([3, '\r\n                                        榜上无人\r\n                                    ']);Z([3, 'http://img.he29.com/play/05d1b50a5de7367f3246ded4c080a15300b3efae.jpeg']);Z([3, 'mt20c']);Z([3, 'speak']);Z([3, 'tipsShow']);Z([3, 'margin-top:0;background:none']);Z([3, 'weui-article__section']);Z([3, 'weui-article__title']);Z([3, 'weui-article__h3']);Z([3, '1. 打榜说明']);Z([3, 'weui-article__p']);Z([3, '\r\n                       打榜可以给你一个绝对曝光自己的机会,打榜进入首屏的玩家可以直接展示在首页黄金位置,\r\n                        榜单上蓝色进度条则是当前用户打榜的剩余时间,当进度条达到100%;则玩家自动退出打榜模式\r\n                    ']);Z([3, '2. 预约规则说明']);Z([3, '\r\n                        需要支付一定的金币兑换一次打榜机会,\r\n                        可以选择一定的停留时间, 在每个榜单下, 根据其他用户的有效点赞数量进行排序,获得点赞越多,就会越靠近当前榜单的第一名!\r\n                        打榜结束后, 榜单自动刷新,只要有空余位置,所有人都可以进行打榜!\r\n                    ']);Z([3, 'bgcs']);Z([[2, "||"],[[6],[[7],[3, "config"]],[3, "bgt"]],[1, "http://img.he29.com/play/188290489d327c7af7e7ee4cbec82b375547fadd.jpeg"]]);Z([a, [3, 'width:'],[[6],[[7],[3, "sysInfo"]],[3, "windowWidth"]],[3, 'px;height:'],[[2, "+"], [[6],[[7],[3, "sysInfo"]],[3, "windowHeight"]], [1, 20]],[3, 'px']]);Z([[7],[3, "$panal$open"]]);Z([3, 'panal']);Z([3, 'contentText']);Z([a, [3, 'height:'],[[7],[3, "$panal$height"]],[3, 'px;margin-top:'],[[7],[3, "$panal$top"]],[3, 'px;']]);Z([3, '$panal$goUrl']);Z([3, 'father']);Z([[7],[3, "$panal$url"]]);Z([3, 'background']);Z([3, '上榜设置']);Z([3, 'PanContent']);Z([3, 'BdformSubmit']);Z([3, 'true']);Z([3, 'weui-cells_after-title']);Z([3, 'weui-cell weui-cell_access']);Z([3, 'weui-cell_active']);Z([3, 'weui-cell__bd']);Z([3, '当前榜单']);Z([3, 'chosePosition']);Z([3, 'weui-cell__ft weui-cell__ft_in-access']);Z([a, [3, '\r\n                                '],[[6],[[7],[3, "form"]],[3, "zindex"]],[3, '\r\n                            ']]);Z([3, '当前位置']);Z([3, '09号']);Z([3, 'weui-cell weui-cell_input']);Z([3, 'weui-cell__hd']);Z([3, 'weui-label']);Z([3, '开始日期']);Z([3, 'bindDateChange']);Z([3, '2027-09-01']);Z([3, 'date']);Z([3, '2017-12-01']);Z([[7],[3, "date"]]);Z([3, 'weui-input']);Z([a, [[6],[[7],[3, "form"]],[3, "start"]]]);Z([3, 'margin:5px 0']);Z([3, '开始时间']);Z([3, 'bindTimeChange']);Z([3, '23:59']);Z([3, 'time']);Z([3, '00:00']);Z([[7],[3, "time"]]);Z([a, [[6],[[7],[3, "form"]],[3, "time"]]]);Z([3, 'border-top:1rpx solid #D9D9D9;']);Z([3, '上榜时间']);Z([3, 'choseTime']);Z([a, [[6],[[7],[3, "form"]],[3, "times"]],[3, '小时']]);Z([3, '需要金币']);Z([a, [[6],[[7],[3, "form"]],[3, "money"]],[3, '个']]);Z([3, 'h10']);Z([3, 'btnGroup']);Z([a, [3, 'zan-btn zan-btn--primary btnBgc animated '],[[6],[[7],[3, "btnName"]],[3, "className"]]]);Z([3, 'submit']);Z([a, [3, '\r\n                            '],[[6],[[7],[3, "btnName"]],[3, "text"]],[3, '\r\n                        ']]);Z([3, 'line']);Z([3, '$panal$close']);Z([3, 'close']);Z([3, 'X']);Z([3, 'loading']);Z([[7],[3, "$msg$open"]]);Z([3, 'loadmsg animated fadeIn']);Z([3, 'msg']);Z([3, 'layui-m-layercont']);Z([a, [[7],[3, "$msg$msg"]]]);
  })(z);d_["./pages/index/billboard.wxml"] = {};
  var m0=function(e,s,r,gg){
    var oIW = _n("view");_r(oIW, 'class', 0, e, s, gg);var oJW = _m( "image", ["class", 1,"src", 1,"style", 2], e, s, gg);_(oIW,oJW);var oKW = _n("view");_r(oKW, 'class', 4, e, s, gg);var oLW = _v();var oMW = function(oQW,oPW,oOW,gg){var oNW = _n("view");_r(oNW, 'class', 9, oQW, oPW, gg);var oSW = _n("view");_r(oSW, 'class', 10, oQW, oPW, gg);var oTW = _n("view");_r(oTW, 'class', 11, oQW, oPW, gg);var oUW = _n("view");_r(oUW, 'class', 12, oQW, oPW, gg);var oVW = _o(13, oQW, oPW, gg);_(oUW,oVW);_(oTW,oUW);_(oSW,oTW);var oWW = _n("view");_r(oWW, 'class', 14, oQW, oPW, gg);var oXW = _m( "text", ["bindtap", 15,"class", 1], oQW, oPW, gg);var oYW = _o(17, oQW, oPW, gg);_(oXW,oYW);_(oWW,oXW);var oZW = _n("image");_r(oZW, 'src', 18, oQW, oPW, gg);_(oWW,oZW);_(oSW,oWW);_(oNW,oSW);var oaW = _m( "view", ["nid", 13,"class", 6], oQW, oPW, gg);var obW = _v();var ocW = function(ogW,ofW,oeW,gg){var odW = _n("view");_r(odW, 'class', 21, ogW, ofW, gg);var oiW = _n("view");_r(oiW, 'style', 22, ogW, ofW, gg);var ojW = _v();
      if (_o(23, ogW, ofW, gg)) {
        ojW.wxVkey = 1;var okW = _m( "view", ["data-id", 23,"bindtap", 1,"class", 2], ogW, ofW, gg);var omW = _n("view");_r(omW, 'class', 26, ogW, ofW, gg);var onW = _m( "image", ["mode", 27,"src", 1], ogW, ofW, gg);_(omW,onW);_(okW,omW);var ooW = _n("view");_r(ooW, 'class', 29, ogW, ofW, gg);var opW = _o(30, ogW, ofW, gg);_(ooW,opW);_(okW,ooW);var oqW = _n("view");_r(oqW, 'class', 31, ogW, ofW, gg);var orW = _o(32, ogW, ofW, gg);_(oqW,orW);_(okW,oqW);var osW = _n("view");_r(osW, 'class', 33, ogW, ofW, gg);var otW = _m( "image", ["class", 34,"src", 1], ogW, ofW, gg);_(osW,otW);_(okW,osW);var ouW = _v();
      if (_o(36, ogW, ofW, gg)) {
        ouW.wxVkey = 1;var ovW = _n("view");_r(ovW, 'class', 37, ogW, ofW, gg);var oxW = _n("image");_r(oxW, 'src', 38, ogW, ofW, gg);_(ovW,oxW);var oyW = _n("image");_r(oyW, 'src', 39, ogW, ofW, gg);_(ovW,oyW);var ozW = _n("image");_r(ozW, 'src', 40, ogW, ofW, gg);_(ovW,ozW);_(ouW, ovW);
      } _(okW,ouW);var o_W = _n("view");_r(o_W, 'class', 41, ogW, ofW, gg);var oAX = _n("view");_r(oAX, 'class', 42, ogW, ofW, gg);var oBX = _m( "progress", ["color", 43,"percent", 1,"strokeWidth", 2], ogW, ofW, gg);_(oAX,oBX);_(o_W,oAX);var oCX = _n("view");_r(oCX, 'class', 46, ogW, ofW, gg);var oDX = _o(47, ogW, ofW, gg);_(oCX,oDX);_(o_W,oCX);_(okW,o_W);_(ojW, okW);
      }else {
        ojW.wxVkey = 2;var oEX = _n("view");_r(oEX, 'class', 48, e, s, gg);var oGX = _m( "view", ["data-uid", 23,"class", 10,"bindtap", 26,"data-bid", 27,"data-index", 28,"data-money", 29], e, s, gg);var oHX = _n("view");_r(oHX, 'class', 53, e, s, gg);var oIX = _o(54, e, s, gg);_(oHX,oIX);_(oGX,oHX);var oJX = _m( "image", ["class", 34,"src", 21], e, s, gg);_(oGX,oJX);_(oEX,oGX);_(ojW, oEX);
      }_(oiW,ojW);_(odW,oiW);_(oeW, odW);return oeW;};_2(20, ocW, oQW, oPW, gg, obW, "item", "index", 'key');_(oaW,obW);_(oNW,oaW);_(oOW, oNW);return oOW;};_2(5, oMW, e, s, gg, oLW, "itemName", "idx", 'key');_(oKW,oLW);_(oIW,oKW);var oKX = _n("view");_r(oKX, 'class', 56, e, s, gg);_(oIW,oKX);var oLX = _n("view");_r(oLX, 'class', 57, e, s, gg);var oMX = _n("view");_r(oMX, 'class', 56, e, s, gg);_(oLX,oMX);var oNX = _m( "view", ["class", 58,"style", 1], e, s, gg);var oOX = _n("view");_r(oOX, 'class', 60, e, s, gg);var oPX = _n("view");_r(oPX, 'class', 61, e, s, gg);_(oOX,oPX);var oQX = _n("view");var oRX = _n("view");_r(oRX, 'class', 60, e, s, gg);var oSX = _n("view");_r(oSX, 'class', 62, e, s, gg);var oTX = _o(63, e, s, gg);_(oSX,oTX);_(oRX,oSX);var oUX = _n("view");_r(oUX, 'class', 64, e, s, gg);var oVX = _o(65, e, s, gg);_(oUX,oVX);_(oRX,oUX);var oWX = _n("view");_r(oWX, 'class', 62, e, s, gg);var oXX = _o(66, e, s, gg);_(oWX,oXX);_(oRX,oWX);var oYX = _n("view");_r(oYX, 'class', 64, e, s, gg);var oZX = _o(67, e, s, gg);_(oYX,oZX);_(oRX,oYX);_(oQX,oRX);_(oOX,oQX);_(oNX,oOX);_(oLX,oNX);_(oIW,oLX);var oaX = _m( "image", ["class", 68,"src", 1,"style", 2], e, s, gg);_(oIW,oaX);var obX = _v();
      if (_o(71, e, s, gg)) {
        obX.wxVkey = 1;var ocX = _n("view");_r(ocX, 'class', 72, e, s, gg);var oeX = _m( "view", ["class", 73,"style", 1], e, s, gg);var ofX = _m( "view", ["bindtap", 75,"class", 1,"data-url", 2], e, s, gg);var ogX = _n("view");_r(ogX, 'class', 78, e, s, gg);_(ofX,ogX);var ohX = _n("view");_r(ohX, 'class', 4, e, s, gg);var oiX = _n("view");_r(oiX, 'class', 60, e, s, gg);var ojX = _n("view");_r(ojX, 'class', 62, e, s, gg);var okX = _n("view");var olX = _o(79, e, s, gg);_(okX,olX);_(ojX,okX);_(oiX,ojX);var omX = _n("view");_r(omX, 'class', 80, e, s, gg);var onX = _m( "form", ["bindsubmit", 81,"reportSubmit", 1], e, s, gg);var ooX = _n("view");_r(ooX, 'class', 83, e, s, gg);var opX = _m( "view", ["class", 84,"hoverClass", 1], e, s, gg);var oqX = _n("view");_r(oqX, 'class', 86, e, s, gg);var orX = _o(87, e, s, gg);_(oqX,orX);_(opX,oqX);var osX = _m( "view", ["bindtap", 88,"class", 1], e, s, gg);var otX = _o(90, e, s, gg);_(osX,otX);_(opX,osX);_(ooX,opX);var ouX = _m( "view", ["class", 84,"hoverClass", 1], e, s, gg);var ovX = _n("view");_r(ovX, 'class', 86, e, s, gg);var owX = _o(91, e, s, gg);_(ovX,owX);_(ouX,ovX);var oxX = _n("view");_r(oxX, 'class', 89, e, s, gg);var oyX = _o(92, e, s, gg);_(oxX,oyX);_(ouX,oxX);_(ooX,ouX);_(onX,ooX);var ozX = _n("view");_r(ozX, 'class', 93, e, s, gg);var o_X = _n("view");_r(o_X, 'class', 94, e, s, gg);var oAY = _n("view");_r(oAY, 'class', 95, e, s, gg);var oBY = _o(96, e, s, gg);_(oAY,oBY);_(o_X,oAY);_(ozX,o_X);var oCY = _n("view");_r(oCY, 'class', 86, e, s, gg);var oDY = _m( "picker", ["bindchange", 97,"end", 1,"mode", 2,"start", 3,"value", 4], e, s, gg);var oEY = _n("view");_r(oEY, 'class', 102, e, s, gg);var oFY = _o(103, e, s, gg);_(oEY,oFY);_(oDY,oEY);_(oCY,oDY);_(ozX,oCY);_(onX,ozX);var oGY = _m( "view", ["class", 93,"style", 11], e, s, gg);var oHY = _n("view");_r(oHY, 'class', 94, e, s, gg);var oIY = _n("view");_r(oIY, 'class', 95, e, s, gg);var oJY = _o(105, e, s, gg);_(oIY,oJY);_(oHY,oIY);_(oGY,oHY);var oKY = _n("view");_r(oKY, 'class', 86, e, s, gg);var oLY = _m( "picker", ["bindchange", 106,"end", 1,"mode", 2,"start", 3,"value", 4], e, s, gg);var oMY = _n("view");_r(oMY, 'class', 102, e, s, gg);var oNY = _o(111, e, s, gg);_(oMY,oNY);_(oLY,oMY);_(oKY,oLY);_(oGY,oKY);_(onX,oGY);var oOY = _m( "view", ["class", 83,"style", 29], e, s, gg);var oPY = _m( "view", ["class", 84,"hoverClass", 1], e, s, gg);var oQY = _n("view");_r(oQY, 'class', 86, e, s, gg);var oRY = _o(113, e, s, gg);_(oQY,oRY);_(oPY,oQY);var oSY = _m( "view", ["class", 89,"bindtap", 25], e, s, gg);var oTY = _o(115, e, s, gg);_(oSY,oTY);_(oPY,oSY);_(oOY,oPY);var oUY = _m( "view", ["class", 84,"hoverClass", 1], e, s, gg);var oVY = _n("view");_r(oVY, 'class', 86, e, s, gg);var oWY = _o(116, e, s, gg);_(oVY,oWY);_(oUY,oVY);var oXY = _n("view");_r(oXY, 'class', 89, e, s, gg);var oYY = _o(117, e, s, gg);_(oXY,oYY);_(oUY,oXY);_(oOY,oUY);_(onX,oOY);var oZY = _n("view");_r(oZY, 'class', 93, e, s, gg);var oaY = _n("view");_r(oaY, 'class', 94, e, s, gg);_(oZY,oaY);var obY = _n("view");_r(obY, 'class', 86, e, s, gg);_(oZY,obY);_(onX,oZY);var ocY = _n("view");_r(ocY, 'class', 118, e, s, gg);_(onX,ocY);var odY = _n("view");_r(odY, 'class', 119, e, s, gg);var oeY = _m( "button", ["class", 120,"formType", 1], e, s, gg);var ofY = _o(122, e, s, gg);_(oeY,ofY);_(odY,oeY);_(onX,odY);_(omX,onX);_(oiX,omX);var ogY = _n("view");_r(ogY, 'class', 64, e, s, gg);_(oiX,ogY);_(ohX,oiX);_(ofX,ohX);_(oeX,ofX);var ohY = _n("view");_r(ohY, 'class', 123, e, s, gg);_(oeX,ohY);var oiY = _m( "view", ["bindtap", 124,"class", 1], e, s, gg);var ojY = _o(126, e, s, gg);_(oiY,ojY);_(oeX,oiY);_(ocX,oeX);var okY = _n("view");_r(okY, 'class', 127, e, s, gg);_(ocX,okY);_(obX, ocX);
      } _(oIW,obX);var olY = _v();
      if (_o(128, e, s, gg)) {
        olY.wxVkey = 1;var omY = _n("view");_r(omY, 'class', 129, e, s, gg);var ooY = _n("view");_r(ooY, 'class', 130, e, s, gg);var opY = _n("text");_r(opY, 'class', 131, e, s, gg);var oqY = _o(132, e, s, gg);_(opY,oqY);_(ooY,opY);_(omY,ooY);_(olY, omY);
      } _(oIW,olY);_(r,oIW);
    return r;
  };
        e_["./pages/index/billboard.wxml"]={f:m0,j:[],i:[],ti:[],ic:[]};
if(path&&e_[path]){
window.__wxml_comp_version__=0.02
return function(env,dd,global){$gwxc=0;var root={"tag":"wx-page"};root.children=[]
var main=e_[path].f
if(typeof(window.__webview_engine_version__)!='undefined'&&window.__webview_engine_version__+1e-6>=0.02+1e-6&&window.__mergeData__)
{env=window.__mergeData__(env,dd);}
try{
main(env,{},root,global);
if(typeof(window.__webview_engine_version__)=='undefined'||window.__webview_engine_version__+1e-6<0.01+1e-6){return _ev(root);}}catch(err){console.log(err)}return root;}}}@code-separator-line:.msg{width:auto;max-width:80%;margin:0 auto;background-color:rgba(0,0,0,.7);color:#fff;display:inline-block;font-size:16px;border-radius:5px;box-shadow:0 0 8px rgba(0,0,0,.1);pointer-events:auto;-webkit-overflow-scrolling:touch;-webkit-animation-fill-mode:both;animation-fill-mode:both;-webkit-animation-duration:.2s;animation-duration:.2s;box-sizing:content-box;position:fixed;bottom:20%;z-index:999;text-align:center;padding:5px 10px}.layui-m-layercont{line-height:22px;text-align:center}.loadmsg{display:flex;justify-content:center;margin:0 auto;text-align:center}.speak .weui-article__title{font-size:%%?36rpx?%%;padding:%%?20rpx?%% %%?1rpx?%%;border-bottom:1px solid #f1f1f1;margin-bottom:%%?20rpx?%%;color:#333}.speak .weui-article__h3{color:#333}.speak .weui-article__p{color:#666}.panal{z-index:999;position:relative}.panal .contentText{position:fixed;left:0;top:42px;right:0;bottom:0;width:70%;z-index:99;margin:0 auto;display:flex;justify-content:center;background:#fff;height:auto;font-size:%%?26rpx?%%}.panal .contentText .close{position:absolute;display:flex;justify-content:center;width:%%?50rpx?%%;height:%%?50rpx?%%;border-radius:50%;background:rgba(0,0,0,.5);bottom:%%?-120rpx?%%;color:rgba(255,255,255,.55);line-height:%%?50rpx?%%;text-align:center;border:1px solid rgba(255,255,255,.5)}.panal .contentText .line{position:absolute;display:flex;justify-content:center;height:%%?120rpx?%%;width:1px;background:rgba(255,255,255,.47);bottom:%%?-120rpx?%%}.panal .contentText .content{width:100%;display:block;z-index:999}.panal .contentText .father{width:100%;height:100%;font-size:%%?24rpx?%%}.panal .loading{position:fixed;width:100%;height:100%;background:rgba(0,0,0,.5);top:42px;left:0;z-index:10}.panal .weui-article__h3{font-size:%%?28rpx?%%;color:#656b6f;padding:%%?20rpx?%%;border-bottom:1px solid #f1f1f1}.panal .background{width:100%;height:100%;position:absolute;top:0;left:0;z-index:-1;overflow:hidden;border-radius:%%?20rpx?%%}.panal .background wx-image{height:100%;width:100%}.navcates{width:100%;height:%%?75rpx?%%;line-height:%%?75rpx?%%;display:flex;background-color:#fff;margin:%%?10rpx?%% 0;padding:%%?5rpx?%% %%?12rpx?%%;font-size:%%?28rpx?%%;color:#4b4b4b;border-left:4px solid #3a97fe;box-shadow:0 0 2px #fff;overflow:hidden;box-sizing:border-box}.navcates wx-text{flex:1}.navcates .left{flex:4;text-align:left;letter-spacing:%%?3rpx?%%}.navcates .left .name{display:inline-block;position:relative;top:%%?-1rpx?%%;left:%%?5rpx?%%;width:100%}.navcates .left .icon{display:inline-block;position:relative;top:%%?7rpx?%%}.navcates .left .icon wx-image{width:%%?40rpx?%%;height:%%?40rpx?%%}.navcates .right{flex:2;text-align:right;line-height:37px}.navcates .right .rtext{position:relative;top:%%?-12rpx?%%}.navcates .right wx-image{height:%%?45rpx?%%;width:%%?45rpx?%%;padding-top:%%?17.5rpx?%%}.noborder{border-left:none}.billboard .bgcs{position:fixed;width:100%;height:100%;padding:0;margin:0;top:42px;left:0;z-index:-1}.billboard .content{z-index:10;position:relative;width:98%;margin:0 auto}.billboard .content .item .tablist{display:block;height:%%?180rpx?%%}.billboard .content .item .tablist .listItem{width:25%;height:100%;padding:%%?5rpx?%%;box-sizing:border-box;float:left;margin-bottom:%%?10rpx?%%}.billboard .content .item .tablist .listItem .itemshow{background:#fff;height:100%;display:flex;position:relative;overflow:hidden}.billboard .content .item .tablist .listItem .itemshow .left{width:100%;height:100%}.billboard .content .item .tablist .listItem .itemshow .left .hd{width:100%;height:100%}.billboard .content .item .tablist .listItem .itemshow .icon{width:%%?47rpx?%%}.billboard .content .item .tablist .listItem .itemshow .icon wx-image{width:%%?45rpx?%%;height:%%?45rpx?%%}.billboard .content .item .tablist .listItem .itemshow .tag{text-align:center;line-height:%%?30rpx?%%;position:absolute;top:%%?5rpx?%%;right:%%?5rpx?%%;color:#fff;border-radius:%%?10rpx?%%;font-size:%%?22rpx?%%;padding:%%?5rpx?%%}.billboard .content .item .tablist .listItem .itemshow .tag wx-image{position:absolute;top:%%?-5rpx?%%;right:%%?5rpx?%%;width:%%?45rpx?%%;height:%%?60rpx?%%}.billboard .content .item .tablist .listItem .itemshow .nameline{height:%%?45rpx?%%;line-height:%%?45rpx?%%;background:rgba(0,0,0,.6);color:#ddd9d0;font-size:%%?22rpx?%%;position:absolute;bottom:0;width:100%;padding:0 %%?10rpx?%%;text-align:center}.billboard .content .item .tablist .listItem .itemshow .zan{position:absolute;top:%%?10rpx?%%;left:%%?10rpx?%%;font-size:%%?22rpx?%%;color:#fff;text-shadow:0 0 2px #000}.billboard .content .item .tablist .listItem .itemshow .opa5{background:rgba(0,0,0,.5);position:absolute;width:100%;height:100%;top:0;left:0;text-align:center;padding-top:40%;color:#f2f2f2;font-size:%%?28rpx?%%}.billboard .content .item .tablist .listItem .itemshow .weui-progress{position:absolute;bottom:30%;width:80%;margin:0 auto;text-align:center;left:12%;opacity:1}.billboard .content .item .tablist .listItem .itemshow .weui-progress .pretext{color:#fff;font-size:%%?24rpx?%%;margin-left:%%?5rpx?%%}.billboard .content .item .title{display:flex;height:%%?80rpx?%%;color:#fff;margin:%%?20rpx?%% auto;justify-content:center;line-height:%%?80rpx?%%;font-size:%%?36rpx?%%}.billboard .content .item .title .name{padding:0 %%?40rpx?%%;background:rgba(241,241,241,.21)}.panal .PanContent{width:100%;display:block}.panal .PanContent .weui-cell__bd{color:#8b8f93}.panal .PanContent .weui-label{color:#8b8f93}.panal .PanContent .weui-input{text-align:right}.panal .PanContent .weui-cells::after{border:none}@code-separator-line:__wxRoute = "pages/index/billboard";__wxRouteBegin = true;
define("pages/index/billboard.js", function(require, module, exports, window,document,frames,self,location,navigator,localStorage,history,Caches,screen,alert,confirm,prompt,fetch,XMLHttpRequest,WebSocket,webkit,WeixinJSCore,ServiceJSBridge,Reporter){
"use strict";function _interopRequireDefault(e){return e&&e.__esModule?e:{default:e}}function _asyncToGenerator(e){return function(){var t=e.apply(this,arguments);return new Promise(function(e,n){function r(i,a){try{var o=t[i](a),s=o.value}catch(e){return void n(e)}if(!o.done)return Promise.resolve(s).then(function(e){r("next",e)},function(e){r("throw",e)});e(s)}return r("next")})}}function _classCallCheck(e,t){if(!(e instanceof t))throw new TypeError("Cannot call a class as a function")}function _possibleConstructorReturn(e,t){if(!e)throw new ReferenceError("this hasn't been initialised - super() hasn't been called");return!t||"object"!=typeof t&&"function"!=typeof t?e:t}function _inherits(e,t){if("function"!=typeof t&&null!==t)throw new TypeError("Super expression must either be null or a function, not "+typeof t);e.prototype=Object.create(t&&t.prototype,{constructor:{value:e,enumerable:!1,writable:!0,configurable:!0}}),t&&(Object.setPrototypeOf?Object.setPrototypeOf(e,t):e.__proto__=t)}Object.defineProperty(exports,"__esModule",{value:!0});var _createClass=function(){function e(e,t){for(var n=0;n<t.length;n++){var r=t[n];r.enumerable=r.enumerable||!1,r.configurable=!0,"value"in r&&(r.writable=!0),Object.defineProperty(e,r.key,r)}}return function(t,n,r){return n&&e(t.prototype,n),r&&e(t,r),t}}(),_wepy=require("./../../npm/wepy/lib/wepy.js"),_wepy2=_interopRequireDefault(_wepy),_util=require("./../../tools/util.js"),_navcate=require("./../../base/navcate.js"),_navcate2=_interopRequireDefault(_navcate),_panal=require("./../../base/panal.js"),_panal2=_interopRequireDefault(_panal),_request=require("./../../api/request.js"),_tip=require("./../../tools/tip.js"),_tip2=_interopRequireDefault(_tip),_speak=require("./../../base/speak.js"),_speak2=_interopRequireDefault(_speak),_msg=require("./../../base/tips/msg.js"),_msg2=_interopRequireDefault(_msg),_set=require("./../../tools/set.js"),_set2=_interopRequireDefault(_set),IndexBillboard=function(e){function t(){var e,n,r,i;_classCallCheck(this,t);for(var a=arguments.length,o=Array(a),s=0;s<a;s++)o[s]=arguments[s];return n=r=_possibleConstructorReturn(this,(e=t.__proto__||Object.getPrototypeOf(t)).call.apply(e,[this].concat(o))),r.config={navigationBarTitleText:"天天爱打榜",backgroundTextStyle:"light",navigationBarBackgroundColor:"#45afd9"},r.$props={panal:{height:"380",class:"panal"}},r.$events={},r.components={navcate:_navcate2.default,panal:_panal2.default,speak:_speak2.default,msg:_msg2.default},r.data={sysInfo:{},bdlist:{},form:{start:"",time:"",pos:"",uid:"",price:"",money:"",times:24,zindex:"首榜",bid:""},zindexChange:0,btnName:{text:"我要上榜",className:""},config:[]},r.computed={},r.watch={zindexChange:function(){var e=this.zindexChange.replace(/[^0-9]+/g,"");"首榜"==this.zindexChange&&(e=1);var t=this.bdlist[e-1];this.form.money=t.money*this.form.times,this.$apply()}},r.methods={GoHome:function(e){var t=(0,_util.get)(e,"id");(0,_util.href)("home/index?uid="+t+"&come=bill")},noBodoy:function(e){this.form.pos=(0,_util.get)(e,"index"),this.form.uid=(0,_util.get)(e,"uid"),this.form.price=(0,_util.get)(e,"money"),this.form.bid=(0,_util.get)(e,"bid"),this.form.money=this.form.price*this.form.times,this.btnName={text:"我要上榜",className:""},this.$apply(),this.updateBD()},choseTime:function(){var e=["3小时","6小时","12小时","24小时","48小时"],t=this;wx.showActionSheet({itemList:e,success:function(n){n.cancel||(t.form.times=parseInt(e[n.tapIndex]),t.form.money=t.form.times*t.form.price,t.$apply())}})},chosePosition:function(){for(var e=[],t=1;t<7;t++)1==t?e.push("首榜"):e.push("第"+t+"榜");var n=this;wx.showActionSheet({itemList:e,success:function(t){t.cancel||(n.form.zindex=e[t.tapIndex],n.zindexChange=n.form.zindex,n.$apply())}})},NavcateRightClick:function(){this.$broadcast("layerMsg","请在榜单区域选择坐席进入榜单",2500)}},r.events={BasePnalOpen:function(e){console.log(e)}},i=n,_possibleConstructorReturn(r,i)}return _inherits(t,e),_createClass(t,[{key:"BdformSubmit",value:function(){function e(e){return t.apply(this,arguments)}var t=_asyncToGenerator(regeneratorRuntime.mark(function e(t){var n=this;return regeneratorRuntime.wrap(function(e){for(;;)switch(e.prev=e.next){case 0:this.form.formId=t.detail.formId,(0,_request.requrst)("Billboard/UploadBd",this.form,function(e){n.btnName.text=e.msg,n.btnName.className="shake",setTimeout(function(){e.code>0?(n.getData(),n.updateBD(!1)):-10==e.code&&_tip2.default.confirm("您可以选择充值或者每日签到获取大量,也可以直接使用豌豆充值兑换!",function(e){e&&(0,_util.href)("money/index")},"充值提醒")},1500),n.$apply()});case 2:case"end":return e.stop()}},e,this)}));return e}()},{key:"bindDateChange",value:function(){function e(e){return t.apply(this,arguments)}var t=_asyncToGenerator(regeneratorRuntime.mark(function e(t){return regeneratorRuntime.wrap(function(e){for(;;)switch(e.prev=e.next){case 0:this.form.start=t.detail.value,this.$apply();case 2:case"end":return e.stop()}},e,this)}));return e}()},{key:"bindTimeChange",value:function(){function e(e){return t.apply(this,arguments)}var t=_asyncToGenerator(regeneratorRuntime.mark(function e(t){return regeneratorRuntime.wrap(function(e){for(;;)switch(e.prev=e.next){case 0:this.form.time=t.detail.value,this.$apply();case 2:case"end":return e.stop()}},e,this)}));return e}()},{key:"updateBD",value:function(){function e(){return t.apply(this,arguments)}var t=_asyncToGenerator(regeneratorRuntime.mark(function e(){var t=!(arguments.length>0&&void 0!==arguments[0])||arguments[0];return regeneratorRuntime.wrap(function(e){for(;;)switch(e.prev=e.next){case 0:this.$broadcast("BasePnalHeight",360,t,"");case 1:case"end":return e.stop()}},e,this)}));return e}()},{key:"getData",value:function(){function e(){return t.apply(this,arguments)}var t=_asyncToGenerator(regeneratorRuntime.mark(function e(){var t=this;return regeneratorRuntime.wrap(function(e){for(;;)switch(e.prev=e.next){case 0:(0,_request.requrst)("Billboard/getList",{},function(e){t.bdlist=e.info.list,t.config=e.info.config,_set2.default.setTop(t.config.bgc),t.$apply()});case 1:case"end":return e.stop()}},e,this)}));return e}()},{key:"onShow",value:function(){this.getData()}},{key:"onLoad",value:function(){this.sysInfo=(0,_util.sysInfo)();var e=(0,_util.nowTime)();this.form.start=e.y,this.form.time=e.s,this.$apply()}}]),t}(_wepy2.default.page);Page(require("./../../npm/wepy/lib/wepy.js").default.$createPage(IndexBillboard,"pages/index/billboard"));
});require("pages/index/billboard.js")@code-separator-line:["div","view","image","text","progress","form","picker","button"]