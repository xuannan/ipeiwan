/*v0.6vv_20170214_fbi*/
window.__wcc_version__='v0.6vv_20170214_fbi'
window.__wxml_transpiler_version__='v0.1'
var $gwxc
var $gaic={}
$gwx=function(path,global){
function _(a,b){b&&a.children.push(b);}
function _v(k){if(typeof(k)!='undefined')return {tag:'virtual','wxKey':k,children:[]};return {tag:'virtual',children:[]};}
function _n(tag){$gwxc++;if($gwxc>=16000){throw 'Dom limit exceeded, please check if there\'s any mistake you\'ve made.'};return {tag:tag.substr(0,3)=='wx-'?tag:'wx-'+tag,attr:{},children:[],n:[]}}
function _p(a,b){b&&a.properities.push(b);}
function _s(scope,env,key){return typeof(scope[key])!='undefined'?scope[key]:env[key]}function _wl(tname,prefix){console.warn('WXMLRT:'+prefix+':-1:-1:-1: Template `' + tname + '` is being called recursively, will be stop.')}$gwn=console.warn;
$gwl=console.log;
function $gwh()
{
function x(){}
x.prototype =
{
hn: function( obj )
{
if( typeof(obj) == 'object' )
{
var cnt=0;
var any=false;
for(var x in obj)
{
any|=x==='__value__';
cnt++;
if(cnt>2)break;
}
return cnt == 2 && any && obj.hasOwnProperty('__wxspec__') ? "h" : "n";
}
return "n";
},
nh: function( obj, special )
{
return { __value__: obj, __wxspec__: special ? special : true }
},
rv: function( obj )
{
return this.hn(obj)==='n'?obj:this.rv(obj.__value__);
}
}
return new x;
}
wh=$gwh();
function $gwrt( should_pass_type_info )
{
function ArithmeticEv( ops, e, s, g, o )
{
var rop = ops[0][1];
var _a,_b,_c,_d, _aa, _bb;
switch( rop )
{
case '?:':
_a = rev( ops[1], e, s, g, o );
_c = should_pass_type_info && ( wh.hn(_a) === 'h' );
_d = wh.rv( _a ) ? rev( ops[2], e, s, g, o ) : rev( ops[3], e, s, g, o );
_d = _c && wh.hn( _d ) === 'n' ? wh.nh( _d, 'c' ) : _d;
return _d;
break;
case '&&':
_a = rev( ops[1], e, s, g, o );
_c = should_pass_type_info && ( wh.hn(_a) === 'h' );
_d = wh.rv( _a ) ? rev( ops[2], e, s, g, o ) : wh.rv( _a );
_d = _c && wh.hn( _d ) === 'n' ? wh.nh( _d, 'c' ) : _d;
return _d;
break;
case '||':
_a = rev( ops[1], e, s, g, o );
_c = should_pass_type_info && ( wh.hn(_a) === 'h' );
_d = wh.rv( _a ) ? wh.rv(_a) : rev( ops[2], e, s, g, o );
_d = _c && wh.hn( _d ) === 'n' ? wh.nh( _d, 'c' ) : _d;
return _d;
break;
case '+':
case '*':
case '/':
case '%':
case '|':
case '^':
case '&':
case '===':
case '==':
case '!=':
case '!==':
case '>=':
case '<=':
case '>':
case '<':
case '<<':
case '>>':
_a = rev( ops[1], e, s, g, o );
_b = rev( ops[2], e, s, g, o );
_c = should_pass_type_info && (wh.hn( _a ) === 'h' || wh.hn( _b ) === 'h');
switch( rop )
{
case '+':
_d = wh.rv( _a ) + wh.rv( _b );
break;
case '*':
_d = wh.rv( _a ) * wh.rv( _b );
break;
case '/':
_d = wh.rv( _a ) / wh.rv( _b );
break;
case '%':
_d = wh.rv( _a ) % wh.rv( _b );
break;
case '|':
_d = wh.rv( _a ) | wh.rv( _b );
break;
case '^':
_d = wh.rv( _a ) ^ wh.rv( _b );
break;
case '&':
_d = wh.rv( _a ) & wh.rv( _b );
break;
case '===':
_d = wh.rv( _a ) === wh.rv( _b );
break;
case '==':
_d = wh.rv( _a ) == wh.rv( _b );
break;
case '!=':
_d = wh.rv( _a ) != wh.rv( _b );
break;
case '!==':
_d = wh.rv( _a ) !== wh.rv( _b );
break;
case '>=':
_d = wh.rv( _a ) >= wh.rv( _b );
break;
case '<=':
_d = wh.rv( _a ) <= wh.rv( _b );
break;
case '>':
_d = wh.rv( _a ) > wh.rv( _b );
break;
case '<':
_d = wh.rv( _a ) < wh.rv( _b );
break;
case '<<':
_d = wh.rv( _a ) << wh.rv( _b );
break;
case '>>':
_d = wh.rv( _a ) >> wh.rv( _b );
break;
default:
break;
}
return _c ? wh.nh( _d, "c" ) : _d;
break;
case '-':
_a = ops.length === 3 ? rev( ops[1], e, s, g, o ) : 0;
_b = ops.length === 3 ? rev( ops[2], e, s, g, o ) : rev( ops[1], e, s, g, o );
_c = should_pass_type_info && (wh.hn( _a ) === 'h' || wh.hn( _b ) === 'h');
_d = _c ? wh.rv( _a ) - wh.rv( _b ) : _a - _b;
return _c ? wh.nh( _d, "c" ) : _d;
break;
case '!':
_a = rev( ops[1], e, s, g, o );
_c = should_pass_type_info && (wh.hn( _a ) == 'h');
_d = !wh.rv(_a);
return _c ? wh.nh( _d, "c" ) : _d;
case '~':
_a = rev( ops[1], e, s, g, o );
_c = should_pass_type_info && (wh.hn( _a ) == 'h');
_d = ~wh.rv(_a);
return _c ? wh.nh( _d, "c" ) : _d;
default:
$gwn('unrecognized op' + rop );
}
}
function rev( ops, e, s, g, o )
{
var op = ops[0];
if( typeof(op)==='object' )
{
var vop=op[0];
var _a, _aa, _b, _bb, _c, _d, _s, _e, _ta, _tb, _td;
switch(vop)
{
case 2:
return ArithmeticEv(ops,e,s,g,o);
break;
case 4:
return rev( ops[1], e, s, g, o );
break;
case 5:
switch( ops.length )
{
case 2:
return should_pass_type_info ?
[rev(ops[1],e,s,g,o)] :
[wh.rv(rev(ops[1],e,s,g,o))];
break;
case 1:
return [];
break;
default:
_a = rev( ops[1],e,s,g,o );
_a.push(
should_pass_type_info ?
rev( ops[2],e,s,g,o ) :
wh.rv( rev(ops[2],e,s,g,o) )
);
return _a;
break;
}
break;
case 6:
_a = rev(ops[1],e,s,g,o);
_ta = wh.hn(_a)==='h';
_aa = _ta ? wh.rv(_a) : _a;
o.is_affected |= _ta;
if( should_pass_type_info )
{
if( _aa===null || typeof(_aa) === 'undefined' )
{
return _ta ? wh.nh(undefined, 'e') : undefined;
}
_b = rev(ops[2],e,s,g,o);
_tb = wh.hn(_b) === 'h';
_bb = _tb ? wh.rv(_b) : _b;
o.is_affected |= _tb;
if( _bb===null || typeof(_bb) === 'undefined' )
{
return (_ta || _tb) ? wh.nh(undefined, 'e') : undefined;
}
_d = _aa[_bb];
_td = wh.hn(_d)==='h';
o.is_affected |= _td;
return (_ta || _tb) ? (_td ? _d : wh.nh(_d, 'e')) : _d;
}
else
{
if( _aa===null || typeof(_aa) === 'undefined' )
{
return undefined;
}
_b = rev(ops[2],e,s,g,o);
_tb = wh.hn(_b) === 'h';
_bb = _tb ? wh.rv(_b) : _b;
o.is_affected |= _tb;
if( _bb===null || typeof(_bb) === 'undefined' )
{
return undefined;
}
_d = _aa[_bb];
_td = wh.hn(_d)==='h';
o.is_affected |= _td;
return _td ? wh.rv(_d) : _d;
}
case 7:
switch(ops[1][0])
{
case 11:
o.is_affected |= wh.hn(g)==='h';
return g;
case 3:
_s = wh.rv( s );
_e = wh.rv( e );
_b = ops[1][1];
_a = _s && _s.hasOwnProperty(_b) ?
s : _e && ( _e.hasOwnProperty(_b) ? e : undefined );
if( should_pass_type_info )
{
if( _a )
{
_ta = wh.hn(_a) === 'h';
_aa = _ta ? wh.rv( _a ) : _a;
_d = _aa[_b];
_td = wh.hn(_d) === 'h';
o.is_affected |= _ta || _td;
_d = _ta && !_td ? wh.nh(_d,'e') : _d;
return _d;
}
}
else
{
if( _a )
{
_ta = wh.hn(_a) === 'h';
_aa = _ta ? wh.rv( _a ) : _a;
_d = _aa[_b];
_td = wh.hn(_d) === 'h';
o.is_affected |= _ta || _td;
return wh.rv(_d);
}
}
return undefined;
}
break;
case 8:
_a = {};
_a[ops[1]] = rev(ops[2],e,s,g,o);
return _a;
break;
case 9:
_a = rev(ops[1],e,s,g,o);
_b = rev(ops[2],e,s,g,o);
function merge( _a, _b, _ow )
{
_ta = wh.hn(_a)==='h';
_tb = wh.hn(_b)==='h';
_aa = wh.rv(_a);
_bb = wh.rv(_b);
if( should_pass_type_info )
{
if( _tb )
{
for(var k in _bb)
{
if ( _ow || !_aa.hasOwnProperty(k) )
_aa[k]=wh.nh(_bb[k],'e');
}
}
else
{
for(var k in _bb)
{
if ( _ow || !_aa.hasOwnProperty(k) )
_aa[k]=_bb[k];
}
}
}
else
{
for(var k in _bb)
{
if ( _ow || _aa.hasOwnProperty(k) )
_aa[k]=wh.rv(_bb[k]);
}
}
return _a;
}
var _c = _a
var _ow = true
if ( typeof(ops[1][0]) === "object" && ops[1][0][0] === 10 ) {
_a = _b
_b = _c
_ow = false
}
if ( typeof(ops[1][0]) === "object" && ops[1][0][0] === 10 ) {
var _r = {}
return merge( merge( _r, _a, _ow ), _b, _ow );
}
else
return merge( _a, _b, _ow );
break;
case 10:
return should_pass_type_info ? rev(ops[1],e,s,g,o) : wh.rv(rev(ops[1],e,s,g,o));
}
}
else
{
if( op === 3 || op === 1 ) return ops[1];
else if( op === 11 )
{
var _a='';
for( var i = 1 ; i < ops.length ; i++ )
{
var xp = wh.rv(rev(ops[i],e,s,g,o));
_a += typeof(xp) === 'undefined' ? '' : xp;
}
return _a;
}
}
}
return rev;
}
gra=$gwrt(true);
grb=$gwrt(false);
function TestTest( expr, ops, e,s,g, expect_a, expect_b, expect_affected )
{
{
var o = {is_affected:false};
var a = gra( ops, e,s,g, o );
if( JSON.stringify(a) != JSON.stringify( expect_a )
|| o.is_affected != expect_affected )
{
console.warn( "A. " + expr + " get result " + JSON.stringify(a) + ", " + o.is_affected + ", but " + JSON.stringify( expect_a ) + ", " + expect_affected + " is expected" );
}
}
{
var o = {is_affected:false};
var a = grb( ops, e,s,g, o );
if( JSON.stringify(a) != JSON.stringify( expect_b )
|| o.is_affected != expect_affected )
{
console.warn( "B. " + expr + " get result " + JSON.stringify(a) + ", " + o.is_affected + ", but " + JSON.stringify( expect_b ) + ", " + expect_affected + " is expected" );
}
}
}

function wfor( to_iter, func, env, _s, global, father, itemname, indexname, keyname )
{
var _n = wh.hn( to_iter ) === 'n';
var scope = wh.rv( _s );
var has_old_item = scope.hasOwnProperty(itemname);
var has_old_index = scope.hasOwnProperty(indexname);
var old_item = scope[itemname];
var old_index = scope[indexname];
var full = Object.prototype.toString.call(wh.rv(to_iter));
var type = full[8];
if( type === 'N' && full[10] === 'l' ) type = 'X';
var _y;
if( _n )
{
if( type === 'A' )
{
for( var i = 0 ; i < to_iter.length ; i++ )
{
scope[itemname] = to_iter[i];
scope[indexname] = wh.nh(i, 'h');
_y = keyname ? (keyname==="*this" ? _v(wh.rv(to_iter[i])) : _v(wh.rv(wh.rv(to_iter[i])[keyname]))) : _v();
_(father,_y);
func( env, scope, _y, global );
}
}
else if( type === 'O' )
{
for( var k in to_iter )
{
scope[itemname] = to_iter[k];
scope[indexname] = wh.nh(k, 'h');
_y = keyname ? (keyname==="*this" ? _v(wh.rv(to_iter[k])) : _v(wh.rv(wh.rv(to_iter[k])[keyname]))) : _v();
_(father,_y);
func( env,scope,_y,global );
}
}
else if( type === 'S' )
{
for( var i = 0 ; i < to_iter.length ; i++ )
{
scope[itemname] = to_iter[i];
scope[indexname] = wh.nh(i, 'h');
_y = _v( to_iter[i] + i );
_(father,_y);
func( env, scope, _y, global );
}
}
else if( type === 'N' )
{
for( var i = 0 ; i < to_iter ; i++ )
{
scope[itemname] = i;
scope[indexname] = wh.nh(i, 'h');
_y = _v( i );
_(father,_y);
func(env,scope,_y,global);
}
}
else
{
}
}
else
{
var r_to_iter = wh.rv(to_iter);
var r_iter_item, iter_item;
if( type === 'A' )
{
for( var i = 0 ; i < r_to_iter.length ; i++ )
{
iter_item = r_to_iter[i];
iter_item = wh.hn(iter_item)==='n' ? wh.nh(iter_item,'h') : iter_item;
r_iter_item = wh.rv( iter_item );
scope[itemname] = iter_item
scope[indexname] = wh.nh(i, 'h');
_y = keyname ? (keyname==="*this" ? _v(r_iter_item) : _v(wh.rv(r_iter_item[keyname]))) : _v();
_(father,_y);
func( env, scope, _y, global );
}
}
else if( type === 'O' )
{
for( var k in r_to_iter )
{
iter_item = r_to_iter[k];
iter_item = wh.hn(iter_item)==='n'? wh.nh(iter_item,'h') : iter_item;
r_iter_item = wh.rv( iter_item );
scope[itemname] = iter_item;
scope[indexname] = wh.nh(k, 'h');
_y = keyname ? (keyname==="*this" ? _v(r_iter_item) : _v(wh.rv(r_iter_item[keyname]))) : _v();
_(father,_y);
func( env, scope, _y, global );
}
}
else if( type === 'S' )
{
for( var i = 0 ; i < r_to_iter.length ; i++ )
{
scope[itemname] = wh.nh(r_to_iter[i],'h');
scope[indexname] = wh.nh(i, 'h');
_y = _v( to_iter[i] + i );
_(father,_y);
func( env, scope, _y, global );
}
}
else if( type === 'N' )
{
for( var i = 0 ; i < r_to_iter ; i++ )
{
scope[itemname] = wh.nh(i,'h');
scope[indexname]= wh.nh(i,'h');
_y = _v( i );
_(father,_y);
func(env,scope,_y,global);
}
}
else
{
}
}
if(has_old_item)
{
scope[itemname]=old_item;
}
else
{
delete scope[itemname];
}
if(has_old_index)
{
scope[indexname]=old_index;
}
else{delete scope[indexname];}}


function _r( node, attrname, opindex, env, scope, global )
{
var o = {};
var a = grb( z[opindex], env, scope, global, o );
node.attr[attrname] = a;
if( o.is_affected ) node.n.push( attrname );
}
function _o( opindex, env, scope, global )
{
var nothing = {};
return grb( z[opindex], env, scope, global, nothing );
}
function _1( opindex, env, scope, global )
{
var nothing = {};
return gra( z[opindex], env, scope, global, nothing );
}
function _2( opindex, func, env, scope, global, father, itemname, indexname, keyname )
{
var to_iter = _1( opindex, env, scope, global, father, itemname, indexname, keyname );
wfor( to_iter, func, env, scope, global, father, itemname, indexname, keyname );
}
function _gv( )
{
if( typeof(window.__webview_engine_version__) == 'undefined' ) return 0.0;
return window.__webview_engine_version__;
}
function _m(tag,attrs,env,scope,global)
{
var tmp=_n(tag);
var base=0;
for(var i = 0 ; i < attrs.length ; i+=2 )
{
if(attrs[i+1]<0)
{tmp.attr[attrs[i]]=true;}else{_r(tmp,attrs[i],base+attrs[i+1],env,scope,global);
if(base===0)base=attrs[i+1];}}return tmp;};function _ai(i,p,e,me,r,c){var x=_grp(p,e,me);if(x)i.push(x);else{i.push('');console.warn('WXMLRT:'+me+':import:'+r+':'+c+': Path `'+p+'` not found from `'+me+'`.')}}function _grp(p,e,me){if(p[0]!='/'){var mepart=me.split('/');mepart.pop();var ppart=p.split('/');for(var i=0;i<ppart.length;i++){if(ppart[i]=='..')mepart.pop();else if(ppart[i]=='.' || !ppart[i])continue;else mepart.push(ppart[i]);}p=mepart.join('/');}if(me[0]=='.'&&p[0]=='/')p='.'+p;if(e[p])return p;if(e[p+'.wxml'])return p+'.wxml';}function _gd(p,c,e,d){if(!c)return;if(d[p][c])return d[p][c];for(var x=e[p].i.length-1;x>=0;x--){if(e[p].i[x]&&d[e[p].i[x]][c])return d[e[p].i[x]][c]};for(var x=e[p].ti.length-1;x>=0;x--){var q=_grp(e[p].ti[x],e,p);if(q&&d[q][c])return d[q][c]}
var ii=_gapi(e,p);for(var x=0;x<ii.length;x++){if(ii[x]&&d[ii[x]][c])return d[ii[x]][c]}for(var k=e[p].j.length-1;k>=0;k--)if(e[p].j[k]){for(var q=e[e[p].j[k]].ti.length-1;q>=0;q--){var pp=_grp(e[e[p].j[k]].ti[q],e,p);if(pp&&d[pp][c]){return d[pp][c]}}}}function _gapi(e,p){if(!p)return [];if($gaic[p]){return $gaic[p]};var ret=[],q=[],h=0,t=0,put={},visited={};q.push(p);visited[p]=true;t++;while(h<t){var a=q[h++];for(var i=0;i<e[a].ic.length;i++){var nd=e[a].ic[i];var np=_grp(nd,e,a);if(np&&!visited[np]){visited[np]=true;q.push(np);t++;}}for(var i=0;a!=p&&i<e[a].ti.length;i++){var ni=e[a].ti[i];var nm=_grp(ni,e,a);if(nm&&!put[nm]){put[nm]=true;ret.push(nm);}}}$gaic[p]=ret;return ret;}var $ixc={};function _ic(p,ent,me,e,s,r,gg){var x=_grp(p,ent,me);ent[me].j.push(x);if(x){if($ixc[x]){console.warn('WXMLRT:-1:include:-1:-1: `'+p+'` is being included in a loop, will be stop.');return;}$ixc[x]=true;try{ent[x].f(e,s,r,gg)}catch(e){}$ixc[x]=false;}else{console.warn('WXMLRT:'+me+':include:-1:-1: Included path `'+p+'` not found from `'+me+'`.')}}function _w(tn,f,line,c){console.warn('WXMLRT:'+f+':template:'+line+':'+c+': Template `'+tn+'` not found.');}function _ev(dom){var changed=false;delete dom.properities;delete dom.n;if(dom.children){do{changed=false;var newch = [];for(var i=0;i<dom.children.length;i++){var ch=dom.children[i];if( ch.tag=='virtual'){changed=true;for(var j=0;ch.children&&j<ch.children.length;j++){newch.push(ch.children[j]);}}else { newch.push(ch); } } dom.children = newch; }while(changed);for(var i=0;i<dom.children.length;i++){_ev(dom.children[i]);}} return dom; }var e_={}
if(global&&typeof(global.entrys)=='object')e_=global.entrys
var d_={}
if(global&&typeof(global.defines)=='object')d_=global.defines
var p_={}
var z = [];
  (function(z){
    var a = 11;
    function Z(ops){z.push(ops)};
    Z([[7],[3, "show"]]);Z([3, 'send']);Z([3, 'page']);Z([3, 'page__hd']);Z([3, 'section']);Z([3, 'textarea']);Z([3, 'inputTab']);Z([3, '请输入您想说的话']);Z([3, 'page__bd']);Z([3, 'weui-cells']);Z([a, [3, 'navcates '],[[2,'?:'],[[2, "=="], [[7],[3, "$navcate$leftColor"]], [1, 0]],[1, "noborder"],[1, ""]]]);Z([3, 'left']);Z([3, 'icon']);Z([[7],[3, "$navcate$icon"]]);Z([3, 'name']);Z([a, [[7],[3, "$navcate$title"]]]);Z([3, '$navcate$NavcateRight']);Z([3, 'right']);Z([3, 'rtext']);Z([a, [[7],[3, "$navcate$right"]]]);Z([3, 'http://img.he29.com/play/f4fe7786635ee901a46590b7f1926bc08d628bdf.png']);Z([3, 'weui-cell']);Z([3, 'weui-cell__bd']);Z([3, 'weui-uploader']);Z([3, 'weui-uploader__bd']);Z([3, 'weui-uploader__files']);Z([3, 'uploaderFiles']);Z([[7],[3, "uploadList"]]);Z([3, 'key']);Z([3, 'delImg']);Z([[7],[3, "index"]]);Z([3, 'http://img.he29.com/play/9a9d3c74892a3aafff658e192109cb879f4323cc.png']);Z([3, 'uploadShow']);Z([3, 'goRuns']);Z([[6],[[7],[3, "item"]],[3, "img"]]);Z([[6],[[7],[3, "item"]],[3, "up"]]);Z([3, '已推荐']);Z([3, '推荐']);Z([3, 'langUp']);Z([3, 'previewImage']);Z([3, 'weui-uploader__file']);Z([3, 'weui-uploader__img']);Z([3, 'aspectFill']);Z([[7],[3, "uploads"]]);Z([3, 'weui-uploader__file weui-uploader__file_status']);Z([3, 'weui-uploader__file-content']);Z([3, '上传中...']);Z([3, 'weui-uploader__input-box']);Z([3, 'chooseImage']);Z([3, 'weui-uploader__input']);Z([3, 'tips']);Z([3, '推荐的照片会显示的首页哦!']);Z([3, 'h10']);Z([3, 'btnGroup']);Z([3, 'sendActive']);Z([3, 'btnBgc']);Z([3, 'primary']);Z([3, '发布']);Z([3, 'quit']);Z([3, 'cancel']);Z([3, 'margin-top:15px']);Z([3, 'default']);Z([3, '取消']);Z([[7],[3, "$layer$open"]]);Z([3, 'loadmsg animated fadeIn']);Z([3, 'msg']);Z([3, 'layui-m-layercont']);Z([a, [[7],[3, "$layer$msg"]]]);
  })(z);d_["./pages/home/send.wxml"] = {};
  var m0=function(e,s,r,gg){
    var otu = _v();
      if (_o(0, e, s, gg)) {
        otu.wxVkey = 1;var ouu = _n("view");_r(ouu, 'class', 1, e, s, gg);var owu = _n("view");_r(owu, 'class', 2, e, s, gg);var oxu = _n("view");_r(oxu, 'class', 3, e, s, gg);var oyu = _n("view");_r(oyu, 'class', 4, e, s, gg);var ozu = _m( "textarea", ["autoFocus", -1,"bindinput", 5,"class", 1,"placeholder", 2], e, s, gg);_(oyu,ozu);_(oxu,oyu);_(owu,oxu);var o_u = _n("view");_r(o_u, 'class', 8, e, s, gg);var oAv = _n("view");_r(oAv, 'class', 9, e, s, gg);var oBv = _n("view");_r(oBv, 'class', 10, e, s, gg);var oCv = _n("view");_r(oCv, 'class', 11, e, s, gg);var oDv = _n("view");_r(oDv, 'class', 12, e, s, gg);var oEv = _v();
      if (_o(13, e, s, gg)) {
        oEv.wxVkey = 1;var oFv = _n("image");_r(oFv, 'src', 13, e, s, gg);_(oEv, oFv);
      } _(oDv,oEv);_(oCv,oDv);var oHv = _n("view");_r(oHv, 'class', 14, e, s, gg);var oIv = _o(15, e, s, gg);_(oHv,oIv);_(oCv,oHv);_(oBv,oCv);var oJv = _m( "view", ["bindtap", 16,"class", 1], e, s, gg);var oKv = _n("text");_r(oKv, 'class', 18, e, s, gg);var oLv = _o(19, e, s, gg);_(oKv,oLv);_(oJv,oKv);var oMv = _n("image");_r(oMv, 'src', 20, e, s, gg);_(oJv,oMv);_(oBv,oJv);_(oAv,oBv);var oNv = _n("view");_r(oNv, 'class', 21, e, s, gg);var oOv = _n("view");_r(oOv, 'class', 22, e, s, gg);var oPv = _n("view");_r(oPv, 'class', 23, e, s, gg);var oQv = _n("view");_r(oQv, 'class', 24, e, s, gg);var oRv = _m( "view", ["class", 25,"id", 1], e, s, gg);var oSv = _v();var oTv = function(oXv,oWv,oVv,gg){var oZv = _m( "image", ["bindtap", 29,"class", 0,"data-index", 1,"src", 2], oXv, oWv, gg);_(oVv,oZv);var oav = _m( "view", ["data-index", 30,"bindtap", 2,"class", 3,"data-url", 4], oXv, oWv, gg);var obv = _v();
      if (_o(35, oXv, oWv, gg)) {
        obv.wxVkey = 1;var ocv = _n("text");var oev = _o(36, oXv, oWv, gg);_(ocv,oev);_(obv, ocv);
      }else {
        obv.wxVkey = 2;var ofv = _n("text");var ohv = _o(37, e, s, gg);_(ofv,ohv);_(obv, ofv);
      }_(oav,obv);_(oVv,oav);var oiv = _m( "view", ["data-index", 30,"id", 4,"bindlongpress", 8,"bindtap", 9,"class", 10], oXv, oWv, gg);var ojv = _m( "image", ["src", 34,"class", 7,"mode", 8], oXv, oWv, gg);_(oiv,ojv);_(oVv,oiv);return oVv;};_2(27, oTv, e, s, gg, oSv, "item", "index", 'key');_(oRv,oSv);var okv = _v();
      if (_o(43, e, s, gg)) {
        okv.wxVkey = 1;var olv = _n("view");_r(olv, 'class', 44, e, s, gg);var onv = _m( "image", ["src", -1,"class", 41,"mode", 1], e, s, gg);_(olv,onv);var oov = _n("view");_r(oov, 'class', 45, e, s, gg);var opv = _o(46, e, s, gg);_(oov,opv);_(olv,oov);_(okv, olv);
      } _(oRv,okv);_(oQv,oRv);var oqv = _n("view");_r(oqv, 'class', 47, e, s, gg);var orv = _m( "view", ["bindtap", 48,"class", 1], e, s, gg);_(oqv,orv);_(oQv,oqv);_(oPv,oQv);_(oOv,oPv);_(oNv,oOv);_(oAv,oNv);var osv = _n("view");_r(osv, 'class', 50, e, s, gg);var otv = _o(51, e, s, gg);_(osv,otv);_(oAv,osv);_(o_u,oAv);_(owu,o_u);var ouv = _n("view");_r(ouv, 'class', 52, e, s, gg);_(owu,ouv);var ovv = _n("view");_r(ovv, 'class', 53, e, s, gg);var owv = _m( "button", ["bindtap", 54,"class", 1,"type", 2], e, s, gg);var oxv = _o(57, e, s, gg);_(owv,oxv);_(ovv,owv);var oyv = _n("div");_r(oyv, 'class', 52, e, s, gg);_(ovv,oyv);var ozv = _m( "button", ["bindtap", 58,"class", 1,"style", 2,"type", 3], e, s, gg);var o_v = _o(62, e, s, gg);_(ozv,o_v);_(ovv,ozv);_(owu,ovv);var oAw = _v();
      if (_o(63, e, s, gg)) {
        oAw.wxVkey = 1;var oBw = _n("view");_r(oBw, 'class', 64, e, s, gg);var oDw = _n("view");_r(oDw, 'class', 65, e, s, gg);var oEw = _n("text");_r(oEw, 'class', 66, e, s, gg);var oFw = _o(67, e, s, gg);_(oEw,oFw);_(oDw,oEw);_(oBw,oDw);_(oAw, oBw);
      } _(owu,oAw);_(ouu,owu);_(otu, ouu);
      } _(r,otu);
    return r;
  };
        e_["./pages/home/send.wxml"]={f:m0,j:[],i:[],ti:[],ic:[]};
if(path&&e_[path]){
window.__wxml_comp_version__=0.02
return function(env,dd,global){$gwxc=0;var root={"tag":"wx-page"};root.children=[]
var main=e_[path].f
if(typeof(window.__webview_engine_version__)!='undefined'&&window.__webview_engine_version__+1e-6>=0.02+1e-6&&window.__mergeData__)
{env=window.__mergeData__(env,dd);}
try{
main(env,{},root,global);
if(typeof(window.__webview_engine_version__)=='undefined'||window.__webview_engine_version__+1e-6<0.01+1e-6){return _ev(root);}}catch(err){console.log(err)}return root;}}}@code-separator-line:.msg{width:auto;max-width:80%;margin:0 auto;background-color:rgba(0,0,0,.7);color:#fff;display:inline-block;font-size:16px;border-radius:5px;box-shadow:0 0 8px rgba(0,0,0,.1);pointer-events:auto;-webkit-overflow-scrolling:touch;-webkit-animation-fill-mode:both;animation-fill-mode:both;-webkit-animation-duration:.2s;animation-duration:.2s;box-sizing:content-box;position:fixed;bottom:20%;z-index:999;text-align:center;padding:5px 10px}.layui-m-layercont{line-height:22px;text-align:center}.loadmsg{display:flex;justify-content:center;margin:0 auto;text-align:center}.navcates{width:100%;height:%%?75rpx?%%;line-height:%%?75rpx?%%;display:flex;background-color:#fff;margin:%%?10rpx?%% 0;padding:%%?5rpx?%% %%?12rpx?%%;font-size:%%?28rpx?%%;color:#4b4b4b;border-left:4px solid #3a97fe;box-shadow:0 0 2px #fff;overflow:hidden;box-sizing:border-box}.navcates wx-text{flex:1}.navcates .left{flex:4;text-align:left;letter-spacing:%%?3rpx?%%}.navcates .left .name{display:inline-block;position:relative;top:%%?-1rpx?%%;left:%%?5rpx?%%;width:100%}.navcates .left .icon{display:inline-block;position:relative;top:%%?7rpx?%%}.navcates .left .icon wx-image{width:%%?40rpx?%%;height:%%?40rpx?%%}.navcates .right{flex:2;text-align:right;line-height:37px}.navcates .right .rtext{position:relative;top:%%?-12rpx?%%}.navcates .right wx-image{height:%%?45rpx?%%;width:%%?45rpx?%%;padding-top:%%?17.5rpx?%%}.noborder{border-left:none}.send{height:100%;background:#fff}.send .inputTab{margin:%%?10rpx?%%;font-size:%%?28rpx?%%;color:#999;padding:%%?10rpx?%%;width:100%;box-sizing:border-box}.send .goRuns{position:absolute;display:inline-block;padding:0 12px;background:rgba(0,204,153,.75);color:#fff;border-radius:2px;width:79px;bottom:15px;text-align:center;box-sizing:border-box;opacity:.7}.send .delImg{position:absolute;left:79px;top:0;width:%%?30rpx?%%;height:%%?30rpx?%%;z-index:99;border:2px solid #fff;border-radius:50%;background:#fff}.send .tips{font-size:%%?24rpx?%%;color:rgba(101,107,111,.72);position:relative;top:-8px;left:12px}@code-separator-line:__wxRoute = "pages/home/send";__wxRouteBegin = true;
define("pages/home/send.js", function(require, module, exports, window,document,frames,self,location,navigator,localStorage,history,Caches,screen,alert,confirm,prompt,fetch,XMLHttpRequest,WebSocket,webkit,WeixinJSCore,ServiceJSBridge,Reporter){
"use strict";function _interopRequireDefault(e){return e&&e.__esModule?e:{default:e}}function _asyncToGenerator(e){return function(){var t=e.apply(this,arguments);return new Promise(function(e,n){function r(o,i){try{var a=t[o](i),s=a.value}catch(e){return void n(e)}if(!a.done)return Promise.resolve(s).then(function(e){r("next",e)},function(e){r("throw",e)});e(s)}return r("next")})}}function _classCallCheck(e,t){if(!(e instanceof t))throw new TypeError("Cannot call a class as a function")}function _possibleConstructorReturn(e,t){if(!e)throw new ReferenceError("this hasn't been initialised - super() hasn't been called");return!t||"object"!=typeof t&&"function"!=typeof t?e:t}function _inherits(e,t){if("function"!=typeof t&&null!==t)throw new TypeError("Super expression must either be null or a function, not "+typeof t);e.prototype=Object.create(t&&t.prototype,{constructor:{value:e,enumerable:!1,writable:!0,configurable:!0}}),t&&(Object.setPrototypeOf?Object.setPrototypeOf(e,t):e.__proto__=t)}Object.defineProperty(exports,"__esModule",{value:!0});var _createClass=function(){function e(e,t){for(var n=0;n<t.length;n++){var r=t[n];r.enumerable=r.enumerable||!1,r.configurable=!0,"value"in r&&(r.writable=!0),Object.defineProperty(e,r.key,r)}}return function(t,n,r){return n&&e(t.prototype,n),r&&e(t,r),t}}(),_wepy=require("./../../npm/wepy/lib/wepy.js"),_wepy2=_interopRequireDefault(_wepy),_navcate=require("./../../base/navcate.js"),_navcate2=_interopRequireDefault(_navcate),_request=require("./../../api/request.js"),_msg=require("./../../base/tips/msg.js"),_msg2=_interopRequireDefault(_msg),_util=require("./../../tools/util.js"),HomeSend=function(e){function t(){var e,n,r,o;_classCallCheck(this,t);for(var i=arguments.length,a=Array(i),s=0;s<i;s++)a[s]=arguments[s];return n=r=_possibleConstructorReturn(this,(e=t.__proto__||Object.getPrototypeOf(t)).call.apply(e,[this].concat(a))),r.config={navigationBarTitleText:"发布动态"},r.$props={navcate:{title:"照片上传",right:""}},r.$events={},r.components={navcate:_navcate2.default,layer:_msg2.default},r.data={uploadList:[],uploads:!1,text:"",show:!0},r.computed={},r.methods={delImg:function(e){var t=(0,_util.get)(e,"index");this.uploadList.splice(t,1),this.$apply()},langUp:function(e){console.log(e)},quit:function(){this.hideSend()},textarea:function(e){this.text=e.detail.value,this.$apply()},sendActive:function(){var e=this;if(!this.text)return this.$broadcast("layerMsg","请输入文字内容"),!1;(0,_request.requrst)("Active/addData",{imgs:JSON.stringify(this.uploadList),text:this.text},function(t){e.$broadcast("layerMsg",t.msg),t.code>0&&setTimeout(function(){e.hideSend(),e.uploadList=[],e.$apply()},3e3)})},uploadShow:function(e){var t=(0,_util.get)(e,"index");this.uploadList[t].up=!this.uploadList[t].up,console.log(this.uploadList),this.$apply()},chooseImage:function(e){var t=this;if(this.uploadList.length>=3)return tips.error("最多只能上传3张图片哦"),!1;wx.chooseImage({sizeType:["original","compressed"],sourceType:["album","camera"],success:function(e){var n=e.tempFilePaths[0];console.log(n),t.uploads=!0,(0,_request.fileUpload)("Upload/formData",n,{},function(e){e.code>0&&t.uploadList.push({img:e.info.url,up:!1}),t.uploads=!1,t.$apply()})}})},previewImage:function(e){wx.previewImage({current:e.currentTarget.id,urls:this.data.files})}},r.events={HomeSendShow:function(e){this.show=e}},o=n,_possibleConstructorReturn(r,o)}return _inherits(t,e),_createClass(t,[{key:"hideSend",value:function(){function e(){return t.apply(this,arguments)}var t=_asyncToGenerator(regeneratorRuntime.mark(function e(){return regeneratorRuntime.wrap(function(e){for(;;)switch(e.prev=e.next){case 0:this.$emit("HomeSendHide",!0);case 1:case"end":return e.stop()}},e,this)}));return e}()},{key:"onLoad",value:function(){}}]),t}(_wepy2.default.page);Page(require("./../../npm/wepy/lib/wepy.js").default.$createPage(HomeSend,"pages/home/send"));
});require("pages/home/send.js")@code-separator-line:["div","view","textarea","image","text","block","button"]