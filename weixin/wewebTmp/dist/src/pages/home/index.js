/*v0.6vv_20170214_fbi*/
window.__wcc_version__='v0.6vv_20170214_fbi'
window.__wxml_transpiler_version__='v0.1'
var $gwxc
var $gaic={}
$gwx=function(path,global){
function _(a,b){b&&a.children.push(b);}
function _v(k){if(typeof(k)!='undefined')return {tag:'virtual','wxKey':k,children:[]};return {tag:'virtual',children:[]};}
function _n(tag){$gwxc++;if($gwxc>=16000){throw 'Dom limit exceeded, please check if there\'s any mistake you\'ve made.'};return {tag:tag.substr(0,3)=='wx-'?tag:'wx-'+tag,attr:{},children:[],n:[]}}
function _p(a,b){b&&a.properities.push(b);}
function _s(scope,env,key){return typeof(scope[key])!='undefined'?scope[key]:env[key]}function _wl(tname,prefix){console.warn('WXMLRT:'+prefix+':-1:-1:-1: Template `' + tname + '` is being called recursively, will be stop.')}$gwn=console.warn;
$gwl=console.log;
function $gwh()
{
function x(){}
x.prototype =
{
hn: function( obj )
{
if( typeof(obj) == 'object' )
{
var cnt=0;
var any=false;
for(var x in obj)
{
any|=x==='__value__';
cnt++;
if(cnt>2)break;
}
return cnt == 2 && any && obj.hasOwnProperty('__wxspec__') ? "h" : "n";
}
return "n";
},
nh: function( obj, special )
{
return { __value__: obj, __wxspec__: special ? special : true }
},
rv: function( obj )
{
return this.hn(obj)==='n'?obj:this.rv(obj.__value__);
}
}
return new x;
}
wh=$gwh();
function $gwrt( should_pass_type_info )
{
function ArithmeticEv( ops, e, s, g, o )
{
var rop = ops[0][1];
var _a,_b,_c,_d, _aa, _bb;
switch( rop )
{
case '?:':
_a = rev( ops[1], e, s, g, o );
_c = should_pass_type_info && ( wh.hn(_a) === 'h' );
_d = wh.rv( _a ) ? rev( ops[2], e, s, g, o ) : rev( ops[3], e, s, g, o );
_d = _c && wh.hn( _d ) === 'n' ? wh.nh( _d, 'c' ) : _d;
return _d;
break;
case '&&':
_a = rev( ops[1], e, s, g, o );
_c = should_pass_type_info && ( wh.hn(_a) === 'h' );
_d = wh.rv( _a ) ? rev( ops[2], e, s, g, o ) : wh.rv( _a );
_d = _c && wh.hn( _d ) === 'n' ? wh.nh( _d, 'c' ) : _d;
return _d;
break;
case '||':
_a = rev( ops[1], e, s, g, o );
_c = should_pass_type_info && ( wh.hn(_a) === 'h' );
_d = wh.rv( _a ) ? wh.rv(_a) : rev( ops[2], e, s, g, o );
_d = _c && wh.hn( _d ) === 'n' ? wh.nh( _d, 'c' ) : _d;
return _d;
break;
case '+':
case '*':
case '/':
case '%':
case '|':
case '^':
case '&':
case '===':
case '==':
case '!=':
case '!==':
case '>=':
case '<=':
case '>':
case '<':
case '<<':
case '>>':
_a = rev( ops[1], e, s, g, o );
_b = rev( ops[2], e, s, g, o );
_c = should_pass_type_info && (wh.hn( _a ) === 'h' || wh.hn( _b ) === 'h');
switch( rop )
{
case '+':
_d = wh.rv( _a ) + wh.rv( _b );
break;
case '*':
_d = wh.rv( _a ) * wh.rv( _b );
break;
case '/':
_d = wh.rv( _a ) / wh.rv( _b );
break;
case '%':
_d = wh.rv( _a ) % wh.rv( _b );
break;
case '|':
_d = wh.rv( _a ) | wh.rv( _b );
break;
case '^':
_d = wh.rv( _a ) ^ wh.rv( _b );
break;
case '&':
_d = wh.rv( _a ) & wh.rv( _b );
break;
case '===':
_d = wh.rv( _a ) === wh.rv( _b );
break;
case '==':
_d = wh.rv( _a ) == wh.rv( _b );
break;
case '!=':
_d = wh.rv( _a ) != wh.rv( _b );
break;
case '!==':
_d = wh.rv( _a ) !== wh.rv( _b );
break;
case '>=':
_d = wh.rv( _a ) >= wh.rv( _b );
break;
case '<=':
_d = wh.rv( _a ) <= wh.rv( _b );
break;
case '>':
_d = wh.rv( _a ) > wh.rv( _b );
break;
case '<':
_d = wh.rv( _a ) < wh.rv( _b );
break;
case '<<':
_d = wh.rv( _a ) << wh.rv( _b );
break;
case '>>':
_d = wh.rv( _a ) >> wh.rv( _b );
break;
default:
break;
}
return _c ? wh.nh( _d, "c" ) : _d;
break;
case '-':
_a = ops.length === 3 ? rev( ops[1], e, s, g, o ) : 0;
_b = ops.length === 3 ? rev( ops[2], e, s, g, o ) : rev( ops[1], e, s, g, o );
_c = should_pass_type_info && (wh.hn( _a ) === 'h' || wh.hn( _b ) === 'h');
_d = _c ? wh.rv( _a ) - wh.rv( _b ) : _a - _b;
return _c ? wh.nh( _d, "c" ) : _d;
break;
case '!':
_a = rev( ops[1], e, s, g, o );
_c = should_pass_type_info && (wh.hn( _a ) == 'h');
_d = !wh.rv(_a);
return _c ? wh.nh( _d, "c" ) : _d;
case '~':
_a = rev( ops[1], e, s, g, o );
_c = should_pass_type_info && (wh.hn( _a ) == 'h');
_d = ~wh.rv(_a);
return _c ? wh.nh( _d, "c" ) : _d;
default:
$gwn('unrecognized op' + rop );
}
}
function rev( ops, e, s, g, o )
{
var op = ops[0];
if( typeof(op)==='object' )
{
var vop=op[0];
var _a, _aa, _b, _bb, _c, _d, _s, _e, _ta, _tb, _td;
switch(vop)
{
case 2:
return ArithmeticEv(ops,e,s,g,o);
break;
case 4:
return rev( ops[1], e, s, g, o );
break;
case 5:
switch( ops.length )
{
case 2:
return should_pass_type_info ?
[rev(ops[1],e,s,g,o)] :
[wh.rv(rev(ops[1],e,s,g,o))];
break;
case 1:
return [];
break;
default:
_a = rev( ops[1],e,s,g,o );
_a.push(
should_pass_type_info ?
rev( ops[2],e,s,g,o ) :
wh.rv( rev(ops[2],e,s,g,o) )
);
return _a;
break;
}
break;
case 6:
_a = rev(ops[1],e,s,g,o);
_ta = wh.hn(_a)==='h';
_aa = _ta ? wh.rv(_a) : _a;
o.is_affected |= _ta;
if( should_pass_type_info )
{
if( _aa===null || typeof(_aa) === 'undefined' )
{
return _ta ? wh.nh(undefined, 'e') : undefined;
}
_b = rev(ops[2],e,s,g,o);
_tb = wh.hn(_b) === 'h';
_bb = _tb ? wh.rv(_b) : _b;
o.is_affected |= _tb;
if( _bb===null || typeof(_bb) === 'undefined' )
{
return (_ta || _tb) ? wh.nh(undefined, 'e') : undefined;
}
_d = _aa[_bb];
_td = wh.hn(_d)==='h';
o.is_affected |= _td;
return (_ta || _tb) ? (_td ? _d : wh.nh(_d, 'e')) : _d;
}
else
{
if( _aa===null || typeof(_aa) === 'undefined' )
{
return undefined;
}
_b = rev(ops[2],e,s,g,o);
_tb = wh.hn(_b) === 'h';
_bb = _tb ? wh.rv(_b) : _b;
o.is_affected |= _tb;
if( _bb===null || typeof(_bb) === 'undefined' )
{
return undefined;
}
_d = _aa[_bb];
_td = wh.hn(_d)==='h';
o.is_affected |= _td;
return _td ? wh.rv(_d) : _d;
}
case 7:
switch(ops[1][0])
{
case 11:
o.is_affected |= wh.hn(g)==='h';
return g;
case 3:
_s = wh.rv( s );
_e = wh.rv( e );
_b = ops[1][1];
_a = _s && _s.hasOwnProperty(_b) ?
s : _e && ( _e.hasOwnProperty(_b) ? e : undefined );
if( should_pass_type_info )
{
if( _a )
{
_ta = wh.hn(_a) === 'h';
_aa = _ta ? wh.rv( _a ) : _a;
_d = _aa[_b];
_td = wh.hn(_d) === 'h';
o.is_affected |= _ta || _td;
_d = _ta && !_td ? wh.nh(_d,'e') : _d;
return _d;
}
}
else
{
if( _a )
{
_ta = wh.hn(_a) === 'h';
_aa = _ta ? wh.rv( _a ) : _a;
_d = _aa[_b];
_td = wh.hn(_d) === 'h';
o.is_affected |= _ta || _td;
return wh.rv(_d);
}
}
return undefined;
}
break;
case 8:
_a = {};
_a[ops[1]] = rev(ops[2],e,s,g,o);
return _a;
break;
case 9:
_a = rev(ops[1],e,s,g,o);
_b = rev(ops[2],e,s,g,o);
function merge( _a, _b, _ow )
{
_ta = wh.hn(_a)==='h';
_tb = wh.hn(_b)==='h';
_aa = wh.rv(_a);
_bb = wh.rv(_b);
if( should_pass_type_info )
{
if( _tb )
{
for(var k in _bb)
{
if ( _ow || !_aa.hasOwnProperty(k) )
_aa[k]=wh.nh(_bb[k],'e');
}
}
else
{
for(var k in _bb)
{
if ( _ow || !_aa.hasOwnProperty(k) )
_aa[k]=_bb[k];
}
}
}
else
{
for(var k in _bb)
{
if ( _ow || _aa.hasOwnProperty(k) )
_aa[k]=wh.rv(_bb[k]);
}
}
return _a;
}
var _c = _a
var _ow = true
if ( typeof(ops[1][0]) === "object" && ops[1][0][0] === 10 ) {
_a = _b
_b = _c
_ow = false
}
if ( typeof(ops[1][0]) === "object" && ops[1][0][0] === 10 ) {
var _r = {}
return merge( merge( _r, _a, _ow ), _b, _ow );
}
else
return merge( _a, _b, _ow );
break;
case 10:
return should_pass_type_info ? rev(ops[1],e,s,g,o) : wh.rv(rev(ops[1],e,s,g,o));
}
}
else
{
if( op === 3 || op === 1 ) return ops[1];
else if( op === 11 )
{
var _a='';
for( var i = 1 ; i < ops.length ; i++ )
{
var xp = wh.rv(rev(ops[i],e,s,g,o));
_a += typeof(xp) === 'undefined' ? '' : xp;
}
return _a;
}
}
}
return rev;
}
gra=$gwrt(true);
grb=$gwrt(false);
function TestTest( expr, ops, e,s,g, expect_a, expect_b, expect_affected )
{
{
var o = {is_affected:false};
var a = gra( ops, e,s,g, o );
if( JSON.stringify(a) != JSON.stringify( expect_a )
|| o.is_affected != expect_affected )
{
console.warn( "A. " + expr + " get result " + JSON.stringify(a) + ", " + o.is_affected + ", but " + JSON.stringify( expect_a ) + ", " + expect_affected + " is expected" );
}
}
{
var o = {is_affected:false};
var a = grb( ops, e,s,g, o );
if( JSON.stringify(a) != JSON.stringify( expect_b )
|| o.is_affected != expect_affected )
{
console.warn( "B. " + expr + " get result " + JSON.stringify(a) + ", " + o.is_affected + ", but " + JSON.stringify( expect_b ) + ", " + expect_affected + " is expected" );
}
}
}

function wfor( to_iter, func, env, _s, global, father, itemname, indexname, keyname )
{
var _n = wh.hn( to_iter ) === 'n';
var scope = wh.rv( _s );
var has_old_item = scope.hasOwnProperty(itemname);
var has_old_index = scope.hasOwnProperty(indexname);
var old_item = scope[itemname];
var old_index = scope[indexname];
var full = Object.prototype.toString.call(wh.rv(to_iter));
var type = full[8];
if( type === 'N' && full[10] === 'l' ) type = 'X';
var _y;
if( _n )
{
if( type === 'A' )
{
for( var i = 0 ; i < to_iter.length ; i++ )
{
scope[itemname] = to_iter[i];
scope[indexname] = wh.nh(i, 'h');
_y = keyname ? (keyname==="*this" ? _v(wh.rv(to_iter[i])) : _v(wh.rv(wh.rv(to_iter[i])[keyname]))) : _v();
_(father,_y);
func( env, scope, _y, global );
}
}
else if( type === 'O' )
{
for( var k in to_iter )
{
scope[itemname] = to_iter[k];
scope[indexname] = wh.nh(k, 'h');
_y = keyname ? (keyname==="*this" ? _v(wh.rv(to_iter[k])) : _v(wh.rv(wh.rv(to_iter[k])[keyname]))) : _v();
_(father,_y);
func( env,scope,_y,global );
}
}
else if( type === 'S' )
{
for( var i = 0 ; i < to_iter.length ; i++ )
{
scope[itemname] = to_iter[i];
scope[indexname] = wh.nh(i, 'h');
_y = _v( to_iter[i] + i );
_(father,_y);
func( env, scope, _y, global );
}
}
else if( type === 'N' )
{
for( var i = 0 ; i < to_iter ; i++ )
{
scope[itemname] = i;
scope[indexname] = wh.nh(i, 'h');
_y = _v( i );
_(father,_y);
func(env,scope,_y,global);
}
}
else
{
}
}
else
{
var r_to_iter = wh.rv(to_iter);
var r_iter_item, iter_item;
if( type === 'A' )
{
for( var i = 0 ; i < r_to_iter.length ; i++ )
{
iter_item = r_to_iter[i];
iter_item = wh.hn(iter_item)==='n' ? wh.nh(iter_item,'h') : iter_item;
r_iter_item = wh.rv( iter_item );
scope[itemname] = iter_item
scope[indexname] = wh.nh(i, 'h');
_y = keyname ? (keyname==="*this" ? _v(r_iter_item) : _v(wh.rv(r_iter_item[keyname]))) : _v();
_(father,_y);
func( env, scope, _y, global );
}
}
else if( type === 'O' )
{
for( var k in r_to_iter )
{
iter_item = r_to_iter[k];
iter_item = wh.hn(iter_item)==='n'? wh.nh(iter_item,'h') : iter_item;
r_iter_item = wh.rv( iter_item );
scope[itemname] = iter_item;
scope[indexname] = wh.nh(k, 'h');
_y = keyname ? (keyname==="*this" ? _v(r_iter_item) : _v(wh.rv(r_iter_item[keyname]))) : _v();
_(father,_y);
func( env, scope, _y, global );
}
}
else if( type === 'S' )
{
for( var i = 0 ; i < r_to_iter.length ; i++ )
{
scope[itemname] = wh.nh(r_to_iter[i],'h');
scope[indexname] = wh.nh(i, 'h');
_y = _v( to_iter[i] + i );
_(father,_y);
func( env, scope, _y, global );
}
}
else if( type === 'N' )
{
for( var i = 0 ; i < r_to_iter ; i++ )
{
scope[itemname] = wh.nh(i,'h');
scope[indexname]= wh.nh(i,'h');
_y = _v( i );
_(father,_y);
func(env,scope,_y,global);
}
}
else
{
}
}
if(has_old_item)
{
scope[itemname]=old_item;
}
else
{
delete scope[itemname];
}
if(has_old_index)
{
scope[indexname]=old_index;
}
else{delete scope[indexname];}}


function _r( node, attrname, opindex, env, scope, global )
{
var o = {};
var a = grb( z[opindex], env, scope, global, o );
node.attr[attrname] = a;
if( o.is_affected ) node.n.push( attrname );
}
function _o( opindex, env, scope, global )
{
var nothing = {};
return grb( z[opindex], env, scope, global, nothing );
}
function _1( opindex, env, scope, global )
{
var nothing = {};
return gra( z[opindex], env, scope, global, nothing );
}
function _2( opindex, func, env, scope, global, father, itemname, indexname, keyname )
{
var to_iter = _1( opindex, env, scope, global, father, itemname, indexname, keyname );
wfor( to_iter, func, env, scope, global, father, itemname, indexname, keyname );
}
function _gv( )
{
if( typeof(window.__webview_engine_version__) == 'undefined' ) return 0.0;
return window.__webview_engine_version__;
}
function _m(tag,attrs,env,scope,global)
{
var tmp=_n(tag);
var base=0;
for(var i = 0 ; i < attrs.length ; i+=2 )
{
if(attrs[i+1]<0)
{tmp.attr[attrs[i]]=true;}else{_r(tmp,attrs[i],base+attrs[i+1],env,scope,global);
if(base===0)base=attrs[i+1];}}return tmp;};function _ai(i,p,e,me,r,c){var x=_grp(p,e,me);if(x)i.push(x);else{i.push('');console.warn('WXMLRT:'+me+':import:'+r+':'+c+': Path `'+p+'` not found from `'+me+'`.')}}function _grp(p,e,me){if(p[0]!='/'){var mepart=me.split('/');mepart.pop();var ppart=p.split('/');for(var i=0;i<ppart.length;i++){if(ppart[i]=='..')mepart.pop();else if(ppart[i]=='.' || !ppart[i])continue;else mepart.push(ppart[i]);}p=mepart.join('/');}if(me[0]=='.'&&p[0]=='/')p='.'+p;if(e[p])return p;if(e[p+'.wxml'])return p+'.wxml';}function _gd(p,c,e,d){if(!c)return;if(d[p][c])return d[p][c];for(var x=e[p].i.length-1;x>=0;x--){if(e[p].i[x]&&d[e[p].i[x]][c])return d[e[p].i[x]][c]};for(var x=e[p].ti.length-1;x>=0;x--){var q=_grp(e[p].ti[x],e,p);if(q&&d[q][c])return d[q][c]}
var ii=_gapi(e,p);for(var x=0;x<ii.length;x++){if(ii[x]&&d[ii[x]][c])return d[ii[x]][c]}for(var k=e[p].j.length-1;k>=0;k--)if(e[p].j[k]){for(var q=e[e[p].j[k]].ti.length-1;q>=0;q--){var pp=_grp(e[e[p].j[k]].ti[q],e,p);if(pp&&d[pp][c]){return d[pp][c]}}}}function _gapi(e,p){if(!p)return [];if($gaic[p]){return $gaic[p]};var ret=[],q=[],h=0,t=0,put={},visited={};q.push(p);visited[p]=true;t++;while(h<t){var a=q[h++];for(var i=0;i<e[a].ic.length;i++){var nd=e[a].ic[i];var np=_grp(nd,e,a);if(np&&!visited[np]){visited[np]=true;q.push(np);t++;}}for(var i=0;a!=p&&i<e[a].ti.length;i++){var ni=e[a].ti[i];var nm=_grp(ni,e,a);if(nm&&!put[nm]){put[nm]=true;ret.push(nm);}}}$gaic[p]=ret;return ret;}var $ixc={};function _ic(p,ent,me,e,s,r,gg){var x=_grp(p,ent,me);ent[me].j.push(x);if(x){if($ixc[x]){console.warn('WXMLRT:-1:include:-1:-1: `'+p+'` is being included in a loop, will be stop.');return;}$ixc[x]=true;try{ent[x].f(e,s,r,gg)}catch(e){}$ixc[x]=false;}else{console.warn('WXMLRT:'+me+':include:-1:-1: Included path `'+p+'` not found from `'+me+'`.')}}function _w(tn,f,line,c){console.warn('WXMLRT:'+f+':template:'+line+':'+c+': Template `'+tn+'` not found.');}function _ev(dom){var changed=false;delete dom.properities;delete dom.n;if(dom.children){do{changed=false;var newch = [];for(var i=0;i<dom.children.length;i++){var ch=dom.children[i];if( ch.tag=='virtual'){changed=true;for(var j=0;ch.children&&j<ch.children.length;j++){newch.push(ch.children[j]);}}else { newch.push(ch); } } dom.children = newch; }while(changed);for(var i=0;i<dom.children.length;i++){_ev(dom.children[i]);}} return dom; }var e_={}
if(global&&typeof(global.entrys)=='object')e_=global.entrys
var d_={}
if(global&&typeof(global.defines)=='object')d_=global.defines
var p_={}
var z = [];
  (function(z){
    var a = 11;
    function Z(ops){z.push(ops)};
    Z([3, 'tolower']);Z([3, 'view']);Z([3, '2']);Z([3, 'true']);Z([a, [3, 'height:'],[[7],[3, "windowHeight"]],[3, 'px']]);Z([3, 'home']);Z([[7],[3, "formShow"]]);Z([3, 'normal']);Z([3, 'key']);Z([3, 'info']);Z([a, [3, 'background:'],[[6],[[7],[3, "userSet"]],[3, "backgroundColor"]]]);Z([3, 'user']);Z([3, 'header']);Z([3, 'imgs']);Z([3, 'ico-xji']);Z([3, 'iconfont icon-xiangji']);Z([3, '▣']);Z([[6],[[7],[3, "userInfo"]],[3, "user_avatar"]]);Z([3, 'ico-gender']);Z([[2, "=="], [[6],[[7],[3, "userInfo"]],[3, "user_sex"]], [1, 1]]);Z([3, 'iconfont icon-nan']);Z([3, '♂']);Z([3, 'color:#FFC0CB']);Z([3, '♀']);Z([3, 'name']);Z([a, [3, '\r\n                        '],[[6],[[7],[3, "userInfo"]],[3, "user_nickname"]],[3, '\r\n                        ']]);Z([3, 'level']);Z([a, [3, 'background:'],[[6],[[7],[3, "userInfo"]],[3, "leavel_color"]]]);Z([a, [[6],[[7],[3, "userInfo"]],[3, "leavel_name"]]]);Z([3, 'udes']);Z([a, [3, '\r\n                        '],[[2, "||"],[[6],[[6],[[7],[3, "userInfo"]],[3, "sign"]],[3, "active_text"]],[1, "来和我玩嘛!"]],[3, '\r\n                    ']]);Z([[7],[3, "isThatUid"]]);Z([3, 'userSetStart']);Z([3, 'setBg']);Z([3, 'http://img.he29.com/play/65201731384568183614408b8d20ed070f911267.png']);Z([3, 'Homebgc']);Z([3, 'aspectFill']);Z([[6],[[7],[3, "userSet"]],[3, "backgroundImg"]]);Z([[7],[3, "$watch$open"]]);Z([3, 'watchs']);Z([3, 'watch animated fadeIn']);Z([3, 'message']);Z([a, [[7],[3, "$watch$msgStr"]]]);Z([[2, "||"],[[7],[3, "$watch$imgs"]],[1, "http://img.he29.com/play/58c358435cad9d985da5236e9d497f2bc2ebcb0d.jpeg"]]);Z([3, 'btns']);Z([3, '$watch$waitRun']);Z([3, 'flex']);Z([a, [[2, "||"],[[7],[3, "$watch$left"]],[1, "稍后进入"]]]);Z([3, '$watch$nowRun']);Z([a, [[2, "||"],[[7],[3, "$watch$right"]],[1, "马上进入"]]]);Z([3, 'loading']);Z([a, [3, 'navcates '],[[2,'?:'],[[2, "=="], [[7],[3, "$photoTitle$leftColor"]], [1, 0]],[1, "noborder"],[1, ""]]]);Z([3, 'left']);Z([3, 'icon']);Z([[7],[3, "$photoTitle$icon"]]);Z([a, [[7],[3, "$photoTitle$title"]]]);Z([3, '$photoTitle$NavcateRight']);Z([3, 'right']);Z([3, 'rtext']);Z([a, [[7],[3, "$photoTitle$right"]]]);Z([3, 'http://img.he29.com/play/f4fe7786635ee901a46590b7f1926bc08d628bdf.png']);Z([3, 'photo']);Z([3, 'photoList']);Z([[2, ">"], [[6],[[7],[3, "PhotoWall"]],[3, "length"]], [1, 0]]);Z([3, 'list']);Z([[7],[3, "PhotoWall"]]);Z([3, 'previewImage']);Z([[6],[[7],[3, "item"]],[3, "wall_img"]]);Z([a, [3, 'width:'],[[6],[[7],[3, "wall"]],[3, "width"]],[3, 'px;height:'],[[6],[[7],[3, "wall"]],[3, "width"]],[3, 'px']]);Z([3, 'chooseImage']);Z([3, 'addSimg']);Z([3, 'http://img.he29.com/play/04cbfc244f5e1fcba4d966351c7f3ced24ae17e7.png']);Z([[7],[3, "lastImgShow"]]);Z([1, false]);Z([3, 'http://img.he29.com/play/c1e2e76216d7ce68b92aaf815f3ee4f749572642.png']);Z([3, 'nopicShow']);Z([3, '当前用户暂无精选照片...']);Z([3, 'clear']);Z([[2, ">"], [[6],[[7],[3, "hotView"]],[3, "length"]], [1, 0]]);Z([3, 'hot-view hot']);Z([3, 'title']);Z([3, '关注的游戏']);Z([3, 'img-box']);Z([[7],[3, "hotView"]]);Z([[7],[3, "index"]]);Z([3, 'startYuyue']);Z([3, 'img']);Z([[6],[[7],[3, "item"]],[3, "game_id"]]);Z([[6],[[7],[3, "item"]],[3, "game_thumb"]]);Z([[6],[[6],[[7],[3, "item"]],[3, "setting"]],[3, "set_id"]]);Z([[6],[[7],[3, "item"]],[3, "my_uid"]]);Z([[6],[[7],[3, "item"]],[3, "setting"]]);Z([3, 'tag level']);Z([3, '可预约']);Z([3, 'wx-text']);Z([a, [[6],[[7],[3, "item"]],[3, "game_name"]]]);Z([3, 'make']);Z([a, [3, 'navcates '],[[2,'?:'],[[2, "=="], [[7],[3, "$navcate$leftColor"]], [1, 0]],[1, "noborder"],[1, ""]]]);Z([[7],[3, "$navcate$icon"]]);Z([a, [[7],[3, "$navcate$title"]]]);Z([3, '$navcate$NavcateRight']);Z([a, [[7],[3, "$navcate$right"]]]);Z([3, 'active']);Z([[7],[3, "$active$list"]]);Z([[2, ">"], [[6],[[7],[3, "$active$list"]],[3, "length"]], [1, 0]]);Z([3, 'activeList']);Z([3, 'contentBox']);Z([3, 'actLeft']);Z([[6],[[7],[3, "item"]],[3, "user_avatar"]]);Z([3, 'actright']);Z([a, [[6],[[7],[3, "item"]],[3, "user_nickname"]]]);Z([a, [[6],[[7],[3, "item"]],[3, "time2"]]]);Z([3, 'actSpeak']);Z([a, [3, '\r\n                            '],[[6],[[7],[3, "item"]],[3, "active_text"]],[3, '\r\n                        ']]);Z([[6],[[7],[3, "item"]],[3, "active_img"]]);Z([3, 'imgItem']);Z([3, 'actImg']);Z([[7],[3, "$active$isThatUid"]]);Z([3, '$active$delActive']);Z([3, 'delete']);Z([[6],[[7],[3, "item"]],[3, "active_id"]]);Z([3, '删除']);Z([a, [3, 'imgFather'],[[6],[[6],[[7],[3, "item"]],[3, "active_img"]],[3, "length"]]]);Z([3, '$active$previewImage']);Z([3, 'showImg']);Z([[6],[[7],[3, "imgItem"]],[3, "img"]]);Z([3, 'line']);Z([[7],[3, "$active$emptys"]]);Z([3, 'empty']);Z([3, 'abnor']);Z([3, 'abnor__box']);Z([[7],[3, "$active$empty$image"]]);Z([3, 'abnor__image']);Z([3, 'widthFix']);Z([[7],[3, "$active$empty$title"]]);Z([3, 'abnor__text']);Z([[7],[3, "$active$empty$tip"]]);Z([3, 'abnor__tip']);Z([[7],[3, "$active$empty$button"]]);Z([3, '$active$empty$emitAbnorTap']);Z([3, 'abnor__btn']);Z([3, 'loadingMore']);Z([[2, "!="], [[7],[3, "$active$loadingMore$flag"]], [1, 0]]);Z([[2, "=="], [[7],[3, "$active$loadingMore$flag"]], [1, 1]]);Z([3, 'weui-loadmore']);Z([3, 'weui-loading']);Z([3, 'weui-loadmore__tips']);Z([3, '正在加载']);Z([[2, "<"], [[7],[3, "$active$loadingMore$flag"]], [1, 0]]);Z([3, 'weui-loadmore weui-loadmore_line']);Z([3, 'weui-loadmore__tips weui-loadmore__tips_in-line']);Z([3, '加载完成']);Z([[2, "=="], [[7],[3, "$active$loadingMore$flag"]], [1, 2]]);Z([3, '滑动加载更多']);Z([3, 'sendComp']);Z([[2, "=="], [[7],[3, "favStatus"]], [1, 0]]);Z([3, 'panal']);Z([3, 'contentText']);Z([a, [3, 'height:'],[[7],[3, "$panal$height"]],[3, 'px;margin-top:'],[[7],[3, "$panal$top"]],[3, 'px;']]);Z([3, '$panal$goUrl']);Z([3, 'father']);Z([[7],[3, "$panal$url"]]);Z([3, 'background']);Z([3, 'http://img.he29.com/play/35e202bfd6dd3a3c0fddb6a841c7e34f115c9ffd.jpeg']);Z([3, 'content']);Z([3, 'weui-article__section']);Z([3, 'weui-article__h3']);Z([3, 'text-align:center;color:#ffffff']);Z([3, '为好友打榜点赞']);Z([3, 'PanContent']);Z([3, '您的好友参与了打榜活动']);Z([3, '赶快为她点赞助力吧!']);Z([3, 'btnDefault']);Z([3, 'addZan']);Z([3, 'leftbtn']);Z([a, [3, '\r\n                        '],[[6],[[7],[3, "btns"]],[1, 0]],[3, '\r\n                    ']]);Z([3, 'shareApp']);Z([3, 'rightbtn']);Z([3, 'defBtn']);Z([3, 'share']);Z([a, [3, '\r\n                            '],[[6],[[7],[3, "btns"]],[1, 1]],[3, '\r\n                        ']]);Z([3, 'weui-article__p']);Z([3, '$panal$close']);Z([3, 'close']);Z([3, 'X']);Z([[6],[[7],[3, "userSet"]],[3, "open"]]);Z([a, [3, 'height:'],[[7],[3, "$panal2$height"]],[3, 'px;margin-top:'],[[7],[3, "$panal2$top"]],[3, 'px;']]);Z([3, '$panal2$goUrl']);Z([[7],[3, "$panal2$url"]]);Z([3, 'text-align:center']);Z([3, '自定义我的主页']);Z([3, 'weui-cells weui-cells_after-title']);Z([3, 'weui-cell weui-cell_access']);Z([3, 'weui-cell__bd']);Z([3, '背景颜色']);Z([3, 'weui-cell__ft weui-cell__ft_in-access']);Z([a, [3, 'color:'],[[6],[[7],[3, "userSet"]],[3, "closeColor"]]]);Z([a, [3, '\r\n                            '],[[6],[[7],[3, "userSet"]],[3, "closeColor"]],[3, '\r\n                        ']]);Z([[6],[[7],[3, "userSet"]],[3, "color"]]);Z([3, 'choseColor']);Z([3, 'colorRadius']);Z([[7],[3, "item"]]);Z([a, [3, 'background:'],[[7],[3, "item"]]]);Z([3, 'uploadSetBgc']);Z([3, '背景图']);Z([3, '上传']);Z([[6],[[7],[3, "userSet"]],[3, "look"]]);Z([3, 'width: 100%; max-height: 120px;']);Z([3, 'setBtnClick']);Z([3, '1']);Z([3, '\r\n                        预览颜色\r\n                    ']);Z([3, '\r\n                            保存设置\r\n                        ']);Z([3, '$panal2$close']);
  })(z);d_["./pages/home/index.wxml"] = {};
  var m0=function(e,s,r,gg){
    var oJM = _m( "scroll-view", ["bindscrolltolower", 0,"class", 1,"lowerThreshold", 1,"scrollY", 2,"style", 3], e, s, gg);var oKM = _n("view");_r(oKM, 'class', 5, e, s, gg);var oLM = _v();
      if (_o(6, e, s, gg)) {
        oLM.wxVkey = 1;var oMM = _m( "view", ["class", 7,"wx:key", 1], e, s, gg);var oOM = _m( "view", ["class", 9,"style", 1], e, s, gg);var oPM = _n("view");_r(oPM, 'class', 11, e, s, gg);var oQM = _n("view");_r(oQM, 'class', 12, e, s, gg);var oRM = _n("view");_r(oRM, 'class', 13, e, s, gg);var oSM = _n("view");_r(oSM, 'class', 14, e, s, gg);var oTM = _n("text");_r(oTM, 'class', 15, e, s, gg);var oUM = _o(16, e, s, gg);_(oTM,oUM);_(oSM,oTM);_(oRM,oSM);var oVM = _n("image");_r(oVM, 'src', 17, e, s, gg);_(oRM,oVM);var oWM = _n("view");_r(oWM, 'class', 18, e, s, gg);var oXM = _v();
      if (_o(19, e, s, gg)) {
        oXM.wxVkey = 1;var oYM = _n("text");_r(oYM, 'class', 20, e, s, gg);var oaM = _o(21, e, s, gg);_(oYM,oaM);_(oXM, oYM);
      }else {
        oXM.wxVkey = 2;var obM = _m( "text", ["class", 20,"style", 2], e, s, gg);var odM = _o(23, e, s, gg);_(obM,odM);_(oXM, obM);
      }_(oWM,oXM);_(oRM,oWM);_(oQM,oRM);_(oPM,oQM);var oeM = _n("view");_r(oeM, 'class', 24, e, s, gg);var ofM = _o(25, e, s, gg);_(oeM,ofM);var ogM = _m( "view", ["class", 26,"style", 1], e, s, gg);var ohM = _o(28, e, s, gg);_(ogM,ohM);_(oeM,ogM);_(oPM,oeM);var oiM = _n("view");_r(oiM, 'class', 29, e, s, gg);var ojM = _o(30, e, s, gg);_(oiM,ojM);_(oPM,oiM);var okM = _v();
      if (_o(31, e, s, gg)) {
        okM.wxVkey = 1;var olM = _m( "view", ["bindtap", 32,"class", 1], e, s, gg);var onM = _n("image");_r(onM, 'src', 34, e, s, gg);_(olM,onM);_(okM, olM);
      } _(oPM,okM);_(oOM,oPM);var ooM = _n("view");_r(ooM, 'class', 35, e, s, gg);var opM = _m( "image", ["mode", 36,"src", 1], e, s, gg);_(ooM,opM);_(oOM,ooM);_(oMM,oOM);var oqM = _v();
      if (_o(38, e, s, gg)) {
        oqM.wxVkey = 1;var orM = _n("view");_r(orM, 'class', 39, e, s, gg);var otM = _n("view");_r(otM, 'class', 40, e, s, gg);var ouM = _n("text");_r(ouM, 'class', 41, e, s, gg);var ovM = _o(42, e, s, gg);_(ouM,ovM);_(otM,ouM);var owM = _n("image");_r(owM, 'src', 43, e, s, gg);_(otM,owM);var oxM = _n("view");_r(oxM, 'class', 44, e, s, gg);var oyM = _m( "text", ["bindtap", 45,"class", 1], e, s, gg);var ozM = _o(47, e, s, gg);_(oyM,ozM);_(oxM,oyM);var o_M = _m( "text", ["class", 46,"bindtap", 2], e, s, gg);var oAN = _o(49, e, s, gg);_(o_M,oAN);_(oxM,o_M);_(otM,oxM);_(orM,otM);var oBN = _n("view");_r(oBN, 'class', 50, e, s, gg);_(orM,oBN);_(oqM, orM);
      } _(oMM,oqM);var oCN = _n("view");_r(oCN, 'class', 51, e, s, gg);var oDN = _n("view");_r(oDN, 'class', 52, e, s, gg);var oEN = _n("view");_r(oEN, 'class', 53, e, s, gg);var oFN = _v();
      if (_o(54, e, s, gg)) {
        oFN.wxVkey = 1;var oGN = _n("image");_r(oGN, 'src', 54, e, s, gg);_(oFN, oGN);
      } _(oEN,oFN);_(oDN,oEN);var oIN = _n("view");_r(oIN, 'class', 24, e, s, gg);var oJN = _o(55, e, s, gg);_(oIN,oJN);_(oDN,oIN);_(oCN,oDN);var oKN = _m( "view", ["bindtap", 56,"class", 1], e, s, gg);var oLN = _n("text");_r(oLN, 'class', 58, e, s, gg);var oMN = _o(59, e, s, gg);_(oLN,oMN);_(oKN,oLN);var oNN = _n("image");_r(oNN, 'src', 60, e, s, gg);_(oKN,oNN);_(oCN,oKN);_(oMM,oCN);var oON = _n("view");_r(oON, 'class', 61, e, s, gg);var oPN = _n("view");_r(oPN, 'class', 62, e, s, gg);var oQN = _v();
      if (_o(63, e, s, gg)) {
        oQN.wxVkey = 1;var oRN = _n("view");_r(oRN, 'class', 64, e, s, gg);var oTN = _v();var oUN = function(oYN,oXN,oWN,gg){var oVN = _m( "image", ["mode", 36,"bindtap", 30,"data-img", 31,"src", 31,"style", 32], oYN, oXN, gg);_(oWN, oVN);return oWN;};_2(65, oUN, e, s, gg, oTN, "item", "index", 'key');_(oRN,oTN);var oaN = _v();
      if (_o(31, e, s, gg)) {
        oaN.wxVkey = 1;var obN = _m( "image", ["style", 68,"bindtap", 1,"class", 2,"src", 3], e, s, gg);_(oaN, obN);
      } _(oRN,oaN);var odN = _v();var oeN = function(oiN,ohN,ogN,gg){var ofN = _v();
      if (_o(73, oiN, ohN, gg)) {
        ofN.wxVkey = 1;var okN = _m( "image", ["class", 70,"src", 4], oiN, ohN, gg);_(ofN, okN);
      } _(ogN, ofN);return ogN;};_2(72, oeN, e, s, gg, odN, "item", "index", 'key');_(oRN,odN);_(oQN, oRN);
      }else {
        oQN.wxVkey = 2;var omN = _n("view");_r(omN, 'class', 64, e, s, gg);var ooN = _n("view");_r(ooN, 'class', 75, e, s, gg);var opN = _o(76, e, s, gg);_(ooN,opN);_(omN,ooN);_(oQN, omN);
      }_(oPN,oQN);_(oON,oPN);_(oMM,oON);var oqN = _n("view");_r(oqN, 'class', 77, e, s, gg);_(oMM,oqN);var orN = _v();
      if (_o(78, e, s, gg)) {
        orN.wxVkey = 1;var osN = _n("view");_r(osN, 'class', 79, e, s, gg);var ouN = _n("view");_r(ouN, 'class', 80, e, s, gg);var ovN = _n("text");_(ouN,ovN);var owN = _n("text");var oxN = _o(81, e, s, gg);_(owN,oxN);_(ouN,owN);var oyN = _n("text");_(ouN,oyN);_(osN,ouN);var ozN = _n("view");_r(ozN, 'class', 82, e, s, gg);var o_N = _v();var oAO = function(oEO,oDO,oCO,gg){var oBO = _m( "view", ["bindtap", 85,"class", 1,"data-gid", 2,"data-id", 2,"data-img", 3,"data-set", 4,"data-uid", 5], oEO, oDO, gg);var oGO = _v();
      if (_o(91, oEO, oDO, gg)) {
        oGO.wxVkey = 1;var oHO = _n("view");_r(oHO, 'class', 92, oEO, oDO, gg);var oJO = _o(93, oEO, oDO, gg);_(oHO,oJO);_(oGO, oHO);
      } _(oBO,oGO);var oKO = _m( "image", ["mode", 36,"src", 52], oEO, oDO, gg);_(oBO,oKO);var oLO = _n("text");_r(oLO, 'class', 94, oEO, oDO, gg);var oMO = _o(95, oEO, oDO, gg);_(oLO,oMO);_(oBO,oLO);var oNO = _n("view");_r(oNO, 'class', 96, oEO, oDO, gg);_(oBO,oNO);_(oCO, oBO);return oCO;};_2(83, oAO, e, s, gg, o_N, "item", "index", '{{index}}');_(ozN,o_N);_(osN,ozN);_(orN, osN);
      } _(oMM,orN);var oOO = _n("view");_r(oOO, 'class', 97, e, s, gg);var oPO = _n("view");_r(oPO, 'class', 52, e, s, gg);var oQO = _n("view");_r(oQO, 'class', 53, e, s, gg);var oRO = _v();
      if (_o(98, e, s, gg)) {
        oRO.wxVkey = 1;var oSO = _n("image");_r(oSO, 'src', 98, e, s, gg);_(oRO, oSO);
      } _(oQO,oRO);_(oPO,oQO);var oUO = _n("view");_r(oUO, 'class', 24, e, s, gg);var oVO = _o(99, e, s, gg);_(oUO,oVO);_(oPO,oUO);_(oOO,oPO);var oWO = _m( "view", ["class", 57,"bindtap", 43], e, s, gg);var oXO = _n("text");_r(oXO, 'class', 58, e, s, gg);var oYO = _o(101, e, s, gg);_(oXO,oYO);_(oWO,oXO);var oZO = _n("image");_r(oZO, 'src', 60, e, s, gg);_(oWO,oZO);_(oOO,oWO);_(oMM,oOO);var oaO = _n("view");_r(oaO, 'class', 102, e, s, gg);var obO = _n("view");_r(obO, 'class', 102, e, s, gg);var ocO = _v();var odO = function(ohO,ogO,ofO,gg){var oeO = _v();
      if (_o(104, ohO, ogO, gg)) {
        oeO.wxVkey = 1;var ojO = _n("view");_r(ojO, 'class', 105, ohO, ogO, gg);var olO = _n("view");_r(olO, 'class', 106, ohO, ogO, gg);var omO = _n("view");_r(omO, 'class', 107, ohO, ogO, gg);var onO = _m( "image", ["class", 12,"mode", 24,"src", 96], ohO, ogO, gg);_(omO,onO);_(olO,omO);var ooO = _n("view");_r(ooO, 'class', 109, ohO, ogO, gg);var opO = _n("view");_r(opO, 'class', 24, ohO, ogO, gg);var oqO = _n("view");_r(oqO, 'class', 52, ohO, ogO, gg);var orO = _o(110, ohO, ogO, gg);_(oqO,orO);_(opO,oqO);var osO = _n("view");_r(osO, 'class', 57, ohO, ogO, gg);var otO = _o(111, ohO, ogO, gg);_(osO,otO);_(opO,osO);_(ooO,opO);var ouO = _n("view");_r(ouO, 'class', 112, ohO, ogO, gg);var ovO = _o(113, ohO, ogO, gg);_(ouO,ovO);_(ooO,ouO);var owO = _v();var oxO = function(oAP,o_O,ozO,gg){var oyO = _n("view");_r(oyO, 'class', 116, oAP, o_O, gg);var oCP = _v();
      if (_o(117, oAP, o_O, gg)) {
        oCP.wxVkey = 1;var oDP = _m( "view", ["bindtap", 118,"class", 1,"data-aid", 2], oAP, o_O, gg);var oFP = _o(121, oAP, o_O, gg);_(oDP,oFP);_(oCP, oDP);
      } _(oyO,oCP);var oGP = _n("view");_r(oGP, 'class', 122, oAP, o_O, gg);var oHP = _m( "image", ["mode", 36,"data-index", 48,"bindtap", 87,"class", 88,"data-img", 89,"src", 89], oAP, o_O, gg);_(oGP,oHP);_(oyO,oGP);_(ozO, oyO);return ozO;};_2(114, oxO, ohO, ogO, gg, owO, "imgItem", "index", 'key');_(ooO,owO);_(olO,ooO);_(ojO,olO);var oIP = _n("view");_r(oIP, 'class', 126, ohO, ogO, gg);_(ojO,oIP);_(oeO, ojO);
      } _(ofO, oeO);return ofO;};_2(103, odO, e, s, gg, ocO, "item", "index", 'key');_(obO,ocO);var oJP = _v();
      if (_o(127, e, s, gg)) {
        oJP.wxVkey = 1;var oKP = _n("view");_r(oKP, 'class', 128, e, s, gg);var oMP = _n("view");_r(oMP, 'class', 128, e, s, gg);var oNP = _n("view");_r(oNP, 'class', 129, e, s, gg);var oOP = _n("view");_r(oOP, 'class', 130, e, s, gg);var oPP = _v();
      if (_o(131, e, s, gg)) {
        oPP.wxVkey = 1;var oQP = _m( "image", ["src", 131,"class", 1,"mode", 2], e, s, gg);_(oPP, oQP);
      } _(oOP,oPP);var oSP = _v();
      if (_o(134, e, s, gg)) {
        oSP.wxVkey = 1;var oTP = _n("view");_r(oTP, 'class', 135, e, s, gg);var oVP = _o(134, e, s, gg);_(oTP,oVP);_(oSP, oTP);
      } _(oOP,oSP);var oWP = _v();
      if (_o(136, e, s, gg)) {
        oWP.wxVkey = 1;var oXP = _n("view");_r(oXP, 'class', 137, e, s, gg);var oZP = _o(136, e, s, gg);_(oXP,oZP);_(oWP, oXP);
      } _(oOP,oWP);var oaP = _v();
      if (_o(138, e, s, gg)) {
        oaP.wxVkey = 1;var obP = _m( "view", ["bindtap", 139,"class", 1], e, s, gg);var odP = _o(138, e, s, gg);_(obP,odP);_(oaP, obP);
      } _(oOP,oaP);_(oNP,oOP);_(oMP,oNP);_(oKP,oMP);_(oJP, oKP);
      } _(obO,oJP);var oeP = _n("view");_r(oeP, 'class', 141, e, s, gg);var ofP = _v();
      if (_o(142, e, s, gg)) {
        ofP.wxVkey = 1;var ogP = _n("view");_r(ogP, 'class', 141, e, s, gg);var oiP = _v();
      if (_o(143, e, s, gg)) {
        oiP.wxVkey = 1;var ojP = _n("view");_r(ojP, 'class', 144, e, s, gg);var olP = _n("view");_r(olP, 'class', 145, e, s, gg);_(ojP,olP);var omP = _n("view");_r(omP, 'class', 146, e, s, gg);var onP = _o(147, e, s, gg);_(omP,onP);_(ojP,omP);_(oiP, ojP);
      } _(ogP,oiP);var ooP = _v();
      if (_o(148, e, s, gg)) {
        ooP.wxVkey = 1;var opP = _n("view");_r(opP, 'class', 149, e, s, gg);var orP = _n("view");_r(orP, 'class', 150, e, s, gg);var osP = _o(151, e, s, gg);_(orP,osP);_(opP,orP);_(ooP, opP);
      } _(ogP,ooP);var otP = _v();
      if (_o(152, e, s, gg)) {
        otP.wxVkey = 1;var ouP = _n("view");_r(ouP, 'class', 149, e, s, gg);var owP = _n("view");_r(owP, 'class', 150, e, s, gg);var oxP = _o(153, e, s, gg);_(owP,oxP);_(ouP,owP);_(otP, ouP);
      } _(ogP,otP);_(ofP, ogP);
      } _(oeP,ofP);_(obO,oeP);_(oaO,obO);_(oMM,oaO);_(oLM, oMM);
      }else {
        oLM.wxVkey = 2;var oyP = _n("view");_r(oyP, 'class', 154, e, s, gg);_(oLM, oyP);
      }_(oKM,oLM);_(oJM,oKM);var o_P = _v();
      if (_o(155, e, s, gg)) {
        o_P.wxVkey = 1;var oAQ = _n("view");_r(oAQ, 'class', 156, e, s, gg);var oCQ = _m( "view", ["class", 157,"style", 1], e, s, gg);var oDQ = _m( "view", ["bindtap", 159,"class", 1,"data-url", 2], e, s, gg);var oEQ = _n("view");_r(oEQ, 'class', 162, e, s, gg);var oFQ = _n("image");_r(oFQ, 'src', 163, e, s, gg);_(oEQ,oFQ);_(oDQ,oEQ);var oGQ = _n("view");_r(oGQ, 'class', 164, e, s, gg);var oHQ = _n("view");_r(oHQ, 'class', 165, e, s, gg);var oIQ = _n("view");_r(oIQ, 'class', 166, e, s, gg);var oJQ = _n("view");_r(oJQ, 'style', 167, e, s, gg);var oKQ = _o(168, e, s, gg);_(oJQ,oKQ);_(oIQ,oJQ);_(oHQ,oIQ);var oLQ = _n("view");_r(oLQ, 'class', 169, e, s, gg);var oMQ = _n("view");_r(oMQ, 'class', 80, e, s, gg);var oNQ = _o(170, e, s, gg);_(oMQ,oNQ);_(oLQ,oMQ);var oOQ = _n("view");_r(oOQ, 'class', 80, e, s, gg);var oPQ = _o(171, e, s, gg);_(oOQ,oPQ);_(oLQ,oOQ);var oQQ = _n("view");_r(oQQ, 'class', 172, e, s, gg);var oRQ = _m( "view", ["bindtap", 173,"class", 1], e, s, gg);var oSQ = _o(175, e, s, gg);_(oRQ,oSQ);_(oQQ,oRQ);var oTQ = _m( "view", ["bindtap", 176,"class", 1], e, s, gg);var oUQ = _m( "button", ["class", 178,"openType", 1], e, s, gg);var oVQ = _o(180, e, s, gg);_(oUQ,oVQ);_(oTQ,oUQ);_(oQQ,oTQ);_(oLQ,oQQ);_(oHQ,oLQ);var oWQ = _n("view");_r(oWQ, 'class', 181, e, s, gg);_(oHQ,oWQ);_(oGQ,oHQ);_(oDQ,oGQ);_(oCQ,oDQ);var oXQ = _n("view");_r(oXQ, 'class', 126, e, s, gg);_(oCQ,oXQ);var oYQ = _m( "view", ["bindtap", 182,"class", 1], e, s, gg);var oZQ = _o(184, e, s, gg);_(oYQ,oZQ);_(oCQ,oYQ);_(oAQ,oCQ);var oaQ = _n("view");_r(oaQ, 'class', 50, e, s, gg);_(oAQ,oaQ);_(o_P, oAQ);
      } _(oJM,o_P);var obQ = _v();
      if (_o(185, e, s, gg)) {
        obQ.wxVkey = 1;var ocQ = _n("view");_r(ocQ, 'class', 156, e, s, gg);var oeQ = _m( "view", ["class", 157,"style", 29], e, s, gg);var ofQ = _m( "view", ["class", 160,"bindtap", 27,"data-url", 28], e, s, gg);var ogQ = _n("view");_r(ogQ, 'class', 162, e, s, gg);_(ofQ,ogQ);var ohQ = _n("view");_r(ohQ, 'class', 164, e, s, gg);var oiQ = _n("view");_r(oiQ, 'class', 165, e, s, gg);var ojQ = _n("view");_r(ojQ, 'class', 166, e, s, gg);var okQ = _n("view");_r(okQ, 'style', 189, e, s, gg);var olQ = _o(190, e, s, gg);_(okQ,olQ);_(ojQ,okQ);_(oiQ,ojQ);var omQ = _n("view");_r(omQ, 'class', 169, e, s, gg);var onQ = _n("view");_r(onQ, 'class', 191, e, s, gg);var ooQ = _n("view");_r(ooQ, 'class', 192, e, s, gg);var opQ = _n("view");_r(opQ, 'class', 193, e, s, gg);var oqQ = _o(194, e, s, gg);_(opQ,oqQ);_(ooQ,opQ);var orQ = _m( "view", ["class", 195,"style", 1], e, s, gg);var osQ = _o(197, e, s, gg);_(orQ,osQ);_(ooQ,orQ);_(onQ,ooQ);var otQ = _n("view");_r(otQ, 'class', 192, e, s, gg);var ouQ = _v();var ovQ = function(ozQ,oyQ,oxQ,gg){var owQ = _m( "text", ["bindtouchstart", 199,"class", 1,"data-color", 2,"style", 3], ozQ, oyQ, gg);_(oxQ, owQ);return oxQ;};_2(198, ovQ, e, s, gg, ouQ, "item", "index", 'key');_(otQ,ouQ);_(onQ,otQ);var oAR = _m( "view", ["class", 192,"bindtap", 11], e, s, gg);var oBR = _n("view");_r(oBR, 'class', 193, e, s, gg);var oCR = _o(204, e, s, gg);_(oBR,oCR);_(oAR,oBR);var oDR = _n("view");_r(oDR, 'class', 195, e, s, gg);var oER = _o(205, e, s, gg);_(oDR,oER);_(oAR,oDR);_(onQ,oAR);var oFR = _n("view");_r(oFR, 'class', 192, e, s, gg);var oGR = _m( "image", ["src", 206,"style", 1], e, s, gg);_(oFR,oGR);_(onQ,oFR);_(omQ,onQ);var oHR = _n("view");_r(oHR, 'class', 172, e, s, gg);var oIR = _m( "view", ["class", 174,"bindtap", 34,"data-id", 35], e, s, gg);var oJR = _o(210, e, s, gg);_(oIR,oJR);_(oHR,oIR);var oKR = _n("view");_r(oKR, 'class', 174, e, s, gg);var oLR = _m( "button", ["data-id", 2,"class", 176,"bindtap", 206], e, s, gg);var oMR = _o(211, e, s, gg);_(oLR,oMR);_(oKR,oLR);_(oHR,oKR);_(omQ,oHR);_(oiQ,omQ);var oNR = _n("view");_r(oNR, 'class', 181, e, s, gg);_(oiQ,oNR);_(ohQ,oiQ);_(ofQ,ohQ);_(oeQ,ofQ);var oOR = _n("view");_r(oOR, 'class', 126, e, s, gg);_(oeQ,oOR);var oPR = _m( "view", ["class", 183,"bindtap", 29], e, s, gg);var oQR = _o(184, e, s, gg);_(oPR,oQR);_(oeQ,oPR);_(ocQ,oeQ);var oRR = _n("view");_r(oRR, 'class', 50, e, s, gg);_(ocQ,oRR);_(obQ, ocQ);
      } _(oJM,obQ);_(r,oJM);
    return r;
  };
        e_["./pages/home/index.wxml"]={f:m0,j:[],i:[],ti:[],ic:[]};
if(path&&e_[path]){
window.__wxml_comp_version__=0.02
return function(env,dd,global){$gwxc=0;var root={"tag":"wx-page"};root.children=[]
var main=e_[path].f
if(typeof(window.__webview_engine_version__)!='undefined'&&window.__webview_engine_version__+1e-6>=0.02+1e-6&&window.__mergeData__)
{env=window.__mergeData__(env,dd);}
try{
main(env,{},root,global);
if(typeof(window.__webview_engine_version__)=='undefined'||window.__webview_engine_version__+1e-6<0.01+1e-6){return _ev(root);}}catch(err){console.log(err)}return root;}}}@code-separator-line:.panal{z-index:999;position:relative}.panal .contentText{position:fixed;left:0;top:42px;right:0;bottom:0;width:70%;z-index:99;margin:0 auto;display:flex;justify-content:center;background:#fff;height:auto;font-size:%%?26rpx?%%}.panal .contentText .close{position:absolute;display:flex;justify-content:center;width:%%?50rpx?%%;height:%%?50rpx?%%;border-radius:50%;background:rgba(0,0,0,.5);bottom:%%?-120rpx?%%;color:rgba(255,255,255,.55);line-height:%%?50rpx?%%;text-align:center;border:1px solid rgba(255,255,255,.5)}.panal .contentText .line{position:absolute;display:flex;justify-content:center;height:%%?120rpx?%%;width:1px;background:rgba(255,255,255,.47);bottom:%%?-120rpx?%%}.panal .contentText .content{width:100%;display:block;z-index:999}.panal .contentText .father{width:100%;height:100%;font-size:%%?24rpx?%%}.panal .loading{position:fixed;width:100%;height:100%;background:rgba(0,0,0,.5);top:42px;left:0;z-index:10}.panal .weui-article__h3{font-size:%%?28rpx?%%;color:#656b6f;padding:%%?20rpx?%%;border-bottom:1px solid #f1f1f1}.panal .background{width:100%;height:100%;position:absolute;top:0;left:0;z-index:-1;overflow:hidden;border-radius:%%?20rpx?%%}.panal .background wx-image{height:100%;width:100%}.panal{z-index:999;position:relative}.panal .contentText{position:fixed;left:0;top:42px;right:0;bottom:0;width:70%;z-index:99;margin:0 auto;display:flex;justify-content:center;background:#fff;height:auto;font-size:%%?26rpx?%%}.panal .contentText .close{position:absolute;display:flex;justify-content:center;width:%%?50rpx?%%;height:%%?50rpx?%%;border-radius:50%;background:rgba(0,0,0,.5);bottom:%%?-120rpx?%%;color:rgba(255,255,255,.55);line-height:%%?50rpx?%%;text-align:center;border:1px solid rgba(255,255,255,.5)}.panal .contentText .line{position:absolute;display:flex;justify-content:center;height:%%?120rpx?%%;width:1px;background:rgba(255,255,255,.47);bottom:%%?-120rpx?%%}.panal .contentText .content{width:100%;display:block;z-index:999}.panal .contentText .father{width:100%;height:100%;font-size:%%?24rpx?%%}.panal .loading{position:fixed;width:100%;height:100%;background:rgba(0,0,0,.5);top:42px;left:0;z-index:10}.panal .weui-article__h3{font-size:%%?28rpx?%%;color:#656b6f;padding:%%?20rpx?%%;border-bottom:1px solid #f1f1f1}.panal .background{width:100%;height:100%;position:absolute;top:0;left:0;z-index:-1;overflow:hidden;border-radius:%%?20rpx?%%}.panal .background wx-image{height:100%;width:100%}.watch{text-align:center;position:fixed;margin:auto;left:0;right:0;top:42px;bottom:0;z-index:100;border-radius:10px;width:%%?480rpx?%%;height:%%?320rpx?%%}.watch wx-image{width:100%;height:%%?300rpx?%%;border-top-left-radius:10px;border-top-right-radius:10px;opacity:.9}.watch .btns{display:flex;background:#fff;z-index:100;font-size:%%?32rpx?%%;color:#969696;position:absolute;bottom:0;width:100%;height:%%?80rpx?%%;line-height:%%?80rpx?%%;text-align:center;border-bottom-left-radius:10px;border-bottom-right-radius:10px}.watch .btns .flex{flex:1}.watch .btns .flex:last-child{color:#0c9}.watch .message{position:absolute;top:%%?20rpx?%%;font-size:%%?32rpx?%%;color:#fff;padding:%%?10rpx?%%;margin:%%?10rpx?%%;z-index:99;text-shadow:0 0 2px #f1f1f1}.watchs .loading{position:absolute;top:0;left:0;width:100%;height:100%;z-index:1;background:rgba(0,0,0,.5)}.empty .abnor{position:relative;display:block;width:100%;height:0;padding-bottom:100%;overflow:hidden}.empty .abnor__box{position:absolute;display:flex;top:0;bottom:0;left:0;right:0;flex-direction:column;justify-content:center;align-items:center}.empty .abnor__btn{min-width:%%?228rpx?%%;height:%%?66rpx?%%;margin-top:%%?30rpx?%%;padding:0 %%?30rpx?%%;background-color:#3e9aff;border:0 none;border-radius:%%?2rpx?%%;color:#fff;font-size:%%?28rpx?%%;text-align:center;overflow:hidden;line-height:%%?66rpx?%%}.empty .abnor__btn:active{background-color:#3e9aff}.empty .abnor__image{width:%%?514rpx?%%;background:transparent no-repeat;background-size:cover}.empty .abnor__text{margin-top:%%?30rpx?%%;color:#333;font-size:%%?28rpx?%%}.empty .abnor__tip{margin-top:%%?20rpx?%%;color:#666;font-size:%%?24rpx?%%}.loadingMore{clear:both;display:block}.active{background:#fff;padding-top:2px}.active .activeList .contentBox{display:flex;width:100%;justify-content:center;margin:0 auto;padding:%%?10rpx?%% %%?15rpx?%%;box-sizing:border-box}.active .activeList .contentBox .actLeft{flex:1}.active .activeList .contentBox .actLeft .header{width:%%?60rpx?%%;height:%%?60rpx?%%}.active .activeList .contentBox .actright{flex:8;font-size:%%?28rpx?%%}.active .activeList .contentBox .actright .name{height:%%?50rpx?%%;line-height:%%?50rpx?%%;color:#4f5b85;font-size:%%?32rpx?%%;display:flex}.active .activeList .contentBox .actright .name .left{flex:1;text-align:left}.active .activeList .contentBox .actright .name .right{flex:1;text-align:right;color:#656b6f;font-size:%%?24rpx?%%;padding-right:10px}.active .activeList .contentBox .actright .actImg{margin-top:10px;position:relative}.active .activeList .contentBox .actright .actImg .imgFather1 .showImg{max-width:180px;vertical-align:middle;max-height:260px}.active .activeList .contentBox .actright .actImg .imgFather2 .showImg{width:48%;vertical-align:middle;float:left;height:150px;margin:%%?5rpx?%%}.active .activeList .contentBox .actright .actImg .imgFather3 .showImg{width:31%;vertical-align:middle;float:left;height:90px;margin:%%?5rpx?%%;box-sizing:border-box}.active .activeList .contentBox .actright .actImg .delete{position:absolute;bottom:0;right:%%?10rpx?%%;color:#666;font-size:%%?24rpx?%%}.active .activeList .line{height:1px;width:100%;clear:both;margin:%%?15rpx?%% 0;background-image:-webkit-linear-gradient(bottom,#efefef 50%,transparent 50%);background-image:linear-gradient(bottom,red 50%,transparent 50%);background-size:100% 1px;background-repeat:no-repeat;background-position:bottom right}.active .empty{margin:60px auto;display:block;justify-content:center;font-size:%%?36rpx?%%;color:#656b6f;text-align:center}.active .empty .empimg{display:block;margin:0 auto}.active .empty .empimg .noimg{width:120px;height:120px;overflow:hidden}.active .empty .notext{margin-top:20px}.navcates{width:100%;height:%%?75rpx?%%;line-height:%%?75rpx?%%;display:flex;background-color:#fff;margin:%%?10rpx?%% 0;padding:%%?5rpx?%% %%?12rpx?%%;font-size:%%?28rpx?%%;color:#4b4b4b;border-left:4px solid #3a97fe;box-shadow:0 0 2px #fff;overflow:hidden;box-sizing:border-box}.navcates wx-text{flex:1}.navcates .left{flex:4;text-align:left;letter-spacing:%%?3rpx?%%}.navcates .left .name{display:inline-block;position:relative;top:%%?-1rpx?%%;left:%%?5rpx?%%;width:100%}.navcates .left .icon{display:inline-block;position:relative;top:%%?7rpx?%%}.navcates .left .icon wx-image{width:%%?40rpx?%%;height:%%?40rpx?%%}.navcates .right{flex:2;text-align:right;line-height:37px}.navcates .right .rtext{position:relative;top:%%?-12rpx?%%}.navcates .right wx-image{height:%%?45rpx?%%;width:%%?45rpx?%%;padding-top:%%?17.5rpx?%%}.noborder{border-left:none}.navcates{width:100%;height:%%?75rpx?%%;line-height:%%?75rpx?%%;display:flex;background-color:#fff;margin:%%?10rpx?%% 0;padding:%%?5rpx?%% %%?12rpx?%%;font-size:%%?28rpx?%%;color:#4b4b4b;border-left:4px solid #3a97fe;box-shadow:0 0 2px #fff;overflow:hidden;box-sizing:border-box}.navcates wx-text{flex:1}.navcates .left{flex:4;text-align:left;letter-spacing:%%?3rpx?%%}.navcates .left .name{display:inline-block;position:relative;top:%%?-1rpx?%%;left:%%?5rpx?%%;width:100%}.navcates .left .icon{display:inline-block;position:relative;top:%%?7rpx?%%}.navcates .left .icon wx-image{width:%%?40rpx?%%;height:%%?40rpx?%%}.navcates .right{flex:2;text-align:right;line-height:37px}.navcates .right .rtext{position:relative;top:%%?-12rpx?%%}.navcates .right wx-image{height:%%?45rpx?%%;width:%%?45rpx?%%;padding-top:%%?17.5rpx?%%}.noborder{border-left:none}.home{height:100%}.home .hot-city,.home .hot-view{padding:0 %%?40rpx?%%;padding-top:%%?28rpx?%%;background:#fff}.home .hot .title{height:%%?34rpx?%%;display:flex;align-items:center;padding:0 %%?10rpx?%%;box-sizing:border-box;font-size:%%?30rpx?%%;color:#948475;font-weight:700}.home .hot .title wx-text{flex:1 1 auto;text-align:center}.home .hot .title wx-text:first-child,.home .hot .title wx-text:last-child{height:1px;background:#eae1d2}.home .city-box,.home .img-box{overflow:hidden}.home .img-box{margin-top:%%?44rpx?%%;padding-bottom:%%?26rpx?%%}.home .img-box .tag{position:absolute;right:0;top:0;background:#3e9aff}.home .img-box .img{position:relative;width:%%?328rpx?%%;height:%%?194rpx?%%;border-radius:10px;float:left}.home .img-box .img .wx-text{color:#333}.home .img-box .img:nth-of-type(wx-2n){margin-left:%%?14rpx?%%}.home .img-box .img:nth-of-type(wx-n+wx-3){margin-top:%%?14rpx?%%}.home .img-box wx-image{width:100%;height:100%}.home .img-box wx-text{position:absolute;display:block;bottom:0;left:0;width:100%;padding-left:%%?28rpx?%%;box-sizing:border-box;height:%%?48rpx?%%;line-height:%%?48rpx?%%;background:rgba(255,255,255,.5);color:#8b8f93;font-size:%%?26rpx?%%}.home .divider{width:100%;height:%%?18rpx?%%;background:#f0ede2}.info{height:180px;width:100%;background:#3e9aff;position:relative}.info .user{margin:0 auto;text-align:center;justify-content:center;padding-top:40px;position:relative;z-index:10}.info .user .header{border-radius:50%;width:100%;display:block;margin:0 auto}.info .user .header .imgs{width:60px;height:60px;border-radius:50%;padding:%%?3rpx?%%;display:flex;justify-content:center;margin:0 auto;text-align:center;-webkit-border-radius:50%;border:2px solid #7facf9;position:relative}.info .user .header .imgs .ico-xji{position:absolute;left:-5px;top:-5px;z-index:100;width:15px;height:15px;text-align:center;border-radius:50%;-webkit-border-radius:50%;border:1px solid #5b95f7;background:rgba(255,255,255,.6);color:#4385f6;line-height:15px;font-size:%%?22rpx?%%}.info .user .header .imgs .ico-gender{position:absolute;z-index:100;width:15px;height:15px;line-height:15px;bottom:-4px;right:-4px;text-align:center;border-radius:50%;-webkit-border-radius:50%;border:1px solid #5b95f7;background:rgba(255,255,255,.6);color:#4385f6;font-size:%%?22rpx?%%}.info .user .header .imgs wx-image{width:100%;height:100%;border-radius:50%}.info .user .name{width:100%;display:block;text-align:center;color:#fff;font-size:%%?32rpx?%%}.info .user .udes{padding:10px 0;color:#b2c8fb}.info .user .setBg{position:absolute;top:%%?30rpx?%%;right:%%?30rpx?%%;width:%%?40rpx?%%;height:%%?40rpx?%%}.info .user .setBg wx-image{width:100%;height:100%}.info .Homebgc{position:absolute;width:100%;height:100%;top:0;left:0;z-index:1}.info .Homebgc wx-image{width:100%;height:100%}.active .empty{margin:60px auto;display:block;justify-content:center;font-size:%%?36rpx?%%;color:#656b6f;text-align:center}.active .empty .empimg{display:block;margin:0 auto}.active .empty .empimg .noimg{width:120px;height:120px;overflow:hidden}.active .empty .notext{margin-top:20px}.photo{overflow:hidden;background:#fff;margin:10px 0;padding:%%?5rpx?%% 0}.photo .photoList{width:96%;margin:0 auto;clear:both}.photo .photoList .list wx-image{width:18.8%;float:left;height:75px;margin:%%?5rpx?%%;box-sizing:border-box;border-radius:%%?5rpx?%%;background:rgba(245,245,245,.68)}.clear{clear:both}.addSimg{background:#fff;padding:10px;box-sizing:border-box;font-size:%%?16rpx?%%}.sendComp{height:100%}.btnDefault{position:absolute;bottom:%%?18rpx?%%;display:flex;height:%%?75rpx?%%;line-height:%%?75rpx?%%;box-sizing:border-box;width:100%;border-radius:%%?20rpx?%%;margin:0 auto;justify-content:center;color:#fff;text-align:center;font-size:%%?28rpx?%%}.btnDefault .leftbtn{flex:1;margin-right:%%?15rpx?%%;background:rgba(62,154,255,.79)}.btnDefault .rightbtn{flex:1;margin-left:%%?15rpx?%%;background:rgba(62,154,255,.79)}.PanContent .title{font-size:%%?38rpx?%%;color:#fff;text-shadow:0 0 2px #000;text-align:center}.defBtn{background-color:rgba(0,0,0,0);font-size:inherit;color:inherit}.colorRadius{width:%%?60rpx?%%;height:%%?60rpx?%%;float:left}.nopicShow{padding:%%?20rpx?%% %%?10rpx?%%;color:#bbb}@code-separator-line:__wxRoute = "pages/home/index";__wxRouteBegin = true;
define("pages/home/index.js", function(require, module, exports, window,document,frames,self,location,navigator,localStorage,history,Caches,screen,alert,confirm,prompt,fetch,XMLHttpRequest,WebSocket,webkit,WeixinJSCore,ServiceJSBridge,Reporter){
"use strict";function _interopRequireDefault(e){return e&&e.__esModule?e:{default:e}}function _asyncToGenerator(e){return function(){var t=e.apply(this,arguments);return new Promise(function(e,a){function r(n,i){try{var o=t[n](i),u=o.value}catch(e){return void a(e)}if(!o.done)return Promise.resolve(u).then(function(e){r("next",e)},function(e){r("throw",e)});e(u)}return r("next")})}}function _classCallCheck(e,t){if(!(e instanceof t))throw new TypeError("Cannot call a class as a function")}function _possibleConstructorReturn(e,t){if(!e)throw new ReferenceError("this hasn't been initialised - super() hasn't been called");return!t||"object"!=typeof t&&"function"!=typeof t?e:t}function _inherits(e,t){if("function"!=typeof t&&null!==t)throw new TypeError("Super expression must either be null or a function, not "+typeof t);e.prototype=Object.create(t&&t.prototype,{constructor:{value:e,enumerable:!1,writable:!0,configurable:!0}}),t&&(Object.setPrototypeOf?Object.setPrototypeOf(e,t):e.__proto__=t)}Object.defineProperty(exports,"__esModule",{value:!0});var _createClass=function(){function e(e,t){for(var a=0;a<t.length;a++){var r=t[a];r.enumerable=r.enumerable||!1,r.configurable=!0,"value"in r&&(r.writable=!0),Object.defineProperty(e,r.key,r)}}return function(t,a,r){return a&&e(t.prototype,a),r&&e(t,r),t}}(),_wepy=require("./../../npm/wepy/lib/wepy.js"),_wepy2=_interopRequireDefault(_wepy),_navcate=require("./../../base/navcate.js"),_navcate2=_interopRequireDefault(_navcate),_request=require("./../../api/request.js"),_active=require("./../../base/active.js"),_active2=_interopRequireDefault(_active),_util=require("./../../tools/util.js"),_util2=_interopRequireDefault(_util),_watch=require("./../../base/tips/watch.js"),_watch2=_interopRequireDefault(_watch),_cache=require("./../../tools/cache.js"),_cache2=_interopRequireDefault(_cache),_panal=require("./../../base/panal.js"),_panal2=_interopRequireDefault(_panal),_tip=require("./../../tools/tip.js"),_tip2=_interopRequireDefault(_tip),_upload=require("./../../tools/upload.js"),_upload2=_interopRequireDefault(_upload),Setting=function(e){function t(){var e,a,r,n;_classCallCheck(this,t);for(var i=arguments.length,o=Array(i),u=0;u<i;u++)o[u]=arguments[u];return a=r=_possibleConstructorReturn(this,(e=t.__proto__||Object.getPrototypeOf(t)).call.apply(e,[this].concat(o))),r.config={navigationBarTitleText:"我的主页",backgroundTextStyle:"light",navigationBarBackgroundColor:"#3e9aff"},r.$props={navcate:{title:"近期动态","v-bind:right.sync":"update"},photoTitle:{title:"照片精选",right:""},active:{"v-bind:uid.sync":"userId"},panal:{height:"380",class:"panal"},panal2:{height:"380",class:"pana2"}},r.$events={},r.components={navcate:_navcate2.default,photoTitle:_navcate2.default,active:_active2.default,watch:_watch2.default,panal:_panal2.default,panal2:_panal2.default},r.data={hotView:[],userId:0,userInfo:{},formShow:!0,PhotoWall:[],lastImgShow:[],windowHeight:"",previewImageData:[],authName:"",currentData:{gid:"",uid:""},isThatUid:!0,update:"更新",btns:["点赞助力","分享给好友"],favStatus:1,uploadList:[],uploads:!0,myuid:"",sysinfo:{},wall:{width:""},userSet:{backgroundImg:"",backgroundColor:"",open:!1,color:[],closeColor:"",homeBgc:"",look:""},shareData:{}},r.computed={},r.methods={addZan:function(){var e=this;(0,_request.requrst)("Billboard/addZan",{uuid:this.userId},function(t){_tip2.default.alert(t.msg,3e3),e.btns[0]=t.msg,t.code>0&&(e.favStatus=!e.favStatus),e.$apply()})},shareApp:function(){},tolower:function(e){this.$broadcast("activeGetList",!0)},previewImage:function(e){var t=_util2.default.get(e,"img");wx.previewImage({current:t,urls:this.previewImageData})},startYuyue:function(e){var t=_util2.default.get(e,"gid"),a=_util2.default.get(e,"uid"),r=_util2.default.get(e,"set");if(this.isThatUid)return void _util2.default.href("order/setting?id="+t);if(r){this.currentData.gid=t,this.currentData.uid=a;this.authName,_util2.default.get(e,"img");_util2.default.href("game/bespoke?gid="+this.currentData.gid+"&uuid="+this.currentData.uid)}else _tip2.default.alert("此用户未设置接单条件,暂不可预约");this.$apply()},chooseImage:function(e){var t=this;wx.chooseImage({sizeType:["original","compressed"],sourceType:["album","camera"],success:function(e){var a=e.tempFilePaths[0];t.uploads=!0,(0,_request.fileUpload)("Upload/formData",a,{},function(e){e.code>0&&t.uploadList.push({img:e.info.url,up:!1}),t.uploads=!1,(0,_request.requrst)("home/photowall",{img:e.info.url},function(e){e.code>0&&t.getAlldata(),_tip2.default.alert(e.msg)}),t.$apply()})}})},userSetStart:function(){this.userSet.open=!this.userSet.open,this.userSet.color=_util2.default.getColor(120),this.$apply()},choseColor:function(e){this.userSet.closeColor=_util2.default.get(e,"color"),this.$apply()},setBtnClick:function(e){var t=this,a=_util2.default.get(e,"id"),r=this;1==a?(this.userSet.backgroundImg=this.userSet.look,this.userSet.backgroundColor=this.userSet.closeColor,console.log(r.userSet.backgroundColor),wx.setNavigationBarColor({frontColor:"#ffffff",backgroundColor:r.userSet.backgroundColor,animation:{duration:400,timingFunc:"easeIn"}}),this.$apply()):(0,_request.requrst)("user/updateSet",{backgroundImg:this.userSet.look,backgroundColor:this.userSet.closeColor},function(e){e.code>0&&(_tip2.default.alert(e.msg),setTimeout(function(){t.userSet.open=!1,t.getAlldata(),t.$apply()},1e3))})},uploadSetBgc:function(){var e=this;_upload2.default.uploadFile(function(t){e.userSet.look=t,e.$apply()})}},r.events={BasePnalOpen:function(e){this.favStatus=1,this.$apply()},HomeSendHide:function(e){this.formShow=!0,this.$broadcast("HomeSendShow",!1),this.getAlldata(),this.$apply()},NavcateRightClick:function(e,t){"更新"==t&&(_util2.default.goto("send"),this.$apply())}},n=a,_possibleConstructorReturn(r,n)}return _inherits(t,e),_createClass(t,[{key:"isBall",value:function(){function e(){return t.apply(this,arguments)}var t=_asyncToGenerator(regeneratorRuntime.mark(function e(){return regeneratorRuntime.wrap(function(e){for(;;)switch(e.prev=e.next){case 0:this.$broadcast("BasePnalHeight",320,!0,"");case 1:case"end":return e.stop()}},e,this)}));return e}()},{key:"makePre",value:function(){function e(e){return t.apply(this,arguments)}var t=_asyncToGenerator(regeneratorRuntime.mark(function e(t){var a=this;return regeneratorRuntime.wrap(function(e){for(;;)switch(e.prev=e.next){case 0:t.forEach(function(e){a.previewImageData.push(e.wall_img)});case 1:case"end":return e.stop()}},e,this)}));return e}()},{key:"getPhotoWall",value:function(){function e(){return t.apply(this,arguments)}var t=_asyncToGenerator(regeneratorRuntime.mark(function e(){var t=this;return regeneratorRuntime.wrap(function(e){for(;;)switch(e.prev=e.next){case 0:(0,_request.requrst)("Active/photowall",{uuid:this.userId},function(e){t.PhotoWall=e.info,t.makePre(t.PhotoWall);for(var a=10-e.info.length,r=0;r<a;r++)t.lastImgShow.push({});t.$apply()});case 1:case"end":return e.stop()}},e,this)}));return e}()},{key:"getUserInfoData",value:function(){function e(){return t.apply(this,arguments)}var t=_asyncToGenerator(regeneratorRuntime.mark(function e(){var t=this;return regeneratorRuntime.wrap(function(e){for(;;)switch(e.prev=e.next){case 0:(0,_request.requrst)("user/getUserInfo",{uuid:this.userId},function(e){t.userInfo=e.info,t.authName=t.userInfo.user_nickname,"bill"==t.active&&(t.favStatus=e.msg),t.userInfo.set&&(t.userSet.backgroundImg=t.userInfo.set.set_backgroundImg,t.userSet.backgroundColor=t.userInfo.set.set_backgroundColor),t.$apply()});case 1:case"end":return e.stop()}},e,this)}));return e}()},{key:"getUserAbout",value:function(){function e(){return t.apply(this,arguments)}var t=_asyncToGenerator(regeneratorRuntime.mark(function e(){var t=this;return regeneratorRuntime.wrap(function(e){for(;;)switch(e.prev=e.next){case 0:(0,_request.requrst)("game/HomeMyGame",{uuid:this.userId},function(e){t.hotView=e.info.data,t.$apply()});case 1:case"end":return e.stop()}},e,this)}));return e}()},{key:"onShareAppMessage",value:function(e){if(this.shareData)return{title:this.shareData.title,path:this.shareData.path,imageUrl:this.shareData.imageUrl,success:function(e){},fail:function(e){}}}},{key:"getAlldata",value:function(){function e(){return t.apply(this,arguments)}var t=_asyncToGenerator(regeneratorRuntime.mark(function e(){return regeneratorRuntime.wrap(function(e){for(;;)switch(e.prev=e.next){case 0:this.getUserInfoData(),this.getPhotoWall(),this.getUserAbout(),this.$broadcast("activeStart",this.userId,0,this.isThatUid);case 4:case"end":return e.stop()}},e,this)}));return e}()},{key:"getShareData",value:function(){function e(){return t.apply(this,arguments)}var t=_asyncToGenerator(regeneratorRuntime.mark(function e(){var t=this;return regeneratorRuntime.wrap(function(e){for(;;)switch(e.prev=e.next){case 0:(0,_request.requrst)("home/getShareData",{uuid:this.userId},function(e){t.shareData=e.info,t.$apply()});case 1:case"end":return e.stop()}},e,this)}));return e}()},{key:"onShow",value:function(){this.windowHeight=_util2.default.sysInfo("windowHeight"),this.userId!=_cache2.default.get("user_uid")&&(this.isThatUid=!1,this.update=""),this.getAlldata(),this.isBall(),this.sysinfo=_util2.default.sysInfo(),this.wall.width=(this.sysinfo.windowWidth-40)/5,this.$apply()}},{key:"onLoad",value:function(e){this.userId=e.uid,this.active=e.come,this.getShareData()}}]),t}(_wepy2.default.page);Page(require("./../../npm/wepy/lib/wepy.js").default.$createPage(Setting,"pages/home/index"));
});require("pages/home/index.js")@code-separator-line:["div","scroll-view","view","text","image","button"]