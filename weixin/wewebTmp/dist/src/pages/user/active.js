/*v0.6vv_20170214_fbi*/
window.__wcc_version__='v0.6vv_20170214_fbi'
window.__wxml_transpiler_version__='v0.1'
var $gwxc
var $gaic={}
$gwx=function(path,global){
function _(a,b){b&&a.children.push(b);}
function _v(k){if(typeof(k)!='undefined')return {tag:'virtual','wxKey':k,children:[]};return {tag:'virtual',children:[]};}
function _n(tag){$gwxc++;if($gwxc>=16000){throw 'Dom limit exceeded, please check if there\'s any mistake you\'ve made.'};return {tag:tag.substr(0,3)=='wx-'?tag:'wx-'+tag,attr:{},children:[],n:[]}}
function _p(a,b){b&&a.properities.push(b);}
function _s(scope,env,key){return typeof(scope[key])!='undefined'?scope[key]:env[key]}function _wl(tname,prefix){console.warn('WXMLRT:'+prefix+':-1:-1:-1: Template `' + tname + '` is being called recursively, will be stop.')}$gwn=console.warn;
$gwl=console.log;
function $gwh()
{
function x(){}
x.prototype =
{
hn: function( obj )
{
if( typeof(obj) == 'object' )
{
var cnt=0;
var any=false;
for(var x in obj)
{
any|=x==='__value__';
cnt++;
if(cnt>2)break;
}
return cnt == 2 && any && obj.hasOwnProperty('__wxspec__') ? "h" : "n";
}
return "n";
},
nh: function( obj, special )
{
return { __value__: obj, __wxspec__: special ? special : true }
},
rv: function( obj )
{
return this.hn(obj)==='n'?obj:this.rv(obj.__value__);
}
}
return new x;
}
wh=$gwh();
function $gwrt( should_pass_type_info )
{
function ArithmeticEv( ops, e, s, g, o )
{
var rop = ops[0][1];
var _a,_b,_c,_d, _aa, _bb;
switch( rop )
{
case '?:':
_a = rev( ops[1], e, s, g, o );
_c = should_pass_type_info && ( wh.hn(_a) === 'h' );
_d = wh.rv( _a ) ? rev( ops[2], e, s, g, o ) : rev( ops[3], e, s, g, o );
_d = _c && wh.hn( _d ) === 'n' ? wh.nh( _d, 'c' ) : _d;
return _d;
break;
case '&&':
_a = rev( ops[1], e, s, g, o );
_c = should_pass_type_info && ( wh.hn(_a) === 'h' );
_d = wh.rv( _a ) ? rev( ops[2], e, s, g, o ) : wh.rv( _a );
_d = _c && wh.hn( _d ) === 'n' ? wh.nh( _d, 'c' ) : _d;
return _d;
break;
case '||':
_a = rev( ops[1], e, s, g, o );
_c = should_pass_type_info && ( wh.hn(_a) === 'h' );
_d = wh.rv( _a ) ? wh.rv(_a) : rev( ops[2], e, s, g, o );
_d = _c && wh.hn( _d ) === 'n' ? wh.nh( _d, 'c' ) : _d;
return _d;
break;
case '+':
case '*':
case '/':
case '%':
case '|':
case '^':
case '&':
case '===':
case '==':
case '!=':
case '!==':
case '>=':
case '<=':
case '>':
case '<':
case '<<':
case '>>':
_a = rev( ops[1], e, s, g, o );
_b = rev( ops[2], e, s, g, o );
_c = should_pass_type_info && (wh.hn( _a ) === 'h' || wh.hn( _b ) === 'h');
switch( rop )
{
case '+':
_d = wh.rv( _a ) + wh.rv( _b );
break;
case '*':
_d = wh.rv( _a ) * wh.rv( _b );
break;
case '/':
_d = wh.rv( _a ) / wh.rv( _b );
break;
case '%':
_d = wh.rv( _a ) % wh.rv( _b );
break;
case '|':
_d = wh.rv( _a ) | wh.rv( _b );
break;
case '^':
_d = wh.rv( _a ) ^ wh.rv( _b );
break;
case '&':
_d = wh.rv( _a ) & wh.rv( _b );
break;
case '===':
_d = wh.rv( _a ) === wh.rv( _b );
break;
case '==':
_d = wh.rv( _a ) == wh.rv( _b );
break;
case '!=':
_d = wh.rv( _a ) != wh.rv( _b );
break;
case '!==':
_d = wh.rv( _a ) !== wh.rv( _b );
break;
case '>=':
_d = wh.rv( _a ) >= wh.rv( _b );
break;
case '<=':
_d = wh.rv( _a ) <= wh.rv( _b );
break;
case '>':
_d = wh.rv( _a ) > wh.rv( _b );
break;
case '<':
_d = wh.rv( _a ) < wh.rv( _b );
break;
case '<<':
_d = wh.rv( _a ) << wh.rv( _b );
break;
case '>>':
_d = wh.rv( _a ) >> wh.rv( _b );
break;
default:
break;
}
return _c ? wh.nh( _d, "c" ) : _d;
break;
case '-':
_a = ops.length === 3 ? rev( ops[1], e, s, g, o ) : 0;
_b = ops.length === 3 ? rev( ops[2], e, s, g, o ) : rev( ops[1], e, s, g, o );
_c = should_pass_type_info && (wh.hn( _a ) === 'h' || wh.hn( _b ) === 'h');
_d = _c ? wh.rv( _a ) - wh.rv( _b ) : _a - _b;
return _c ? wh.nh( _d, "c" ) : _d;
break;
case '!':
_a = rev( ops[1], e, s, g, o );
_c = should_pass_type_info && (wh.hn( _a ) == 'h');
_d = !wh.rv(_a);
return _c ? wh.nh( _d, "c" ) : _d;
case '~':
_a = rev( ops[1], e, s, g, o );
_c = should_pass_type_info && (wh.hn( _a ) == 'h');
_d = ~wh.rv(_a);
return _c ? wh.nh( _d, "c" ) : _d;
default:
$gwn('unrecognized op' + rop );
}
}
function rev( ops, e, s, g, o )
{
var op = ops[0];
if( typeof(op)==='object' )
{
var vop=op[0];
var _a, _aa, _b, _bb, _c, _d, _s, _e, _ta, _tb, _td;
switch(vop)
{
case 2:
return ArithmeticEv(ops,e,s,g,o);
break;
case 4:
return rev( ops[1], e, s, g, o );
break;
case 5:
switch( ops.length )
{
case 2:
return should_pass_type_info ?
[rev(ops[1],e,s,g,o)] :
[wh.rv(rev(ops[1],e,s,g,o))];
break;
case 1:
return [];
break;
default:
_a = rev( ops[1],e,s,g,o );
_a.push(
should_pass_type_info ?
rev( ops[2],e,s,g,o ) :
wh.rv( rev(ops[2],e,s,g,o) )
);
return _a;
break;
}
break;
case 6:
_a = rev(ops[1],e,s,g,o);
_ta = wh.hn(_a)==='h';
_aa = _ta ? wh.rv(_a) : _a;
o.is_affected |= _ta;
if( should_pass_type_info )
{
if( _aa===null || typeof(_aa) === 'undefined' )
{
return _ta ? wh.nh(undefined, 'e') : undefined;
}
_b = rev(ops[2],e,s,g,o);
_tb = wh.hn(_b) === 'h';
_bb = _tb ? wh.rv(_b) : _b;
o.is_affected |= _tb;
if( _bb===null || typeof(_bb) === 'undefined' )
{
return (_ta || _tb) ? wh.nh(undefined, 'e') : undefined;
}
_d = _aa[_bb];
_td = wh.hn(_d)==='h';
o.is_affected |= _td;
return (_ta || _tb) ? (_td ? _d : wh.nh(_d, 'e')) : _d;
}
else
{
if( _aa===null || typeof(_aa) === 'undefined' )
{
return undefined;
}
_b = rev(ops[2],e,s,g,o);
_tb = wh.hn(_b) === 'h';
_bb = _tb ? wh.rv(_b) : _b;
o.is_affected |= _tb;
if( _bb===null || typeof(_bb) === 'undefined' )
{
return undefined;
}
_d = _aa[_bb];
_td = wh.hn(_d)==='h';
o.is_affected |= _td;
return _td ? wh.rv(_d) : _d;
}
case 7:
switch(ops[1][0])
{
case 11:
o.is_affected |= wh.hn(g)==='h';
return g;
case 3:
_s = wh.rv( s );
_e = wh.rv( e );
_b = ops[1][1];
_a = _s && _s.hasOwnProperty(_b) ?
s : _e && ( _e.hasOwnProperty(_b) ? e : undefined );
if( should_pass_type_info )
{
if( _a )
{
_ta = wh.hn(_a) === 'h';
_aa = _ta ? wh.rv( _a ) : _a;
_d = _aa[_b];
_td = wh.hn(_d) === 'h';
o.is_affected |= _ta || _td;
_d = _ta && !_td ? wh.nh(_d,'e') : _d;
return _d;
}
}
else
{
if( _a )
{
_ta = wh.hn(_a) === 'h';
_aa = _ta ? wh.rv( _a ) : _a;
_d = _aa[_b];
_td = wh.hn(_d) === 'h';
o.is_affected |= _ta || _td;
return wh.rv(_d);
}
}
return undefined;
}
break;
case 8:
_a = {};
_a[ops[1]] = rev(ops[2],e,s,g,o);
return _a;
break;
case 9:
_a = rev(ops[1],e,s,g,o);
_b = rev(ops[2],e,s,g,o);
function merge( _a, _b, _ow )
{
_ta = wh.hn(_a)==='h';
_tb = wh.hn(_b)==='h';
_aa = wh.rv(_a);
_bb = wh.rv(_b);
if( should_pass_type_info )
{
if( _tb )
{
for(var k in _bb)
{
if ( _ow || !_aa.hasOwnProperty(k) )
_aa[k]=wh.nh(_bb[k],'e');
}
}
else
{
for(var k in _bb)
{
if ( _ow || !_aa.hasOwnProperty(k) )
_aa[k]=_bb[k];
}
}
}
else
{
for(var k in _bb)
{
if ( _ow || _aa.hasOwnProperty(k) )
_aa[k]=wh.rv(_bb[k]);
}
}
return _a;
}
var _c = _a
var _ow = true
if ( typeof(ops[1][0]) === "object" && ops[1][0][0] === 10 ) {
_a = _b
_b = _c
_ow = false
}
if ( typeof(ops[1][0]) === "object" && ops[1][0][0] === 10 ) {
var _r = {}
return merge( merge( _r, _a, _ow ), _b, _ow );
}
else
return merge( _a, _b, _ow );
break;
case 10:
return should_pass_type_info ? rev(ops[1],e,s,g,o) : wh.rv(rev(ops[1],e,s,g,o));
}
}
else
{
if( op === 3 || op === 1 ) return ops[1];
else if( op === 11 )
{
var _a='';
for( var i = 1 ; i < ops.length ; i++ )
{
var xp = wh.rv(rev(ops[i],e,s,g,o));
_a += typeof(xp) === 'undefined' ? '' : xp;
}
return _a;
}
}
}
return rev;
}
gra=$gwrt(true);
grb=$gwrt(false);
function TestTest( expr, ops, e,s,g, expect_a, expect_b, expect_affected )
{
{
var o = {is_affected:false};
var a = gra( ops, e,s,g, o );
if( JSON.stringify(a) != JSON.stringify( expect_a )
|| o.is_affected != expect_affected )
{
console.warn( "A. " + expr + " get result " + JSON.stringify(a) + ", " + o.is_affected + ", but " + JSON.stringify( expect_a ) + ", " + expect_affected + " is expected" );
}
}
{
var o = {is_affected:false};
var a = grb( ops, e,s,g, o );
if( JSON.stringify(a) != JSON.stringify( expect_b )
|| o.is_affected != expect_affected )
{
console.warn( "B. " + expr + " get result " + JSON.stringify(a) + ", " + o.is_affected + ", but " + JSON.stringify( expect_b ) + ", " + expect_affected + " is expected" );
}
}
}

function wfor( to_iter, func, env, _s, global, father, itemname, indexname, keyname )
{
var _n = wh.hn( to_iter ) === 'n';
var scope = wh.rv( _s );
var has_old_item = scope.hasOwnProperty(itemname);
var has_old_index = scope.hasOwnProperty(indexname);
var old_item = scope[itemname];
var old_index = scope[indexname];
var full = Object.prototype.toString.call(wh.rv(to_iter));
var type = full[8];
if( type === 'N' && full[10] === 'l' ) type = 'X';
var _y;
if( _n )
{
if( type === 'A' )
{
for( var i = 0 ; i < to_iter.length ; i++ )
{
scope[itemname] = to_iter[i];
scope[indexname] = wh.nh(i, 'h');
_y = keyname ? (keyname==="*this" ? _v(wh.rv(to_iter[i])) : _v(wh.rv(wh.rv(to_iter[i])[keyname]))) : _v();
_(father,_y);
func( env, scope, _y, global );
}
}
else if( type === 'O' )
{
for( var k in to_iter )
{
scope[itemname] = to_iter[k];
scope[indexname] = wh.nh(k, 'h');
_y = keyname ? (keyname==="*this" ? _v(wh.rv(to_iter[k])) : _v(wh.rv(wh.rv(to_iter[k])[keyname]))) : _v();
_(father,_y);
func( env,scope,_y,global );
}
}
else if( type === 'S' )
{
for( var i = 0 ; i < to_iter.length ; i++ )
{
scope[itemname] = to_iter[i];
scope[indexname] = wh.nh(i, 'h');
_y = _v( to_iter[i] + i );
_(father,_y);
func( env, scope, _y, global );
}
}
else if( type === 'N' )
{
for( var i = 0 ; i < to_iter ; i++ )
{
scope[itemname] = i;
scope[indexname] = wh.nh(i, 'h');
_y = _v( i );
_(father,_y);
func(env,scope,_y,global);
}
}
else
{
}
}
else
{
var r_to_iter = wh.rv(to_iter);
var r_iter_item, iter_item;
if( type === 'A' )
{
for( var i = 0 ; i < r_to_iter.length ; i++ )
{
iter_item = r_to_iter[i];
iter_item = wh.hn(iter_item)==='n' ? wh.nh(iter_item,'h') : iter_item;
r_iter_item = wh.rv( iter_item );
scope[itemname] = iter_item
scope[indexname] = wh.nh(i, 'h');
_y = keyname ? (keyname==="*this" ? _v(r_iter_item) : _v(wh.rv(r_iter_item[keyname]))) : _v();
_(father,_y);
func( env, scope, _y, global );
}
}
else if( type === 'O' )
{
for( var k in r_to_iter )
{
iter_item = r_to_iter[k];
iter_item = wh.hn(iter_item)==='n'? wh.nh(iter_item,'h') : iter_item;
r_iter_item = wh.rv( iter_item );
scope[itemname] = iter_item;
scope[indexname] = wh.nh(k, 'h');
_y = keyname ? (keyname==="*this" ? _v(r_iter_item) : _v(wh.rv(r_iter_item[keyname]))) : _v();
_(father,_y);
func( env, scope, _y, global );
}
}
else if( type === 'S' )
{
for( var i = 0 ; i < r_to_iter.length ; i++ )
{
scope[itemname] = wh.nh(r_to_iter[i],'h');
scope[indexname] = wh.nh(i, 'h');
_y = _v( to_iter[i] + i );
_(father,_y);
func( env, scope, _y, global );
}
}
else if( type === 'N' )
{
for( var i = 0 ; i < r_to_iter ; i++ )
{
scope[itemname] = wh.nh(i,'h');
scope[indexname]= wh.nh(i,'h');
_y = _v( i );
_(father,_y);
func(env,scope,_y,global);
}
}
else
{
}
}
if(has_old_item)
{
scope[itemname]=old_item;
}
else
{
delete scope[itemname];
}
if(has_old_index)
{
scope[indexname]=old_index;
}
else{delete scope[indexname];}}


function _r( node, attrname, opindex, env, scope, global )
{
var o = {};
var a = grb( z[opindex], env, scope, global, o );
node.attr[attrname] = a;
if( o.is_affected ) node.n.push( attrname );
}
function _o( opindex, env, scope, global )
{
var nothing = {};
return grb( z[opindex], env, scope, global, nothing );
}
function _1( opindex, env, scope, global )
{
var nothing = {};
return gra( z[opindex], env, scope, global, nothing );
}
function _2( opindex, func, env, scope, global, father, itemname, indexname, keyname )
{
var to_iter = _1( opindex, env, scope, global, father, itemname, indexname, keyname );
wfor( to_iter, func, env, scope, global, father, itemname, indexname, keyname );
}
function _gv( )
{
if( typeof(window.__webview_engine_version__) == 'undefined' ) return 0.0;
return window.__webview_engine_version__;
}
function _m(tag,attrs,env,scope,global)
{
var tmp=_n(tag);
var base=0;
for(var i = 0 ; i < attrs.length ; i+=2 )
{
if(attrs[i+1]<0)
{tmp.attr[attrs[i]]=true;}else{_r(tmp,attrs[i],base+attrs[i+1],env,scope,global);
if(base===0)base=attrs[i+1];}}return tmp;};function _ai(i,p,e,me,r,c){var x=_grp(p,e,me);if(x)i.push(x);else{i.push('');console.warn('WXMLRT:'+me+':import:'+r+':'+c+': Path `'+p+'` not found from `'+me+'`.')}}function _grp(p,e,me){if(p[0]!='/'){var mepart=me.split('/');mepart.pop();var ppart=p.split('/');for(var i=0;i<ppart.length;i++){if(ppart[i]=='..')mepart.pop();else if(ppart[i]=='.' || !ppart[i])continue;else mepart.push(ppart[i]);}p=mepart.join('/');}if(me[0]=='.'&&p[0]=='/')p='.'+p;if(e[p])return p;if(e[p+'.wxml'])return p+'.wxml';}function _gd(p,c,e,d){if(!c)return;if(d[p][c])return d[p][c];for(var x=e[p].i.length-1;x>=0;x--){if(e[p].i[x]&&d[e[p].i[x]][c])return d[e[p].i[x]][c]};for(var x=e[p].ti.length-1;x>=0;x--){var q=_grp(e[p].ti[x],e,p);if(q&&d[q][c])return d[q][c]}
var ii=_gapi(e,p);for(var x=0;x<ii.length;x++){if(ii[x]&&d[ii[x]][c])return d[ii[x]][c]}for(var k=e[p].j.length-1;k>=0;k--)if(e[p].j[k]){for(var q=e[e[p].j[k]].ti.length-1;q>=0;q--){var pp=_grp(e[e[p].j[k]].ti[q],e,p);if(pp&&d[pp][c]){return d[pp][c]}}}}function _gapi(e,p){if(!p)return [];if($gaic[p]){return $gaic[p]};var ret=[],q=[],h=0,t=0,put={},visited={};q.push(p);visited[p]=true;t++;while(h<t){var a=q[h++];for(var i=0;i<e[a].ic.length;i++){var nd=e[a].ic[i];var np=_grp(nd,e,a);if(np&&!visited[np]){visited[np]=true;q.push(np);t++;}}for(var i=0;a!=p&&i<e[a].ti.length;i++){var ni=e[a].ti[i];var nm=_grp(ni,e,a);if(nm&&!put[nm]){put[nm]=true;ret.push(nm);}}}$gaic[p]=ret;return ret;}var $ixc={};function _ic(p,ent,me,e,s,r,gg){var x=_grp(p,ent,me);ent[me].j.push(x);if(x){if($ixc[x]){console.warn('WXMLRT:-1:include:-1:-1: `'+p+'` is being included in a loop, will be stop.');return;}$ixc[x]=true;try{ent[x].f(e,s,r,gg)}catch(e){}$ixc[x]=false;}else{console.warn('WXMLRT:'+me+':include:-1:-1: Included path `'+p+'` not found from `'+me+'`.')}}function _w(tn,f,line,c){console.warn('WXMLRT:'+f+':template:'+line+':'+c+': Template `'+tn+'` not found.');}function _ev(dom){var changed=false;delete dom.properities;delete dom.n;if(dom.children){do{changed=false;var newch = [];for(var i=0;i<dom.children.length;i++){var ch=dom.children[i];if( ch.tag=='virtual'){changed=true;for(var j=0;ch.children&&j<ch.children.length;j++){newch.push(ch.children[j]);}}else { newch.push(ch); } } dom.children = newch; }while(changed);for(var i=0;i<dom.children.length;i++){_ev(dom.children[i]);}} return dom; }var e_={}
if(global&&typeof(global.entrys)=='object')e_=global.entrys
var d_={}
if(global&&typeof(global.defines)=='object')d_=global.defines
var p_={}
var z = [];
  (function(z){
    var a = 11;
    function Z(ops){z.push(ops)};
    Z([3, 'tolower']);Z([3, 'view']);Z([3, '2']);Z([3, 'true']);Z([a, [3, 'height:'],[[7],[3, "windowHeight"]],[3, 'px']]);Z([3, 'active']);Z([[7],[3, "$active$list"]]);Z([3, 'key']);Z([[2, ">"], [[6],[[7],[3, "$active$list"]],[3, "length"]], [1, 0]]);Z([3, 'activeList']);Z([3, 'contentBox']);Z([3, 'actLeft']);Z([3, 'header']);Z([3, 'aspectFill']);Z([[6],[[7],[3, "item"]],[3, "user_avatar"]]);Z([3, 'actright']);Z([3, 'name']);Z([3, 'left']);Z([a, [[6],[[7],[3, "item"]],[3, "user_nickname"]]]);Z([3, 'right']);Z([a, [[6],[[7],[3, "item"]],[3, "time2"]]]);Z([3, 'actSpeak']);Z([a, [3, '\r\n                            '],[[6],[[7],[3, "item"]],[3, "active_text"]],[3, '\r\n                        ']]);Z([[6],[[7],[3, "item"]],[3, "active_img"]]);Z([3, 'imgItem']);Z([3, 'actImg']);Z([[7],[3, "$active$isThatUid"]]);Z([3, '$active$delActive']);Z([3, 'delete']);Z([[6],[[7],[3, "item"]],[3, "active_id"]]);Z([3, '删除']);Z([a, [3, 'imgFather'],[[6],[[6],[[7],[3, "item"]],[3, "active_img"]],[3, "length"]]]);Z([3, '$active$previewImage']);Z([3, 'showImg']);Z([[6],[[7],[3, "imgItem"]],[3, "img"]]);Z([[7],[3, "index"]]);Z([3, 'line']);Z([[7],[3, "$active$emptys"]]);Z([3, 'empty']);Z([3, 'abnor']);Z([3, 'abnor__box']);Z([[7],[3, "$active$empty$image"]]);Z([3, 'abnor__image']);Z([3, 'widthFix']);Z([[7],[3, "$active$empty$title"]]);Z([3, 'abnor__text']);Z([[7],[3, "$active$empty$tip"]]);Z([3, 'abnor__tip']);Z([[7],[3, "$active$empty$button"]]);Z([3, '$active$empty$emitAbnorTap']);Z([3, 'abnor__btn']);Z([3, 'loadingMore']);Z([[2, "!="], [[7],[3, "$active$loadingMore$flag"]], [1, 0]]);Z([[2, "=="], [[7],[3, "$active$loadingMore$flag"]], [1, 1]]);Z([3, 'weui-loadmore']);Z([3, 'weui-loading']);Z([3, 'weui-loadmore__tips']);Z([3, '正在加载']);Z([[2, "<"], [[7],[3, "$active$loadingMore$flag"]], [1, 0]]);Z([3, 'weui-loadmore weui-loadmore_line']);Z([3, 'weui-loadmore__tips weui-loadmore__tips_in-line']);Z([3, '加载完成']);Z([[2, "=="], [[7],[3, "$active$loadingMore$flag"]], [1, 2]]);Z([3, '滑动加载更多']);
  })(z);d_["./pages/user/active.wxml"] = {};
  var m0=function(e,s,r,gg){
    var oly = _m( "scroll-view", ["bindscrolltolower", 0,"class", 1,"lowerThreshold", 1,"scrollY", 2,"style", 3], e, s, gg);var omy = _n("view");_r(omy, 'class', 5, e, s, gg);var ony = _n("view");_r(ony, 'class', 5, e, s, gg);var ooy = _v();var opy = function(oty,osy,ory,gg){var oqy = _v();
      if (_o(8, oty, osy, gg)) {
        oqy.wxVkey = 1;var ovy = _n("view");_r(ovy, 'class', 9, oty, osy, gg);var oxy = _n("view");_r(oxy, 'class', 10, oty, osy, gg);var oyy = _n("view");_r(oyy, 'class', 11, oty, osy, gg);var ozy = _m( "image", ["class", 12,"mode", 1,"src", 2], oty, osy, gg);_(oyy,ozy);_(oxy,oyy);var o_y = _n("view");_r(o_y, 'class', 15, oty, osy, gg);var oAz = _n("view");_r(oAz, 'class', 16, oty, osy, gg);var oBz = _n("view");_r(oBz, 'class', 17, oty, osy, gg);var oCz = _o(18, oty, osy, gg);_(oBz,oCz);_(oAz,oBz);var oDz = _n("view");_r(oDz, 'class', 19, oty, osy, gg);var oEz = _o(20, oty, osy, gg);_(oDz,oEz);_(oAz,oDz);_(o_y,oAz);var oFz = _n("view");_r(oFz, 'class', 21, oty, osy, gg);var oGz = _o(22, oty, osy, gg);_(oFz,oGz);_(o_y,oFz);var oHz = _v();var oIz = function(oMz,oLz,oKz,gg){var oJz = _n("view");_r(oJz, 'class', 25, oMz, oLz, gg);var oOz = _v();
      if (_o(26, oMz, oLz, gg)) {
        oOz.wxVkey = 1;var oPz = _m( "view", ["bindtap", 27,"class", 1,"data-aid", 2], oMz, oLz, gg);var oRz = _o(30, oMz, oLz, gg);_(oPz,oRz);_(oOz, oPz);
      } _(oJz,oOz);var oSz = _n("view");_r(oSz, 'class', 31, oMz, oLz, gg);var oTz = _m( "image", ["mode", 13,"bindtap", 19,"class", 20,"data-img", 21,"src", 21,"data-index", 22], oMz, oLz, gg);_(oSz,oTz);_(oJz,oSz);_(oKz, oJz);return oKz;};_2(23, oIz, oty, osy, gg, oHz, "imgItem", "index", 'key');_(o_y,oHz);_(oxy,o_y);_(ovy,oxy);var oUz = _n("view");_r(oUz, 'class', 36, oty, osy, gg);_(ovy,oUz);_(oqy, ovy);
      } _(ory, oqy);return ory;};_2(6, opy, e, s, gg, ooy, "item", "index", 'key');_(ony,ooy);var oVz = _v();
      if (_o(37, e, s, gg)) {
        oVz.wxVkey = 1;var oWz = _n("view");_r(oWz, 'class', 38, e, s, gg);var oYz = _n("view");_r(oYz, 'class', 38, e, s, gg);var oZz = _n("view");_r(oZz, 'class', 39, e, s, gg);var oaz = _n("view");_r(oaz, 'class', 40, e, s, gg);var obz = _v();
      if (_o(41, e, s, gg)) {
        obz.wxVkey = 1;var ocz = _m( "image", ["src", 41,"class", 1,"mode", 2], e, s, gg);_(obz, ocz);
      } _(oaz,obz);var oez = _v();
      if (_o(44, e, s, gg)) {
        oez.wxVkey = 1;var ofz = _n("view");_r(ofz, 'class', 45, e, s, gg);var ohz = _o(44, e, s, gg);_(ofz,ohz);_(oez, ofz);
      } _(oaz,oez);var oiz = _v();
      if (_o(46, e, s, gg)) {
        oiz.wxVkey = 1;var ojz = _n("view");_r(ojz, 'class', 47, e, s, gg);var olz = _o(46, e, s, gg);_(ojz,olz);_(oiz, ojz);
      } _(oaz,oiz);var omz = _v();
      if (_o(48, e, s, gg)) {
        omz.wxVkey = 1;var onz = _m( "view", ["bindtap", 49,"class", 1], e, s, gg);var opz = _o(48, e, s, gg);_(onz,opz);_(omz, onz);
      } _(oaz,omz);_(oZz,oaz);_(oYz,oZz);_(oWz,oYz);_(oVz, oWz);
      } _(ony,oVz);var oqz = _n("view");_r(oqz, 'class', 51, e, s, gg);var orz = _v();
      if (_o(52, e, s, gg)) {
        orz.wxVkey = 1;var osz = _n("view");_r(osz, 'class', 51, e, s, gg);var ouz = _v();
      if (_o(53, e, s, gg)) {
        ouz.wxVkey = 1;var ovz = _n("view");_r(ovz, 'class', 54, e, s, gg);var oxz = _n("view");_r(oxz, 'class', 55, e, s, gg);_(ovz,oxz);var oyz = _n("view");_r(oyz, 'class', 56, e, s, gg);var ozz = _o(57, e, s, gg);_(oyz,ozz);_(ovz,oyz);_(ouz, ovz);
      } _(osz,ouz);var o_z = _v();
      if (_o(58, e, s, gg)) {
        o_z.wxVkey = 1;var oA_ = _n("view");_r(oA_, 'class', 59, e, s, gg);var oC_ = _n("view");_r(oC_, 'class', 60, e, s, gg);var oD_ = _o(61, e, s, gg);_(oC_,oD_);_(oA_,oC_);_(o_z, oA_);
      } _(osz,o_z);var oE_ = _v();
      if (_o(62, e, s, gg)) {
        oE_.wxVkey = 1;var oF_ = _n("view");_r(oF_, 'class', 59, e, s, gg);var oH_ = _n("view");_r(oH_, 'class', 60, e, s, gg);var oI_ = _o(63, e, s, gg);_(oH_,oI_);_(oF_,oH_);_(oE_, oF_);
      } _(osz,oE_);_(orz, osz);
      } _(oqz,orz);_(ony,oqz);_(omy,ony);_(oly,omy);_(r,oly);
    return r;
  };
        e_["./pages/user/active.wxml"]={f:m0,j:[],i:[],ti:[],ic:[]};
if(path&&e_[path]){
window.__wxml_comp_version__=0.02
return function(env,dd,global){$gwxc=0;var root={"tag":"wx-page"};root.children=[]
var main=e_[path].f
if(typeof(window.__webview_engine_version__)!='undefined'&&window.__webview_engine_version__+1e-6>=0.02+1e-6&&window.__mergeData__)
{env=window.__mergeData__(env,dd);}
try{
main(env,{},root,global);
if(typeof(window.__webview_engine_version__)=='undefined'||window.__webview_engine_version__+1e-6<0.01+1e-6){return _ev(root);}}catch(err){console.log(err)}return root;}}}@code-separator-line:.empty .abnor{position:relative;display:block;width:100%;height:0;padding-bottom:100%;overflow:hidden}.empty .abnor__box{position:absolute;display:flex;top:0;bottom:0;left:0;right:0;flex-direction:column;justify-content:center;align-items:center}.empty .abnor__btn{min-width:%%?228rpx?%%;height:%%?66rpx?%%;margin-top:%%?30rpx?%%;padding:0 %%?30rpx?%%;background-color:#3e9aff;border:0 none;border-radius:%%?2rpx?%%;color:#fff;font-size:%%?28rpx?%%;text-align:center;overflow:hidden;line-height:%%?66rpx?%%}.empty .abnor__btn:active{background-color:#3e9aff}.empty .abnor__image{width:%%?514rpx?%%;background:transparent no-repeat;background-size:cover}.empty .abnor__text{margin-top:%%?30rpx?%%;color:#333;font-size:%%?28rpx?%%}.empty .abnor__tip{margin-top:%%?20rpx?%%;color:#666;font-size:%%?24rpx?%%}.loadingMore{clear:both;display:block}.active{background:#fff;padding-top:2px}.active .activeList .contentBox{display:flex;width:100%;justify-content:center;margin:0 auto;padding:%%?10rpx?%% %%?15rpx?%%;box-sizing:border-box}.active .activeList .contentBox .actLeft{flex:1}.active .activeList .contentBox .actLeft .header{width:%%?60rpx?%%;height:%%?60rpx?%%}.active .activeList .contentBox .actright{flex:8;font-size:%%?28rpx?%%}.active .activeList .contentBox .actright .name{height:%%?50rpx?%%;line-height:%%?50rpx?%%;color:#4f5b85;font-size:%%?32rpx?%%;display:flex}.active .activeList .contentBox .actright .name .left{flex:1;text-align:left}.active .activeList .contentBox .actright .name .right{flex:1;text-align:right;color:#656b6f;font-size:%%?24rpx?%%;padding-right:10px}.active .activeList .contentBox .actright .actImg{margin-top:10px;position:relative}.active .activeList .contentBox .actright .actImg .imgFather1 .showImg{max-width:180px;vertical-align:middle;max-height:260px}.active .activeList .contentBox .actright .actImg .imgFather2 .showImg{width:48%;vertical-align:middle;float:left;height:150px;margin:%%?5rpx?%%}.active .activeList .contentBox .actright .actImg .imgFather3 .showImg{width:31%;vertical-align:middle;float:left;height:90px;margin:%%?5rpx?%%;box-sizing:border-box}.active .activeList .contentBox .actright .actImg .delete{position:absolute;bottom:0;right:%%?10rpx?%%;color:#666;font-size:%%?24rpx?%%}.active .activeList .line{height:1px;width:100%;clear:both;margin:%%?15rpx?%% 0;background-image:-webkit-linear-gradient(bottom,#efefef 50%,transparent 50%);background-image:linear-gradient(bottom,red 50%,transparent 50%);background-size:100% 1px;background-repeat:no-repeat;background-position:bottom right}.active .empty{margin:60px auto;display:block;justify-content:center;font-size:%%?36rpx?%%;color:#656b6f;text-align:center}.active .empty .empimg{display:block;margin:0 auto}.active .empty .empimg .noimg{width:120px;height:120px;overflow:hidden}.active .empty .notext{margin-top:20px}@code-separator-line:__wxRoute = "pages/user/active";__wxRouteBegin = true;
define("pages/user/active.js", function(require, module, exports, window,document,frames,self,location,navigator,localStorage,history,Caches,screen,alert,confirm,prompt,fetch,XMLHttpRequest,WebSocket,webkit,WeixinJSCore,ServiceJSBridge,Reporter){
"use strict";function _interopRequireDefault(e){return e&&e.__esModule?e:{default:e}}function _classCallCheck(e,t){if(!(e instanceof t))throw new TypeError("Cannot call a class as a function")}function _possibleConstructorReturn(e,t){if(!e)throw new ReferenceError("this hasn't been initialised - super() hasn't been called");return!t||"object"!=typeof t&&"function"!=typeof t?e:t}function _inherits(e,t){if("function"!=typeof t&&null!==t)throw new TypeError("Super expression must either be null or a function, not "+typeof t);e.prototype=Object.create(t&&t.prototype,{constructor:{value:e,enumerable:!1,writable:!0,configurable:!0}}),t&&(Object.setPrototypeOf?Object.setPrototypeOf(e,t):e.__proto__=t)}Object.defineProperty(exports,"__esModule",{value:!0});var _createClass=function(){function e(e,t){for(var r=0;r<t.length;r++){var i=t[r];i.enumerable=i.enumerable||!1,i.configurable=!0,"value"in i&&(i.writable=!0),Object.defineProperty(e,i.key,i)}}return function(t,r,i){return r&&e(t.prototype,r),i&&e(t,i),t}}(),_wepy=require("./../../npm/wepy/lib/wepy.js"),_wepy2=_interopRequireDefault(_wepy),_active=require("./../../base/active.js"),_active2=_interopRequireDefault(_active),_util=require("./../../tools/util.js"),_util2=_interopRequireDefault(_util),_cache=require("./../../tools/cache.js"),_cache2=_interopRequireDefault(_cache),Setting=function(e){function t(){var e,r,i,n;_classCallCheck(this,t);for(var o=arguments.length,a=Array(o),u=0;u<o;u++)a[u]=arguments[u];return r=i=_possibleConstructorReturn(this,(e=t.__proto__||Object.getPrototypeOf(t)).call.apply(e,[this].concat(a))),i.config={navigationBarTitleText:"我的玩伴日常"},i.components={active:_active2.default},i.data={windowHeight:""},i.computed={},i.methods={tolower:function(e){this.$broadcast("activeStart",this.uuid,0,!0)}},i.events={},n=r,_possibleConstructorReturn(i,n)}return _inherits(t,e),_createClass(t,[{key:"onLoad",value:function(e){this.uuid=_cache2.default.get("user_uid"),this.windowHeight=_util2.default.sysInfo("windowHeight"),this.$broadcast("activeStart",this.uuid,0,!0),this.$apply()}}]),t}(_wepy2.default.page);Page(require("./../../npm/wepy/lib/wepy.js").default.$createPage(Setting,"pages/user/active"));
});require("pages/user/active.js")@code-separator-line:["div","scroll-view","view","image"]