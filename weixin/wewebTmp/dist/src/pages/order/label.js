/*v0.6vv_20170214_fbi*/
window.__wcc_version__='v0.6vv_20170214_fbi'
window.__wxml_transpiler_version__='v0.1'
var $gwxc
var $gaic={}
$gwx=function(path,global){
function _(a,b){b&&a.children.push(b);}
function _v(k){if(typeof(k)!='undefined')return {tag:'virtual','wxKey':k,children:[]};return {tag:'virtual',children:[]};}
function _n(tag){$gwxc++;if($gwxc>=16000){throw 'Dom limit exceeded, please check if there\'s any mistake you\'ve made.'};return {tag:tag.substr(0,3)=='wx-'?tag:'wx-'+tag,attr:{},children:[],n:[]}}
function _p(a,b){b&&a.properities.push(b);}
function _s(scope,env,key){return typeof(scope[key])!='undefined'?scope[key]:env[key]}function _wl(tname,prefix){console.warn('WXMLRT:'+prefix+':-1:-1:-1: Template `' + tname + '` is being called recursively, will be stop.')}$gwn=console.warn;
$gwl=console.log;
function $gwh()
{
function x(){}
x.prototype =
{
hn: function( obj )
{
if( typeof(obj) == 'object' )
{
var cnt=0;
var any=false;
for(var x in obj)
{
any|=x==='__value__';
cnt++;
if(cnt>2)break;
}
return cnt == 2 && any && obj.hasOwnProperty('__wxspec__') ? "h" : "n";
}
return "n";
},
nh: function( obj, special )
{
return { __value__: obj, __wxspec__: special ? special : true }
},
rv: function( obj )
{
return this.hn(obj)==='n'?obj:this.rv(obj.__value__);
}
}
return new x;
}
wh=$gwh();
function $gwrt( should_pass_type_info )
{
function ArithmeticEv( ops, e, s, g, o )
{
var rop = ops[0][1];
var _a,_b,_c,_d, _aa, _bb;
switch( rop )
{
case '?:':
_a = rev( ops[1], e, s, g, o );
_c = should_pass_type_info && ( wh.hn(_a) === 'h' );
_d = wh.rv( _a ) ? rev( ops[2], e, s, g, o ) : rev( ops[3], e, s, g, o );
_d = _c && wh.hn( _d ) === 'n' ? wh.nh( _d, 'c' ) : _d;
return _d;
break;
case '&&':
_a = rev( ops[1], e, s, g, o );
_c = should_pass_type_info && ( wh.hn(_a) === 'h' );
_d = wh.rv( _a ) ? rev( ops[2], e, s, g, o ) : wh.rv( _a );
_d = _c && wh.hn( _d ) === 'n' ? wh.nh( _d, 'c' ) : _d;
return _d;
break;
case '||':
_a = rev( ops[1], e, s, g, o );
_c = should_pass_type_info && ( wh.hn(_a) === 'h' );
_d = wh.rv( _a ) ? wh.rv(_a) : rev( ops[2], e, s, g, o );
_d = _c && wh.hn( _d ) === 'n' ? wh.nh( _d, 'c' ) : _d;
return _d;
break;
case '+':
case '*':
case '/':
case '%':
case '|':
case '^':
case '&':
case '===':
case '==':
case '!=':
case '!==':
case '>=':
case '<=':
case '>':
case '<':
case '<<':
case '>>':
_a = rev( ops[1], e, s, g, o );
_b = rev( ops[2], e, s, g, o );
_c = should_pass_type_info && (wh.hn( _a ) === 'h' || wh.hn( _b ) === 'h');
switch( rop )
{
case '+':
_d = wh.rv( _a ) + wh.rv( _b );
break;
case '*':
_d = wh.rv( _a ) * wh.rv( _b );
break;
case '/':
_d = wh.rv( _a ) / wh.rv( _b );
break;
case '%':
_d = wh.rv( _a ) % wh.rv( _b );
break;
case '|':
_d = wh.rv( _a ) | wh.rv( _b );
break;
case '^':
_d = wh.rv( _a ) ^ wh.rv( _b );
break;
case '&':
_d = wh.rv( _a ) & wh.rv( _b );
break;
case '===':
_d = wh.rv( _a ) === wh.rv( _b );
break;
case '==':
_d = wh.rv( _a ) == wh.rv( _b );
break;
case '!=':
_d = wh.rv( _a ) != wh.rv( _b );
break;
case '!==':
_d = wh.rv( _a ) !== wh.rv( _b );
break;
case '>=':
_d = wh.rv( _a ) >= wh.rv( _b );
break;
case '<=':
_d = wh.rv( _a ) <= wh.rv( _b );
break;
case '>':
_d = wh.rv( _a ) > wh.rv( _b );
break;
case '<':
_d = wh.rv( _a ) < wh.rv( _b );
break;
case '<<':
_d = wh.rv( _a ) << wh.rv( _b );
break;
case '>>':
_d = wh.rv( _a ) >> wh.rv( _b );
break;
default:
break;
}
return _c ? wh.nh( _d, "c" ) : _d;
break;
case '-':
_a = ops.length === 3 ? rev( ops[1], e, s, g, o ) : 0;
_b = ops.length === 3 ? rev( ops[2], e, s, g, o ) : rev( ops[1], e, s, g, o );
_c = should_pass_type_info && (wh.hn( _a ) === 'h' || wh.hn( _b ) === 'h');
_d = _c ? wh.rv( _a ) - wh.rv( _b ) : _a - _b;
return _c ? wh.nh( _d, "c" ) : _d;
break;
case '!':
_a = rev( ops[1], e, s, g, o );
_c = should_pass_type_info && (wh.hn( _a ) == 'h');
_d = !wh.rv(_a);
return _c ? wh.nh( _d, "c" ) : _d;
case '~':
_a = rev( ops[1], e, s, g, o );
_c = should_pass_type_info && (wh.hn( _a ) == 'h');
_d = ~wh.rv(_a);
return _c ? wh.nh( _d, "c" ) : _d;
default:
$gwn('unrecognized op' + rop );
}
}
function rev( ops, e, s, g, o )
{
var op = ops[0];
if( typeof(op)==='object' )
{
var vop=op[0];
var _a, _aa, _b, _bb, _c, _d, _s, _e, _ta, _tb, _td;
switch(vop)
{
case 2:
return ArithmeticEv(ops,e,s,g,o);
break;
case 4:
return rev( ops[1], e, s, g, o );
break;
case 5:
switch( ops.length )
{
case 2:
return should_pass_type_info ?
[rev(ops[1],e,s,g,o)] :
[wh.rv(rev(ops[1],e,s,g,o))];
break;
case 1:
return [];
break;
default:
_a = rev( ops[1],e,s,g,o );
_a.push(
should_pass_type_info ?
rev( ops[2],e,s,g,o ) :
wh.rv( rev(ops[2],e,s,g,o) )
);
return _a;
break;
}
break;
case 6:
_a = rev(ops[1],e,s,g,o);
_ta = wh.hn(_a)==='h';
_aa = _ta ? wh.rv(_a) : _a;
o.is_affected |= _ta;
if( should_pass_type_info )
{
if( _aa===null || typeof(_aa) === 'undefined' )
{
return _ta ? wh.nh(undefined, 'e') : undefined;
}
_b = rev(ops[2],e,s,g,o);
_tb = wh.hn(_b) === 'h';
_bb = _tb ? wh.rv(_b) : _b;
o.is_affected |= _tb;
if( _bb===null || typeof(_bb) === 'undefined' )
{
return (_ta || _tb) ? wh.nh(undefined, 'e') : undefined;
}
_d = _aa[_bb];
_td = wh.hn(_d)==='h';
o.is_affected |= _td;
return (_ta || _tb) ? (_td ? _d : wh.nh(_d, 'e')) : _d;
}
else
{
if( _aa===null || typeof(_aa) === 'undefined' )
{
return undefined;
}
_b = rev(ops[2],e,s,g,o);
_tb = wh.hn(_b) === 'h';
_bb = _tb ? wh.rv(_b) : _b;
o.is_affected |= _tb;
if( _bb===null || typeof(_bb) === 'undefined' )
{
return undefined;
}
_d = _aa[_bb];
_td = wh.hn(_d)==='h';
o.is_affected |= _td;
return _td ? wh.rv(_d) : _d;
}
case 7:
switch(ops[1][0])
{
case 11:
o.is_affected |= wh.hn(g)==='h';
return g;
case 3:
_s = wh.rv( s );
_e = wh.rv( e );
_b = ops[1][1];
_a = _s && _s.hasOwnProperty(_b) ?
s : _e && ( _e.hasOwnProperty(_b) ? e : undefined );
if( should_pass_type_info )
{
if( _a )
{
_ta = wh.hn(_a) === 'h';
_aa = _ta ? wh.rv( _a ) : _a;
_d = _aa[_b];
_td = wh.hn(_d) === 'h';
o.is_affected |= _ta || _td;
_d = _ta && !_td ? wh.nh(_d,'e') : _d;
return _d;
}
}
else
{
if( _a )
{
_ta = wh.hn(_a) === 'h';
_aa = _ta ? wh.rv( _a ) : _a;
_d = _aa[_b];
_td = wh.hn(_d) === 'h';
o.is_affected |= _ta || _td;
return wh.rv(_d);
}
}
return undefined;
}
break;
case 8:
_a = {};
_a[ops[1]] = rev(ops[2],e,s,g,o);
return _a;
break;
case 9:
_a = rev(ops[1],e,s,g,o);
_b = rev(ops[2],e,s,g,o);
function merge( _a, _b, _ow )
{
_ta = wh.hn(_a)==='h';
_tb = wh.hn(_b)==='h';
_aa = wh.rv(_a);
_bb = wh.rv(_b);
if( should_pass_type_info )
{
if( _tb )
{
for(var k in _bb)
{
if ( _ow || !_aa.hasOwnProperty(k) )
_aa[k]=wh.nh(_bb[k],'e');
}
}
else
{
for(var k in _bb)
{
if ( _ow || !_aa.hasOwnProperty(k) )
_aa[k]=_bb[k];
}
}
}
else
{
for(var k in _bb)
{
if ( _ow || _aa.hasOwnProperty(k) )
_aa[k]=wh.rv(_bb[k]);
}
}
return _a;
}
var _c = _a
var _ow = true
if ( typeof(ops[1][0]) === "object" && ops[1][0][0] === 10 ) {
_a = _b
_b = _c
_ow = false
}
if ( typeof(ops[1][0]) === "object" && ops[1][0][0] === 10 ) {
var _r = {}
return merge( merge( _r, _a, _ow ), _b, _ow );
}
else
return merge( _a, _b, _ow );
break;
case 10:
return should_pass_type_info ? rev(ops[1],e,s,g,o) : wh.rv(rev(ops[1],e,s,g,o));
}
}
else
{
if( op === 3 || op === 1 ) return ops[1];
else if( op === 11 )
{
var _a='';
for( var i = 1 ; i < ops.length ; i++ )
{
var xp = wh.rv(rev(ops[i],e,s,g,o));
_a += typeof(xp) === 'undefined' ? '' : xp;
}
return _a;
}
}
}
return rev;
}
gra=$gwrt(true);
grb=$gwrt(false);
function TestTest( expr, ops, e,s,g, expect_a, expect_b, expect_affected )
{
{
var o = {is_affected:false};
var a = gra( ops, e,s,g, o );
if( JSON.stringify(a) != JSON.stringify( expect_a )
|| o.is_affected != expect_affected )
{
console.warn( "A. " + expr + " get result " + JSON.stringify(a) + ", " + o.is_affected + ", but " + JSON.stringify( expect_a ) + ", " + expect_affected + " is expected" );
}
}
{
var o = {is_affected:false};
var a = grb( ops, e,s,g, o );
if( JSON.stringify(a) != JSON.stringify( expect_b )
|| o.is_affected != expect_affected )
{
console.warn( "B. " + expr + " get result " + JSON.stringify(a) + ", " + o.is_affected + ", but " + JSON.stringify( expect_b ) + ", " + expect_affected + " is expected" );
}
}
}

function wfor( to_iter, func, env, _s, global, father, itemname, indexname, keyname )
{
var _n = wh.hn( to_iter ) === 'n';
var scope = wh.rv( _s );
var has_old_item = scope.hasOwnProperty(itemname);
var has_old_index = scope.hasOwnProperty(indexname);
var old_item = scope[itemname];
var old_index = scope[indexname];
var full = Object.prototype.toString.call(wh.rv(to_iter));
var type = full[8];
if( type === 'N' && full[10] === 'l' ) type = 'X';
var _y;
if( _n )
{
if( type === 'A' )
{
for( var i = 0 ; i < to_iter.length ; i++ )
{
scope[itemname] = to_iter[i];
scope[indexname] = wh.nh(i, 'h');
_y = keyname ? (keyname==="*this" ? _v(wh.rv(to_iter[i])) : _v(wh.rv(wh.rv(to_iter[i])[keyname]))) : _v();
_(father,_y);
func( env, scope, _y, global );
}
}
else if( type === 'O' )
{
for( var k in to_iter )
{
scope[itemname] = to_iter[k];
scope[indexname] = wh.nh(k, 'h');
_y = keyname ? (keyname==="*this" ? _v(wh.rv(to_iter[k])) : _v(wh.rv(wh.rv(to_iter[k])[keyname]))) : _v();
_(father,_y);
func( env,scope,_y,global );
}
}
else if( type === 'S' )
{
for( var i = 0 ; i < to_iter.length ; i++ )
{
scope[itemname] = to_iter[i];
scope[indexname] = wh.nh(i, 'h');
_y = _v( to_iter[i] + i );
_(father,_y);
func( env, scope, _y, global );
}
}
else if( type === 'N' )
{
for( var i = 0 ; i < to_iter ; i++ )
{
scope[itemname] = i;
scope[indexname] = wh.nh(i, 'h');
_y = _v( i );
_(father,_y);
func(env,scope,_y,global);
}
}
else
{
}
}
else
{
var r_to_iter = wh.rv(to_iter);
var r_iter_item, iter_item;
if( type === 'A' )
{
for( var i = 0 ; i < r_to_iter.length ; i++ )
{
iter_item = r_to_iter[i];
iter_item = wh.hn(iter_item)==='n' ? wh.nh(iter_item,'h') : iter_item;
r_iter_item = wh.rv( iter_item );
scope[itemname] = iter_item
scope[indexname] = wh.nh(i, 'h');
_y = keyname ? (keyname==="*this" ? _v(r_iter_item) : _v(wh.rv(r_iter_item[keyname]))) : _v();
_(father,_y);
func( env, scope, _y, global );
}
}
else if( type === 'O' )
{
for( var k in r_to_iter )
{
iter_item = r_to_iter[k];
iter_item = wh.hn(iter_item)==='n'? wh.nh(iter_item,'h') : iter_item;
r_iter_item = wh.rv( iter_item );
scope[itemname] = iter_item;
scope[indexname] = wh.nh(k, 'h');
_y = keyname ? (keyname==="*this" ? _v(r_iter_item) : _v(wh.rv(r_iter_item[keyname]))) : _v();
_(father,_y);
func( env, scope, _y, global );
}
}
else if( type === 'S' )
{
for( var i = 0 ; i < r_to_iter.length ; i++ )
{
scope[itemname] = wh.nh(r_to_iter[i],'h');
scope[indexname] = wh.nh(i, 'h');
_y = _v( to_iter[i] + i );
_(father,_y);
func( env, scope, _y, global );
}
}
else if( type === 'N' )
{
for( var i = 0 ; i < r_to_iter ; i++ )
{
scope[itemname] = wh.nh(i,'h');
scope[indexname]= wh.nh(i,'h');
_y = _v( i );
_(father,_y);
func(env,scope,_y,global);
}
}
else
{
}
}
if(has_old_item)
{
scope[itemname]=old_item;
}
else
{
delete scope[itemname];
}
if(has_old_index)
{
scope[indexname]=old_index;
}
else{delete scope[indexname];}}


function _r( node, attrname, opindex, env, scope, global )
{
var o = {};
var a = grb( z[opindex], env, scope, global, o );
node.attr[attrname] = a;
if( o.is_affected ) node.n.push( attrname );
}
function _o( opindex, env, scope, global )
{
var nothing = {};
return grb( z[opindex], env, scope, global, nothing );
}
function _1( opindex, env, scope, global )
{
var nothing = {};
return gra( z[opindex], env, scope, global, nothing );
}
function _2( opindex, func, env, scope, global, father, itemname, indexname, keyname )
{
var to_iter = _1( opindex, env, scope, global, father, itemname, indexname, keyname );
wfor( to_iter, func, env, scope, global, father, itemname, indexname, keyname );
}
function _gv( )
{
if( typeof(window.__webview_engine_version__) == 'undefined' ) return 0.0;
return window.__webview_engine_version__;
}
function _m(tag,attrs,env,scope,global)
{
var tmp=_n(tag);
var base=0;
for(var i = 0 ; i < attrs.length ; i+=2 )
{
if(attrs[i+1]<0)
{tmp.attr[attrs[i]]=true;}else{_r(tmp,attrs[i],base+attrs[i+1],env,scope,global);
if(base===0)base=attrs[i+1];}}return tmp;};function _ai(i,p,e,me,r,c){var x=_grp(p,e,me);if(x)i.push(x);else{i.push('');console.warn('WXMLRT:'+me+':import:'+r+':'+c+': Path `'+p+'` not found from `'+me+'`.')}}function _grp(p,e,me){if(p[0]!='/'){var mepart=me.split('/');mepart.pop();var ppart=p.split('/');for(var i=0;i<ppart.length;i++){if(ppart[i]=='..')mepart.pop();else if(ppart[i]=='.' || !ppart[i])continue;else mepart.push(ppart[i]);}p=mepart.join('/');}if(me[0]=='.'&&p[0]=='/')p='.'+p;if(e[p])return p;if(e[p+'.wxml'])return p+'.wxml';}function _gd(p,c,e,d){if(!c)return;if(d[p][c])return d[p][c];for(var x=e[p].i.length-1;x>=0;x--){if(e[p].i[x]&&d[e[p].i[x]][c])return d[e[p].i[x]][c]};for(var x=e[p].ti.length-1;x>=0;x--){var q=_grp(e[p].ti[x],e,p);if(q&&d[q][c])return d[q][c]}
var ii=_gapi(e,p);for(var x=0;x<ii.length;x++){if(ii[x]&&d[ii[x]][c])return d[ii[x]][c]}for(var k=e[p].j.length-1;k>=0;k--)if(e[p].j[k]){for(var q=e[e[p].j[k]].ti.length-1;q>=0;q--){var pp=_grp(e[e[p].j[k]].ti[q],e,p);if(pp&&d[pp][c]){return d[pp][c]}}}}function _gapi(e,p){if(!p)return [];if($gaic[p]){return $gaic[p]};var ret=[],q=[],h=0,t=0,put={},visited={};q.push(p);visited[p]=true;t++;while(h<t){var a=q[h++];for(var i=0;i<e[a].ic.length;i++){var nd=e[a].ic[i];var np=_grp(nd,e,a);if(np&&!visited[np]){visited[np]=true;q.push(np);t++;}}for(var i=0;a!=p&&i<e[a].ti.length;i++){var ni=e[a].ti[i];var nm=_grp(ni,e,a);if(nm&&!put[nm]){put[nm]=true;ret.push(nm);}}}$gaic[p]=ret;return ret;}var $ixc={};function _ic(p,ent,me,e,s,r,gg){var x=_grp(p,ent,me);ent[me].j.push(x);if(x){if($ixc[x]){console.warn('WXMLRT:-1:include:-1:-1: `'+p+'` is being included in a loop, will be stop.');return;}$ixc[x]=true;try{ent[x].f(e,s,r,gg)}catch(e){}$ixc[x]=false;}else{console.warn('WXMLRT:'+me+':include:-1:-1: Included path `'+p+'` not found from `'+me+'`.')}}function _w(tn,f,line,c){console.warn('WXMLRT:'+f+':template:'+line+':'+c+': Template `'+tn+'` not found.');}function _ev(dom){var changed=false;delete dom.properities;delete dom.n;if(dom.children){do{changed=false;var newch = [];for(var i=0;i<dom.children.length;i++){var ch=dom.children[i];if( ch.tag=='virtual'){changed=true;for(var j=0;ch.children&&j<ch.children.length;j++){newch.push(ch.children[j]);}}else { newch.push(ch); } } dom.children = newch; }while(changed);for(var i=0;i<dom.children.length;i++){_ev(dom.children[i]);}} return dom; }var e_={}
if(global&&typeof(global.entrys)=='object')e_=global.entrys
var d_={}
if(global&&typeof(global.defines)=='object')d_=global.defines
var p_={}
var z = [];
  (function(z){
    var a = 11;
    function Z(ops){z.push(ops)};
    Z([3, 'gameIndex cfff']);Z([3, 'tolower']);Z([3, 'view']);Z([3, '2']);Z([3, 'true']);Z([a, [3, 'height:'],[[7],[3, "windowHeight"]],[3, 'px']]);Z([a, [3, 'animated '],[[7],[3, "change"]]]);Z([3, 'nav-header']);Z([[7],[3, "status"]]);Z([3, 'key']);Z([3, 'headerTap']);Z([a, [3, 'nav-cell '],[[2,'?:'],[[2, "=="], [[6],[[7],[3, "item"]],[3, "current"]], [1, 1]],[1, "active"],[1, ""]]]);Z([[6],[[7],[3, "item"]],[3, "id"]]);Z([[6],[[7],[3, "item"]],[3, "ids"]]);Z([a, [[6],[[7],[3, "item"]],[3, "name"]],[3, '\r\n                    ']]);Z([3, 'glist']);Z([[7],[3, "$glist$list"]]);Z([3, 'mt20c']);Z([3, 'order-list']);Z([3, 'order-cell']);Z([3, 'cell-status']);Z([3, 'left']);Z([[2, "!="], [[7],[3, "$glist$label"]], [1, 1]]);Z([3, '$glist$goHome']);Z([[6],[[7],[3, "item"]],[3, "bes_myid"]]);Z([3, 'randh']);Z([3, 'aspectFill']);Z([[6],[[7],[3, "item"]],[3, "user_avatar"]]);Z([3, 'name']);Z([a, [[6],[[7],[3, "item"]],[3, "user_nickname"]]]);Z([[6],[[7],[3, "item"]],[3, "bes_from"]]);Z([[6],[[6],[[7],[3, "item"]],[3, "yours"]],[3, "user_avatar"]]);Z([a, [[6],[[6],[[7],[3, "item"]],[3, "yours"]],[3, "user_nickname"]]]);Z([[2, "=="], [[6],[[7],[3, "item"]],[3, "bes_status"]], [1, 0]]);Z([3, 'right']);Z([3, '\r\n                                已过期\r\n                            ']);Z([[2, "=="], [[6],[[7],[3, "item"]],[3, "bes_status"]], [1, 1]]);Z([3, '$glist$chatForUser']);Z([a, [3, '\r\n                                '],[[6],[[7],[3, "item"]],[3, "times"]],[3, '后开始\r\n                            ']]);Z([3, 'h10']);Z([3, 'cell-item']);Z([3, 'item']);Z([3, 'good-thumb']);Z([[6],[[7],[3, "item"]],[3, "game_thumb"]]);Z([3, 'item-content']);Z([3, 'content-name']);Z([a, [[6],[[7],[3, "item"]],[3, "game_name"]]]);Z([3, 'content-attr']);Z([a, [[6],[[7],[3, "item"]],[3, "game_auth"]]]);Z([3, 'content-count']);Z([a, [3, '\r\n                                        '],[[6],[[7],[3, "item"]],[3, "bes_starttime"]],[3, '\r\n                                        ']]);Z([3, '$glist$besNote']);Z([3, 'content-amount']);Z([[6],[[7],[3, "item"]],[3, "bes_note"]]);Z([3, '\r\n                                            备注\r\n                                        ']);Z([3, 'cell-count']);Z([3, 'count-amount']);Z([a, [3, '共'],[[6],[[7],[3, "item"]],[3, "bes_number"]],[3, '局']]);Z([3, 'count-price']);Z([3, 'count-carriage']);Z([a, [3, '已支付 '],[[6],[[7],[3, "item"]],[3, "bes_pay"]],[3, ' 豌豆']]);Z([3, 'btnLine']);Z([3, 'leftLine']);Z([[2, "=="], [[7],[3, "$glist$label"]], [1, 1]]);Z([3, 'labels']);Z([3, 'cell-btn']);Z([3, 'btn-order btn-solid-red']);Z([3, '未接单']);Z([3, '$glist$channel']);Z([3, 'btn-order btn-hollow-gray']);Z([[6],[[7],[3, "item"]],[3, "bes_gid"]]);Z([[6],[[7],[3, "item"]],[3, "bes_id"]]);Z([3, '0']);Z([3, '取消订单']);Z([[2, "=="], [[6],[[7],[3, "item"]],[3, "bes_status"]], [1, 3]]);Z([3, '$glist$okay']);Z([3, '5']);Z([3, '确认完成']);Z([[7],[3, "$glist$false"]]);Z([[2, "=="], [[6],[[7],[3, "item"]],[3, "bes_status"]], [1, 5]]);Z([3, '$glist$commit']);Z([3, '7']);Z([3, '去评价']);Z([[2, "=="], [[6],[[7],[3, "item"]],[3, "bes_status"]], [1, 7]]);Z([3, '已完成']);Z([3, '$glist$LookCommit']);Z([3, '查看评价']);Z([3, '3']);Z([3, '确认接单']);Z([3, '待确认']);Z([3, '待评价']);Z([[7],[3, "$glist$msg$open"]]);Z([3, 'loadmsg animated fadeIn']);Z([3, 'msg']);Z([3, 'layui-m-layercont']);Z([a, [[7],[3, "$glist$msg$msg"]]]);Z([[2, "<"], [[7],[3, "flag"]], [1, 0]]);Z([3, 'empty']);Z([3, 'abnor']);Z([3, 'abnor__box']);Z([[7],[3, "$empty$image"]]);Z([3, 'abnor__image']);Z([3, 'widthFix']);Z([[7],[3, "$empty$title"]]);Z([3, 'abnor__text']);Z([[7],[3, "$empty$tip"]]);Z([3, 'abnor__tip']);Z([[7],[3, "$empty$button"]]);Z([3, '$empty$emitAbnorTap']);Z([3, 'abnor__btn']);Z([[2, ">="], [[7],[3, "flag"]], [1, 0]]);Z([3, 'loadingMore']);Z([[2, "!="], [[7],[3, "$more$flag"]], [1, 0]]);Z([[2, "=="], [[7],[3, "$more$flag"]], [1, 1]]);Z([3, 'weui-loadmore']);Z([3, 'weui-loading']);Z([3, 'weui-loadmore__tips']);Z([3, '正在加载']);Z([[2, "<"], [[7],[3, "$more$flag"]], [1, 0]]);Z([3, 'weui-loadmore weui-loadmore_line']);Z([3, 'weui-loadmore__tips weui-loadmore__tips_in-line']);Z([3, '加载完成']);Z([[2, "=="], [[7],[3, "$more$flag"]], [1, 2]]);Z([3, '滑动加载更多']);
  })(z);d_["./pages/order/label.wxml"] = {};
  var m0=function(e,s,r,gg){
    var oYq = _n("view");_r(oYq, 'class', 0, e, s, gg);var oZq = _m( "scroll-view", ["bindscrolltolower", 1,"class", 1,"lowerThreshold", 2,"scrollY", 3,"style", 4], e, s, gg);var oaq = _n("view");_r(oaq, 'class', 6, e, s, gg);var obq = _n("view");_r(obq, 'class', 7, e, s, gg);var ocq = _v();var odq = function(ohq,ogq,ofq,gg){var oeq = _m( "view", ["bindtap", 10,"class", 1,"data-id", 2,"data-ids", 3], ohq, ogq, gg);var ojq = _o(14, ohq, ogq, gg);_(oeq,ojq);_(ofq, oeq);return ofq;};_2(8, odq, e, s, gg, ocq, "item", "index", 'key');_(obq,ocq);_(oaq,obq);_(oZq,oaq);var okq = _n("view");_r(okq, 'class', 15, e, s, gg);var olq = _v();var omq = function(oqq,opq,ooq,gg){var onq = _n("view");var osq = _n("view");_r(osq, 'class', 17, oqq, opq, gg);_(onq,osq);var otq = _n("view");_r(otq, 'class', 18, oqq, opq, gg);var ouq = _n("view");_r(ouq, 'class', 19, oqq, opq, gg);var ovq = _n("view");_r(ovq, 'class', 20, oqq, opq, gg);var owq = _n("view");_r(owq, 'class', 21, oqq, opq, gg);var oxq = _v();
      if (_o(22, oqq, opq, gg)) {
        oxq.wxVkey = 1;var oyq = _m( "view", ["class", -1,"bindtap", 23,"data-uid", 1], oqq, opq, gg);var o_q = _m( "image", ["class", 25,"mode", 1,"src", 2], oqq, opq, gg);_(oyq,o_q);var oAr = _n("view");_r(oAr, 'class', 28, oqq, opq, gg);var oBr = _o(29, oqq, opq, gg);_(oAr,oBr);_(oyq,oAr);_(oxq, oyq);
      }else {
        oxq.wxVkey = 2;var oCr = _m( "view", ["class", -1,"bindtap", 23,"data-uid", 7], e, s, gg);var oEr = _m( "image", ["class", 25,"mode", 1,"src", 6], e, s, gg);_(oCr,oEr);var oFr = _n("view");_r(oFr, 'class', 28, e, s, gg);var oGr = _o(32, e, s, gg);_(oFr,oGr);_(oCr,oFr);_(oxq, oCr);
      }_(owq,oxq);_(ovq,owq);var oHr = _v();
      if (_o(33, oqq, opq, gg)) {
        oHr.wxVkey = 1;var oIr = _n("view");_r(oIr, 'class', 34, oqq, opq, gg);var oKr = _o(35, oqq, opq, gg);_(oIr,oKr);_(oHr, oIr);
      } _(ovq,oHr);var oLr = _v();
      if (_o(36, oqq, opq, gg)) {
        oLr.wxVkey = 1;var oMr = _m( "view", ["data-myid", 24,"data-uid", 6,"class", 10,"bindtap", 13], oqq, opq, gg);var oOr = _o(38, oqq, opq, gg);_(oMr,oOr);_(oLr, oMr);
      } _(ovq,oLr);_(ouq,ovq);var oPr = _n("view");_r(oPr, 'class', 39, oqq, opq, gg);_(ouq,oPr);var oQr = _n("view");_r(oQr, 'class', 40, oqq, opq, gg);var oRr = _n("view");var oSr = _m( "image", ["alt", 41,"class", 1,"src", 2], oqq, opq, gg);_(oRr,oSr);var oTr = _n("view");_r(oTr, 'class', 44, oqq, opq, gg);var oUr = _n("view");_r(oUr, 'class', 45, oqq, opq, gg);var oVr = _o(46, oqq, opq, gg);_(oUr,oVr);_(oTr,oUr);var oWr = _n("view");_r(oWr, 'class', 47, oqq, opq, gg);var oXr = _o(48, oqq, opq, gg);_(oWr,oXr);_(oTr,oWr);var oYr = _n("view");_r(oYr, 'class', 49, oqq, opq, gg);var oZr = _o(50, oqq, opq, gg);_(oYr,oZr);var oar = _m( "view", ["bindtap", 51,"class", 1,"data-text", 2], oqq, opq, gg);var obr = _o(54, oqq, opq, gg);_(oar,obr);_(oYr,oar);_(oTr,oYr);_(oRr,oTr);_(oQr,oRr);_(ouq,oQr);var ocr = _n("view");_r(ocr, 'class', 55, oqq, opq, gg);var odr = _n("view");_r(odr, 'class', 56, oqq, opq, gg);var oer = _o(57, oqq, opq, gg);_(odr,oer);_(ocr,odr);var ofr = _n("view");_r(ofr, 'class', 58, oqq, opq, gg);_(ocr,ofr);var ogr = _n("view");_r(ogr, 'class', 59, oqq, opq, gg);var ohr = _n("text");var oir = _o(60, oqq, opq, gg);_(ohr,oir);_(ogr,ohr);_(ocr,ogr);_(ouq,ocr);var ojr = _n("view");_r(ojr, 'class', 61, oqq, opq, gg);var okr = _n("view");_r(okr, 'class', 62, oqq, opq, gg);_(ojr,okr);var olr = _v();
      if (_o(63, oqq, opq, gg)) {
        olr.wxVkey = 1;var omr = _n("view");_r(omr, 'class', 64, oqq, opq, gg);var oor = _v();
      if (_o(36, oqq, opq, gg)) {
        oor.wxVkey = 1;var opr = _n("view");_r(opr, 'class', 65, oqq, opq, gg);var orr = _n("button");_r(orr, 'class', 66, oqq, opq, gg);var osr = _o(67, oqq, opq, gg);_(orr,osr);_(opr,orr);var otr = _m( "button", ["bindtap", 68,"class", 1,"data-gid", 2,"data-id", 3,"data-sta", 4], oqq, opq, gg);var our = _o(73, oqq, opq, gg);_(otr,our);_(opr,otr);_(oor, opr);
      } _(omr,oor);var ovr = _v();
      if (_o(74, oqq, opq, gg)) {
        ovr.wxVkey = 1;var owr = _n("view");_r(owr, 'class', 65, oqq, opq, gg);var oyr = _m( "button", ["class", 66,"data-gid", 4,"data-id", 5,"bindtap", 9,"data-sta", 10], oqq, opq, gg);var ozr = _o(77, oqq, opq, gg);_(oyr,ozr);_(owr,oyr);var o_r = _v();
      if (_o(78, oqq, opq, gg)) {
        o_r.wxVkey = 1;var oAs = _n("button");_r(oAs, 'class', 69, oqq, opq, gg);_(o_r, oAs);
      } _(owr,o_r);_(ovr, owr);
      } _(omr,ovr);var oCs = _v();
      if (_o(79, oqq, opq, gg)) {
        oCs.wxVkey = 1;var oDs = _n("view");_r(oDs, 'class', 65, oqq, opq, gg);var oFs = _m( "button", ["data-from", 30,"class", 36,"data-gid", 40,"data-id", 41,"bindtap", 50,"data-sta", 51], oqq, opq, gg);var oGs = _o(82, oqq, opq, gg);_(oFs,oGs);_(oDs,oFs);var oHs = _v();
      if (_o(78, oqq, opq, gg)) {
        oHs.wxVkey = 1;var oIs = _n("button");_r(oIs, 'class', 69, oqq, opq, gg);_(oHs, oIs);
      } _(oDs,oHs);_(oCs, oDs);
      } _(omr,oCs);var oKs = _v();
      if (_o(83, oqq, opq, gg)) {
        oKs.wxVkey = 1;var oLs = _n("view");_r(oLs, 'class', 65, oqq, opq, gg);var oNs = _n("button");_r(oNs, 'class', 66, oqq, opq, gg);var oOs = _o(84, oqq, opq, gg);_(oNs,oOs);_(oLs,oNs);var oPs = _m( "button", ["class", 69,"data-id", 2,"bindtap", 16], oqq, opq, gg);var oQs = _o(86, oqq, opq, gg);_(oPs,oQs);_(oLs,oPs);_(oKs, oLs);
      } _(omr,oKs);_(olr, omr);
      }else {
        olr.wxVkey = 2;var oRs = _n("view");_r(oRs, 'class', 64, e, s, gg);var oTs = _v();
      if (_o(36, e, s, gg)) {
        oTs.wxVkey = 1;var oUs = _n("view");_r(oUs, 'class', 65, e, s, gg);var oWs = _m( "button", ["class", 66,"bindtap", 2,"data-gid", 4,"data-id", 5,"data-sta", 21], e, s, gg);var oXs = _o(88, e, s, gg);_(oWs,oXs);_(oUs,oWs);var oYs = _m( "button", ["bindtap", 68,"class", 1,"data-gid", 2,"data-sta", 4], e, s, gg);var oZs = _o(73, e, s, gg);_(oYs,oZs);_(oUs,oYs);_(oTs, oUs);
      } _(oRs,oTs);var oas = _v();
      if (_o(74, e, s, gg)) {
        oas.wxVkey = 1;var obs = _n("view");_r(obs, 'class', 65, e, s, gg);var ods = _n("button");_r(ods, 'class', 66, e, s, gg);var oes = _o(89, e, s, gg);_(ods,oes);_(obs,ods);var ofs = _v();
      if (_o(78, e, s, gg)) {
        ofs.wxVkey = 1;var ogs = _n("button");_r(ogs, 'class', 69, e, s, gg);_(ofs, ogs);
      } _(obs,ofs);_(oas, obs);
      } _(oRs,oas);var ois = _v();
      if (_o(79, e, s, gg)) {
        ois.wxVkey = 1;var ojs = _n("view");_r(ojs, 'class', 65, e, s, gg);var ols = _n("button");_r(ols, 'class', 66, e, s, gg);var oms = _o(90, e, s, gg);_(ols,oms);_(ojs,ols);var ons = _v();
      if (_o(78, e, s, gg)) {
        ons.wxVkey = 1;var oos = _n("button");_r(oos, 'class', 69, e, s, gg);_(ons, oos);
      } _(ojs,ons);_(ois, ojs);
      } _(oRs,ois);var oqs = _v();
      if (_o(83, e, s, gg)) {
        oqs.wxVkey = 1;var ors = _n("view");_r(ors, 'class', 65, e, s, gg);var ots = _n("button");_r(ots, 'class', 66, e, s, gg);var ous = _o(84, e, s, gg);_(ots,ous);_(ors,ots);var ovs = _m( "button", ["class", 69,"data-id", 2,"bindtap", 16], e, s, gg);var ows = _o(86, e, s, gg);_(ovs,ows);_(ors,ovs);_(oqs, ors);
      } _(oRs,oqs);_(olr, oRs);
      }_(ojr,olr);_(ouq,ojr);_(otq,ouq);_(onq,otq);_(ooq, onq);return ooq;};_2(16, omq, e, s, gg, olq, "item", "index", 'key');_(okq,olq);var oxs = _v();
      if (_o(91, e, s, gg)) {
        oxs.wxVkey = 1;var oys = _n("view");_r(oys, 'class', 92, e, s, gg);var o_s = _n("view");_r(o_s, 'class', 93, e, s, gg);var oAt = _n("text");_r(oAt, 'class', 94, e, s, gg);var oBt = _o(95, e, s, gg);_(oAt,oBt);_(o_s,oAt);_(oys,o_s);_(oxs, oys);
      } _(okq,oxs);var oCt = _n("view");_r(oCt, 'class', 17, e, s, gg);_(okq,oCt);_(oZq,okq);var oDt = _v();
      if (_o(96, e, s, gg)) {
        oDt.wxVkey = 1;var oEt = _n("view");_r(oEt, 'class', 97, e, s, gg);var oGt = _n("view");_r(oGt, 'class', 98, e, s, gg);var oHt = _n("view");_r(oHt, 'class', 99, e, s, gg);var oIt = _v();
      if (_o(100, e, s, gg)) {
        oIt.wxVkey = 1;var oJt = _m( "image", ["src", 100,"class", 1,"mode", 2], e, s, gg);_(oIt, oJt);
      } _(oHt,oIt);var oLt = _v();
      if (_o(103, e, s, gg)) {
        oLt.wxVkey = 1;var oMt = _n("view");_r(oMt, 'class', 104, e, s, gg);var oOt = _o(103, e, s, gg);_(oMt,oOt);_(oLt, oMt);
      } _(oHt,oLt);var oPt = _v();
      if (_o(105, e, s, gg)) {
        oPt.wxVkey = 1;var oQt = _n("view");_r(oQt, 'class', 106, e, s, gg);var oSt = _o(105, e, s, gg);_(oQt,oSt);_(oPt, oQt);
      } _(oHt,oPt);var oTt = _v();
      if (_o(107, e, s, gg)) {
        oTt.wxVkey = 1;var oUt = _m( "view", ["bindtap", 108,"class", 1], e, s, gg);var oWt = _o(107, e, s, gg);_(oUt,oWt);_(oTt, oUt);
      } _(oHt,oTt);_(oGt,oHt);_(oEt,oGt);_(oDt, oEt);
      } _(oZq,oDt);var oXt = _v();
      if (_o(110, e, s, gg)) {
        oXt.wxVkey = 1;var oYt = _n("view");_r(oYt, 'class', 111, e, s, gg);var oat = _v();
      if (_o(112, e, s, gg)) {
        oat.wxVkey = 1;var obt = _n("view");_r(obt, 'class', 111, e, s, gg);var odt = _v();
      if (_o(113, e, s, gg)) {
        odt.wxVkey = 1;var oet = _n("view");_r(oet, 'class', 114, e, s, gg);var ogt = _n("view");_r(ogt, 'class', 115, e, s, gg);_(oet,ogt);var oht = _n("view");_r(oht, 'class', 116, e, s, gg);var oit = _o(117, e, s, gg);_(oht,oit);_(oet,oht);_(odt, oet);
      } _(obt,odt);var ojt = _v();
      if (_o(118, e, s, gg)) {
        ojt.wxVkey = 1;var okt = _n("view");_r(okt, 'class', 119, e, s, gg);var omt = _n("view");_r(omt, 'class', 120, e, s, gg);var ont = _o(121, e, s, gg);_(omt,ont);_(okt,omt);_(ojt, okt);
      } _(obt,ojt);var oot = _v();
      if (_o(122, e, s, gg)) {
        oot.wxVkey = 1;var opt = _n("view");_r(opt, 'class', 119, e, s, gg);var ort = _n("view");_r(ort, 'class', 120, e, s, gg);var ost = _o(123, e, s, gg);_(ort,ost);_(opt,ort);_(oot, opt);
      } _(obt,oot);_(oat, obt);
      } _(oYt,oat);_(oXt, oYt);
      } _(oZq,oXt);_(oYq,oZq);_(r,oYq);
    return r;
  };
        e_["./pages/order/label.wxml"]={f:m0,j:[],i:[],ti:[],ic:[]};
if(path&&e_[path]){
window.__wxml_comp_version__=0.02
return function(env,dd,global){$gwxc=0;var root={"tag":"wx-page"};root.children=[]
var main=e_[path].f
if(typeof(window.__webview_engine_version__)!='undefined'&&window.__webview_engine_version__+1e-6>=0.02+1e-6&&window.__mergeData__)
{env=window.__mergeData__(env,dd);}
try{
main(env,{},root,global);
if(typeof(window.__webview_engine_version__)=='undefined'||window.__webview_engine_version__+1e-6<0.01+1e-6){return _ev(root);}}catch(err){console.log(err)}return root;}}}@code-separator-line:.empty .abnor{position:relative;display:block;width:100%;height:0;padding-bottom:100%;overflow:hidden}.empty .abnor__box{position:absolute;display:flex;top:0;bottom:0;left:0;right:0;flex-direction:column;justify-content:center;align-items:center}.empty .abnor__btn{min-width:%%?228rpx?%%;height:%%?66rpx?%%;margin-top:%%?30rpx?%%;padding:0 %%?30rpx?%%;background-color:#3e9aff;border:0 none;border-radius:%%?2rpx?%%;color:#fff;font-size:%%?28rpx?%%;text-align:center;overflow:hidden;line-height:%%?66rpx?%%}.empty .abnor__btn:active{background-color:#3e9aff}.empty .abnor__image{width:%%?514rpx?%%;background:transparent no-repeat;background-size:cover}.empty .abnor__text{margin-top:%%?30rpx?%%;color:#333;font-size:%%?28rpx?%%}.empty .abnor__tip{margin-top:%%?20rpx?%%;color:#666;font-size:%%?24rpx?%%}.loadingMore{clear:both;display:block}.msg{width:auto;max-width:80%;margin:0 auto;background-color:rgba(0,0,0,.7);color:#fff;display:inline-block;font-size:16px;border-radius:5px;box-shadow:0 0 8px rgba(0,0,0,.1);pointer-events:auto;-webkit-overflow-scrolling:touch;-webkit-animation-fill-mode:both;animation-fill-mode:both;-webkit-animation-duration:.2s;animation-duration:.2s;box-sizing:content-box;position:fixed;bottom:20%;z-index:999;text-align:center;padding:5px 10px}.layui-m-layercont{line-height:22px;text-align:center}.loadmsg{display:flex;justify-content:center;margin:0 auto;text-align:center}.order-cell{background:#fff;padding:0 %%?16.41rpx?%%;overflow:hidden;font-size:%%?28rpx?%%}.cell-status{text-align:right;padding:%%?23.44rpx?%% %%?18.74rpx?%%;color:#ff2d4b;border-bottom:1px solid #f0f0f0;display:flex}.cell-status .left{text-align:left;flex:1}.cell-status .left .randh{width:%%?50rpx?%%;height:%%?50rpx?%%;border-radius:50%;float:left}.cell-status .left .name{float:left;height:%%?50rpx?%%;line-height:%%?50rpx?%%;margin-left:%%?10rpx?%%;color:#333}.cell-status .right{text-align:right;flex:1}.cell-item{padding-bottom:%%?35.15rpx?%%;overflow:hidden;white-space:nowrap;width:100%;height:%%?150.77rpx?%%}.item-content{margin-left:%%?212.35rpx?%%;margin-right:%%?17.84rpx?%%;height:%%?150.77rpx?%%;position:relative}.content-name{font-size:%%?32.44rpx?%%;line-height:%%?32.81rpx?%%;white-space:normal;display:-webkit-box;-webkit-box-orient:vertical;-webkit-line-clamp:2;overflow:hidden}.content-attr{margin-top:%%?11.72rpx?%%;font-size:%%?26.44rpx?%%;color:#a0a0a0;line-height:%%?32.81rpx?%%;white-space:normal;display:-webkit-box;-webkit-box-orient:vertical;-webkit-line-clamp:1;overflow:hidden}.content-count{position:absolute;bottom:0;overflow:hidden;width:100%}.content-amount{float:right;background:rgba(204,204,204,.36);padding:1px 6px;border-radius:10%;color:#666}.cell-count{border-top:1px solid #efefef;border-bottom:1px solid #efefef;overflow:hidden;padding:%%?15rpx?%% %%?18.74rpx?%%;color:#666}.count-amount{float:left}.count-carriage{float:right}.count-price{float:right;margin-left:%%?28.12rpx?%%;color:#ff2b4d}.count-price::before{color:#000}.cell-btn{padding:%%?10rpx?%% %%?18.74rpx?%%;overflow:hidden;box-sizing:border-box;clear:both;display:block;position:relative}.empty{width:100%;height:100%;text-align:center;font-size:%%?28.12rpx?%%;color:#a0a0a0;line-height:%%?48rpx?%%}.empty-img{width:%%?187.49rpx?%%;height:%%?187.49rpx?%%;margin-top:%%?164.05rpx?%%;margin-bottom:%%?23.44rpx?%%}.btn-to-home{margin-top:%%?35.15rpx?%%;width:%%?337.48rpx?%%;height:%%?79.68rpx?%%;line-height:%%?79.68rpx?%%;border-radius:%%?11.72rpx?%%;display:inline-block}.good-thumb{width:%%?145.77rpx?%%;height:%%?145.77rpx?%%;border:1px solid #fff;float:left;margin-left:%%?18.74rpx?%%;border-radius:10px}.btn-order{width:%%?164.05rpx?%%;height:%%?65.62rpx?%%;font-size:%%?28.12rpx?%%;float:right;padding:0;margin-left:%%?35.15rpx?%%}.btn-hollow-gray{border:1px solid rgba(245,245,245,.14);background:#fff}.btn-solid-red{background:#ff2d4b;color:#fff}.btnLine{position:relative}.btnLine .leftLine{position:absolute;color:#333;font-size:%%?28rpx?%%;bottom:%%?17rpx?%%;left:%%?5rpx?%%}.hide{display:none}.orders-wrap{font-size:%%?28.12rpx?%%;height:100%}.orders{height:100%}.nav-header{font-size:%%?28.12rpx?%%;display:-webkit-box;text-align:center;background:#fff;padding:0 %%?16.41rpx?%%;height:%%?100.77rpx?%%;line-height:%%?100.77rpx?%%}.nav-cell{-webkit-box-flex:1}.nav-cell.active{color:#ff4b2d;border-bottom:#ff4b2d 4.69 rpx solid}@code-separator-line:__wxRoute = "pages/order/label";__wxRouteBegin = true;
define("pages/order/label.js", function(require, module, exports, window,document,frames,self,location,navigator,localStorage,history,Caches,screen,alert,confirm,prompt,fetch,XMLHttpRequest,WebSocket,webkit,WeixinJSCore,ServiceJSBridge,Reporter){
"use strict";function _interopRequireDefault(e){return e&&e.__esModule?e:{default:e}}function _asyncToGenerator(e){return function(){var t=e.apply(this,arguments);return new Promise(function(e,r){function n(i,o){try{var a=t[i](o),u=a.value}catch(e){return void r(e)}if(!a.done)return Promise.resolve(u).then(function(e){n("next",e)},function(e){n("throw",e)});e(u)}return n("next")})}}function _classCallCheck(e,t){if(!(e instanceof t))throw new TypeError("Cannot call a class as a function")}function _possibleConstructorReturn(e,t){if(!e)throw new ReferenceError("this hasn't been initialised - super() hasn't been called");return!t||"object"!=typeof t&&"function"!=typeof t?e:t}function _inherits(e,t){if("function"!=typeof t&&null!==t)throw new TypeError("Super expression must either be null or a function, not "+typeof t);e.prototype=Object.create(t&&t.prototype,{constructor:{value:e,enumerable:!1,writable:!0,configurable:!0}}),t&&(Object.setPrototypeOf?Object.setPrototypeOf(e,t):e.__proto__=t)}Object.defineProperty(exports,"__esModule",{value:!0});var _createClass=function(){function e(e,t){for(var r=0;r<t.length;r++){var n=t[r];n.enumerable=n.enumerable||!1,n.configurable=!0,"value"in n&&(n.writable=!0),Object.defineProperty(e,n.key,n)}}return function(t,r,n){return r&&e(t.prototype,r),n&&e(t,n),t}}(),_wepy=require("./../../npm/wepy/lib/wepy.js"),_wepy2=_interopRequireDefault(_wepy),_request=require("./../../api/request.js"),_util=require("./../../tools/util.js"),_util2=_interopRequireDefault(_util),_global=require("./../../config/global.js"),_global2=_interopRequireDefault(_global),_glist=require("./../../base/glist.js"),_glist2=_interopRequireDefault(_glist),_loadMore=require("./../../base/tips/loadMore.js"),_loadMore2=_interopRequireDefault(_loadMore),_empty=require("./../../base/loading/empty.js"),_empty2=_interopRequireDefault(_empty),Setting=function(e){function t(){var e,r,n,i;_classCallCheck(this,t);for(var o=arguments.length,a=Array(o),u=0;u<o;u++)a[u]=arguments[u];return r=n=_possibleConstructorReturn(this,(e=t.__proto__||Object.getPrototypeOf(t)).call.apply(e,[this].concat(a))),n.config={navigationBarTitleText:"我的预约单"},n.$props={more:{},empty:{}},n.$events={},n.components={glist:_glist2.default,more:_loadMore2.default,empty:_empty2.default},n.data={status:{},current:0,listd:[],limit:12,page:1,change:_global2.default.animated,windowHeight:"",flag:0},n.computed={},n.methods={headerTap:function(e){this.current=(0,_util.get)(e,"ids");var t=(0,_util.get)(e,"id"),r=this.status;r.forEach(function(e,t,n){r[t].current=0}),r[t].current=1,this.flag=0,this.page=1,this.listd=[],this.orderList(),this.$apply()},tolower:function(e){this.orderList()}},n.watch={flag:function(){this.$broadcast("LoadingMore",this.flag)}},n.events={sonListen:function(e){2==e&&this.onLoadData()}},i=r,_possibleConstructorReturn(n,i)}return _inherits(t,e),_createClass(t,[{key:"orderList",value:function(){function e(){return t.apply(this,arguments)}var t=_asyncToGenerator(regeneratorRuntime.mark(function e(){var t=this;return regeneratorRuntime.wrap(function(e){for(;;)switch(e.prev=e.next){case 0:if(!(this.flag<0)){e.next=2;break}return e.abrupt("return",!1);case 2:(0,_request.requrst)("order/orderlist",{status:this.current,limit:this.limit,page:this.page++,type:"label"},function(e){e.info.data.length>0?(t.listd=_util2.default.AddData(t.listd,e.info.data),t.flag=2):t.flag=-2,t.$broadcast("glist",t.listd,0),t.$apply()});case 3:case"end":return e.stop()}},e,this)}));return e}()},{key:"orderStatus",value:function(){function e(){return t.apply(this,arguments)}var t=_asyncToGenerator(regeneratorRuntime.mark(function e(){var t=this;return regeneratorRuntime.wrap(function(e){for(;;)switch(e.prev=e.next){case 0:(0,_request.requrst)("order/orderStatus",{type:1},function(e){t.status=e.info,t.$apply()});case 1:case"end":return e.stop()}},e,this)}));return e}()},{key:"onLoad",value:function(){function e(){return t.apply(this,arguments)}var t=_asyncToGenerator(regeneratorRuntime.mark(function e(){return regeneratorRuntime.wrap(function(e){for(;;)switch(e.prev=e.next){case 0:this.orderStatus(),this.orderList(),this.windowHeight=_util2.default.sysInfo("windowHeight");case 3:case"end":return e.stop()}},e,this)}));return e}()}]),t}(_wepy2.default.page);Page(require("./../../npm/wepy/lib/wepy.js").default.$createPage(Setting,"pages/order/label"));
});require("pages/order/label.js")@code-separator-line:["div","view","scroll-view","image","text","button"]