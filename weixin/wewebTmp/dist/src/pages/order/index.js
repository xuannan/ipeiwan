/*v0.6vv_20170214_fbi*/
window.__wcc_version__='v0.6vv_20170214_fbi'
window.__wxml_transpiler_version__='v0.1'
var $gwxc
var $gaic={}
$gwx=function(path,global){
function _(a,b){b&&a.children.push(b);}
function _v(k){if(typeof(k)!='undefined')return {tag:'virtual','wxKey':k,children:[]};return {tag:'virtual',children:[]};}
function _n(tag){$gwxc++;if($gwxc>=16000){throw 'Dom limit exceeded, please check if there\'s any mistake you\'ve made.'};return {tag:tag.substr(0,3)=='wx-'?tag:'wx-'+tag,attr:{},children:[],n:[]}}
function _p(a,b){b&&a.properities.push(b);}
function _s(scope,env,key){return typeof(scope[key])!='undefined'?scope[key]:env[key]}function _wl(tname,prefix){console.warn('WXMLRT:'+prefix+':-1:-1:-1: Template `' + tname + '` is being called recursively, will be stop.')}$gwn=console.warn;
$gwl=console.log;
function $gwh()
{
function x(){}
x.prototype =
{
hn: function( obj )
{
if( typeof(obj) == 'object' )
{
var cnt=0;
var any=false;
for(var x in obj)
{
any|=x==='__value__';
cnt++;
if(cnt>2)break;
}
return cnt == 2 && any && obj.hasOwnProperty('__wxspec__') ? "h" : "n";
}
return "n";
},
nh: function( obj, special )
{
return { __value__: obj, __wxspec__: special ? special : true }
},
rv: function( obj )
{
return this.hn(obj)==='n'?obj:this.rv(obj.__value__);
}
}
return new x;
}
wh=$gwh();
function $gwrt( should_pass_type_info )
{
function ArithmeticEv( ops, e, s, g, o )
{
var rop = ops[0][1];
var _a,_b,_c,_d, _aa, _bb;
switch( rop )
{
case '?:':
_a = rev( ops[1], e, s, g, o );
_c = should_pass_type_info && ( wh.hn(_a) === 'h' );
_d = wh.rv( _a ) ? rev( ops[2], e, s, g, o ) : rev( ops[3], e, s, g, o );
_d = _c && wh.hn( _d ) === 'n' ? wh.nh( _d, 'c' ) : _d;
return _d;
break;
case '&&':
_a = rev( ops[1], e, s, g, o );
_c = should_pass_type_info && ( wh.hn(_a) === 'h' );
_d = wh.rv( _a ) ? rev( ops[2], e, s, g, o ) : wh.rv( _a );
_d = _c && wh.hn( _d ) === 'n' ? wh.nh( _d, 'c' ) : _d;
return _d;
break;
case '||':
_a = rev( ops[1], e, s, g, o );
_c = should_pass_type_info && ( wh.hn(_a) === 'h' );
_d = wh.rv( _a ) ? wh.rv(_a) : rev( ops[2], e, s, g, o );
_d = _c && wh.hn( _d ) === 'n' ? wh.nh( _d, 'c' ) : _d;
return _d;
break;
case '+':
case '*':
case '/':
case '%':
case '|':
case '^':
case '&':
case '===':
case '==':
case '!=':
case '!==':
case '>=':
case '<=':
case '>':
case '<':
case '<<':
case '>>':
_a = rev( ops[1], e, s, g, o );
_b = rev( ops[2], e, s, g, o );
_c = should_pass_type_info && (wh.hn( _a ) === 'h' || wh.hn( _b ) === 'h');
switch( rop )
{
case '+':
_d = wh.rv( _a ) + wh.rv( _b );
break;
case '*':
_d = wh.rv( _a ) * wh.rv( _b );
break;
case '/':
_d = wh.rv( _a ) / wh.rv( _b );
break;
case '%':
_d = wh.rv( _a ) % wh.rv( _b );
break;
case '|':
_d = wh.rv( _a ) | wh.rv( _b );
break;
case '^':
_d = wh.rv( _a ) ^ wh.rv( _b );
break;
case '&':
_d = wh.rv( _a ) & wh.rv( _b );
break;
case '===':
_d = wh.rv( _a ) === wh.rv( _b );
break;
case '==':
_d = wh.rv( _a ) == wh.rv( _b );
break;
case '!=':
_d = wh.rv( _a ) != wh.rv( _b );
break;
case '!==':
_d = wh.rv( _a ) !== wh.rv( _b );
break;
case '>=':
_d = wh.rv( _a ) >= wh.rv( _b );
break;
case '<=':
_d = wh.rv( _a ) <= wh.rv( _b );
break;
case '>':
_d = wh.rv( _a ) > wh.rv( _b );
break;
case '<':
_d = wh.rv( _a ) < wh.rv( _b );
break;
case '<<':
_d = wh.rv( _a ) << wh.rv( _b );
break;
case '>>':
_d = wh.rv( _a ) >> wh.rv( _b );
break;
default:
break;
}
return _c ? wh.nh( _d, "c" ) : _d;
break;
case '-':
_a = ops.length === 3 ? rev( ops[1], e, s, g, o ) : 0;
_b = ops.length === 3 ? rev( ops[2], e, s, g, o ) : rev( ops[1], e, s, g, o );
_c = should_pass_type_info && (wh.hn( _a ) === 'h' || wh.hn( _b ) === 'h');
_d = _c ? wh.rv( _a ) - wh.rv( _b ) : _a - _b;
return _c ? wh.nh( _d, "c" ) : _d;
break;
case '!':
_a = rev( ops[1], e, s, g, o );
_c = should_pass_type_info && (wh.hn( _a ) == 'h');
_d = !wh.rv(_a);
return _c ? wh.nh( _d, "c" ) : _d;
case '~':
_a = rev( ops[1], e, s, g, o );
_c = should_pass_type_info && (wh.hn( _a ) == 'h');
_d = ~wh.rv(_a);
return _c ? wh.nh( _d, "c" ) : _d;
default:
$gwn('unrecognized op' + rop );
}
}
function rev( ops, e, s, g, o )
{
var op = ops[0];
if( typeof(op)==='object' )
{
var vop=op[0];
var _a, _aa, _b, _bb, _c, _d, _s, _e, _ta, _tb, _td;
switch(vop)
{
case 2:
return ArithmeticEv(ops,e,s,g,o);
break;
case 4:
return rev( ops[1], e, s, g, o );
break;
case 5:
switch( ops.length )
{
case 2:
return should_pass_type_info ?
[rev(ops[1],e,s,g,o)] :
[wh.rv(rev(ops[1],e,s,g,o))];
break;
case 1:
return [];
break;
default:
_a = rev( ops[1],e,s,g,o );
_a.push(
should_pass_type_info ?
rev( ops[2],e,s,g,o ) :
wh.rv( rev(ops[2],e,s,g,o) )
);
return _a;
break;
}
break;
case 6:
_a = rev(ops[1],e,s,g,o);
_ta = wh.hn(_a)==='h';
_aa = _ta ? wh.rv(_a) : _a;
o.is_affected |= _ta;
if( should_pass_type_info )
{
if( _aa===null || typeof(_aa) === 'undefined' )
{
return _ta ? wh.nh(undefined, 'e') : undefined;
}
_b = rev(ops[2],e,s,g,o);
_tb = wh.hn(_b) === 'h';
_bb = _tb ? wh.rv(_b) : _b;
o.is_affected |= _tb;
if( _bb===null || typeof(_bb) === 'undefined' )
{
return (_ta || _tb) ? wh.nh(undefined, 'e') : undefined;
}
_d = _aa[_bb];
_td = wh.hn(_d)==='h';
o.is_affected |= _td;
return (_ta || _tb) ? (_td ? _d : wh.nh(_d, 'e')) : _d;
}
else
{
if( _aa===null || typeof(_aa) === 'undefined' )
{
return undefined;
}
_b = rev(ops[2],e,s,g,o);
_tb = wh.hn(_b) === 'h';
_bb = _tb ? wh.rv(_b) : _b;
o.is_affected |= _tb;
if( _bb===null || typeof(_bb) === 'undefined' )
{
return undefined;
}
_d = _aa[_bb];
_td = wh.hn(_d)==='h';
o.is_affected |= _td;
return _td ? wh.rv(_d) : _d;
}
case 7:
switch(ops[1][0])
{
case 11:
o.is_affected |= wh.hn(g)==='h';
return g;
case 3:
_s = wh.rv( s );
_e = wh.rv( e );
_b = ops[1][1];
_a = _s && _s.hasOwnProperty(_b) ?
s : _e && ( _e.hasOwnProperty(_b) ? e : undefined );
if( should_pass_type_info )
{
if( _a )
{
_ta = wh.hn(_a) === 'h';
_aa = _ta ? wh.rv( _a ) : _a;
_d = _aa[_b];
_td = wh.hn(_d) === 'h';
o.is_affected |= _ta || _td;
_d = _ta && !_td ? wh.nh(_d,'e') : _d;
return _d;
}
}
else
{
if( _a )
{
_ta = wh.hn(_a) === 'h';
_aa = _ta ? wh.rv( _a ) : _a;
_d = _aa[_b];
_td = wh.hn(_d) === 'h';
o.is_affected |= _ta || _td;
return wh.rv(_d);
}
}
return undefined;
}
break;
case 8:
_a = {};
_a[ops[1]] = rev(ops[2],e,s,g,o);
return _a;
break;
case 9:
_a = rev(ops[1],e,s,g,o);
_b = rev(ops[2],e,s,g,o);
function merge( _a, _b, _ow )
{
_ta = wh.hn(_a)==='h';
_tb = wh.hn(_b)==='h';
_aa = wh.rv(_a);
_bb = wh.rv(_b);
if( should_pass_type_info )
{
if( _tb )
{
for(var k in _bb)
{
if ( _ow || !_aa.hasOwnProperty(k) )
_aa[k]=wh.nh(_bb[k],'e');
}
}
else
{
for(var k in _bb)
{
if ( _ow || !_aa.hasOwnProperty(k) )
_aa[k]=_bb[k];
}
}
}
else
{
for(var k in _bb)
{
if ( _ow || _aa.hasOwnProperty(k) )
_aa[k]=wh.rv(_bb[k]);
}
}
return _a;
}
var _c = _a
var _ow = true
if ( typeof(ops[1][0]) === "object" && ops[1][0][0] === 10 ) {
_a = _b
_b = _c
_ow = false
}
if ( typeof(ops[1][0]) === "object" && ops[1][0][0] === 10 ) {
var _r = {}
return merge( merge( _r, _a, _ow ), _b, _ow );
}
else
return merge( _a, _b, _ow );
break;
case 10:
return should_pass_type_info ? rev(ops[1],e,s,g,o) : wh.rv(rev(ops[1],e,s,g,o));
}
}
else
{
if( op === 3 || op === 1 ) return ops[1];
else if( op === 11 )
{
var _a='';
for( var i = 1 ; i < ops.length ; i++ )
{
var xp = wh.rv(rev(ops[i],e,s,g,o));
_a += typeof(xp) === 'undefined' ? '' : xp;
}
return _a;
}
}
}
return rev;
}
gra=$gwrt(true);
grb=$gwrt(false);
function TestTest( expr, ops, e,s,g, expect_a, expect_b, expect_affected )
{
{
var o = {is_affected:false};
var a = gra( ops, e,s,g, o );
if( JSON.stringify(a) != JSON.stringify( expect_a )
|| o.is_affected != expect_affected )
{
console.warn( "A. " + expr + " get result " + JSON.stringify(a) + ", " + o.is_affected + ", but " + JSON.stringify( expect_a ) + ", " + expect_affected + " is expected" );
}
}
{
var o = {is_affected:false};
var a = grb( ops, e,s,g, o );
if( JSON.stringify(a) != JSON.stringify( expect_b )
|| o.is_affected != expect_affected )
{
console.warn( "B. " + expr + " get result " + JSON.stringify(a) + ", " + o.is_affected + ", but " + JSON.stringify( expect_b ) + ", " + expect_affected + " is expected" );
}
}
}

function wfor( to_iter, func, env, _s, global, father, itemname, indexname, keyname )
{
var _n = wh.hn( to_iter ) === 'n';
var scope = wh.rv( _s );
var has_old_item = scope.hasOwnProperty(itemname);
var has_old_index = scope.hasOwnProperty(indexname);
var old_item = scope[itemname];
var old_index = scope[indexname];
var full = Object.prototype.toString.call(wh.rv(to_iter));
var type = full[8];
if( type === 'N' && full[10] === 'l' ) type = 'X';
var _y;
if( _n )
{
if( type === 'A' )
{
for( var i = 0 ; i < to_iter.length ; i++ )
{
scope[itemname] = to_iter[i];
scope[indexname] = wh.nh(i, 'h');
_y = keyname ? (keyname==="*this" ? _v(wh.rv(to_iter[i])) : _v(wh.rv(wh.rv(to_iter[i])[keyname]))) : _v();
_(father,_y);
func( env, scope, _y, global );
}
}
else if( type === 'O' )
{
for( var k in to_iter )
{
scope[itemname] = to_iter[k];
scope[indexname] = wh.nh(k, 'h');
_y = keyname ? (keyname==="*this" ? _v(wh.rv(to_iter[k])) : _v(wh.rv(wh.rv(to_iter[k])[keyname]))) : _v();
_(father,_y);
func( env,scope,_y,global );
}
}
else if( type === 'S' )
{
for( var i = 0 ; i < to_iter.length ; i++ )
{
scope[itemname] = to_iter[i];
scope[indexname] = wh.nh(i, 'h');
_y = _v( to_iter[i] + i );
_(father,_y);
func( env, scope, _y, global );
}
}
else if( type === 'N' )
{
for( var i = 0 ; i < to_iter ; i++ )
{
scope[itemname] = i;
scope[indexname] = wh.nh(i, 'h');
_y = _v( i );
_(father,_y);
func(env,scope,_y,global);
}
}
else
{
}
}
else
{
var r_to_iter = wh.rv(to_iter);
var r_iter_item, iter_item;
if( type === 'A' )
{
for( var i = 0 ; i < r_to_iter.length ; i++ )
{
iter_item = r_to_iter[i];
iter_item = wh.hn(iter_item)==='n' ? wh.nh(iter_item,'h') : iter_item;
r_iter_item = wh.rv( iter_item );
scope[itemname] = iter_item
scope[indexname] = wh.nh(i, 'h');
_y = keyname ? (keyname==="*this" ? _v(r_iter_item) : _v(wh.rv(r_iter_item[keyname]))) : _v();
_(father,_y);
func( env, scope, _y, global );
}
}
else if( type === 'O' )
{
for( var k in r_to_iter )
{
iter_item = r_to_iter[k];
iter_item = wh.hn(iter_item)==='n'? wh.nh(iter_item,'h') : iter_item;
r_iter_item = wh.rv( iter_item );
scope[itemname] = iter_item;
scope[indexname] = wh.nh(k, 'h');
_y = keyname ? (keyname==="*this" ? _v(r_iter_item) : _v(wh.rv(r_iter_item[keyname]))) : _v();
_(father,_y);
func( env, scope, _y, global );
}
}
else if( type === 'S' )
{
for( var i = 0 ; i < r_to_iter.length ; i++ )
{
scope[itemname] = wh.nh(r_to_iter[i],'h');
scope[indexname] = wh.nh(i, 'h');
_y = _v( to_iter[i] + i );
_(father,_y);
func( env, scope, _y, global );
}
}
else if( type === 'N' )
{
for( var i = 0 ; i < r_to_iter ; i++ )
{
scope[itemname] = wh.nh(i,'h');
scope[indexname]= wh.nh(i,'h');
_y = _v( i );
_(father,_y);
func(env,scope,_y,global);
}
}
else
{
}
}
if(has_old_item)
{
scope[itemname]=old_item;
}
else
{
delete scope[itemname];
}
if(has_old_index)
{
scope[indexname]=old_index;
}
else{delete scope[indexname];}}


function _r( node, attrname, opindex, env, scope, global )
{
var o = {};
var a = grb( z[opindex], env, scope, global, o );
node.attr[attrname] = a;
if( o.is_affected ) node.n.push( attrname );
}
function _o( opindex, env, scope, global )
{
var nothing = {};
return grb( z[opindex], env, scope, global, nothing );
}
function _1( opindex, env, scope, global )
{
var nothing = {};
return gra( z[opindex], env, scope, global, nothing );
}
function _2( opindex, func, env, scope, global, father, itemname, indexname, keyname )
{
var to_iter = _1( opindex, env, scope, global, father, itemname, indexname, keyname );
wfor( to_iter, func, env, scope, global, father, itemname, indexname, keyname );
}
function _gv( )
{
if( typeof(window.__webview_engine_version__) == 'undefined' ) return 0.0;
return window.__webview_engine_version__;
}
function _m(tag,attrs,env,scope,global)
{
var tmp=_n(tag);
var base=0;
for(var i = 0 ; i < attrs.length ; i+=2 )
{
if(attrs[i+1]<0)
{tmp.attr[attrs[i]]=true;}else{_r(tmp,attrs[i],base+attrs[i+1],env,scope,global);
if(base===0)base=attrs[i+1];}}return tmp;};function _ai(i,p,e,me,r,c){var x=_grp(p,e,me);if(x)i.push(x);else{i.push('');console.warn('WXMLRT:'+me+':import:'+r+':'+c+': Path `'+p+'` not found from `'+me+'`.')}}function _grp(p,e,me){if(p[0]!='/'){var mepart=me.split('/');mepart.pop();var ppart=p.split('/');for(var i=0;i<ppart.length;i++){if(ppart[i]=='..')mepart.pop();else if(ppart[i]=='.' || !ppart[i])continue;else mepart.push(ppart[i]);}p=mepart.join('/');}if(me[0]=='.'&&p[0]=='/')p='.'+p;if(e[p])return p;if(e[p+'.wxml'])return p+'.wxml';}function _gd(p,c,e,d){if(!c)return;if(d[p][c])return d[p][c];for(var x=e[p].i.length-1;x>=0;x--){if(e[p].i[x]&&d[e[p].i[x]][c])return d[e[p].i[x]][c]};for(var x=e[p].ti.length-1;x>=0;x--){var q=_grp(e[p].ti[x],e,p);if(q&&d[q][c])return d[q][c]}
var ii=_gapi(e,p);for(var x=0;x<ii.length;x++){if(ii[x]&&d[ii[x]][c])return d[ii[x]][c]}for(var k=e[p].j.length-1;k>=0;k--)if(e[p].j[k]){for(var q=e[e[p].j[k]].ti.length-1;q>=0;q--){var pp=_grp(e[e[p].j[k]].ti[q],e,p);if(pp&&d[pp][c]){return d[pp][c]}}}}function _gapi(e,p){if(!p)return [];if($gaic[p]){return $gaic[p]};var ret=[],q=[],h=0,t=0,put={},visited={};q.push(p);visited[p]=true;t++;while(h<t){var a=q[h++];for(var i=0;i<e[a].ic.length;i++){var nd=e[a].ic[i];var np=_grp(nd,e,a);if(np&&!visited[np]){visited[np]=true;q.push(np);t++;}}for(var i=0;a!=p&&i<e[a].ti.length;i++){var ni=e[a].ti[i];var nm=_grp(ni,e,a);if(nm&&!put[nm]){put[nm]=true;ret.push(nm);}}}$gaic[p]=ret;return ret;}var $ixc={};function _ic(p,ent,me,e,s,r,gg){var x=_grp(p,ent,me);ent[me].j.push(x);if(x){if($ixc[x]){console.warn('WXMLRT:-1:include:-1:-1: `'+p+'` is being included in a loop, will be stop.');return;}$ixc[x]=true;try{ent[x].f(e,s,r,gg)}catch(e){}$ixc[x]=false;}else{console.warn('WXMLRT:'+me+':include:-1:-1: Included path `'+p+'` not found from `'+me+'`.')}}function _w(tn,f,line,c){console.warn('WXMLRT:'+f+':template:'+line+':'+c+': Template `'+tn+'` not found.');}function _ev(dom){var changed=false;delete dom.properities;delete dom.n;if(dom.children){do{changed=false;var newch = [];for(var i=0;i<dom.children.length;i++){var ch=dom.children[i];if( ch.tag=='virtual'){changed=true;for(var j=0;ch.children&&j<ch.children.length;j++){newch.push(ch.children[j]);}}else { newch.push(ch); } } dom.children = newch; }while(changed);for(var i=0;i<dom.children.length;i++){_ev(dom.children[i]);}} return dom; }var e_={}
if(global&&typeof(global.entrys)=='object')e_=global.entrys
var d_={}
if(global&&typeof(global.defines)=='object')d_=global.defines
var p_={}
var z = [];
  (function(z){
    var a = 11;
    function Z(ops){z.push(ops)};
    Z([3, 'gameIndex cfff']);Z([3, 'tolower']);Z([3, 'view']);Z([3, '2']);Z([3, 'true']);Z([a, [3, 'height:'],[[7],[3, "windowHeight"]],[3, 'px']]);Z([a, [3, 'animated '],[[7],[3, "change"]]]);Z([3, 'nav-header']);Z([[7],[3, "status"]]);Z([3, 'key']);Z([3, 'headerTap']);Z([a, [3, 'nav-cell '],[[2,'?:'],[[2, "=="], [[6],[[7],[3, "item"]],[3, "current"]], [1, 1]],[1, "active"],[1, ""]]]);Z([[6],[[7],[3, "item"]],[3, "id"]]);Z([[6],[[7],[3, "item"]],[3, "ids"]]);Z([a, [[6],[[7],[3, "item"]],[3, "name"]]]);Z([3, 'glist']);Z([[7],[3, "$glist$list"]]);Z([3, 'mt20c']);Z([3, 'order-list']);Z([3, 'order-cell']);Z([3, 'cell-status']);Z([3, 'left']);Z([[2, "!="], [[7],[3, "$glist$label"]], [1, 1]]);Z([3, '$glist$goHome']);Z([[6],[[7],[3, "item"]],[3, "bes_myid"]]);Z([3, 'randh']);Z([3, 'aspectFill']);Z([[6],[[7],[3, "item"]],[3, "user_avatar"]]);Z([3, 'name']);Z([a, [[6],[[7],[3, "item"]],[3, "user_nickname"]]]);Z([[6],[[7],[3, "item"]],[3, "bes_from"]]);Z([[6],[[6],[[7],[3, "item"]],[3, "yours"]],[3, "user_avatar"]]);Z([a, [[6],[[6],[[7],[3, "item"]],[3, "yours"]],[3, "user_nickname"]]]);Z([[2, "=="], [[6],[[7],[3, "item"]],[3, "bes_status"]], [1, 0]]);Z([3, 'right']);Z([3, '\r\n                                已过期\r\n                            ']);Z([[2, "=="], [[6],[[7],[3, "item"]],[3, "bes_status"]], [1, 1]]);Z([3, '$glist$chatForUser']);Z([a, [3, '\r\n                                '],[[6],[[7],[3, "item"]],[3, "times"]],[3, '后开始\r\n                            ']]);Z([3, 'h10']);Z([3, 'cell-item']);Z([3, 'item']);Z([3, 'good-thumb']);Z([[6],[[7],[3, "item"]],[3, "game_thumb"]]);Z([3, 'item-content']);Z([3, 'content-name']);Z([a, [[6],[[7],[3, "item"]],[3, "game_name"]]]);Z([3, 'content-attr']);Z([a, [[6],[[7],[3, "item"]],[3, "game_auth"]]]);Z([3, 'content-count']);Z([a, [3, '\r\n                                        '],[[6],[[7],[3, "item"]],[3, "bes_starttime"]],[3, '\r\n                                        ']]);Z([3, '$glist$besNote']);Z([3, 'content-amount']);Z([[6],[[7],[3, "item"]],[3, "bes_note"]]);Z([3, '\r\n                                            备注\r\n                                        ']);Z([3, 'cell-count']);Z([3, 'count-amount']);Z([a, [3, '共'],[[6],[[7],[3, "item"]],[3, "bes_number"]],[3, '局']]);Z([3, 'count-price']);Z([3, 'count-carriage']);Z([a, [3, '已支付 '],[[6],[[7],[3, "item"]],[3, "bes_pay"]],[3, ' 豌豆']]);Z([3, 'btnLine']);Z([3, 'leftLine']);Z([[2, "=="], [[7],[3, "$glist$label"]], [1, 1]]);Z([3, 'labels']);Z([3, 'cell-btn']);Z([3, 'btn-order btn-solid-red']);Z([3, '未接单']);Z([3, '$glist$channel']);Z([3, 'btn-order btn-hollow-gray']);Z([[6],[[7],[3, "item"]],[3, "bes_gid"]]);Z([[6],[[7],[3, "item"]],[3, "bes_id"]]);Z([3, '0']);Z([3, '取消订单']);Z([[2, "=="], [[6],[[7],[3, "item"]],[3, "bes_status"]], [1, 3]]);Z([3, '$glist$okay']);Z([3, '5']);Z([3, '确认完成']);Z([[7],[3, "$glist$false"]]);Z([[2, "=="], [[6],[[7],[3, "item"]],[3, "bes_status"]], [1, 5]]);Z([3, '$glist$commit']);Z([3, '7']);Z([3, '去评价']);Z([[2, "=="], [[6],[[7],[3, "item"]],[3, "bes_status"]], [1, 7]]);Z([3, '已完成']);Z([3, '$glist$LookCommit']);Z([3, '查看评价']);Z([3, '3']);Z([3, '确认接单']);Z([3, '待确认']);Z([3, '待评价']);Z([[7],[3, "$glist$msg$open"]]);Z([3, 'loadmsg animated fadeIn']);Z([3, 'msg']);Z([3, 'layui-m-layercont']);Z([a, [[7],[3, "$glist$msg$msg"]]]);Z([[2, "<"], [[7],[3, "flag"]], [1, 0]]);Z([3, 'empty']);Z([3, 'abnor']);Z([3, 'abnor__box']);Z([[7],[3, "$empty$image"]]);Z([3, 'abnor__image']);Z([3, 'widthFix']);Z([[7],[3, "$empty$title"]]);Z([3, 'abnor__text']);Z([[7],[3, "$empty$tip"]]);Z([3, 'abnor__tip']);Z([[7],[3, "$empty$button"]]);Z([3, '$empty$emitAbnorTap']);Z([3, 'abnor__btn']);Z([[2, ">="], [[7],[3, "flag"]], [1, 0]]);Z([3, 'loadingMore']);Z([[2, "!="], [[7],[3, "$more$flag"]], [1, 0]]);Z([[2, "=="], [[7],[3, "$more$flag"]], [1, 1]]);Z([3, 'weui-loadmore']);Z([3, 'weui-loading']);Z([3, 'weui-loadmore__tips']);Z([3, '正在加载']);Z([[2, "<"], [[7],[3, "$more$flag"]], [1, 0]]);Z([3, 'weui-loadmore weui-loadmore_line']);Z([3, 'weui-loadmore__tips weui-loadmore__tips_in-line']);Z([3, '加载完成']);Z([[2, "=="], [[7],[3, "$more$flag"]], [1, 2]]);Z([3, '滑动加载更多']);
  })(z);d_["./pages/order/index.wxml"] = {};
  var m0=function(e,s,r,gg){
    var oCn = _n("view");_r(oCn, 'class', 0, e, s, gg);var oDn = _m( "scroll-view", ["bindscrolltolower", 1,"class", 1,"lowerThreshold", 2,"scrollY", 3,"style", 4], e, s, gg);var oEn = _n("view");_r(oEn, 'class', 6, e, s, gg);var oFn = _n("view");_r(oFn, 'class', 7, e, s, gg);var oGn = _v();var oHn = function(oLn,oKn,oJn,gg){var oIn = _m( "view", ["bindtap", 10,"class", 1,"data-id", 2,"data-ids", 3], oLn, oKn, gg);var oNn = _o(14, oLn, oKn, gg);_(oIn,oNn);_(oJn, oIn);return oJn;};_2(8, oHn, e, s, gg, oGn, "item", "index", 'key');_(oFn,oGn);_(oEn,oFn);_(oDn,oEn);var oOn = _n("view");_r(oOn, 'class', 15, e, s, gg);var oPn = _v();var oQn = function(oUn,oTn,oSn,gg){var oRn = _n("view");var oWn = _n("view");_r(oWn, 'class', 17, oUn, oTn, gg);_(oRn,oWn);var oXn = _n("view");_r(oXn, 'class', 18, oUn, oTn, gg);var oYn = _n("view");_r(oYn, 'class', 19, oUn, oTn, gg);var oZn = _n("view");_r(oZn, 'class', 20, oUn, oTn, gg);var oan = _n("view");_r(oan, 'class', 21, oUn, oTn, gg);var obn = _v();
      if (_o(22, oUn, oTn, gg)) {
        obn.wxVkey = 1;var ocn = _m( "view", ["class", -1,"bindtap", 23,"data-uid", 1], oUn, oTn, gg);var oen = _m( "image", ["class", 25,"mode", 1,"src", 2], oUn, oTn, gg);_(ocn,oen);var ofn = _n("view");_r(ofn, 'class', 28, oUn, oTn, gg);var ogn = _o(29, oUn, oTn, gg);_(ofn,ogn);_(ocn,ofn);_(obn, ocn);
      }else {
        obn.wxVkey = 2;var ohn = _m( "view", ["class", -1,"bindtap", 23,"data-uid", 7], e, s, gg);var ojn = _m( "image", ["class", 25,"mode", 1,"src", 6], e, s, gg);_(ohn,ojn);var okn = _n("view");_r(okn, 'class', 28, e, s, gg);var oln = _o(32, e, s, gg);_(okn,oln);_(ohn,okn);_(obn, ohn);
      }_(oan,obn);_(oZn,oan);var omn = _v();
      if (_o(33, oUn, oTn, gg)) {
        omn.wxVkey = 1;var onn = _n("view");_r(onn, 'class', 34, oUn, oTn, gg);var opn = _o(35, oUn, oTn, gg);_(onn,opn);_(omn, onn);
      } _(oZn,omn);var oqn = _v();
      if (_o(36, oUn, oTn, gg)) {
        oqn.wxVkey = 1;var orn = _m( "view", ["data-myid", 24,"data-uid", 6,"class", 10,"bindtap", 13], oUn, oTn, gg);var otn = _o(38, oUn, oTn, gg);_(orn,otn);_(oqn, orn);
      } _(oZn,oqn);_(oYn,oZn);var oun = _n("view");_r(oun, 'class', 39, oUn, oTn, gg);_(oYn,oun);var ovn = _n("view");_r(ovn, 'class', 40, oUn, oTn, gg);var own = _n("view");var oxn = _m( "image", ["alt", 41,"class", 1,"src", 2], oUn, oTn, gg);_(own,oxn);var oyn = _n("view");_r(oyn, 'class', 44, oUn, oTn, gg);var ozn = _n("view");_r(ozn, 'class', 45, oUn, oTn, gg);var o_n = _o(46, oUn, oTn, gg);_(ozn,o_n);_(oyn,ozn);var oAo = _n("view");_r(oAo, 'class', 47, oUn, oTn, gg);var oBo = _o(48, oUn, oTn, gg);_(oAo,oBo);_(oyn,oAo);var oCo = _n("view");_r(oCo, 'class', 49, oUn, oTn, gg);var oDo = _o(50, oUn, oTn, gg);_(oCo,oDo);var oEo = _m( "view", ["bindtap", 51,"class", 1,"data-text", 2], oUn, oTn, gg);var oFo = _o(54, oUn, oTn, gg);_(oEo,oFo);_(oCo,oEo);_(oyn,oCo);_(own,oyn);_(ovn,own);_(oYn,ovn);var oGo = _n("view");_r(oGo, 'class', 55, oUn, oTn, gg);var oHo = _n("view");_r(oHo, 'class', 56, oUn, oTn, gg);var oIo = _o(57, oUn, oTn, gg);_(oHo,oIo);_(oGo,oHo);var oJo = _n("view");_r(oJo, 'class', 58, oUn, oTn, gg);_(oGo,oJo);var oKo = _n("view");_r(oKo, 'class', 59, oUn, oTn, gg);var oLo = _n("text");var oMo = _o(60, oUn, oTn, gg);_(oLo,oMo);_(oKo,oLo);_(oGo,oKo);_(oYn,oGo);var oNo = _n("view");_r(oNo, 'class', 61, oUn, oTn, gg);var oOo = _n("view");_r(oOo, 'class', 62, oUn, oTn, gg);_(oNo,oOo);var oPo = _v();
      if (_o(63, oUn, oTn, gg)) {
        oPo.wxVkey = 1;var oQo = _n("view");_r(oQo, 'class', 64, oUn, oTn, gg);var oSo = _v();
      if (_o(36, oUn, oTn, gg)) {
        oSo.wxVkey = 1;var oTo = _n("view");_r(oTo, 'class', 65, oUn, oTn, gg);var oVo = _n("button");_r(oVo, 'class', 66, oUn, oTn, gg);var oWo = _o(67, oUn, oTn, gg);_(oVo,oWo);_(oTo,oVo);var oXo = _m( "button", ["bindtap", 68,"class", 1,"data-gid", 2,"data-id", 3,"data-sta", 4], oUn, oTn, gg);var oYo = _o(73, oUn, oTn, gg);_(oXo,oYo);_(oTo,oXo);_(oSo, oTo);
      } _(oQo,oSo);var oZo = _v();
      if (_o(74, oUn, oTn, gg)) {
        oZo.wxVkey = 1;var oao = _n("view");_r(oao, 'class', 65, oUn, oTn, gg);var oco = _m( "button", ["class", 66,"data-gid", 4,"data-id", 5,"bindtap", 9,"data-sta", 10], oUn, oTn, gg);var odo = _o(77, oUn, oTn, gg);_(oco,odo);_(oao,oco);var oeo = _v();
      if (_o(78, oUn, oTn, gg)) {
        oeo.wxVkey = 1;var ofo = _n("button");_r(ofo, 'class', 69, oUn, oTn, gg);_(oeo, ofo);
      } _(oao,oeo);_(oZo, oao);
      } _(oQo,oZo);var oho = _v();
      if (_o(79, oUn, oTn, gg)) {
        oho.wxVkey = 1;var oio = _n("view");_r(oio, 'class', 65, oUn, oTn, gg);var oko = _m( "button", ["data-from", 30,"class", 36,"data-gid", 40,"data-id", 41,"bindtap", 50,"data-sta", 51], oUn, oTn, gg);var olo = _o(82, oUn, oTn, gg);_(oko,olo);_(oio,oko);var omo = _v();
      if (_o(78, oUn, oTn, gg)) {
        omo.wxVkey = 1;var ono = _n("button");_r(ono, 'class', 69, oUn, oTn, gg);_(omo, ono);
      } _(oio,omo);_(oho, oio);
      } _(oQo,oho);var opo = _v();
      if (_o(83, oUn, oTn, gg)) {
        opo.wxVkey = 1;var oqo = _n("view");_r(oqo, 'class', 65, oUn, oTn, gg);var oso = _n("button");_r(oso, 'class', 66, oUn, oTn, gg);var oto = _o(84, oUn, oTn, gg);_(oso,oto);_(oqo,oso);var ouo = _m( "button", ["class", 69,"data-id", 2,"bindtap", 16], oUn, oTn, gg);var ovo = _o(86, oUn, oTn, gg);_(ouo,ovo);_(oqo,ouo);_(opo, oqo);
      } _(oQo,opo);_(oPo, oQo);
      }else {
        oPo.wxVkey = 2;var owo = _n("view");_r(owo, 'class', 64, e, s, gg);var oyo = _v();
      if (_o(36, e, s, gg)) {
        oyo.wxVkey = 1;var ozo = _n("view");_r(ozo, 'class', 65, e, s, gg);var oAp = _m( "button", ["class", 66,"bindtap", 2,"data-gid", 4,"data-id", 5,"data-sta", 21], e, s, gg);var oBp = _o(88, e, s, gg);_(oAp,oBp);_(ozo,oAp);var oCp = _m( "button", ["bindtap", 68,"class", 1,"data-gid", 2,"data-sta", 4], e, s, gg);var oDp = _o(73, e, s, gg);_(oCp,oDp);_(ozo,oCp);_(oyo, ozo);
      } _(owo,oyo);var oEp = _v();
      if (_o(74, e, s, gg)) {
        oEp.wxVkey = 1;var oFp = _n("view");_r(oFp, 'class', 65, e, s, gg);var oHp = _n("button");_r(oHp, 'class', 66, e, s, gg);var oIp = _o(89, e, s, gg);_(oHp,oIp);_(oFp,oHp);var oJp = _v();
      if (_o(78, e, s, gg)) {
        oJp.wxVkey = 1;var oKp = _n("button");_r(oKp, 'class', 69, e, s, gg);_(oJp, oKp);
      } _(oFp,oJp);_(oEp, oFp);
      } _(owo,oEp);var oMp = _v();
      if (_o(79, e, s, gg)) {
        oMp.wxVkey = 1;var oNp = _n("view");_r(oNp, 'class', 65, e, s, gg);var oPp = _n("button");_r(oPp, 'class', 66, e, s, gg);var oQp = _o(90, e, s, gg);_(oPp,oQp);_(oNp,oPp);var oRp = _v();
      if (_o(78, e, s, gg)) {
        oRp.wxVkey = 1;var oSp = _n("button");_r(oSp, 'class', 69, e, s, gg);_(oRp, oSp);
      } _(oNp,oRp);_(oMp, oNp);
      } _(owo,oMp);var oUp = _v();
      if (_o(83, e, s, gg)) {
        oUp.wxVkey = 1;var oVp = _n("view");_r(oVp, 'class', 65, e, s, gg);var oXp = _n("button");_r(oXp, 'class', 66, e, s, gg);var oYp = _o(84, e, s, gg);_(oXp,oYp);_(oVp,oXp);var oZp = _m( "button", ["class", 69,"data-id", 2,"bindtap", 16], e, s, gg);var oap = _o(86, e, s, gg);_(oZp,oap);_(oVp,oZp);_(oUp, oVp);
      } _(owo,oUp);_(oPo, owo);
      }_(oNo,oPo);_(oYn,oNo);_(oXn,oYn);_(oRn,oXn);_(oSn, oRn);return oSn;};_2(16, oQn, e, s, gg, oPn, "item", "index", 'key');_(oOn,oPn);var obp = _v();
      if (_o(91, e, s, gg)) {
        obp.wxVkey = 1;var ocp = _n("view");_r(ocp, 'class', 92, e, s, gg);var oep = _n("view");_r(oep, 'class', 93, e, s, gg);var ofp = _n("text");_r(ofp, 'class', 94, e, s, gg);var ogp = _o(95, e, s, gg);_(ofp,ogp);_(oep,ofp);_(ocp,oep);_(obp, ocp);
      } _(oOn,obp);var ohp = _n("view");_r(ohp, 'class', 17, e, s, gg);_(oOn,ohp);_(oDn,oOn);var oip = _v();
      if (_o(96, e, s, gg)) {
        oip.wxVkey = 1;var ojp = _n("view");_r(ojp, 'class', 97, e, s, gg);var olp = _n("view");_r(olp, 'class', 98, e, s, gg);var omp = _n("view");_r(omp, 'class', 99, e, s, gg);var onp = _v();
      if (_o(100, e, s, gg)) {
        onp.wxVkey = 1;var oop = _m( "image", ["src", 100,"class", 1,"mode", 2], e, s, gg);_(onp, oop);
      } _(omp,onp);var oqp = _v();
      if (_o(103, e, s, gg)) {
        oqp.wxVkey = 1;var orp = _n("view");_r(orp, 'class', 104, e, s, gg);var otp = _o(103, e, s, gg);_(orp,otp);_(oqp, orp);
      } _(omp,oqp);var oup = _v();
      if (_o(105, e, s, gg)) {
        oup.wxVkey = 1;var ovp = _n("view");_r(ovp, 'class', 106, e, s, gg);var oxp = _o(105, e, s, gg);_(ovp,oxp);_(oup, ovp);
      } _(omp,oup);var oyp = _v();
      if (_o(107, e, s, gg)) {
        oyp.wxVkey = 1;var ozp = _m( "view", ["bindtap", 108,"class", 1], e, s, gg);var oAq = _o(107, e, s, gg);_(ozp,oAq);_(oyp, ozp);
      } _(omp,oyp);_(olp,omp);_(ojp,olp);_(oip, ojp);
      } _(oDn,oip);var oBq = _v();
      if (_o(110, e, s, gg)) {
        oBq.wxVkey = 1;var oCq = _n("view");_r(oCq, 'class', 111, e, s, gg);var oEq = _v();
      if (_o(112, e, s, gg)) {
        oEq.wxVkey = 1;var oFq = _n("view");_r(oFq, 'class', 111, e, s, gg);var oHq = _v();
      if (_o(113, e, s, gg)) {
        oHq.wxVkey = 1;var oIq = _n("view");_r(oIq, 'class', 114, e, s, gg);var oKq = _n("view");_r(oKq, 'class', 115, e, s, gg);_(oIq,oKq);var oLq = _n("view");_r(oLq, 'class', 116, e, s, gg);var oMq = _o(117, e, s, gg);_(oLq,oMq);_(oIq,oLq);_(oHq, oIq);
      } _(oFq,oHq);var oNq = _v();
      if (_o(118, e, s, gg)) {
        oNq.wxVkey = 1;var oOq = _n("view");_r(oOq, 'class', 119, e, s, gg);var oQq = _n("view");_r(oQq, 'class', 120, e, s, gg);var oRq = _o(121, e, s, gg);_(oQq,oRq);_(oOq,oQq);_(oNq, oOq);
      } _(oFq,oNq);var oSq = _v();
      if (_o(122, e, s, gg)) {
        oSq.wxVkey = 1;var oTq = _n("view");_r(oTq, 'class', 119, e, s, gg);var oVq = _n("view");_r(oVq, 'class', 120, e, s, gg);var oWq = _o(123, e, s, gg);_(oVq,oWq);_(oTq,oVq);_(oSq, oTq);
      } _(oFq,oSq);_(oEq, oFq);
      } _(oCq,oEq);_(oBq, oCq);
      } _(oDn,oBq);_(oCn,oDn);_(r,oCn);
    return r;
  };
        e_["./pages/order/index.wxml"]={f:m0,j:[],i:[],ti:[],ic:[]};
if(path&&e_[path]){
window.__wxml_comp_version__=0.02
return function(env,dd,global){$gwxc=0;var root={"tag":"wx-page"};root.children=[]
var main=e_[path].f
if(typeof(window.__webview_engine_version__)!='undefined'&&window.__webview_engine_version__+1e-6>=0.02+1e-6&&window.__mergeData__)
{env=window.__mergeData__(env,dd);}
try{
main(env,{},root,global);
if(typeof(window.__webview_engine_version__)=='undefined'||window.__webview_engine_version__+1e-6<0.01+1e-6){return _ev(root);}}catch(err){console.log(err)}return root;}}}@code-separator-line:.empty .abnor{position:relative;display:block;width:100%;height:0;padding-bottom:100%;overflow:hidden}.empty .abnor__box{position:absolute;display:flex;top:0;bottom:0;left:0;right:0;flex-direction:column;justify-content:center;align-items:center}.empty .abnor__btn{min-width:%%?228rpx?%%;height:%%?66rpx?%%;margin-top:%%?30rpx?%%;padding:0 %%?30rpx?%%;background-color:#3e9aff;border:0 none;border-radius:%%?2rpx?%%;color:#fff;font-size:%%?28rpx?%%;text-align:center;overflow:hidden;line-height:%%?66rpx?%%}.empty .abnor__btn:active{background-color:#3e9aff}.empty .abnor__image{width:%%?514rpx?%%;background:transparent no-repeat;background-size:cover}.empty .abnor__text{margin-top:%%?30rpx?%%;color:#333;font-size:%%?28rpx?%%}.empty .abnor__tip{margin-top:%%?20rpx?%%;color:#666;font-size:%%?24rpx?%%}.loadingMore{clear:both;display:block}.msg{width:auto;max-width:80%;margin:0 auto;background-color:rgba(0,0,0,.7);color:#fff;display:inline-block;font-size:16px;border-radius:5px;box-shadow:0 0 8px rgba(0,0,0,.1);pointer-events:auto;-webkit-overflow-scrolling:touch;-webkit-animation-fill-mode:both;animation-fill-mode:both;-webkit-animation-duration:.2s;animation-duration:.2s;box-sizing:content-box;position:fixed;bottom:20%;z-index:999;text-align:center;padding:5px 10px}.layui-m-layercont{line-height:22px;text-align:center}.loadmsg{display:flex;justify-content:center;margin:0 auto;text-align:center}.order-cell{background:#fff;padding:0 %%?16.41rpx?%%;overflow:hidden;font-size:%%?28rpx?%%}.cell-status{text-align:right;padding:%%?23.44rpx?%% %%?18.74rpx?%%;color:#ff2d4b;border-bottom:1px solid #f0f0f0;display:flex}.cell-status .left{text-align:left;flex:1}.cell-status .left .randh{width:%%?50rpx?%%;height:%%?50rpx?%%;border-radius:50%;float:left}.cell-status .left .name{float:left;height:%%?50rpx?%%;line-height:%%?50rpx?%%;margin-left:%%?10rpx?%%;color:#333}.cell-status .right{text-align:right;flex:1}.cell-item{padding-bottom:%%?35.15rpx?%%;overflow:hidden;white-space:nowrap;width:100%;height:%%?150.77rpx?%%}.item-content{margin-left:%%?212.35rpx?%%;margin-right:%%?17.84rpx?%%;height:%%?150.77rpx?%%;position:relative}.content-name{font-size:%%?32.44rpx?%%;line-height:%%?32.81rpx?%%;white-space:normal;display:-webkit-box;-webkit-box-orient:vertical;-webkit-line-clamp:2;overflow:hidden}.content-attr{margin-top:%%?11.72rpx?%%;font-size:%%?26.44rpx?%%;color:#a0a0a0;line-height:%%?32.81rpx?%%;white-space:normal;display:-webkit-box;-webkit-box-orient:vertical;-webkit-line-clamp:1;overflow:hidden}.content-count{position:absolute;bottom:0;overflow:hidden;width:100%}.content-amount{float:right;background:rgba(204,204,204,.36);padding:1px 6px;border-radius:10%;color:#666}.cell-count{border-top:1px solid #efefef;border-bottom:1px solid #efefef;overflow:hidden;padding:%%?15rpx?%% %%?18.74rpx?%%;color:#666}.count-amount{float:left}.count-carriage{float:right}.count-price{float:right;margin-left:%%?28.12rpx?%%;color:#ff2b4d}.count-price::before{color:#000}.cell-btn{padding:%%?10rpx?%% %%?18.74rpx?%%;overflow:hidden;box-sizing:border-box;clear:both;display:block;position:relative}.empty{width:100%;height:100%;text-align:center;font-size:%%?28.12rpx?%%;color:#a0a0a0;line-height:%%?48rpx?%%}.empty-img{width:%%?187.49rpx?%%;height:%%?187.49rpx?%%;margin-top:%%?164.05rpx?%%;margin-bottom:%%?23.44rpx?%%}.btn-to-home{margin-top:%%?35.15rpx?%%;width:%%?337.48rpx?%%;height:%%?79.68rpx?%%;line-height:%%?79.68rpx?%%;border-radius:%%?11.72rpx?%%;display:inline-block}.good-thumb{width:%%?145.77rpx?%%;height:%%?145.77rpx?%%;border:1px solid #fff;float:left;margin-left:%%?18.74rpx?%%;border-radius:10px}.btn-order{width:%%?164.05rpx?%%;height:%%?65.62rpx?%%;font-size:%%?28.12rpx?%%;float:right;padding:0;margin-left:%%?35.15rpx?%%}.btn-hollow-gray{border:1px solid rgba(245,245,245,.14);background:#fff}.btn-solid-red{background:#ff2d4b;color:#fff}.btnLine{position:relative}.btnLine .leftLine{position:absolute;color:#333;font-size:%%?28rpx?%%;bottom:%%?17rpx?%%;left:%%?5rpx?%%}.hide{display:none}.orders-wrap{font-size:%%?28.12rpx?%%;height:100%}.orders{height:100%}.nav-header{font-size:%%?28.12rpx?%%;display:-webkit-box;text-align:center;background:#fff;padding:0 %%?16.41rpx?%%;height:%%?100.77rpx?%%;line-height:%%?100.77rpx?%%}.nav-cell{-webkit-box-flex:1}.nav-cell.active{color:#ff4b2d;border-bottom:#ff4b2d %%?4.69rpx?%% solid}.gameIndex{background:#fff}@code-separator-line:__wxRoute = "pages/order/index";__wxRouteBegin = true;
define("pages/order/index.js", function(require, module, exports, window,document,frames,self,location,navigator,localStorage,history,Caches,screen,alert,confirm,prompt,fetch,XMLHttpRequest,WebSocket,webkit,WeixinJSCore,ServiceJSBridge,Reporter){
"use strict";function _interopRequireDefault(e){return e&&e.__esModule?e:{default:e}}function _asyncToGenerator(e){return function(){var t=e.apply(this,arguments);return new Promise(function(e,r){function n(i,o){try{var a=t[i](o),u=a.value}catch(e){return void r(e)}if(!a.done)return Promise.resolve(u).then(function(e){n("next",e)},function(e){n("throw",e)});e(u)}return n("next")})}}function _classCallCheck(e,t){if(!(e instanceof t))throw new TypeError("Cannot call a class as a function")}function _possibleConstructorReturn(e,t){if(!e)throw new ReferenceError("this hasn't been initialised - super() hasn't been called");return!t||"object"!=typeof t&&"function"!=typeof t?e:t}function _inherits(e,t){if("function"!=typeof t&&null!==t)throw new TypeError("Super expression must either be null or a function, not "+typeof t);e.prototype=Object.create(t&&t.prototype,{constructor:{value:e,enumerable:!1,writable:!0,configurable:!0}}),t&&(Object.setPrototypeOf?Object.setPrototypeOf(e,t):e.__proto__=t)}Object.defineProperty(exports,"__esModule",{value:!0});var _createClass=function(){function e(e,t){for(var r=0;r<t.length;r++){var n=t[r];n.enumerable=n.enumerable||!1,n.configurable=!0,"value"in n&&(n.writable=!0),Object.defineProperty(e,n.key,n)}}return function(t,r,n){return r&&e(t.prototype,r),n&&e(t,n),t}}(),_wepy=require("./../../npm/wepy/lib/wepy.js"),_wepy2=_interopRequireDefault(_wepy),_request=require("./../../api/request.js"),_util=require("./../../tools/util.js"),_util2=_interopRequireDefault(_util),_global=require("./../../config/global.js"),_global2=_interopRequireDefault(_global),_glist=require("./../../base/glist.js"),_glist2=_interopRequireDefault(_glist),_loadMore=require("./../../base/tips/loadMore.js"),_loadMore2=_interopRequireDefault(_loadMore),_empty=require("./../../base/loading/empty.js"),_empty2=_interopRequireDefault(_empty),Setting=function(e){function t(){var e,r,n,i;_classCallCheck(this,t);for(var o=arguments.length,a=Array(o),u=0;u<o;u++)a[u]=arguments[u];return r=n=_possibleConstructorReturn(this,(e=t.__proto__||Object.getPrototypeOf(t)).call.apply(e,[this].concat(a))),n.config={navigationBarTitleText:"我的玩伴"},n.$props={glist:{label:"1"},more:{},empty:{}},n.$events={},n.components={glist:_glist2.default,more:_loadMore2.default,empty:_empty2.default},n.data={status:{},current:0,listd:[],limit:12,page:1,change:_global2.default.animated,flag:0,windowHeight:""},n.computed={},n.watch={flag:function(){this.$broadcast("LoadingMore",this.flag)}},n.methods={headerTap:function(e){this.current=(0,_util.get)(e,"ids");var t=(0,_util.get)(e,"id"),r=this.status;r.forEach(function(e,t,n){r[t].current=0}),r[t].current=1,this.flag=0,this.page=1,this.listd=[],this.orderList(),this.$apply()},tolower:function(e){this.orderList()}},n.events={orderUpdateData:function(e){console.log("刷新页面数据"),this.flag=0,this.orderList()}},i=r,_possibleConstructorReturn(n,i)}return _inherits(t,e),_createClass(t,[{key:"orderList",value:function(){function e(){return t.apply(this,arguments)}var t=_asyncToGenerator(regeneratorRuntime.mark(function e(){var t=this;return regeneratorRuntime.wrap(function(e){for(;;)switch(e.prev=e.next){case 0:if(!(this.flag<0)){e.next=2;break}return e.abrupt("return",!1);case 2:(0,_request.requrst)("order/orderlist",{status:this.current,limit:this.limit,page:this.page++,type:"index"},function(e){e.info.data.length>0?(t.listd=_util2.default.AddData(t.listd,e.info.data),t.flag=2):t.flag=-2,t.$broadcast("glist",t.listd,1),t.$apply()});case 3:case"end":return e.stop()}},e,this)}));return e}()},{key:"orderStatus",value:function(){function e(){return t.apply(this,arguments)}var t=_asyncToGenerator(regeneratorRuntime.mark(function e(){var t=this;return regeneratorRuntime.wrap(function(e){for(;;)switch(e.prev=e.next){case 0:(0,_request.requrst)("order/orderStatus",{type:0},function(e){t.status=e.info,t.$apply()});case 1:case"end":return e.stop()}},e,this)}));return e}()},{key:"onLoad",value:function(){function e(){return t.apply(this,arguments)}var t=_asyncToGenerator(regeneratorRuntime.mark(function e(){return regeneratorRuntime.wrap(function(e){for(;;)switch(e.prev=e.next){case 0:this.orderStatus(),this.orderList(),this.windowHeight=_util2.default.sysInfo("windowHeight");case 3:case"end":return e.stop()}},e,this)}));return e}()}]),t}(_wepy2.default.page);Page(require("./../../npm/wepy/lib/wepy.js").default.$createPage(Setting,"pages/order/index"));
});require("pages/order/index.js")@code-separator-line:["div","view","scroll-view","image","text","button"]