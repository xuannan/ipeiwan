/*v0.6vv_20170214_fbi*/
window.__wcc_version__='v0.6vv_20170214_fbi'
window.__wxml_transpiler_version__='v0.1'
var $gwxc
var $gaic={}
$gwx=function(path,global){
function _(a,b){b&&a.children.push(b);}
function _v(k){if(typeof(k)!='undefined')return {tag:'virtual','wxKey':k,children:[]};return {tag:'virtual',children:[]};}
function _n(tag){$gwxc++;if($gwxc>=16000){throw 'Dom limit exceeded, please check if there\'s any mistake you\'ve made.'};return {tag:tag.substr(0,3)=='wx-'?tag:'wx-'+tag,attr:{},children:[],n:[]}}
function _p(a,b){b&&a.properities.push(b);}
function _s(scope,env,key){return typeof(scope[key])!='undefined'?scope[key]:env[key]}function _wl(tname,prefix){console.warn('WXMLRT:'+prefix+':-1:-1:-1: Template `' + tname + '` is being called recursively, will be stop.')}$gwn=console.warn;
$gwl=console.log;
function $gwh()
{
function x(){}
x.prototype =
{
hn: function( obj )
{
if( typeof(obj) == 'object' )
{
var cnt=0;
var any=false;
for(var x in obj)
{
any|=x==='__value__';
cnt++;
if(cnt>2)break;
}
return cnt == 2 && any && obj.hasOwnProperty('__wxspec__') ? "h" : "n";
}
return "n";
},
nh: function( obj, special )
{
return { __value__: obj, __wxspec__: special ? special : true }
},
rv: function( obj )
{
return this.hn(obj)==='n'?obj:this.rv(obj.__value__);
}
}
return new x;
}
wh=$gwh();
function $gwrt( should_pass_type_info )
{
function ArithmeticEv( ops, e, s, g, o )
{
var rop = ops[0][1];
var _a,_b,_c,_d, _aa, _bb;
switch( rop )
{
case '?:':
_a = rev( ops[1], e, s, g, o );
_c = should_pass_type_info && ( wh.hn(_a) === 'h' );
_d = wh.rv( _a ) ? rev( ops[2], e, s, g, o ) : rev( ops[3], e, s, g, o );
_d = _c && wh.hn( _d ) === 'n' ? wh.nh( _d, 'c' ) : _d;
return _d;
break;
case '&&':
_a = rev( ops[1], e, s, g, o );
_c = should_pass_type_info && ( wh.hn(_a) === 'h' );
_d = wh.rv( _a ) ? rev( ops[2], e, s, g, o ) : wh.rv( _a );
_d = _c && wh.hn( _d ) === 'n' ? wh.nh( _d, 'c' ) : _d;
return _d;
break;
case '||':
_a = rev( ops[1], e, s, g, o );
_c = should_pass_type_info && ( wh.hn(_a) === 'h' );
_d = wh.rv( _a ) ? wh.rv(_a) : rev( ops[2], e, s, g, o );
_d = _c && wh.hn( _d ) === 'n' ? wh.nh( _d, 'c' ) : _d;
return _d;
break;
case '+':
case '*':
case '/':
case '%':
case '|':
case '^':
case '&':
case '===':
case '==':
case '!=':
case '!==':
case '>=':
case '<=':
case '>':
case '<':
case '<<':
case '>>':
_a = rev( ops[1], e, s, g, o );
_b = rev( ops[2], e, s, g, o );
_c = should_pass_type_info && (wh.hn( _a ) === 'h' || wh.hn( _b ) === 'h');
switch( rop )
{
case '+':
_d = wh.rv( _a ) + wh.rv( _b );
break;
case '*':
_d = wh.rv( _a ) * wh.rv( _b );
break;
case '/':
_d = wh.rv( _a ) / wh.rv( _b );
break;
case '%':
_d = wh.rv( _a ) % wh.rv( _b );
break;
case '|':
_d = wh.rv( _a ) | wh.rv( _b );
break;
case '^':
_d = wh.rv( _a ) ^ wh.rv( _b );
break;
case '&':
_d = wh.rv( _a ) & wh.rv( _b );
break;
case '===':
_d = wh.rv( _a ) === wh.rv( _b );
break;
case '==':
_d = wh.rv( _a ) == wh.rv( _b );
break;
case '!=':
_d = wh.rv( _a ) != wh.rv( _b );
break;
case '!==':
_d = wh.rv( _a ) !== wh.rv( _b );
break;
case '>=':
_d = wh.rv( _a ) >= wh.rv( _b );
break;
case '<=':
_d = wh.rv( _a ) <= wh.rv( _b );
break;
case '>':
_d = wh.rv( _a ) > wh.rv( _b );
break;
case '<':
_d = wh.rv( _a ) < wh.rv( _b );
break;
case '<<':
_d = wh.rv( _a ) << wh.rv( _b );
break;
case '>>':
_d = wh.rv( _a ) >> wh.rv( _b );
break;
default:
break;
}
return _c ? wh.nh( _d, "c" ) : _d;
break;
case '-':
_a = ops.length === 3 ? rev( ops[1], e, s, g, o ) : 0;
_b = ops.length === 3 ? rev( ops[2], e, s, g, o ) : rev( ops[1], e, s, g, o );
_c = should_pass_type_info && (wh.hn( _a ) === 'h' || wh.hn( _b ) === 'h');
_d = _c ? wh.rv( _a ) - wh.rv( _b ) : _a - _b;
return _c ? wh.nh( _d, "c" ) : _d;
break;
case '!':
_a = rev( ops[1], e, s, g, o );
_c = should_pass_type_info && (wh.hn( _a ) == 'h');
_d = !wh.rv(_a);
return _c ? wh.nh( _d, "c" ) : _d;
case '~':
_a = rev( ops[1], e, s, g, o );
_c = should_pass_type_info && (wh.hn( _a ) == 'h');
_d = ~wh.rv(_a);
return _c ? wh.nh( _d, "c" ) : _d;
default:
$gwn('unrecognized op' + rop );
}
}
function rev( ops, e, s, g, o )
{
var op = ops[0];
if( typeof(op)==='object' )
{
var vop=op[0];
var _a, _aa, _b, _bb, _c, _d, _s, _e, _ta, _tb, _td;
switch(vop)
{
case 2:
return ArithmeticEv(ops,e,s,g,o);
break;
case 4:
return rev( ops[1], e, s, g, o );
break;
case 5:
switch( ops.length )
{
case 2:
return should_pass_type_info ?
[rev(ops[1],e,s,g,o)] :
[wh.rv(rev(ops[1],e,s,g,o))];
break;
case 1:
return [];
break;
default:
_a = rev( ops[1],e,s,g,o );
_a.push(
should_pass_type_info ?
rev( ops[2],e,s,g,o ) :
wh.rv( rev(ops[2],e,s,g,o) )
);
return _a;
break;
}
break;
case 6:
_a = rev(ops[1],e,s,g,o);
_ta = wh.hn(_a)==='h';
_aa = _ta ? wh.rv(_a) : _a;
o.is_affected |= _ta;
if( should_pass_type_info )
{
if( _aa===null || typeof(_aa) === 'undefined' )
{
return _ta ? wh.nh(undefined, 'e') : undefined;
}
_b = rev(ops[2],e,s,g,o);
_tb = wh.hn(_b) === 'h';
_bb = _tb ? wh.rv(_b) : _b;
o.is_affected |= _tb;
if( _bb===null || typeof(_bb) === 'undefined' )
{
return (_ta || _tb) ? wh.nh(undefined, 'e') : undefined;
}
_d = _aa[_bb];
_td = wh.hn(_d)==='h';
o.is_affected |= _td;
return (_ta || _tb) ? (_td ? _d : wh.nh(_d, 'e')) : _d;
}
else
{
if( _aa===null || typeof(_aa) === 'undefined' )
{
return undefined;
}
_b = rev(ops[2],e,s,g,o);
_tb = wh.hn(_b) === 'h';
_bb = _tb ? wh.rv(_b) : _b;
o.is_affected |= _tb;
if( _bb===null || typeof(_bb) === 'undefined' )
{
return undefined;
}
_d = _aa[_bb];
_td = wh.hn(_d)==='h';
o.is_affected |= _td;
return _td ? wh.rv(_d) : _d;
}
case 7:
switch(ops[1][0])
{
case 11:
o.is_affected |= wh.hn(g)==='h';
return g;
case 3:
_s = wh.rv( s );
_e = wh.rv( e );
_b = ops[1][1];
_a = _s && _s.hasOwnProperty(_b) ?
s : _e && ( _e.hasOwnProperty(_b) ? e : undefined );
if( should_pass_type_info )
{
if( _a )
{
_ta = wh.hn(_a) === 'h';
_aa = _ta ? wh.rv( _a ) : _a;
_d = _aa[_b];
_td = wh.hn(_d) === 'h';
o.is_affected |= _ta || _td;
_d = _ta && !_td ? wh.nh(_d,'e') : _d;
return _d;
}
}
else
{
if( _a )
{
_ta = wh.hn(_a) === 'h';
_aa = _ta ? wh.rv( _a ) : _a;
_d = _aa[_b];
_td = wh.hn(_d) === 'h';
o.is_affected |= _ta || _td;
return wh.rv(_d);
}
}
return undefined;
}
break;
case 8:
_a = {};
_a[ops[1]] = rev(ops[2],e,s,g,o);
return _a;
break;
case 9:
_a = rev(ops[1],e,s,g,o);
_b = rev(ops[2],e,s,g,o);
function merge( _a, _b, _ow )
{
_ta = wh.hn(_a)==='h';
_tb = wh.hn(_b)==='h';
_aa = wh.rv(_a);
_bb = wh.rv(_b);
if( should_pass_type_info )
{
if( _tb )
{
for(var k in _bb)
{
if ( _ow || !_aa.hasOwnProperty(k) )
_aa[k]=wh.nh(_bb[k],'e');
}
}
else
{
for(var k in _bb)
{
if ( _ow || !_aa.hasOwnProperty(k) )
_aa[k]=_bb[k];
}
}
}
else
{
for(var k in _bb)
{
if ( _ow || _aa.hasOwnProperty(k) )
_aa[k]=wh.rv(_bb[k]);
}
}
return _a;
}
var _c = _a
var _ow = true
if ( typeof(ops[1][0]) === "object" && ops[1][0][0] === 10 ) {
_a = _b
_b = _c
_ow = false
}
if ( typeof(ops[1][0]) === "object" && ops[1][0][0] === 10 ) {
var _r = {}
return merge( merge( _r, _a, _ow ), _b, _ow );
}
else
return merge( _a, _b, _ow );
break;
case 10:
return should_pass_type_info ? rev(ops[1],e,s,g,o) : wh.rv(rev(ops[1],e,s,g,o));
}
}
else
{
if( op === 3 || op === 1 ) return ops[1];
else if( op === 11 )
{
var _a='';
for( var i = 1 ; i < ops.length ; i++ )
{
var xp = wh.rv(rev(ops[i],e,s,g,o));
_a += typeof(xp) === 'undefined' ? '' : xp;
}
return _a;
}
}
}
return rev;
}
gra=$gwrt(true);
grb=$gwrt(false);
function TestTest( expr, ops, e,s,g, expect_a, expect_b, expect_affected )
{
{
var o = {is_affected:false};
var a = gra( ops, e,s,g, o );
if( JSON.stringify(a) != JSON.stringify( expect_a )
|| o.is_affected != expect_affected )
{
console.warn( "A. " + expr + " get result " + JSON.stringify(a) + ", " + o.is_affected + ", but " + JSON.stringify( expect_a ) + ", " + expect_affected + " is expected" );
}
}
{
var o = {is_affected:false};
var a = grb( ops, e,s,g, o );
if( JSON.stringify(a) != JSON.stringify( expect_b )
|| o.is_affected != expect_affected )
{
console.warn( "B. " + expr + " get result " + JSON.stringify(a) + ", " + o.is_affected + ", but " + JSON.stringify( expect_b ) + ", " + expect_affected + " is expected" );
}
}
}

function wfor( to_iter, func, env, _s, global, father, itemname, indexname, keyname )
{
var _n = wh.hn( to_iter ) === 'n';
var scope = wh.rv( _s );
var has_old_item = scope.hasOwnProperty(itemname);
var has_old_index = scope.hasOwnProperty(indexname);
var old_item = scope[itemname];
var old_index = scope[indexname];
var full = Object.prototype.toString.call(wh.rv(to_iter));
var type = full[8];
if( type === 'N' && full[10] === 'l' ) type = 'X';
var _y;
if( _n )
{
if( type === 'A' )
{
for( var i = 0 ; i < to_iter.length ; i++ )
{
scope[itemname] = to_iter[i];
scope[indexname] = wh.nh(i, 'h');
_y = keyname ? (keyname==="*this" ? _v(wh.rv(to_iter[i])) : _v(wh.rv(wh.rv(to_iter[i])[keyname]))) : _v();
_(father,_y);
func( env, scope, _y, global );
}
}
else if( type === 'O' )
{
for( var k in to_iter )
{
scope[itemname] = to_iter[k];
scope[indexname] = wh.nh(k, 'h');
_y = keyname ? (keyname==="*this" ? _v(wh.rv(to_iter[k])) : _v(wh.rv(wh.rv(to_iter[k])[keyname]))) : _v();
_(father,_y);
func( env,scope,_y,global );
}
}
else if( type === 'S' )
{
for( var i = 0 ; i < to_iter.length ; i++ )
{
scope[itemname] = to_iter[i];
scope[indexname] = wh.nh(i, 'h');
_y = _v( to_iter[i] + i );
_(father,_y);
func( env, scope, _y, global );
}
}
else if( type === 'N' )
{
for( var i = 0 ; i < to_iter ; i++ )
{
scope[itemname] = i;
scope[indexname] = wh.nh(i, 'h');
_y = _v( i );
_(father,_y);
func(env,scope,_y,global);
}
}
else
{
}
}
else
{
var r_to_iter = wh.rv(to_iter);
var r_iter_item, iter_item;
if( type === 'A' )
{
for( var i = 0 ; i < r_to_iter.length ; i++ )
{
iter_item = r_to_iter[i];
iter_item = wh.hn(iter_item)==='n' ? wh.nh(iter_item,'h') : iter_item;
r_iter_item = wh.rv( iter_item );
scope[itemname] = iter_item
scope[indexname] = wh.nh(i, 'h');
_y = keyname ? (keyname==="*this" ? _v(r_iter_item) : _v(wh.rv(r_iter_item[keyname]))) : _v();
_(father,_y);
func( env, scope, _y, global );
}
}
else if( type === 'O' )
{
for( var k in r_to_iter )
{
iter_item = r_to_iter[k];
iter_item = wh.hn(iter_item)==='n'? wh.nh(iter_item,'h') : iter_item;
r_iter_item = wh.rv( iter_item );
scope[itemname] = iter_item;
scope[indexname] = wh.nh(k, 'h');
_y = keyname ? (keyname==="*this" ? _v(r_iter_item) : _v(wh.rv(r_iter_item[keyname]))) : _v();
_(father,_y);
func( env, scope, _y, global );
}
}
else if( type === 'S' )
{
for( var i = 0 ; i < r_to_iter.length ; i++ )
{
scope[itemname] = wh.nh(r_to_iter[i],'h');
scope[indexname] = wh.nh(i, 'h');
_y = _v( to_iter[i] + i );
_(father,_y);
func( env, scope, _y, global );
}
}
else if( type === 'N' )
{
for( var i = 0 ; i < r_to_iter ; i++ )
{
scope[itemname] = wh.nh(i,'h');
scope[indexname]= wh.nh(i,'h');
_y = _v( i );
_(father,_y);
func(env,scope,_y,global);
}
}
else
{
}
}
if(has_old_item)
{
scope[itemname]=old_item;
}
else
{
delete scope[itemname];
}
if(has_old_index)
{
scope[indexname]=old_index;
}
else{delete scope[indexname];}}


function _r( node, attrname, opindex, env, scope, global )
{
var o = {};
var a = grb( z[opindex], env, scope, global, o );
node.attr[attrname] = a;
if( o.is_affected ) node.n.push( attrname );
}
function _o( opindex, env, scope, global )
{
var nothing = {};
return grb( z[opindex], env, scope, global, nothing );
}
function _1( opindex, env, scope, global )
{
var nothing = {};
return gra( z[opindex], env, scope, global, nothing );
}
function _2( opindex, func, env, scope, global, father, itemname, indexname, keyname )
{
var to_iter = _1( opindex, env, scope, global, father, itemname, indexname, keyname );
wfor( to_iter, func, env, scope, global, father, itemname, indexname, keyname );
}
function _gv( )
{
if( typeof(window.__webview_engine_version__) == 'undefined' ) return 0.0;
return window.__webview_engine_version__;
}
function _m(tag,attrs,env,scope,global)
{
var tmp=_n(tag);
var base=0;
for(var i = 0 ; i < attrs.length ; i+=2 )
{
if(attrs[i+1]<0)
{tmp.attr[attrs[i]]=true;}else{_r(tmp,attrs[i],base+attrs[i+1],env,scope,global);
if(base===0)base=attrs[i+1];}}return tmp;};function _ai(i,p,e,me,r,c){var x=_grp(p,e,me);if(x)i.push(x);else{i.push('');console.warn('WXMLRT:'+me+':import:'+r+':'+c+': Path `'+p+'` not found from `'+me+'`.')}}function _grp(p,e,me){if(p[0]!='/'){var mepart=me.split('/');mepart.pop();var ppart=p.split('/');for(var i=0;i<ppart.length;i++){if(ppart[i]=='..')mepart.pop();else if(ppart[i]=='.' || !ppart[i])continue;else mepart.push(ppart[i]);}p=mepart.join('/');}if(me[0]=='.'&&p[0]=='/')p='.'+p;if(e[p])return p;if(e[p+'.wxml'])return p+'.wxml';}function _gd(p,c,e,d){if(!c)return;if(d[p][c])return d[p][c];for(var x=e[p].i.length-1;x>=0;x--){if(e[p].i[x]&&d[e[p].i[x]][c])return d[e[p].i[x]][c]};for(var x=e[p].ti.length-1;x>=0;x--){var q=_grp(e[p].ti[x],e,p);if(q&&d[q][c])return d[q][c]}
var ii=_gapi(e,p);for(var x=0;x<ii.length;x++){if(ii[x]&&d[ii[x]][c])return d[ii[x]][c]}for(var k=e[p].j.length-1;k>=0;k--)if(e[p].j[k]){for(var q=e[e[p].j[k]].ti.length-1;q>=0;q--){var pp=_grp(e[e[p].j[k]].ti[q],e,p);if(pp&&d[pp][c]){return d[pp][c]}}}}function _gapi(e,p){if(!p)return [];if($gaic[p]){return $gaic[p]};var ret=[],q=[],h=0,t=0,put={},visited={};q.push(p);visited[p]=true;t++;while(h<t){var a=q[h++];for(var i=0;i<e[a].ic.length;i++){var nd=e[a].ic[i];var np=_grp(nd,e,a);if(np&&!visited[np]){visited[np]=true;q.push(np);t++;}}for(var i=0;a!=p&&i<e[a].ti.length;i++){var ni=e[a].ti[i];var nm=_grp(ni,e,a);if(nm&&!put[nm]){put[nm]=true;ret.push(nm);}}}$gaic[p]=ret;return ret;}var $ixc={};function _ic(p,ent,me,e,s,r,gg){var x=_grp(p,ent,me);ent[me].j.push(x);if(x){if($ixc[x]){console.warn('WXMLRT:-1:include:-1:-1: `'+p+'` is being included in a loop, will be stop.');return;}$ixc[x]=true;try{ent[x].f(e,s,r,gg)}catch(e){}$ixc[x]=false;}else{console.warn('WXMLRT:'+me+':include:-1:-1: Included path `'+p+'` not found from `'+me+'`.')}}function _w(tn,f,line,c){console.warn('WXMLRT:'+f+':template:'+line+':'+c+': Template `'+tn+'` not found.');}function _ev(dom){var changed=false;delete dom.properities;delete dom.n;if(dom.children){do{changed=false;var newch = [];for(var i=0;i<dom.children.length;i++){var ch=dom.children[i];if( ch.tag=='virtual'){changed=true;for(var j=0;ch.children&&j<ch.children.length;j++){newch.push(ch.children[j]);}}else { newch.push(ch); } } dom.children = newch; }while(changed);for(var i=0;i<dom.children.length;i++){_ev(dom.children[i]);}} return dom; }var e_={}
if(global&&typeof(global.entrys)=='object')e_=global.entrys
var d_={}
if(global&&typeof(global.defines)=='object')d_=global.defines
var p_={}
var z = [];
  (function(z){
    var a = 11;
    function Z(ops){z.push(ops)};
    Z([3, 'container']);Z([3, 'create']);Z([3, 'zan-panel']);Z([3, 'margin-top:0']);Z([3, 'zan-cell zan-field']);Z([3, 'zan-cell__hd zan-field__title fatitle']);Z([a, [[7],[3, "gameName"]],[3, '-接单设置']]);Z([3, 'zan-cell__hd zan-field__title']);Z([3, '性别设定']);Z([3, 'positionnSex']);Z([3, 'zan-field__input zan-cell__bd']);Z([3, '不限']);Z([3, 'readonly']);Z([3, 'text']);Z([[6],[[7],[3, "form"]],[3, "c_sex_show"]]);Z([3, 'choseTag']);Z([3, '我的标签']);Z([3, 'bindPickerChange']);Z([[6],[[7],[3, "packer"]],[3, "item"]]);Z([[7],[3, "index"]]);Z([3, 'picker']);Z([a, [3, '\r\n                                '],[[2, "||"],[[6],[[7],[3, "form"]],[3, "c_tag_show"]],[1, "请选择标签"]],[3, '\r\n                            ']]);Z([1, false]);Z([3, 'weui-cells weui-cells_after-title zan-cell zan-field']);Z([3, 'checkboxChange']);Z([3, 'checkbox']);Z([[7],[3, "checkboxItems"]]);Z([3, 'value']);Z([3, 'weui-cell weui-check__label']);Z([[6],[[7],[3, "item"]],[3, "checked"]]);Z([3, 'weui-check']);Z([[6],[[7],[3, "item"]],[3, "tag_id"]]);Z([3, 'weui-cell__hd weui-check__hd_in-checkbox']);Z([[2, "!"], [[6],[[7],[3, "item"]],[3, "checked"]]]);Z([3, 'weui-icon-checkbox_circle']);Z([3, '23']);Z([3, 'circle']);Z([3, 'weui-icon-checkbox_success']);Z([3, 'success']);Z([3, 'weui-cell__bd']);Z([a, [[6],[[7],[3, "item"]],[3, "tag_name"]]]);Z([3, 'clear']);Z([3, '理想局数']);Z([3, 'play4change']);Z([3, 'body-view']);Z([[6],[[6],[[7],[3, "commData"]],[3, "play"]],[3, "max"]]);Z([[6],[[6],[[7],[3, "commData"]],[3, "play"]],[3, "min"]]);Z([[6],[[6],[[7],[3, "commData"]],[3, "play"]],[3, "step"]]);Z([3, '0']);Z([3, '局\r\n                    ']);Z([3, '理想佣金']);Z([3, 'slider4change']);Z([[6],[[6],[[7],[3, "commData"]],[3, "money"]],[3, "max"]]);Z([[6],[[6],[[7],[3, "commData"]],[3, "money"]],[3, "min"]]);Z([[6],[[6],[[7],[3, "commData"]],[3, "money"]],[3, "step"]]);Z([[6],[[7],[3, "form"]],[3, "c_money"]]);Z([3, '元\r\n                    ']);Z([3, '理想等级']);Z([3, 'slider4level']);Z([[6],[[6],[[7],[3, "commData"]],[3, "level"]],[3, "max"]]);Z([[6],[[6],[[7],[3, "commData"]],[3, "level"]],[3, "min"]]);Z([[6],[[7],[3, "form"]],[3, "c_level"]]);Z([3, '级\r\n                    ']);Z([[2, "=="], [[6],[[7],[3, "gameInfo"]],[3, "game_id"]], [1, 0]]);Z([3, '最低段位']);Z([3, 'positionnSet']);Z([3, '青铜段位']);Z([[6],[[7],[3, "form"]],[3, "c_index"]]);Z([3, '我的签名']);Z([3, 'c_note']);Z([3, 'weui-textarea']);Z([3, '请输入签名']);Z([3, 'height: 3.3em']);Z([[6],[[7],[3, "form"]],[3, "c_note"]]);Z([3, '开启接单']);Z([3, 'weui-cell__ft']);Z([3, 'c_status']);Z([[6],[[7],[3, "form"]],[3, "c_status"]]);Z([3, 'h10']);Z([3, 'btnGroup']);Z([3, 'saveData']);Z([3, 'zan-btn zan-btn--primary']);Z([3, '保存设置']);Z([3, 'goBack']);Z([3, 'zan-btn']);Z([3, '返回']);Z([3, 'h20']);Z([[7],[3, "$msg$open"]]);Z([3, 'loadmsg animated fadeIn']);Z([3, 'msg']);Z([3, 'layui-m-layercont']);Z([a, [[7],[3, "$msg$msg"]]]);
  })(z);d_["./pages/order/setting.wxml"] = {};
  var m0=function(e,s,r,gg){
    var odf = _n("view");_r(odf, 'class', 0, e, s, gg);var oef = _n("view");_r(oef, 'id', 1, e, s, gg);var off = _n("view");_r(off, 'class', 0, e, s, gg);var ogf = _m( "view", ["class", 2,"style", 1], e, s, gg);var ohf = _n("view");_r(ohf, 'class', 4, e, s, gg);var oif = _n("view");_r(oif, 'class', 5, e, s, gg);var ojf = _o(6, e, s, gg);_(oif,ojf);_(ohf,oif);_(ogf,ohf);var okf = _n("view");_r(okf, 'class', 4, e, s, gg);var olf = _n("view");_r(olf, 'class', 7, e, s, gg);var omf = _o(8, e, s, gg);_(olf,omf);_(okf,olf);var onf = _m( "input", ["bindtap", 9,"class", 1,"placeholder", 2,"readonly", 3,"type", 4,"value", 5], e, s, gg);_(okf,onf);_(ogf,okf);var oof = _m( "view", ["class", 4,"bindtap", 11], e, s, gg);var opf = _n("view");_r(opf, 'class', 7, e, s, gg);var oqf = _o(16, e, s, gg);_(opf,oqf);_(oof,opf);var orf = _m( "picker", ["bindchange", 17,"range", 1,"value", 2], e, s, gg);var osf = _n("view");_r(osf, 'class', 20, e, s, gg);var otf = _o(21, e, s, gg);_(osf,otf);_(orf,osf);_(oof,orf);_(ogf,oof);var ouf = _v();
      if (_o(22, e, s, gg)) {
        ouf.wxVkey = 1;var ovf = _n("view");_r(ovf, 'class', 23, e, s, gg);var oxf = _m( "checkbox-group", ["bindchange", 24,"class", 1], e, s, gg);var oyf = _v();var ozf = function(oCg,oBg,oAg,gg){var o_f = _n("label");_r(o_f, 'class', 28, oCg, oBg, gg);var oEg = _m( "checkbox", ["checked", 29,"class", 1,"value", 2], oCg, oBg, gg);_(o_f,oEg);var oFg = _n("view");_r(oFg, 'class', 32, oCg, oBg, gg);var oGg = _v();
      if (_o(33, oCg, oBg, gg)) {
        oGg.wxVkey = 1;var oHg = _m( "icon", ["class", 34,"size", 1,"type", 2], oCg, oBg, gg);_(oGg, oHg);
      } _(oFg,oGg);var oJg = _v();
      if (_o(29, oCg, oBg, gg)) {
        oJg.wxVkey = 1;var oKg = _m( "icon", ["size", 35,"class", 2,"type", 3], oCg, oBg, gg);_(oJg, oKg);
      } _(oFg,oJg);_(o_f,oFg);var oMg = _n("view");_r(oMg, 'class', 39, oCg, oBg, gg);var oNg = _o(40, oCg, oBg, gg);_(oMg,oNg);_(o_f,oMg);_(oAg, o_f);return oAg;};_2(26, ozf, e, s, gg, oyf, "item", "index", 'value');_(oxf,oyf);_(ovf,oxf);_(ouf, ovf);
      } _(ogf,ouf);var oOg = _n("view");_r(oOg, 'class', 41, e, s, gg);_(ogf,oOg);var oPg = _n("view");_r(oPg, 'class', 4, e, s, gg);var oQg = _n("view");_r(oQg, 'class', 7, e, s, gg);var oRg = _o(42, e, s, gg);_(oQg,oRg);_(oPg,oQg);var oSg = _m( "slider", ["showValue", -1,"bindchange", 43,"class", 1,"max", 2,"min", 3,"step", 4,"value", 5], e, s, gg);_(oPg,oSg);var oTg = _o(49, e, s, gg);_(oPg,oTg);_(ogf,oPg);var oUg = _n("view");_r(oUg, 'class', 4, e, s, gg);var oVg = _n("view");_r(oVg, 'class', 7, e, s, gg);var oWg = _o(50, e, s, gg);_(oVg,oWg);_(oUg,oVg);var oXg = _m( "slider", ["showValue", -1,"class", 44,"bindchange", 7,"max", 8,"min", 9,"step", 10,"value", 11], e, s, gg);_(oUg,oXg);var oYg = _o(56, e, s, gg);_(oUg,oYg);_(ogf,oUg);var oZg = _n("view");_r(oZg, 'class', 4, e, s, gg);var oag = _n("view");_r(oag, 'class', 7, e, s, gg);var obg = _o(57, e, s, gg);_(oag,obg);_(oZg,oag);var ocg = _m( "slider", ["showValue", -1,"class", 44,"bindchange", 14,"max", 15,"min", 16,"value", 17], e, s, gg);_(oZg,ocg);var odg = _o(62, e, s, gg);_(oZg,odg);_(ogf,oZg);var oeg = _v();
      if (_o(63, e, s, gg)) {
        oeg.wxVkey = 1;var ofg = _n("view");_r(ofg, 'class', 4, e, s, gg);var ohg = _n("view");_r(ohg, 'class', 7, e, s, gg);var oig = _o(64, e, s, gg);_(ohg,oig);_(ofg,ohg);var ojg = _m( "input", ["disabled", -1,"class", 10,"type", 3,"bindtap", 55,"placeholder", 56,"value", 57], e, s, gg);_(ofg,ojg);_(oeg, ofg);
      } _(ogf,oeg);var okg = _n("view");_r(okg, 'class', 4, e, s, gg);var olg = _n("view");_r(olg, 'class', 7, e, s, gg);var omg = _o(68, e, s, gg);_(olg,omg);_(okg,olg);var ong = _m( "textarea", ["bindchange", 69,"class", 1,"placeholder", 2,"style", 3,"value", 4], e, s, gg);_(okg,ong);_(ogf,okg);var oog = _n("view");_r(oog, 'class', 4, e, s, gg);var opg = _n("view");_r(opg, 'class', 7, e, s, gg);var oqg = _o(74, e, s, gg);_(opg,oqg);_(oog,opg);var org = _n("view");_r(org, 'class', 75, e, s, gg);var osg = _m( "switch", ["checked", -1,"bindchange", 76,"value", 1], e, s, gg);_(org,osg);_(oog,org);_(ogf,oog);_(off,ogf);var otg = _n("view");_r(otg, 'class', 78, e, s, gg);_(off,otg);var oug = _n("view");_r(oug, 'class', 79, e, s, gg);var ovg = _m( "button", ["bindtap", 80,"class", 1], e, s, gg);var owg = _o(82, e, s, gg);_(ovg,owg);_(oug,ovg);var oxg = _m( "button", ["bindtap", 83,"class", 1], e, s, gg);var oyg = _o(85, e, s, gg);_(oxg,oyg);_(oug,oxg);_(off,oug);_(oef,off);_(odf,oef);var ozg = _n("view");_r(ozg, 'class', 86, e, s, gg);_(odf,ozg);var o_g = _v();
      if (_o(87, e, s, gg)) {
        o_g.wxVkey = 1;var oAh = _n("view");_r(oAh, 'class', 88, e, s, gg);var oCh = _n("view");_r(oCh, 'class', 89, e, s, gg);var oDh = _n("text");_r(oDh, 'class', 90, e, s, gg);var oEh = _o(91, e, s, gg);_(oDh,oEh);_(oCh,oDh);_(oAh,oCh);_(o_g, oAh);
      } _(odf,o_g);_(r,odf);
    return r;
  };
        e_["./pages/order/setting.wxml"]={f:m0,j:[],i:[],ti:[],ic:[]};
if(path&&e_[path]){
window.__wxml_comp_version__=0.02
return function(env,dd,global){$gwxc=0;var root={"tag":"wx-page"};root.children=[]
var main=e_[path].f
if(typeof(window.__webview_engine_version__)!='undefined'&&window.__webview_engine_version__+1e-6>=0.02+1e-6&&window.__mergeData__)
{env=window.__mergeData__(env,dd);}
try{
main(env,{},root,global);
if(typeof(window.__webview_engine_version__)=='undefined'||window.__webview_engine_version__+1e-6<0.01+1e-6){return _ev(root);}}catch(err){console.log(err)}return root;}}}@code-separator-line:.msg{width:auto;max-width:80%;margin:0 auto;background-color:rgba(0,0,0,.7);color:#fff;display:inline-block;font-size:16px;border-radius:5px;box-shadow:0 0 8px rgba(0,0,0,.1);pointer-events:auto;-webkit-overflow-scrolling:touch;-webkit-animation-fill-mode:both;animation-fill-mode:both;-webkit-animation-duration:.2s;animation-duration:.2s;box-sizing:content-box;position:fixed;bottom:20%;z-index:999;text-align:center;padding:5px 10px}.layui-m-layercont{line-height:22px;text-align:center}.loadmsg{display:flex;justify-content:center;margin:0 auto;text-align:center}.tag{display:flex;justify-content:center}.tag wx-text{color:#fff;background:#58c4ff;border-radius:5%;box-shadow:0 0 1px #fff;float:left;margin-right:%%?15rpx?%%;padding:%%?5rpx?%% %%?10rpx?%%;font-size:%%?24rpx?%%}.body-view{width:300px}.fatitle{color:#ccc}.weui-check__label{width:35%;float:left}.weui-cell::before{border:none}.weui-cells::after{border:none}.checkbox{width:100%}@code-separator-line:__wxRoute = "pages/order/setting";__wxRouteBegin = true;
define("pages/order/setting.js", function(require, module, exports, window,document,frames,self,location,navigator,localStorage,history,Caches,screen,alert,confirm,prompt,fetch,XMLHttpRequest,WebSocket,webkit,WeixinJSCore,ServiceJSBridge,Reporter){
"use strict";function _interopRequireDefault(e){return e&&e.__esModule?e:{default:e}}function _asyncToGenerator(e){return function(){var t=e.apply(this,arguments);return new Promise(function(e,n){function r(a,i){try{var o=t[a](i),s=o.value}catch(e){return void n(e)}if(!o.done)return Promise.resolve(s).then(function(e){r("next",e)},function(e){r("throw",e)});e(s)}return r("next")})}}function _classCallCheck(e,t){if(!(e instanceof t))throw new TypeError("Cannot call a class as a function")}function _possibleConstructorReturn(e,t){if(!e)throw new ReferenceError("this hasn't been initialised - super() hasn't been called");return!t||"object"!=typeof t&&"function"!=typeof t?e:t}function _inherits(e,t){if("function"!=typeof t&&null!==t)throw new TypeError("Super expression must either be null or a function, not "+typeof t);e.prototype=Object.create(t&&t.prototype,{constructor:{value:e,enumerable:!1,writable:!0,configurable:!0}}),t&&(Object.setPrototypeOf?Object.setPrototypeOf(e,t):e.__proto__=t)}Object.defineProperty(exports,"__esModule",{value:!0});var _createClass=function(){function e(e,t){for(var n=0;n<t.length;n++){var r=t[n];r.enumerable=r.enumerable||!1,r.configurable=!0,"value"in r&&(r.writable=!0),Object.defineProperty(e,r.key,r)}}return function(t,n,r){return n&&e(t.prototype,n),r&&e(t,r),t}}(),_wepy=require("./../../npm/wepy/lib/wepy.js"),_wepy2=_interopRequireDefault(_wepy),_taglist=require("./../../base/taglist.js"),_taglist2=_interopRequireDefault(_taglist),_tip=require("./../../tools/tip.js"),_tip2=_interopRequireDefault(_tip),_request=require("./../../api/request.js"),_util=require("./../../tools/util.js"),_util2=_interopRequireDefault(_util),_msg=require("./../../base/tips/msg.js"),_msg2=_interopRequireDefault(_msg),orderSetting=function(e){function t(){var e,n,r,a;_classCallCheck(this,t);for(var i=arguments.length,o=Array(i),s=0;s<i;s++)o[s]=arguments[s];return n=r=_possibleConstructorReturn(this,(e=t.__proto__||Object.getPrototypeOf(t)).call.apply(e,[this].concat(o))),r.config={navigationBarTitleText:"王者荣耀 - 接单设置"},r.components={utag:_taglist2.default,msg:_msg2.default},r.watch={},r.data={index:0,gameName:"",checkboxItems:{},radioItems:[{name:"cell standard",value:"0",checked:!1},{name:"cell standard",value:"1",checked:!0}],form:{c_tag:"",c_money:5,c_level:10,c_index:"",c_tag_show:"",c_sex_show:"",c_sex:"",c_note:"风一样的美男子",c_gid:"",c_play:2,c_status:1},commData:{},gameInfo:{},packer:{item:{},ids:{}}},r.methods={c_status:function(e){this.form.c_status=1==e.detail.value?1:0},saveData:function(){this.saveAllData()},c_note:function(e){this.form.c_note=e.detail.value},goBack:function(){wx.navigateBack()},slider4change:function(e){this.form.c_money=e.detail.value},slider4level:function(e){this.form.c_level=e.detail.value},play4change:function(e){this.form.c_play=e.detail.value},radioChange:function(){},positionnSex:function(){var e=this,t=this.commData.sex;wx.showActionSheet({itemList:t,success:function(n){n.cancel||(e.form.c_sex_show=t[n.tapIndex],e.form.c_sex=t.indexOf(t[n.tapIndex]),e.$apply())}})},positionnSet:function(){var e=this,t=this.commData.zindex;wx.showActionSheet({itemList:t,success:function(n){n.cancel||(e.form.c_index=t[n.tapIndex],e.$apply())}})},bindPickerChange:function(e){this.form.c_tag_show=this.packer.item[e.detail.value],this.form.c_tag=this.packer.ids[e.detail.value],this.$apply()}},r.events={},a=n,_possibleConstructorReturn(r,a)}return _inherits(t,e),_createClass(t,[{key:"saveAllData",value:function(){function e(){return t.apply(this,arguments)}var t=_asyncToGenerator(regeneratorRuntime.mark(function e(){return regeneratorRuntime.wrap(function(e){for(;;)switch(e.prev=e.next){case 0:return e.next=2,(0,_request.requrst)("setting/save",this.form,function(e){_tip2.default.success(e.msg,3e3,function(){})});case 2:case"end":return e.stop()}},e,this)}));return e}()},{key:"choseTag",value:function(){var e=[],t=[];this.checkboxItems.forEach(function(n,r){e.push(n.tag_name),t.push(n.tag_id)}),this.packer.item=e,this.packer.ids=t,this.form.c_tag_show=e[0],this.$apply()}},{key:"updateData",value:function(){function e(){return t.apply(this,arguments)}var t=_asyncToGenerator(regeneratorRuntime.mark(function e(){var t;return regeneratorRuntime.wrap(function(e){for(;;)switch(e.prev=e.next){case 0:return t=this,e.next=3,(0,_request.requrst)("setting/getdata",{gid:this.form.c_gid},function(e){if(e.info.set){var n=e.info.set;e.info.user;t.form.c_sex=e.set_sex,t.form.c_sex_show=t.commData.sex[n.set_sex],t.form.c_note=n.set_note,t.form.c_money=n.set_monery,t.form.c_level=n.set_leavel,t.form.c_index=n.set_index,t.form.c_play=n.set_once,t.form.c_tag=n.set_tag;var r=t.packer.item[n.set_tag-1];t.form.c_tag_show=r}t.gameName=e.info.game.game_name,t.gameInfo=e.info.game,_util2.default.title(t.gameName),t.$apply()});case 3:case"end":return e.stop()}},e,this)}));return e}()},{key:"makeitem",value:function(){function e(e){return t.apply(this,arguments)}var t=_asyncToGenerator(regeneratorRuntime.mark(function e(t){var n,r;return regeneratorRuntime.wrap(function(e){for(;;)switch(e.prev=e.next){case 0:n=this.checkboxItems,r="",n.forEach(function(e,a,i){t.forEach(function(t,i,o){t.tag_name==e.tag_name&&(r+=e.tag_name+"、",n[a].checked=!0)})}),this.form.c_tag_show=r,this.checkboxItems=n,this.$apply();case 6:case"end":return e.stop()}},e,this)}));return e}()},{key:"getdata",value:function(){function e(){return t.apply(this,arguments)}var t=_asyncToGenerator(regeneratorRuntime.mark(function e(){var t=this;return regeneratorRuntime.wrap(function(e){for(;;)switch(e.prev=e.next){case 0:return e.next=2,(0,_request.requrst)("tag/tagList",{gid:this.form.c_gid},function(e){t.checkboxItems=e.info.tag,t.commData=e.info,t.updateData(),t.choseTag(),t.$apply()});case 2:case"end":return e.stop()}},e,this)}));return e}()},{key:"checkboxChange",value:function(){function e(e){return t.apply(this,arguments)}var t=_asyncToGenerator(regeneratorRuntime.mark(function e(t){var n,r,a,i,o,s,c,u,f,l;return regeneratorRuntime.wrap(function(e){for(;;)switch(e.prev=e.next){case 0:n=3,r=this.checkboxItems,a=t.detail.value,i=this,o=-1,s=0,c=r.length;case 5:if(!(s<c)){e.next=24;break}r[s].checked=!1,u=0,f=a.length;case 8:if(!(u<f)){e.next=21;break}if(r[s].tag_id!=a[u]){e.next=18;break}if(++o!=n){e.next=16;break}return this.$broadcast("layerMsg","最多选择"+n+"个标签"),e.abrupt("break",21);case 16:return r[s].checked=!0,e.abrupt("break",21);case 18:++u,e.next=8;break;case 21:++s,e.next=5;break;case 24:i.form.c_tag="",i.form.c_tag_show="";for(l in r)1==r[l].checked&&(i.form.c_tag_show+=r[l].tag_name+"、",i.form.c_tag+=r[l].tag_id+",");this.setData({checkboxItems:r});case 28:case"end":return e.stop()}},e,this)}));return e}()},{key:"onLoad",value:function(e){var t=this;this.form.c_gid=e.id,this.form.c_gid||_tip2.default.error("参数有误,请返回重试",800,function(){wx.navigateBack()}),t.getdata()}}]),t}(_wepy2.default.page);Page(require("./../../npm/wepy/lib/wepy.js").default.$createPage(orderSetting,"pages/order/setting"));
});require("pages/order/setting.js")@code-separator-line:["div","view","input","picker","checkbox-group","label","checkbox","icon","slider","textarea","switch","button","text"]