/*v0.6vv_20170214_fbi*/
window.__wcc_version__='v0.6vv_20170214_fbi'
window.__wxml_transpiler_version__='v0.1'
var $gwxc
var $gaic={}
$gwx=function(path,global){
function _(a,b){b&&a.children.push(b);}
function _v(k){if(typeof(k)!='undefined')return {tag:'virtual','wxKey':k,children:[]};return {tag:'virtual',children:[]};}
function _n(tag){$gwxc++;if($gwxc>=16000){throw 'Dom limit exceeded, please check if there\'s any mistake you\'ve made.'};return {tag:tag.substr(0,3)=='wx-'?tag:'wx-'+tag,attr:{},children:[],n:[]}}
function _p(a,b){b&&a.properities.push(b);}
function _s(scope,env,key){return typeof(scope[key])!='undefined'?scope[key]:env[key]}function _wl(tname,prefix){console.warn('WXMLRT:'+prefix+':-1:-1:-1: Template `' + tname + '` is being called recursively, will be stop.')}$gwn=console.warn;
$gwl=console.log;
function $gwh()
{
function x(){}
x.prototype =
{
hn: function( obj )
{
if( typeof(obj) == 'object' )
{
var cnt=0;
var any=false;
for(var x in obj)
{
any|=x==='__value__';
cnt++;
if(cnt>2)break;
}
return cnt == 2 && any && obj.hasOwnProperty('__wxspec__') ? "h" : "n";
}
return "n";
},
nh: function( obj, special )
{
return { __value__: obj, __wxspec__: special ? special : true }
},
rv: function( obj )
{
return this.hn(obj)==='n'?obj:this.rv(obj.__value__);
}
}
return new x;
}
wh=$gwh();
function $gwrt( should_pass_type_info )
{
function ArithmeticEv( ops, e, s, g, o )
{
var rop = ops[0][1];
var _a,_b,_c,_d, _aa, _bb;
switch( rop )
{
case '?:':
_a = rev( ops[1], e, s, g, o );
_c = should_pass_type_info && ( wh.hn(_a) === 'h' );
_d = wh.rv( _a ) ? rev( ops[2], e, s, g, o ) : rev( ops[3], e, s, g, o );
_d = _c && wh.hn( _d ) === 'n' ? wh.nh( _d, 'c' ) : _d;
return _d;
break;
case '&&':
_a = rev( ops[1], e, s, g, o );
_c = should_pass_type_info && ( wh.hn(_a) === 'h' );
_d = wh.rv( _a ) ? rev( ops[2], e, s, g, o ) : wh.rv( _a );
_d = _c && wh.hn( _d ) === 'n' ? wh.nh( _d, 'c' ) : _d;
return _d;
break;
case '||':
_a = rev( ops[1], e, s, g, o );
_c = should_pass_type_info && ( wh.hn(_a) === 'h' );
_d = wh.rv( _a ) ? wh.rv(_a) : rev( ops[2], e, s, g, o );
_d = _c && wh.hn( _d ) === 'n' ? wh.nh( _d, 'c' ) : _d;
return _d;
break;
case '+':
case '*':
case '/':
case '%':
case '|':
case '^':
case '&':
case '===':
case '==':
case '!=':
case '!==':
case '>=':
case '<=':
case '>':
case '<':
case '<<':
case '>>':
_a = rev( ops[1], e, s, g, o );
_b = rev( ops[2], e, s, g, o );
_c = should_pass_type_info && (wh.hn( _a ) === 'h' || wh.hn( _b ) === 'h');
switch( rop )
{
case '+':
_d = wh.rv( _a ) + wh.rv( _b );
break;
case '*':
_d = wh.rv( _a ) * wh.rv( _b );
break;
case '/':
_d = wh.rv( _a ) / wh.rv( _b );
break;
case '%':
_d = wh.rv( _a ) % wh.rv( _b );
break;
case '|':
_d = wh.rv( _a ) | wh.rv( _b );
break;
case '^':
_d = wh.rv( _a ) ^ wh.rv( _b );
break;
case '&':
_d = wh.rv( _a ) & wh.rv( _b );
break;
case '===':
_d = wh.rv( _a ) === wh.rv( _b );
break;
case '==':
_d = wh.rv( _a ) == wh.rv( _b );
break;
case '!=':
_d = wh.rv( _a ) != wh.rv( _b );
break;
case '!==':
_d = wh.rv( _a ) !== wh.rv( _b );
break;
case '>=':
_d = wh.rv( _a ) >= wh.rv( _b );
break;
case '<=':
_d = wh.rv( _a ) <= wh.rv( _b );
break;
case '>':
_d = wh.rv( _a ) > wh.rv( _b );
break;
case '<':
_d = wh.rv( _a ) < wh.rv( _b );
break;
case '<<':
_d = wh.rv( _a ) << wh.rv( _b );
break;
case '>>':
_d = wh.rv( _a ) >> wh.rv( _b );
break;
default:
break;
}
return _c ? wh.nh( _d, "c" ) : _d;
break;
case '-':
_a = ops.length === 3 ? rev( ops[1], e, s, g, o ) : 0;
_b = ops.length === 3 ? rev( ops[2], e, s, g, o ) : rev( ops[1], e, s, g, o );
_c = should_pass_type_info && (wh.hn( _a ) === 'h' || wh.hn( _b ) === 'h');
_d = _c ? wh.rv( _a ) - wh.rv( _b ) : _a - _b;
return _c ? wh.nh( _d, "c" ) : _d;
break;
case '!':
_a = rev( ops[1], e, s, g, o );
_c = should_pass_type_info && (wh.hn( _a ) == 'h');
_d = !wh.rv(_a);
return _c ? wh.nh( _d, "c" ) : _d;
case '~':
_a = rev( ops[1], e, s, g, o );
_c = should_pass_type_info && (wh.hn( _a ) == 'h');
_d = ~wh.rv(_a);
return _c ? wh.nh( _d, "c" ) : _d;
default:
$gwn('unrecognized op' + rop );
}
}
function rev( ops, e, s, g, o )
{
var op = ops[0];
if( typeof(op)==='object' )
{
var vop=op[0];
var _a, _aa, _b, _bb, _c, _d, _s, _e, _ta, _tb, _td;
switch(vop)
{
case 2:
return ArithmeticEv(ops,e,s,g,o);
break;
case 4:
return rev( ops[1], e, s, g, o );
break;
case 5:
switch( ops.length )
{
case 2:
return should_pass_type_info ?
[rev(ops[1],e,s,g,o)] :
[wh.rv(rev(ops[1],e,s,g,o))];
break;
case 1:
return [];
break;
default:
_a = rev( ops[1],e,s,g,o );
_a.push(
should_pass_type_info ?
rev( ops[2],e,s,g,o ) :
wh.rv( rev(ops[2],e,s,g,o) )
);
return _a;
break;
}
break;
case 6:
_a = rev(ops[1],e,s,g,o);
_ta = wh.hn(_a)==='h';
_aa = _ta ? wh.rv(_a) : _a;
o.is_affected |= _ta;
if( should_pass_type_info )
{
if( _aa===null || typeof(_aa) === 'undefined' )
{
return _ta ? wh.nh(undefined, 'e') : undefined;
}
_b = rev(ops[2],e,s,g,o);
_tb = wh.hn(_b) === 'h';
_bb = _tb ? wh.rv(_b) : _b;
o.is_affected |= _tb;
if( _bb===null || typeof(_bb) === 'undefined' )
{
return (_ta || _tb) ? wh.nh(undefined, 'e') : undefined;
}
_d = _aa[_bb];
_td = wh.hn(_d)==='h';
o.is_affected |= _td;
return (_ta || _tb) ? (_td ? _d : wh.nh(_d, 'e')) : _d;
}
else
{
if( _aa===null || typeof(_aa) === 'undefined' )
{
return undefined;
}
_b = rev(ops[2],e,s,g,o);
_tb = wh.hn(_b) === 'h';
_bb = _tb ? wh.rv(_b) : _b;
o.is_affected |= _tb;
if( _bb===null || typeof(_bb) === 'undefined' )
{
return undefined;
}
_d = _aa[_bb];
_td = wh.hn(_d)==='h';
o.is_affected |= _td;
return _td ? wh.rv(_d) : _d;
}
case 7:
switch(ops[1][0])
{
case 11:
o.is_affected |= wh.hn(g)==='h';
return g;
case 3:
_s = wh.rv( s );
_e = wh.rv( e );
_b = ops[1][1];
_a = _s && _s.hasOwnProperty(_b) ?
s : _e && ( _e.hasOwnProperty(_b) ? e : undefined );
if( should_pass_type_info )
{
if( _a )
{
_ta = wh.hn(_a) === 'h';
_aa = _ta ? wh.rv( _a ) : _a;
_d = _aa[_b];
_td = wh.hn(_d) === 'h';
o.is_affected |= _ta || _td;
_d = _ta && !_td ? wh.nh(_d,'e') : _d;
return _d;
}
}
else
{
if( _a )
{
_ta = wh.hn(_a) === 'h';
_aa = _ta ? wh.rv( _a ) : _a;
_d = _aa[_b];
_td = wh.hn(_d) === 'h';
o.is_affected |= _ta || _td;
return wh.rv(_d);
}
}
return undefined;
}
break;
case 8:
_a = {};
_a[ops[1]] = rev(ops[2],e,s,g,o);
return _a;
break;
case 9:
_a = rev(ops[1],e,s,g,o);
_b = rev(ops[2],e,s,g,o);
function merge( _a, _b, _ow )
{
_ta = wh.hn(_a)==='h';
_tb = wh.hn(_b)==='h';
_aa = wh.rv(_a);
_bb = wh.rv(_b);
if( should_pass_type_info )
{
if( _tb )
{
for(var k in _bb)
{
if ( _ow || !_aa.hasOwnProperty(k) )
_aa[k]=wh.nh(_bb[k],'e');
}
}
else
{
for(var k in _bb)
{
if ( _ow || !_aa.hasOwnProperty(k) )
_aa[k]=_bb[k];
}
}
}
else
{
for(var k in _bb)
{
if ( _ow || _aa.hasOwnProperty(k) )
_aa[k]=wh.rv(_bb[k]);
}
}
return _a;
}
var _c = _a
var _ow = true
if ( typeof(ops[1][0]) === "object" && ops[1][0][0] === 10 ) {
_a = _b
_b = _c
_ow = false
}
if ( typeof(ops[1][0]) === "object" && ops[1][0][0] === 10 ) {
var _r = {}
return merge( merge( _r, _a, _ow ), _b, _ow );
}
else
return merge( _a, _b, _ow );
break;
case 10:
return should_pass_type_info ? rev(ops[1],e,s,g,o) : wh.rv(rev(ops[1],e,s,g,o));
}
}
else
{
if( op === 3 || op === 1 ) return ops[1];
else if( op === 11 )
{
var _a='';
for( var i = 1 ; i < ops.length ; i++ )
{
var xp = wh.rv(rev(ops[i],e,s,g,o));
_a += typeof(xp) === 'undefined' ? '' : xp;
}
return _a;
}
}
}
return rev;
}
gra=$gwrt(true);
grb=$gwrt(false);
function TestTest( expr, ops, e,s,g, expect_a, expect_b, expect_affected )
{
{
var o = {is_affected:false};
var a = gra( ops, e,s,g, o );
if( JSON.stringify(a) != JSON.stringify( expect_a )
|| o.is_affected != expect_affected )
{
console.warn( "A. " + expr + " get result " + JSON.stringify(a) + ", " + o.is_affected + ", but " + JSON.stringify( expect_a ) + ", " + expect_affected + " is expected" );
}
}
{
var o = {is_affected:false};
var a = grb( ops, e,s,g, o );
if( JSON.stringify(a) != JSON.stringify( expect_b )
|| o.is_affected != expect_affected )
{
console.warn( "B. " + expr + " get result " + JSON.stringify(a) + ", " + o.is_affected + ", but " + JSON.stringify( expect_b ) + ", " + expect_affected + " is expected" );
}
}
}

function wfor( to_iter, func, env, _s, global, father, itemname, indexname, keyname )
{
var _n = wh.hn( to_iter ) === 'n';
var scope = wh.rv( _s );
var has_old_item = scope.hasOwnProperty(itemname);
var has_old_index = scope.hasOwnProperty(indexname);
var old_item = scope[itemname];
var old_index = scope[indexname];
var full = Object.prototype.toString.call(wh.rv(to_iter));
var type = full[8];
if( type === 'N' && full[10] === 'l' ) type = 'X';
var _y;
if( _n )
{
if( type === 'A' )
{
for( var i = 0 ; i < to_iter.length ; i++ )
{
scope[itemname] = to_iter[i];
scope[indexname] = wh.nh(i, 'h');
_y = keyname ? (keyname==="*this" ? _v(wh.rv(to_iter[i])) : _v(wh.rv(wh.rv(to_iter[i])[keyname]))) : _v();
_(father,_y);
func( env, scope, _y, global );
}
}
else if( type === 'O' )
{
for( var k in to_iter )
{
scope[itemname] = to_iter[k];
scope[indexname] = wh.nh(k, 'h');
_y = keyname ? (keyname==="*this" ? _v(wh.rv(to_iter[k])) : _v(wh.rv(wh.rv(to_iter[k])[keyname]))) : _v();
_(father,_y);
func( env,scope,_y,global );
}
}
else if( type === 'S' )
{
for( var i = 0 ; i < to_iter.length ; i++ )
{
scope[itemname] = to_iter[i];
scope[indexname] = wh.nh(i, 'h');
_y = _v( to_iter[i] + i );
_(father,_y);
func( env, scope, _y, global );
}
}
else if( type === 'N' )
{
for( var i = 0 ; i < to_iter ; i++ )
{
scope[itemname] = i;
scope[indexname] = wh.nh(i, 'h');
_y = _v( i );
_(father,_y);
func(env,scope,_y,global);
}
}
else
{
}
}
else
{
var r_to_iter = wh.rv(to_iter);
var r_iter_item, iter_item;
if( type === 'A' )
{
for( var i = 0 ; i < r_to_iter.length ; i++ )
{
iter_item = r_to_iter[i];
iter_item = wh.hn(iter_item)==='n' ? wh.nh(iter_item,'h') : iter_item;
r_iter_item = wh.rv( iter_item );
scope[itemname] = iter_item
scope[indexname] = wh.nh(i, 'h');
_y = keyname ? (keyname==="*this" ? _v(r_iter_item) : _v(wh.rv(r_iter_item[keyname]))) : _v();
_(father,_y);
func( env, scope, _y, global );
}
}
else if( type === 'O' )
{
for( var k in r_to_iter )
{
iter_item = r_to_iter[k];
iter_item = wh.hn(iter_item)==='n'? wh.nh(iter_item,'h') : iter_item;
r_iter_item = wh.rv( iter_item );
scope[itemname] = iter_item;
scope[indexname] = wh.nh(k, 'h');
_y = keyname ? (keyname==="*this" ? _v(r_iter_item) : _v(wh.rv(r_iter_item[keyname]))) : _v();
_(father,_y);
func( env, scope, _y, global );
}
}
else if( type === 'S' )
{
for( var i = 0 ; i < r_to_iter.length ; i++ )
{
scope[itemname] = wh.nh(r_to_iter[i],'h');
scope[indexname] = wh.nh(i, 'h');
_y = _v( to_iter[i] + i );
_(father,_y);
func( env, scope, _y, global );
}
}
else if( type === 'N' )
{
for( var i = 0 ; i < r_to_iter ; i++ )
{
scope[itemname] = wh.nh(i,'h');
scope[indexname]= wh.nh(i,'h');
_y = _v( i );
_(father,_y);
func(env,scope,_y,global);
}
}
else
{
}
}
if(has_old_item)
{
scope[itemname]=old_item;
}
else
{
delete scope[itemname];
}
if(has_old_index)
{
scope[indexname]=old_index;
}
else{delete scope[indexname];}}


function _r( node, attrname, opindex, env, scope, global )
{
var o = {};
var a = grb( z[opindex], env, scope, global, o );
node.attr[attrname] = a;
if( o.is_affected ) node.n.push( attrname );
}
function _o( opindex, env, scope, global )
{
var nothing = {};
return grb( z[opindex], env, scope, global, nothing );
}
function _1( opindex, env, scope, global )
{
var nothing = {};
return gra( z[opindex], env, scope, global, nothing );
}
function _2( opindex, func, env, scope, global, father, itemname, indexname, keyname )
{
var to_iter = _1( opindex, env, scope, global, father, itemname, indexname, keyname );
wfor( to_iter, func, env, scope, global, father, itemname, indexname, keyname );
}
function _gv( )
{
if( typeof(window.__webview_engine_version__) == 'undefined' ) return 0.0;
return window.__webview_engine_version__;
}
function _m(tag,attrs,env,scope,global)
{
var tmp=_n(tag);
var base=0;
for(var i = 0 ; i < attrs.length ; i+=2 )
{
if(attrs[i+1]<0)
{tmp.attr[attrs[i]]=true;}else{_r(tmp,attrs[i],base+attrs[i+1],env,scope,global);
if(base===0)base=attrs[i+1];}}return tmp;};function _ai(i,p,e,me,r,c){var x=_grp(p,e,me);if(x)i.push(x);else{i.push('');console.warn('WXMLRT:'+me+':import:'+r+':'+c+': Path `'+p+'` not found from `'+me+'`.')}}function _grp(p,e,me){if(p[0]!='/'){var mepart=me.split('/');mepart.pop();var ppart=p.split('/');for(var i=0;i<ppart.length;i++){if(ppart[i]=='..')mepart.pop();else if(ppart[i]=='.' || !ppart[i])continue;else mepart.push(ppart[i]);}p=mepart.join('/');}if(me[0]=='.'&&p[0]=='/')p='.'+p;if(e[p])return p;if(e[p+'.wxml'])return p+'.wxml';}function _gd(p,c,e,d){if(!c)return;if(d[p][c])return d[p][c];for(var x=e[p].i.length-1;x>=0;x--){if(e[p].i[x]&&d[e[p].i[x]][c])return d[e[p].i[x]][c]};for(var x=e[p].ti.length-1;x>=0;x--){var q=_grp(e[p].ti[x],e,p);if(q&&d[q][c])return d[q][c]}
var ii=_gapi(e,p);for(var x=0;x<ii.length;x++){if(ii[x]&&d[ii[x]][c])return d[ii[x]][c]}for(var k=e[p].j.length-1;k>=0;k--)if(e[p].j[k]){for(var q=e[e[p].j[k]].ti.length-1;q>=0;q--){var pp=_grp(e[e[p].j[k]].ti[q],e,p);if(pp&&d[pp][c]){return d[pp][c]}}}}function _gapi(e,p){if(!p)return [];if($gaic[p]){return $gaic[p]};var ret=[],q=[],h=0,t=0,put={},visited={};q.push(p);visited[p]=true;t++;while(h<t){var a=q[h++];for(var i=0;i<e[a].ic.length;i++){var nd=e[a].ic[i];var np=_grp(nd,e,a);if(np&&!visited[np]){visited[np]=true;q.push(np);t++;}}for(var i=0;a!=p&&i<e[a].ti.length;i++){var ni=e[a].ti[i];var nm=_grp(ni,e,a);if(nm&&!put[nm]){put[nm]=true;ret.push(nm);}}}$gaic[p]=ret;return ret;}var $ixc={};function _ic(p,ent,me,e,s,r,gg){var x=_grp(p,ent,me);ent[me].j.push(x);if(x){if($ixc[x]){console.warn('WXMLRT:-1:include:-1:-1: `'+p+'` is being included in a loop, will be stop.');return;}$ixc[x]=true;try{ent[x].f(e,s,r,gg)}catch(e){}$ixc[x]=false;}else{console.warn('WXMLRT:'+me+':include:-1:-1: Included path `'+p+'` not found from `'+me+'`.')}}function _w(tn,f,line,c){console.warn('WXMLRT:'+f+':template:'+line+':'+c+': Template `'+tn+'` not found.');}function _ev(dom){var changed=false;delete dom.properities;delete dom.n;if(dom.children){do{changed=false;var newch = [];for(var i=0;i<dom.children.length;i++){var ch=dom.children[i];if( ch.tag=='virtual'){changed=true;for(var j=0;ch.children&&j<ch.children.length;j++){newch.push(ch.children[j]);}}else { newch.push(ch); } } dom.children = newch; }while(changed);for(var i=0;i<dom.children.length;i++){_ev(dom.children[i]);}} return dom; }var e_={}
if(global&&typeof(global.entrys)=='object')e_=global.entrys
var d_={}
if(global&&typeof(global.defines)=='object')d_=global.defines
var p_={}
var z = [];
  (function(z){
    var a = 11;
    function Z(ops){z.push(ops)};
    Z([3, 'tolower']);Z([3, 'view']);Z([3, '2']);Z([3, 'true']);Z([a, [3, 'height:'],[[7],[3, "windowHeight"]],[3, 'px']]);Z([3, 'ArticleList']);Z([3, 'bbs-list']);Z([[7],[3, "list"]]);Z([3, 'key']);Z([3, 'gotoContent']);Z([3, 'li']);Z([[6],[[7],[3, "item"]],[3, "article_id"]]);Z([3, 'user-area']);Z([3, 'left-img']);Z([[6],[[7],[3, "item"]],[3, "user_avatar"]]);Z([3, 'gender man']);Z([3, 'fa fa-mars']);Z([3, 'user-name diplay-inline-block']);Z([a, [3, '\r\n                                '],[[6],[[7],[3, "item"]],[3, "user_nickname"]]]);Z([3, 'level']);Z([3, 'background: #d48edc']);Z([3, '官方人员']);Z([3, 'bbs-time aui-pull-right']);Z([3, 'span']);Z([a, [[6],[[7],[3, "item"]],[3, "times"]]]);Z([3, 'article-area']);Z([3, 'article-info']);Z([3, 'aui-ellipsis-1 bbs-title']);Z([3, 'ico-zhan']);Z([3, '推荐']);Z([a, [[6],[[7],[3, "item"]],[3, "art_title"]],[3, '\r\n                                ']]);Z([3, 'bbs-des aui-ellipsis-2']);Z([a, [3, '\r\n                                    '],[[6],[[7],[3, "item"]],[3, "art_desc"]],[3, '\r\n                                ']]);Z([3, 'ico-s']);Z([3, 'font']);Z([3, '技术分享']);Z([3, 'ims-img']);Z([3, '1图']);Z([3, 'article-img']);Z([1, true]);Z([[2, "||"],[[6],[[7],[3, "item"]],[3, "art_thumb"]],[1, "http://img.he29.com/play/f5853478b79ba70ead785f50f40d65d32a765717.jpeg"]]);Z([3, 'mt20c']);Z([3, 'loadingMore']);Z([[2, "!="], [[7],[3, "$more$flag"]], [1, 0]]);Z([[2, "=="], [[7],[3, "$more$flag"]], [1, 1]]);Z([3, 'weui-loadmore']);Z([3, 'weui-loading']);Z([3, 'weui-loadmore__tips']);Z([3, '正在加载']);Z([[2, "<"], [[7],[3, "$more$flag"]], [1, 0]]);Z([3, 'weui-loadmore weui-loadmore_line']);Z([3, 'weui-loadmore__tips weui-loadmore__tips_in-line']);Z([3, '加载完成']);Z([[2, "=="], [[7],[3, "$more$flag"]], [1, 2]]);Z([3, '滑动加载更多']);
  })(z);d_["./pages/article/list.wxml"] = {};
  var m0=function(e,s,r,gg){
    var oyh = _m( "scroll-view", ["bindscrolltolower", 0,"class", 1,"lowerThreshold", 1,"scrollY", 2,"style", 3], e, s, gg);var ozh = _n("view");_r(ozh, 'class', 5, e, s, gg);var o_h = _n("view");_r(o_h, 'id', 6, e, s, gg);var oAi = _v();var oBi = function(oFi,oEi,oDi,gg){var oCi = _n("view");var oHi = _m( "view", ["bindtap", 9,"class", 1,"data-id", 2], oFi, oEi, gg);var oIi = _n("view");_r(oIi, 'class', 12, oFi, oEi, gg);var oJi = _n("view");_r(oJi, 'class', 13, oFi, oEi, gg);var oKi = _m( "image", ["data-echo", 14,"src", 0], oFi, oEi, gg);_(oJi,oKi);var oLi = _n("view");_r(oLi, 'class', 15, oFi, oEi, gg);var oMi = _m( "text", ["ariaHidden", 3,"class", 13], oFi, oEi, gg);_(oLi,oMi);_(oJi,oLi);_(oIi,oJi);var oNi = _n("view");_r(oNi, 'class', 17, oFi, oEi, gg);var oOi = _o(18, oFi, oEi, gg);_(oNi,oOi);var oPi = _m( "label", ["class", 19,"style", 1], oFi, oEi, gg);var oQi = _o(21, oFi, oEi, gg);_(oPi,oQi);_(oNi,oPi);_(oIi,oNi);var oRi = _n("view");_r(oRi, 'class', 22, oFi, oEi, gg);var oSi = _n("text");_r(oSi, 'class', 23, oFi, oEi, gg);var oTi = _o(24, oFi, oEi, gg);_(oSi,oTi);_(oRi,oSi);_(oIi,oRi);_(oHi,oIi);var oUi = _n("view");_r(oUi, 'class', 25, oFi, oEi, gg);var oVi = _n("view");_r(oVi, 'class', 26, oFi, oEi, gg);var oWi = _n("view");_r(oWi, 'class', 27, oFi, oEi, gg);var oXi = _n("text");_r(oXi, 'class', 28, oFi, oEi, gg);var oYi = _o(29, oFi, oEi, gg);_(oXi,oYi);_(oWi,oXi);var oZi = _o(30, oFi, oEi, gg);_(oWi,oZi);_(oVi,oWi);var oai = _n("view");_r(oai, 'class', 31, oFi, oEi, gg);var obi = _o(32, oFi, oEi, gg);_(oai,obi);_(oVi,oai);var oci = _n("view");_r(oci, 'class', 33, oFi, oEi, gg);var odi = _n("text");_r(odi, 'class', 34, oFi, oEi, gg);var oei = _o(35, oFi, oEi, gg);_(odi,oei);_(oci,odi);_(oVi,oci);_(oUi,oVi);var ofi = _n("view");_r(ofi, 'class', 36, oFi, oEi, gg);var ogi = _n("text");var ohi = _o(37, oFi, oEi, gg);_(ogi,ohi);_(ofi,ogi);var oii = _m( "image", ["class", 38,"lazyLoad", 1,"src", 2], oFi, oEi, gg);_(ofi,oii);_(oUi,ofi);_(oHi,oUi);_(oCi,oHi);var oji = _n("view");_r(oji, 'class', 41, oFi, oEi, gg);_(oCi,oji);_(oDi, oCi);return oDi;};_2(7, oBi, e, s, gg, oAi, "item", "index", 'key');_(o_h,oAi);_(ozh,o_h);_(oyh,ozh);var oki = _n("view");_r(oki, 'class', 42, e, s, gg);var oli = _v();
      if (_o(43, e, s, gg)) {
        oli.wxVkey = 1;var omi = _n("view");_r(omi, 'class', 42, e, s, gg);var ooi = _v();
      if (_o(44, e, s, gg)) {
        ooi.wxVkey = 1;var opi = _n("view");_r(opi, 'class', 45, e, s, gg);var ori = _n("view");_r(ori, 'class', 46, e, s, gg);_(opi,ori);var osi = _n("view");_r(osi, 'class', 47, e, s, gg);var oti = _o(48, e, s, gg);_(osi,oti);_(opi,osi);_(ooi, opi);
      } _(omi,ooi);var oui = _v();
      if (_o(49, e, s, gg)) {
        oui.wxVkey = 1;var ovi = _n("view");_r(ovi, 'class', 50, e, s, gg);var oxi = _n("view");_r(oxi, 'class', 51, e, s, gg);var oyi = _o(52, e, s, gg);_(oxi,oyi);_(ovi,oxi);_(oui, ovi);
      } _(omi,oui);var ozi = _v();
      if (_o(53, e, s, gg)) {
        ozi.wxVkey = 1;var o_i = _n("view");_r(o_i, 'class', 50, e, s, gg);var oBj = _n("view");_r(oBj, 'class', 51, e, s, gg);var oCj = _o(54, e, s, gg);_(oBj,oCj);_(o_i,oBj);_(ozi, o_i);
      } _(omi,ozi);_(oli, omi);
      } _(oki,oli);_(oyh,oki);_(r,oyh);
    return r;
  };
        e_["./pages/article/list.wxml"]={f:m0,j:[],i:[],ti:[],ic:[]};
if(path&&e_[path]){
window.__wxml_comp_version__=0.02
return function(env,dd,global){$gwxc=0;var root={"tag":"wx-page"};root.children=[]
var main=e_[path].f
if(typeof(window.__webview_engine_version__)!='undefined'&&window.__webview_engine_version__+1e-6>=0.02+1e-6&&window.__mergeData__)
{env=window.__mergeData__(env,dd);}
try{
main(env,{},root,global);
if(typeof(window.__webview_engine_version__)=='undefined'||window.__webview_engine_version__+1e-6<0.01+1e-6){return _ev(root);}}catch(err){console.log(err)}return root;}}}@code-separator-line:.loadingMore{clear:both;display:block}wx-scroll-view{background:#fff}.push-fixed,.refresh-fixed{width:20px;height:20px;border-radius:50%;-webkit-border-radius:50%;background:rgba(62,154,255,.43);position:fixed;bottom:10px;right:10px;z-index:10000}.refresh-fixed{bottom:60px}.winu-center-all{display:-webkit-box;-webkit-box-orient:horizontal;-webkit-box-pack:center;-webkit-box-align:center}.push-fixed wx-i.iconfont,.refresh-fixed wx-i.iconfont{color:#fff;font-size:20px}wx-label{font-size:%%?24rpx?%%}#bbs-list .li{clear:both;position:relative;border-bottom:1px solid #d7d7d7;background:#fff;padding:10px 8px 10px 8px;min-height:0;height:auto;overflow:hidden}.user-area{margin-bottom:%%?10rpx?%%;padding-bottom:%%?10rpx?%%;border-bottom:1px solid #f1f1f1}.user-area wx-image{height:40px;width:40px;border-radius:50%;-webkit-border-radius:50%}.left-img{display:inline-block;height:40px;width:40px;position:relative;vertical-align:middle;overflow:hidden}.left-img .gender{display:block;height:15px;width:15px;border-radius:50%;-webkit-border-radius:50%;color:#fff;position:absolute;right:0;bottom:0;text-align:center}.left-img .gender wx-i{font-size:10px;position:relative;top:-6px}.left-img .gender.man{background:#1d9dd5}.left-img .gender.max{background:#f54171}.diplay-inline-block{display:inline-block;height:40px;vertical-align:middle;line-height:40px;margin-left:8px}.user-name{font-size:15px}.bbs-time{height:40px;vertical-align:middle}.bbs-time .span{color:#999;line-height:50px;font-size:12px}.level{display:inline-block;vertical-align:middle;font-size:9px;color:#fff;background:#1d9dd5;height:15px;line-height:16px;padding:0 4px;border-radius:2px;-webkit-border-radius:2px;margin-left:10px}.article-area{margin-top:10px;display:-webkit-box;display:-webkit-flex;display:flex;-webkit-box-orient:horizontal;-webkit-flex-flow:row;flex-flow:row;width:100%}.article-info{-webkit-box-flex:1;-webkit-flex:1;flex:1;position:relative}.article-info wx-p{color:#333;font-size:15px}.article-info wx-span{padding-top:7px;display:block;font-size:9px;color:#999}.ims-img{width:100px;height:100px;margin-left:8px;position:relative}.ims-img wx-text{position:absolute;right:0;bottom:0;background:rgba(0,0,0,.5);color:#fff;display:block;width:30px;font-size:10px;text-align:center;height:15px;line-height:15px}.article-img{width:100px;height:100px}.ico-zhan{display:inline-block;font-size:10px;color:#fff;background:#999;padding:1px 3px;vertical-align:middle;margin-right:8px}.ico-zhan.hot{background:#fdba7d}.aui-ellipsis-1,.aui-ellipsis-2{padding-right:0!important}.bbs-title{font-size:16px}.bbs-des{padding-top:10px;font-size:12px;color:#999;height:48px;line-height:22px;overflow:hidden;text-overflow:ellipsis;white-space:inherit}.ico-s{width:100%}.ico-s wx-label{margin-right:8px}.ico-s wx-label wx-i{vertical-align:bottom}.ico-s .font{float:right;display:inline-block;padding:2px 3px 2px 3px;border:1px solid #ccc;border-radius:2px;-webkit-border-radius:2px;line-height:normal;margin-top:2px;font-size:9px}#bbs-list wx-li:active{background:#f4f4f4}.aui-pull-right{float:right!important}@code-separator-line:__wxRoute = "pages/article/list";__wxRouteBegin = true;
define("pages/article/list.js", function(require, module, exports, window,document,frames,self,location,navigator,localStorage,history,Caches,screen,alert,confirm,prompt,fetch,XMLHttpRequest,WebSocket,webkit,WeixinJSCore,ServiceJSBridge,Reporter){
"use strict";function _interopRequireDefault(e){return e&&e.__esModule?e:{default:e}}function _asyncToGenerator(e){return function(){var t=e.apply(this,arguments);return new Promise(function(e,r){function n(o,i){try{var a=t[o](i),u=a.value}catch(e){return void r(e)}if(!a.done)return Promise.resolve(u).then(function(e){n("next",e)},function(e){n("throw",e)});e(u)}return n("next")})}}function _classCallCheck(e,t){if(!(e instanceof t))throw new TypeError("Cannot call a class as a function")}function _possibleConstructorReturn(e,t){if(!e)throw new ReferenceError("this hasn't been initialised - super() hasn't been called");return!t||"object"!=typeof t&&"function"!=typeof t?e:t}function _inherits(e,t){if("function"!=typeof t&&null!==t)throw new TypeError("Super expression must either be null or a function, not "+typeof t);e.prototype=Object.create(t&&t.prototype,{constructor:{value:e,enumerable:!1,writable:!0,configurable:!0}}),t&&(Object.setPrototypeOf?Object.setPrototypeOf(e,t):e.__proto__=t)}Object.defineProperty(exports,"__esModule",{value:!0});var _createClass=function(){function e(e,t){for(var r=0;r<t.length;r++){var n=t[r];n.enumerable=n.enumerable||!1,n.configurable=!0,"value"in n&&(n.writable=!0),Object.defineProperty(e,n.key,n)}}return function(t,r,n){return r&&e(t.prototype,r),n&&e(t,n),t}}(),_wepy=require("./../../npm/wepy/lib/wepy.js"),_wepy2=_interopRequireDefault(_wepy),_util=require("./../../tools/util.js"),_request=require("./../../api/request.js"),_loadMore=require("./../../base/tips/loadMore.js"),_loadMore2=_interopRequireDefault(_loadMore),ArticleList=function(e){function t(){var e,r,n,o;_classCallCheck(this,t);for(var i=arguments.length,a=Array(i),u=0;u<i;u++)a[u]=arguments[u];return r=n=_possibleConstructorReturn(this,(e=t.__proto__||Object.getPrototypeOf(t)).call.apply(e,[this].concat(a))),n.config={navigationBarTitleText:"文章列表"},n.components={more:_loadMore2.default},n.data={list:[],flag:0,windowHeight:"",page:1},n.watch={flag:function(){this.$broadcast("LoadingMore",this.flag)}},n.computed={},n.methods={gotoContent:function(e){var t=(0,_util.get)(e,"id");(0,_util.goto)("content?id="+t)},tolower:function(){this.getHttpData()}},n.events={},o=r,_possibleConstructorReturn(n,o)}return _inherits(t,e),_createClass(t,[{key:"getHttpData",value:function(){function e(){return t.apply(this,arguments)}var t=_asyncToGenerator(regeneratorRuntime.mark(function e(){var t=this;return regeneratorRuntime.wrap(function(e){for(;;)switch(e.prev=e.next){case 0:if(!(this.flag<0)){e.next=2;break}return e.abrupt("return",!1);case 2:(0,_request.requrst)("article/listd",{page:this.page++,cate:0,limit:12},function(e){e.code>0&&(e.info.data.length>0?(t.flag=2,t.list=(0,_util.AddData)(t.list,t.makeData(e.info.data))):t.flag=-2,t.$apply())});case 3:case"end":return e.stop()}},e,this)}));return e}()},{key:"makeData",value:function(e){return e.forEach(function(t,r){e[r].times=(0,_util.getDateDiff)(t.art_time,!0)}),e}},{key:"onLoad",value:function(){this.getHttpData(),this.windowHeight=(0,_util.sysInfo)("windowHeight")}}]),t}(_wepy2.default.page);Page(require("./../../npm/wepy/lib/wepy.js").default.$createPage(ArticleList,"pages/article/list"));
});require("pages/article/list.js")@code-separator-line:["div","scroll-view","view","image","text","label"]