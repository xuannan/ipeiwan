<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

// [ 应用入口文件 ]

// 定义应用目录
define('APP_PATH', __DIR__ . '/../application/');
define('SYS_ROOT',dirname(__DIR__));
define('DOC_ROOT',$_SERVER['DOCUMENT_ROOT']);
define('CHATDB','mysql://chatdb:rGXOOcrBpYSgG4k@127.0.0.1:3306/chatdb#utf8');
define('FILEBASE',SYS_ROOT.'/config/database');
require_once SYS_ROOT .'/config/function.php';

spl_autoload_register(function ($class) {
    try{
        $file = dirname(__DIR__) .'/'. $class . '.php';
        include $file;
    }catch(\Exception $e){}
});
// 加载框架引导文件
require __DIR__ . '/../thinkphp/start.php';
