<?php
/**
 * Created by PhpStorm.
 * User: iyahe@qq.com (天明)
 * Date: 2017/12/7
 * Time: 10:57
 */
$footer = [
    'list'=> [
        [
        'name'=>'首页',
        'icon'=>'http://img.he29.com/play/aace1c5a2f4e061eb12be75c8ac65e3bd4c58508.png',
        'current'=>1,
        'curricon'=>'http://img.he29.com/play/47dea6d2bb31901479176b74e439a516117822e1.png',
        'url'=>'index/index'
        ],
        [
            'name'=>'找个伙伴',
            'icon'=>'http://img.he29.com/play/80ece3c9d04c0e22bd54239c163c0ce76aac4945.png',
            'current'=>0,
            'curricon'=>'http://img.he29.com/play/c9bb47bbec6d278a1416183f51ddcdfac8967f05.png',
            'url'=>'game/myplay'
        ],
        [
            'name'=>'我的预约',
            'icon'=>'http://img.he29.com/play/3bedc842ef2158cde58805c8d1ec36e8f3741c38.png',
            'current'=>0,
            'curricon'=>'http://img.he29.com/play/434a3c0e7d6d276842396e67988fa21e16068cec.png',
            'url'=>'game/play'
        ],
        [
            'name'=>'个人中心',
            'icon'=>'http://img.he29.com/play/00a7881090306864fa4bf202af072aedd8cec15f.png',
            'curricon'=>'http://img.he29.com/play/fce84f940bcb28e5bea1abe7b7414217b99f57cc.png',
            'current'=>0,
            'url'=>'user/index'
        ]],
    'current'=>'#1296db',
    'defaultColor'=>'#666'
];
$btnIcon = [
    [
        'name'=>'签到',
        'icon'=>'http://img.he29.com/play/167f90525f9271c9924e2467b8e9b00ecde7c335.png',
        'current'=>1,
        'url'=>'index/sign'
    ],
    [
        'name'=>'接单监听',
        'icon'=>'http://img.he29.com/play/8b56b121be484ec169366fd0d1d2bf329765124b.png',
        'current'=>0,
        'url'=>'home/away?id=1'
    ],
    [
        'name'=>'问题反馈',
        'icon'=>'http://img.he29.com/play/ce122e9a90d7d5233301baed84a950aba1f8099f.png',
        'current'=>0,
        'url'=>'chat/index?uid=1'
    ],
];
return [
    'footer'=>$footer,
    'btnIcon'=>$btnIcon
];