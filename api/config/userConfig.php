<?php
/**
 * Created by PhpStorm.
 * User: iyahe@qq.com (天明)
 * Date: 2017/12/14
 * Time: 13:49
 */

$nav = array(
    [
        'id' => 1,
        'icon' => 'http://img.he29.com/play/533143a646201e583e446e19f2917c546d560c6c.png',
        'name' => '主页',
        'url' => '/pages/home/index',
        'disable' => 0
    ],
    [
        'id' => 2,
        'icon' => 'http://img.he29.com/play/26fa6e55901f07c917c471f941d8bc26800b029b.png',
        'name' => '积分',
        'url' => '/pages/money/index',
        'disable' => 0
    ], [
        'id' =>3,
        'icon' => 'http://img.he29.com/play/5aacc8aea963ddb886eb43623b16078716453718.png',
        'name' => '优惠券',
        'url' => '/pages/money/coupon',
        'disable' => 0
    ], [
        'id' => 4,
        'icon' => 'http://img.he29.com/play/e63061c62293bf112132c6bfcc1d131079a6840e.jpeg',
        'name' => '我的游戏',
        'url' => '/pages/game/mygame',
        'disable' => 0
    ], [
        'id' => 5,
        'icon' => 'http://img.he29.com/play/9b919e8db197563fa8c2bc4f7b3a0c8ae8454561.png',
        'name' => '匹配',
        'url' => '/pages/index?tab=2',
        'disable' => 0
    ], [
        'id' => 6,
        'icon' => 'http://img.he29.com/play/0eb0c1b597ce7a0bf96d1910c9816f895311287e.png',
        'name' => '广场',
        'url' => '/pages/index?tab=2',
        'disable' => 0
    ], [
        'id' => 7,
        'icon' => 'http://img.he29.com/play/6e1afe9c3e4fe5f1964a3f607849b6c736d92626.png',
        'name' => '大厅',
        'url' => '/pages/index?tab=2',
        'disable' => 0
    ], [
        'id' => 8,
        'icon' => 'http://img.he29.com/play/2b4bb31ae3fa309bab3b5421e88ed7ec65c15688.jpeg',
        'name' => '签到',
        'url' => '/pages/index/sign',
        'disable' => 0
    ], [
        'id' => 9,
        'icon' => 'http://img.he29.com/play/4c15d00df1110e5521dcdcea7b689808a69fa736.jpeg',
        'name' => '豌豆荚',
        'url' => '/pages/money/index',
        'disable' => 0
    ], [
        'id' => 10,
        'icon' => 'http://img.he29.com/play/ddc1c4d310bbd78e9db526b39dd7622fc52c5798.jpeg',
        'name' => '收藏',
        'url' => '/pages/index/fav',
        'disable' => 0
    ]
);
$fot = array(
    [
        'id' => 1,
        'icon' => 'http://img.he29.com/play/aed7da898c85d28a46c3a937f8e6fe5b09004577.jpeg',
        'name' => '我关注的游戏',
        'url' => '/pages/game/mygame',
        'disable' => 1
    ],
    [
        'id' => 2,
        'icon' => 'http://img.he29.com/play/e833f2a42ed3ea2daf33d0bd64d7d57cb02ddc82.png',
        'name' => '我关注的用户',
        'url' => '/pages/user/about',
        'disable' => 2
    ], [
        'id' =>3,
        'icon' => 'http://img.he29.com/play/fda58bed5780b09d26af5d8f628b8ceba1b8484a.png',
        'name' => '提现记录',
        'url' => '',
        'disable' => 0
    ], [
        'id' => 4,
        'icon' => 'http://img.he29.com/play/62f7e9c2a5c325cd2b83acfca374a97f86104864.png',
        'name' => '充值记录',
        'url' => '',
        'disable' => 0
    ], [
        'id' => 5,
        'icon' => 'http://img.he29.com/play/2a2738dfe11f5e19b08d08e4002e1d4179263756.png',
        'name' => '认证设置',
        'url' => '',
        'disable' => 0
    ], [
        'id' => 6,
        'icon' => 'http://img.he29.com/play/88bf94a80f72e91f2cb0073f950f93ec7144c5db.png',
        'name' => '意见反馈',
        'url' => '/pages/chat/index',
        'disable' => 2
    ], [
        'id' => 7,
        'icon' => 'http://img.he29.com/play/e833f2a42ed3ea2daf33d0bd64d7d57cb02ddc82.png',
        'name' => '关于我们',
        'url' => '/pages/home/away?id=2',
        'disable' => 2
    ]
);
return [
    'navcate'=>$nav,
    'navfot'=>$fot
];