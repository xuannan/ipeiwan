<?php
/**
 * Created by PhpStorm.
 * User: iyahe@qq.com (天明)
 * Date: 2017/12/26
 * Time: 15:41
 */
namespace app;

class redis
{
    public $redis;
    public $handler;
    public function __construct(){
        $this->redis = new \think\cache\driver\Redis();
        $this->handler = $this->redis->handler();
    }
    public function set($key,$value,$expire){
        return $this->handler->setex($key, $expire, $value);
    }
    public function get($key){
        return $this->redis->get($key);
    }
    //从头部追加
    public function lpush($key,$value){
        return  $this->handler->lpush($key,$value);
    }
    //从尾部追加
    public function rpush($key,$value){
        return  $this->handler->lpush($key,$value);
    }
    public function lrange($key,$length){
        return $this->handler->lrange($key, 0 ,$length);
    }
    //返回列表长度
    public function lsize($key){
        return $this->handler->lsize($key);
    }
    //返回指定键存储在列表中指定的元素
    public function lget($key,$len){
        return  $this->handler->lget($key,$len);
    }
    //为一个Key添加一个值。如果这个值已经在这个Key中，则返回FALSE
    public function sadd($key,$value){
        return  $this->handler->sadd($key,$value);
    }
    //返回所有的值
    public function smembers($key){
        return  $this->handler->smembers($key);
    }
    //删除Key中指定的value值
    public function sremove($key,$value){
        return  $this->handler->sremove($key,$value);
    }
    //检查集合中是否存在指定的值。
    public function scontains($key,$value){
        return  $this->handler->scontains($key,$value);
    }
    //称为key的集合中查找是否有value元素，有ture 没有 false
    public function sIsMember($key,$value){
        return  $this->handler->sIsMember($key,$value);
    }
    //随机移除并返回key中的一个值
    public function spop($key){
        return $this->handler->spop($key);
    }
    //返回集合中存储值的数量
    public function ssize($key){
        return $this->handler->ssize($key);
    }
    public function keys(){
        return $this->handler->keys("*");
    }
    public function delete($key){
        return $this->handler->delete($key);
    }
}