<?php
/**
 * Created by PhpStorm.
 * User: iyahe@qq.com (天明)
 * Date: 2017/12/2/002
 * Time: 21:42
 */
namespace app;

class action
{
    public static $repost;
    public $uid;
    public $openid;
    public $token;
    public $limit = 12;
    public function __construct()
    {
        $this->uid = input('uid');
        $this->openid = input('openid');
        $this->token = input('token');
        if($_POST) self::$repost = true;
    }
    public function index(){}
    public static function AjaxReturn($data=[],$info='',$code=1){
        $data = [
            'info'=>$data,
            'msg'=>$info,
            'code'=>$code
        ];
        if(input('callback')){
            $type = 'JSONP';
        }else{
            $type = 'JSON';
        }
        ajaxReturn($data,$type);
    }
    public static function ajaxReturnError($msg='',$data=[],$code=0){
        self::AjaxReturn($data,$msg,$code);
    }
    public static function ajaxReturnSuccess($msg,$data=[],$code=0){
        self::AjaxReturn($data,$msg,1);
    }
    public static function returnLuck($msg){
        if($msg){
            self::AjaxReturn('操作成功',$msg,1);
        }else{
            self::AjaxReturn('操作失败',$msg,0);
        }
    }
    public function getpwd($str){
        return md5(sha1($str));
    }
    public static function logger($str,$name='',$lev=3,$nameType='info'){
        $type = 'text';
        if(!is_array($str)){
            $inputData = $str;
        }else{
            $inputData = json_encode($str,JSON_UNESCAPED_UNICODE);
            $type = 'array';
        }
        $insert = '['.$name.'('.$type.')]['.date('Y-m-d H:i:s',time()).']'.PHP_EOL.'-------------------------------------'.PHP_EOL.$inputData.PHP_EOL.'-------------------------------------'.PHP_EOL;
        $file = $_SERVER['DOCUMENT_ROOT'].'/public/log/info.log';
        file_put_contents($file,$insert,FILE_APPEND);
    }
    public static function getSexYou($num){
        return $num==1?'他':'她';
    }
    public static function chatDB($name='default'){
       return[
           // 数据库类型
           'type'        => 'mysql',
           // 数据库连接DSN配置
           'dsn'         => '',
           // 服务器地址
           'hostname'    => '127.0.0.1',
           // 数据库名
           'database'    => 'chatlog',
           // 数据库用户名
           'username'    => 'chatlog',
           // 数据库密码
           'password'    => 'taCeaPNET7',
           // 数据库连接端口
           'hostport'    => '',
           // 数据库连接参数
           'params'      => [],
           // 数据库编码默认采用utf8
           'charset'     => 'utf8',
           // 数据库表前缀
           'prefix'      => '',
       ];
    }
    public static function setDB($name,$data=[]){
        $path = FILEBASE.'/'.encrypt($name).'.cache';
        return file_put_contents($path,encrypt(serialize($data)));
    }
    public static function getDB($name){
        $path = FILEBASE.'/'.encrypt($name).'.cache';
        $data = file_get_contents($path);
        return unserialize(decrypt($data));
    }
}