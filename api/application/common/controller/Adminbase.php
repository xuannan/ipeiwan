<?php
namespace app\common\controller;

use app\action;
use app\common\controller\Base;
use think\Db;
use think\Request;

/**
 * admin 基类控制器
 */
class AdminBase extends Base
{
    /**
     * 初始化方法
     */
    public function _initialize()
    {
        parent::_initialize();
        $auth = new \think\Auth();
        $request = Request::instance();
        $m = $request->module();
        $c = $request->controller();
        $a = $request->action();
        $rule_name = $m . '/' . $c . '/' . $a;
        $result = $auth->check($rule_name, session('user')['id']);
        $list = Db::name('admin_auth_rule')->getTreeData('level', 'id', 'title');
        $this->assign('leftSidder', $list);
        $this->sysConfig();
        if (!$result) {
            //$this->error('您没有权限访问');
        }
    }
    public function sysConfig(){
        $config = [
            '_admin_title'=>'爱陪玩后台管理',
            '_admin_title_desc'=>'爱陪玩',
            '_admin_iyahe'=>'iyahe@qq.com',
            '_admin_name'=>'Aaron 天明'
        ];
        $this->assign('sysConfig',$config);
    }
}

