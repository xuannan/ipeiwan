<?php
/**
 * Created by PhpStorm.
 * User: iyahe@qq.com (天明)
 * Date: 2017/12/4
 * Time: 13:32
 */
namespace app\home\controller;

use app\common\controller\Homebase;
use think\Request;
use app\common\controller;
use think\Db;
class Upload extends Homebase
{
    public function index(){
        if($_POST) {
            $path = explode('/',$_FILES['imgs']['type']);
            $name = '/public/images/'.sha1(time()).'.'.$path[1];
            $path = $_SERVER['DOCUMENT_ROOT'].$name;
            $move = move_uploaded_file($_FILES['imgs']['tmp_name'],$path);
            if($move) {
                $result = controller\Upload::run()->upload($path,'play');
                p($result);
            }
        }
        $list = Db::name('file_list')->order('file_addtime DESC')->paginate(140);
        $this->assign('list',$list);
        return $this->fetch();
    }
}