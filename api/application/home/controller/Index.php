<?php
namespace app\home\controller;

use app\common\controller\Homebase;
use think\Request;
use think\Db;

/**
 * Login Controller
 */
class Index extends Homebase
{
    public function index()
    {
        if (Request::instance()->isPost()) {
            // 做一个简单的登录 组合where数组条件 
            $map = [];
            $map['user_nickname'] = input('username');
            $map['user_password'] = md5(sha1(input('password')));
            $data = Db::name('user')->where($map)->find();
            if (empty($data)) {
                $this->error('账号或密码错误');
            } else {
                $sdata = [
                    'id' => $data['user_id'],
                    'username' => $data['user_nickname'],
                    'avatar' => $data['user_avatar'],
                    'phone' => $data['user_mobile'],
                ];
                session('user', $sdata);
                $this->success('登录成功、前往管理后台', 'Admin/Index/index');
            }
        } else {
            return $this->fetch();
        }
    }
    public function logout()
    {
        session('user', null);
        $this->success('退出成功、前往登录页面', 'Home/Index/index');
    }
}

