<?php
/**
 * Created by PhpStorm.
 * User: iyahe@qq.com (天明)
 * Date: 2017/12/8
 * Time: 16:19
 */
namespace app\api\model;
use think\Db;
use think\Model;

class SignModel extends Model{
    //新增签到
    public function add($uid){
        if(!$this->isSign($uid)){
            $data = [
                'sign_uid'=>$uid,
                'sign_count'=>'',
                'sign_lasttime'=>getStrtime(),
            ];
            return Db::name('sign')->update($data);
        }else{
            return false;
        }
    }
    //今天是否签到过
    public function isSign($uid){
        $signData = Db::name('sign')
            ->where(['sign_uid'=>$uid])
            ->order('sign_lasttime DESC')
            ->find();
        $times = strtotime(date('Y-m-d'));
        if(strtotime($signData['sign_lasttime']) < $times){
            return false;
        }else{
            return true;
        }
    }
}