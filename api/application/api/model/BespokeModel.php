<?php
/**
 * Created by PhpStorm.
 * User: iyahe@qq.com (天明)
 * Date: 2017/12/19/019
 * Time: 21:38
 */
namespace app\api\model;
use think\Db;
use think\Model;

class BespokeModel extends Model
{
    public function index()
    {
    }

    private static $_instance;

    public static function make()
    {
        if (!self::$_instance) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }
    public function changeStatus($where,$status=0){
        return Db::name('bespoke')
            ->where($where)
            ->update(['bes_status'=>$status]);
    }
    public function findInfo($where){
        return Db::name('bespoke')->where($where)->find();
    }
}