<?php
/**
 * Created by PhpStorm.
 * User: iyahe@qq.com (天明)
 * Date: 2017/12/13
 * Time: 16:18
 */

namespace app\api\model;
use app\api\controller\Kefu;
use think\Cache;
use think\Db;
use think\Model;

/** 金币操作表
 * Class MoneyModel
 * @package app\api\model
 */
class MessageModel extends Model{
    private static $_instance;
    public static function make() {
        if (!self::$_instance) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }
    public function KefuCenter($postData,$SessionFrom,$sendOpenid){
        $pex = 'kefu_';
        $form = 'fm_';
        $sendInfo = UserModel::make()->getUserInfo($SessionFrom['uid']);
        $type =  $postData['MsgType'];
        $content = $postData['Content'];
        setlogs($sendInfo,'发起人信息');
        setlogs($sendOpenid,'接收人的OpenId');
        $kefu = new Kefu();
        $content = ['content'=>$content];
        return $kefu->sendKefuMessage($sendOpenid,$content,$type);
    }
}