<?php
/**
 * Created by PhpStorm.
 * User: iyahe@qq.com (天明)
 * Date: 2017/12/25
 * Time: 17:22
 */
namespace app\api\model;
use think\Cache;
use think\Db;
use think\Model;

class CacheModel extends Model
{
    private static $_instance;
    public static function make() {
        if (!self::$_instance) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }
    public function _set($key,$value,$time=3600){
        if($data = $this->_get($key)) {
            return $this->_add($key,$value);
        }else{
            $value = $this->en($value);
            return Cache::set($key,$value,$time);
        }
    }
    public function _get($key){
        return $this->de($key);
    }
    public function en($value){
        return serialize(json_encode($value));
    }
    public function de($key){
        $data = Cache::get($key);
        return json_decode(unserialize($data),true);
    }
    public function _add($key,$value){
        $oldData = $this->_get($key);
        if(!is_array($oldData)){
            $tmp = [];
            array_push($tmp,$oldData);
            $oldData = $tmp;
        }
        array_push($oldData,$value);
        return Cache::set($key,$this->en($oldData));
    }
}