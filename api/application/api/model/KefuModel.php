<?php
/**
 * Created by PhpStorm.
 * User: iyahe@qq.com (天明)
 * Date: 2017/12/16/016
 * Time: 21:11
 */
namespace app\api\model;
use think\Db;
use think\Model;

class KefuModel extends Model
{
    private static $_instance;
    public static function make()
    {
        if (!self::$_instance) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }
    public function makeKefuData($openid,$type,$content){
        $data = [];
        $data['touser'] = $openid;
        $data['msgtype'] = $type;
        switch ($type) {
            case 'text':
                $data['text'] = ['content'=>$content];
                break;
            case 'image':
                $data['image'] = ['media_id'=>$content];
                break;
            case 'news':
                $data['link'] = [
                    'title'=>'',
                    'description'=>'',
                    'url'=>'',
                    'thumb_url'=>''
                ];
                break;
            case 'mini':
                $data['msgtype'] = 'miniprogrampage';
                $data['miniprogrampage'] = [
                    'title'=>$content['title'],
                    'pagepath'=>$content['pagepath'],
                    'thumb_media_id'=>$content['thumb_media_id']
                ];
                break;
            default:
                $data['text'] = ['content'=>''];
                break;
        }
        return $data;
    }
}