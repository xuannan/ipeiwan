<?php
/**
 * Created by PhpStorm.
 * User: iyahe@qq.com (天明)
 * Date: 2017/12/13
 * Time: 13:16
 */
namespace app\api\model;
use think\Db;
use think\Model;

class UserModel extends Model{
    private static $_instance;
    public static function make() {
        if (!self::$_instance) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }
    public function getUserInfo($uid,$field=''){
        return Db::name('user')
            ->join('p_leavel','p_leavel.leavel_id=p_user.user_leavel','left')
            ->where(['user_id'=>$uid])
            ->field($field)
            ->find();
    }
    public function getOpenid($openid,$field=''){
        return Db::name('user')
            ->where(['user_openid'=>$openid])
            ->field($field)
            ->find();
    }
    public function onlineSet($uid,$status=1){
        return Db::name('user')->where(['user_id'=>$uid])->update([
            'user_update'=>getStrtime(),
            'user_online'=>$status
        ]);
    }
}