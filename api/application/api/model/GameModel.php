<?php
/**
 * Created by PhpStorm.
 * User: iyahe@qq.com (天明)
 * Date: 2017/12/4
 * Time: 14:27
 */
namespace app\api\model;
use think\Db;
use think\Model;

class GameModel extends Model{
    public function index(){}
    private static $_instance;
    public static function make() {
        if (!self::$_instance) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }
    public function listd($where=[],$left='INNER',$limit=6){
        return Db::name('gamelist')
            ->join('gametag','p_gametag.type_id=p_gamelist.game_type','left')
            ->where($where)
            ->order('game_order DESC')
            ->paginate($limit)->each(function($item,$key){
                $item['about_number'] = Db::name('mygame')
                    ->where(['my_gid'=>$item['game_id']])->count();
                return $item;
            });
    }
    public function myAbout($uid){
        return Db::name('mygame')->where(['my_uid'=>$uid])->field('my_gid')->select();
    }
    public function userAboutGame($where){
        return Db::name('mygame')
            ->join('gamelist','p_gamelist.game_id=p_mygame.my_gid')
            ->where($where)
            ->paginate()->each(function($item,$key){
                $item['setting'] = Db::name('setting')
                    ->where(['set_uid'=>$item['my_uid'],'set_gid'=>$item['my_gid']])
                    ->find();
                return $item;
            });
    }
}