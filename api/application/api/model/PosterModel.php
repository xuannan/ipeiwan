<?php
/**
 * Created by PhpStorm.
 * User: iyahe@qq.com (天明)
 * Date: 2017/12/23/023
 * Time: 21:47
 */
namespace app\api\model;
use think\Db;
use think\Model;

/**海报控制
 * Class PosterModel
 * @package app\api\model
 */
class PosterModel extends Model
{
    private static $_instance;

    public static function make()
    {
        if (!self::$_instance) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }
    public function makePoster($config=array(),$header=array(),$share=array()){
        $returnData = $config;
        $config = [];
        $config['navigationBarBackgroundColor'] = $header['bgc'];
        $config['navigationBarTextStyle'] = $header['bgt'];
        $config['navigationBarTitleText'] = $header['title'];
        $returnData['config'] = $config;
        $returnData['share'] = $share;
        return $returnData;
    }
}