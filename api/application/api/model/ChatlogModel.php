<?php
/**
 * Created by PhpStorm.
 * User: iyahe@qq.com (天明)
 * Date: 2018/1/3
 * Time: 10:50
 */
namespace app\api\model;
use app\action;
use think\Db;
use think\Model;

class ChatlogModel extends Model
{
    private static $_instance;

    public static function make()
    {
        if (!self::$_instance) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    /** 添加聊天记录
     * @param $text 内容
     * @param $uid 发送人uid
     * @param $send 接收人uid
     * @param $type 消息类型
     * @return int|string
     */
    public function addlog($text,$uid,$send,$type=1){
        $content = action::chatDB();
        $data = [];
        $data['chat_content'] = $text;
        $data['chat_uid'] = $uid;
        $data['chat_send'] = $send;
        $data['chat_update'] = getStrtime();
        $data['chat_look'] = 0;
        $data['chat_del'] = 0;
        $data['chat_type'] = $type;
        $data['chat_unique'] = ($uid+$send);
        return Db::connect($content)->name('chat')->insertGetId($data);
    }
    public function getHistory($uid,$sid,$look=1,$limit=20){
        return Db::connect(action::chatDB())->name('chat')->where([
            'chat_unique'=>($uid+$sid),
            'chat_del'=>0,
            'chat_look'=>$look
        ])->order('chat_update DESC')->paginate($limit);
    }
}