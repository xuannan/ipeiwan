<?php
/**
 * Created by PhpStorm.
 * User: iyahe@qq.com (天明)
 * Date: 2017/12/21
 * Time: 13:30
 */
namespace app\api\model;
use think\Db;
use think\Model;

class ArticleModel extends Model
{
    private static $_instance;

    public static function make()
    {
        if (!self::$_instance) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }
    public function getList($where,$field='',$limit=12){
       return Db::name('article')
           ->join('user','p_user.user_id=p_article.art_auid')
           ->where($where)->field($field)->paginate($limit);
    }
}