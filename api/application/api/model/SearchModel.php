<?php
/**
 * Created by PhpStorm.
 * User: iyahe@qq.com (天明)
 * Date: 2017/12/7
 * Time: 15:53
 */
namespace app\api\model;
use think\Db;
use think\Model;

class SearchModel extends Model{
    public function index(){}
    private static $_instance;
    public static function make() {
        if (!self::$_instance) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }
    public function searchError($order){
        return Db::name('order')->where('order_ids',$order)->update(['order_status'=>-1]);
    }
}