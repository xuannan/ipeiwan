<?php
/**
 * Created by PhpStorm.
 * User: iyahe@qq.com (天明)
 * Date: 2017/12/5
 * Time: 17:48
 */
namespace app\api\model;
use think\Db;
use think\Model;

class OrderModel extends Model{
    private static $_instance;
    public static function make() {
        if (!self::$_instance) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }
    public static function makeOrder(){
        return date('YmdHis').str_pad(mt_rand(1, 99999), 6, '0', STR_PAD_LEFT);
    }
    public function OrderList($where,$limit=10){
        return Db::name('order')
            ->join('bespoke','p_bespoke.bes_order=p_order.order_ids')
            ->join('user','p_user.user_id=p_bespoke.bes_myid')
            ->join('gamelist','p_gamelist.game_id=p_bespoke.bes_gid')
            ->where($where)
            ->order('bes_addtime DESC')
            ->paginate($limit)->each(function($item,$key){
                $item['yours'] = Db::name('user')
                    ->where('user_id',$item['bes_from'])
                    ->field('user_nickname,user_avatar')
                    ->find();
                return $item;
            });
    }
    public function orderInfo($order_id){
        return Db::name('finds')->where('find_order',$order_id)->find();
    }
    public function makeOrderData($id='',$status=1){
        $data = [];
        if($id == '') $id = self::makeOrder();
        $data['order_ids'] = $id;
        $data['order_addtime'] = getStrtime();
        $data['order_status'] = $status;
        $order = Db::name('order')->where('order_ids',$id)->find();
        if(empty($order)){
            return Db::name('order')->insertGetId($data);
        }else{
            return Db::name('order')->where('order_ids',$id)->update($data);
        }
    }
    public function orderFind($oid){
        return Db::name('order')->where('order_ids',$oid)->find();
    }
    //清理过期的未接单的
    public function timeOut($uid){
        $where['bes_status'] = 1;
        $where['bes_myid'] = $uid;
        $where['bes_starttime'] = ['<=',getStrtime()];
        Db::name('bespoke')->where($where)->update(['bes_status'=>0]);
    }
    public function changeOrder($where,$status){
        return Db::name('bespoke')->where($where)->update(['bes_status'=>$status]);
    }
    public function makeAddress($url){
        $satr = stristr($url,'?');
        $len1 = substr($satr,1);
        $explode = explode('&',$len1);
        if(count($explode) == 6){
            return json_encode($explode);
        }else{
            return '';
        }
    }
    public function getAddress($explode){
        $urllen = '';
        $default = 'https://game.weixin.qq.com/cgi-bin/h5/static/smobadynamic/index.html?';
        $explode = json_decode($explode,true);
        foreach($explode as $v){
            $urllen .=$v.'&';
        }
        $ALL = substr($urllen,0,-1);
        return $default.$ALL;
    }
    public function makeUploadImgs($imgs){
        $imgs = json_decode($imgs,true);
        if(is_array($imgs)){
            foreach($imgs as $k=>$v){
                unset($imgs[$k]['up']);
            }
            return json_encode($imgs);
        }else{
            return '';
        }
    }
    public function total($where){
        return Db::name('bespoke')->where($where)->count();
    }
}