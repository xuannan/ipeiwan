<?php
/**
 * Created by PhpStorm.
 * User: iyahe@qq.com (天明)
 * Date: 2017/12/26
 * Time: 17:38
 */
namespace app\api\model;
use think\Db;
use think\Model;

class BillboardModel extends Model
{
    private static $_instance;

    public static function make()
    {
        if (!self::$_instance) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }
    public function isBd($uid){
        return Db::name('billboard')->where('bill_uid',$uid)->find();
    }
}