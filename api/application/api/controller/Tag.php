<?php
/**
 * Created by PhpStorm.
 * User: iyahe@qq.com (天明)
 * Date: 2017/12/4
 * Time: 15:48
 */
namespace app\api\controller;
use app\action;
use think\Db;
use \app\api\model\GameModel;

class Tag extends action
{
    public $gid;
    public function __construct()
    {
        parent::__construct();
        $this->gid = input('gid');
    }

    public function index(){}
    public function sex(){
        return ['不限','帅哥','美女','其他'];
    }
    public function zindex(){
        if($this->gid == 0){
            return ['不限','倔强青铜','秩序白银','荣耀黄金','尊贵铂金','永恒钻石'];
        }
        return [];
    }
    public function tagList(){
        $list = Db::name('tag')->select();
        $return = [];
        $return['tag'] = $list;
        $return['sex'] = $this->sex();
        $return['zindex'] = $this->zindex();
        $return['money'] = ['max'=>60,'min'=>1,'step'=>1];
        $return['level'] = ['max'=>60,'min'=>1,'step'=>1];
        $return['play'] = ['max'=>5,'min'=>1,'step'=>1];
        self::ajaxReturnSuccess('ok',$return);
    }
    public function getAllTag(){
        $list = Db::name('tag')->where(['tag_show'=>1])->select();
        self::ajaxReturnError('ok',$list);
    }
}