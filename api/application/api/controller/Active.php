<?php
/**
 * Created by PhpStorm.
 * User: iyahe@qq.com (天明)
 * Date: 2017/12/9/009
 * Time: 16:26
 */
namespace app\api\controller;
use app\action;
use think\Cache;
use think\Db;

class Active extends action
{
    public function addData(){
        $text = input('text');
        $imgs = json_decode(input('imgs'),true);
        if(!empty($imgs)){
            $insert = [];
            $insert['active_uid'] = $this->uid;
            $insert['active_text'] = $text;
            $insert['active_img'] = $imgs;
            $insert['active_addtime'] = getStrtime();
            $insert['active_type'] = 1;
            $insert['active_game'] = '';
            foreach ($insert['active_img'] as $k=>$v){
                unset($insert['active_img'][$k]['up']);
            }
            $insert['active_img'] = json_encode($insert['active_img']);
            $result = Db::name('active')->insertGetId($insert);
            if($result){
                $wall = [];
                foreach ($imgs as $v){
                    if($v['img']){
                        array_push($wall,$v['img']);
                    }
                }
                if($wall){
                    foreach ($wall as $v){
                        $data = [];
                        $data['wall_img'] = $v;
                        $data['wall_uid'] = $this->uid;
                        $data['wall_active'] = $result;
                        $data['wall_addtime'] = getStrtime();
                        $wallRes = Db::name('photowall')->insertGetId($data);
                    }
                }
                self::ajaxReturnSuccess('发布成功');
            }else{
                self::ajaxReturnError('发布失败');
            }
        }
    }
    public function photowall(){
        $uid = input('uuid');
        $list = Db::name('photowall')
            ->where(['wall_uid'=>$uid,'wall_isdel'=>1])
            ->limit(9)
            ->select();
        self::ajaxReturnSuccess('ok',$list);
    }
    public function getData(){
        $limit = input('limit',12);
        $uid = input('uuid');
        $list = Db::name('active')
            ->join('user','p_user.user_id=p_active.active_uid')
            ->where(['active_uid'=>$uid,'active_isdel'=>1])
            ->order('active_id DESC')
            ->paginate($limit);
        self::ajaxReturnSuccess('ok',$list);
    }
    public function delactive(){
        $aid = input('aid');
        $result = Db::name('active')->where('active_id',$aid)->update(['active_isdel'=>0]);
        if($result){
           $photowall =  Db::name('photowall')->where(['wall_active'=>$aid])->update(['wall_isdel'=>0]);
           if($photowall){
               self::ajaxReturnSuccess('已删除');
           }else{
               self::ajaxReturnError('删除失败');
           }
        }
    }
}