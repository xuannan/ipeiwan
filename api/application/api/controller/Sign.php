<?php
/**
 * Created by PhpStorm.
 * User: iyahe@qq.com (天明)
 * Date: 2017/12/8
 * Time: 16:19
 */
namespace app\api\controller;
use app\action;
use app\api\model\MoneyModel;
use think\Db;

class Sign extends action
{
    public function index(){}
    //开始签到
    public function startSign(){
        $signdata = $this->SignData();
        //最后签到时间大于今早0点,说明今天已经签到
        $today = strtotime(date('Y-m-d'));
        $yesday =  date("Y-m-d",strtotime("-1 day"));
        if(empty($signdata)){
            $signRes = $this->signInsert(1);
            $money = $this->getMoney(1);
            MoneyModel::make()->addMoney(1,$money,$this->uid,'签到送金币');
            self::ajaxReturnSuccess('签到成功',['money'=>$money]);
        }else{
            if(strtotime($signdata['sign_lasttime']) < $today){
                //昨天签到了,连续签到+1
                if($signdata['sign_lasttime'] > $yesday){
                    $count = intval($signdata['sign_count']) + 1;
                }else{
                    $count = 1;
                }
                $signRes = $this->signInsert($count);
                if($signRes){
                    $moneys = $this->getMoney($count);
                    MoneyModel::make()->addMoney(1,$moneys,$this->uid,1,'连续签到'.$count.'天送金币');
                    self::ajaxReturnSuccess('签到成功',['money'=>$moneys]);
                }else{
                    self::ajaxReturnError('签到失败');
                }
            }else{
                self::ajaxReturnError('今天已经签到过了,明天再来吧!');
            }
        }
    }
    private function signInsert($count){
        $data = [];
        $data['sign_uid'] = $this->uid;
        $data['sign_count'] = $count;
        //$data['sign_history'] = $this->uid;
        $data['sign_lasttime'] = getStrtime();
        $data['sign_exts'] = '';
        return Db::name('sign')->insertGetId($data);
    }
    public function SignData(){
        $sign = Db::name('sign')
            ->where(['sign_uid'=>$this->uid])
            ->order('sign_lasttime DESC')
            ->find();
        return $sign;
    }
    public function todaySign($is=true){
        $times = date('Y-m-d');
        $where=[];
        $where['sign_lasttime'] = ['>',$times];
        $resulty =  Db::name('sign')->where($where)->find();
        if($is) {
            self::ajaxReturnSuccess('ok',$resulty);
        }else{
            return $resulty;
        }
    }
    public function SignHistory(){
        $times = date('Y-m');
        $where=[];
        $where['sign_lasttime'] = ['>',$times];
        $where['sign_uid'] = $this->uid;
        $list = Db::name('sign')->where($where)->order('sign_lasttime DESC')->select();
        $today = $this->todaySign(false);
        $return = [];
        $return['list'] = $list;
        $return['today'] = $today;
        $yesday =  strtotime("-1 day");
        $allMoney =  MoneyModel::make()->getMoney($this->uid,1);
        if(empty($today)){
            //如果今天没有签到,查看昨天!
            if(!empty($list)){
                if(strtotime($list[0]['sign_lasttime']) > $yesday){
                    $return['money'] = $this->getMoney(intval($list[0]['sign_count'])+1);
                    $return['sign_count'] = $list[0]['sign_count'];
                }else{
                    $return['money'] = $this->getMoney(1);
                    $return['sign_count'] = 1;
                }
            }else{
                $return['money'] = $this->getMoney(1);
                $return['sign_count'] = 1;
            }
        }else{
            $return['money'] = $this->getMoney($today['sign_count']);
            $return['sign_count'] = $today['sign_count'];
        }
        $return['allMoney'] = $allMoney;
        self::ajaxReturnSuccess('ok',$return);
    }
    public function signGuiZe(){
        $start = 10;
        $ynow = date('Y');
        $month = date('m');
        function is_leap($year) {
            $res = '';
                return ($year%100==0?$res=($year%400==0?1:0):$res=($year%4==0?1:0));
        }
        $day = [31,28+is_leap($ynow),31,30,31,31,30,31,30,31,30,31];
        $days = [];
        for ($i = 1; $i <= $day[$month-1]; $i++){
            array_push($days,['inday'=>$i,'money'=>$this->getMoney($i)]);
        }
    }
    public function getMoney($i){
        return ceil(((10+($i*$i))/2));
    }
}