<?php
/**
 * Created by PhpStorm.
 * User: iyahe@qq.com (天明)
 * Date: 2017/12/18
 * Time: 10:10
 */
namespace app\api\controller;
use app\action;
use app\api\model\SocketModel;
use app\api\model\UserModel;
use GatewayClient\Gateway;
use think\Cache;
use think\Db;
use \app\api\model\GameModel;

/**
 * Class Socket
 * @package app\api\controller
 */
class Socket extends action
{
    public $data;
    public $postData;
    public $client_id;
    public function __construct()
    {
        parent::__construct();
        $this->postData = json_decode(input('s'),true);
        $this->data =$this->postData['message'];
        $this->client_id = $this->postData['client_id'];
        self::logger($this->data,'Socket数据流->'.$this->postData['action']);
        self::logger($this->postData,'postDatat数据流');
    }

    public function onConnect(){}
    public function onMessage(){
        $this->getDataSend();
    }
    public function onClose(){
        $client_id = $this->postData['client_id'];
        $uid = cache($client_id);
        UserModel::make()->onlineSet($uid,0);//设置用户下线
        Gateway::unbindUid($client_id,$uid);//解除绑定
        Cache::rm($client_id);//删除缓存
        self::logger($client_id,$uid.'下线->onClose');
    }
    public function getDataSend(){
        $action = $this->data['action'];
        self::logger($action,'进入控制器');
        self::logger($this->postData,'Socket数据流->onClose');
        if($action){
            SocketModel::make()->$action($this->data,$this->client_id);
        }
    }
    public function test(){
        Gateway::$registerAddress = '127.0.0.1:4005';
        Gateway::sendToUid(1,SocketModel::socketData(123));
    }
}