<?php
/**
 * Created by PhpStorm.
 * User: iyahe@qq.com (天明)
 * Date: 2017/12/18
 * Time: 12:45
 */
namespace app\api\controller;
use app\action;
use think\Cache;
use think\Db;

class Bespoke extends action
{
    public function getbespokeData(){
        $list = Db::name('bespoke')
            ->where('bes_myid',$this->uid)
            ->join('user','p_user.user_id=p_bespoke.bes_from')
            ->paginate(10);
        self::ajaxReturnSuccess('pk',$list);
    }
}