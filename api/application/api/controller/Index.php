<?php
namespace app\api\controller;

use app\action;
use app\common\controller\Upload;
use think\Db;

class Index extends action
{
    public function index(){
        $path = SYS_ROOT.'/config/database/abc.accdb';
        p($path);
        $cont = [
            'type' => 'sqlite',
            'dsn'  => 'odbc:driver={microsoft access driver (*.accdb)};dbq='.$path,
        ];
        $result = Db::connect($cont)->name('user')->select();
        p($result);
    }
}
