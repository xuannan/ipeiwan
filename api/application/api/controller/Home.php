<?php
/**
 * Created by PhpStorm.
 * User: iyahe@qq.com (天明)
 * Date: 2017/12/23/023
 * Time: 21:17
 */
namespace app\api\controller;
use app\action;
use app\api\model\PosterModel;
use app\api\model\UserModel;
use think\Cache;
use think\Db;

class Home extends action
{
    public function HomeAway(){
        $id = input('id');
        $list = Db::name('poster')->where('poster_id',$id)->find();
        $returnData = [];
        $returnData['id'] = $list['poster_id'];
        $returnData['hbs'] = $list['poster_height'];
        $returnData['url'] = $list['poster_url'];
        $returnData['img'] = $list['poster_img'];
        $config = [];
        $config['bgc'] = $list['poster_bgc'];
        $config['bgt'] = $list['poster_bgt'];
        $config['title'] = $list['poster_title'];
        $share = [];
        $share['title'] = $list['poster_share_title'];
        $share['path'] = $list['poster_share_url'];
        $share['imageUrl'] = $list['poster_share_img'];
        $resulrt = PosterModel::make()->makePoster($returnData,$config,$share);
        self::ajaxReturnSuccess('ok',$resulrt);
    }
    public function photowall(){
        $img = input('img');
        //照片墙更新成功,添加一条动态
        $act = [];
        Db::startTrans();
        $act['active_uid'] = $this->uid;
        $act['active_text'] = '';
        $act['active_img'] = json_encode([['img'=>$img]]);
        $act['active_addtime'] = getStrtime();
        $act['active_type'] = 1;
        $act['active_game'] = '';
        $active = Db::name('active')->insertGetId($act);
        //动态更新成功,更新精选照片
        if($active){
            $insert = [];
            $insert['wall_img'] = $img;
            $insert['wall_uid'] = $this->uid;
            $insert['wall_active'] = $active;
            $insert['wall_addtime'] = getStrtime();
            $result = Db::name('photowall')->insertGetId($insert);
            if($result){
                Db::commit();
                self::ajaxReturnSuccess('更新成功');
            }else{
                Db::rollback();
                self::ajaxReturnError('更新失败');
            }
        }else{
            Db::rollback();
            self::ajaxReturnError('动态更新失败');
        }
    }
    public function getShareData(){
        $uid = input('uuid');
        $userInfo = UserModel::make()->getUserInfo($uid);
        $data = [];
        $data['title'] = $userInfo['user_nickname'].'邀请你和'.self::getSexYou($userInfo['user_sex']).'一起玩,你来嘛!';
        $data['path'] = '/pages/home/index?uid='.$userInfo['user_id'];
        $data['imageUrl'] = '';
        self::ajaxReturnSuccess('ok',$data);
    }
    public function makedata(){
        $returnData = [];
        $returnData['poster_name'] = '关于我们';
        $returnData['poster_height'] = 3.42;
        $returnData['poster_url'] = '/pages/index';
        $returnData['poster_img'] = 'http://img.he29.com/play/5fb036cfb4a9d174926ffe1d50426d3b79e471ec.jpeg';
        $returnData['poster_bgc'] = '#c6def8';
        $returnData['poster_bgt'] = '#ffffff';
        $returnData['poster_title'] = '关于我们-来和我玩嘛';
        $returnData['poster_share_title'] = '来和我玩嘛,那些你所不知道的';
        $returnData['poster_share_url'] = 'pages/home/away?id=2';
        $returnData['poster_share_img'] = '';
        $returnData['poster_updatetime'] = getStrtime();
        p($returnData);
        $result = Db::name('poster')->insertGetId($returnData);
        p($result);
    }
}