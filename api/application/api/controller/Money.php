<?php
/**
 * Created by PhpStorm.
 * User: iyahe@qq.com (天明)
 * Date: 2017/12/8
 * Time: 13:41
 */
namespace app\api\controller;
use app\action;
use app\api\model\MoneyModel;
use think\Db;
use \app\api\model\GameModel;

class Money extends action
{
    public function getmoney(){
        $money = new MoneyModel();
        $type = input('type',1);
        $sum = $money->getMoney($this->uid,$type,1);
        self::ajaxReturnSuccess('ok',$sum);
    }
    public function moneyTotal(){}
    public function recharge(){
        $key = input('key');
        if(strlen($key)  != 32){
            self::ajaxReturnError('充值卡有误，请检查');
        }
        Db::startTrans();
        $exchange =  Db::name('exchange')->where(['key_number'=>$key])->find();
        if(!empty($exchange)){
            if(!$exchange['key_okay']){
                self::ajaxReturnError('抱歉,此卡已经被锁定,暂时无法使用');
            }
            if($exchange['key_useId']){
                self::ajaxReturnError('此卡已经被使用了');
            }else{
                $insert = [
                    'key_useId'=>$this->uid,
                    'key_usetime'=>getStrtime()
                ];
                $resu = Db::name('exchange')->where('key_id',$exchange['key_id'])->update($insert);
                if($resu){
                    $result = MoneyModel::make()->addMoney(
                        $exchange['key_type'],
                        $exchange['key_mount'],
                        $this->uid, 1,'充值卡充值',md5(time())
                    );
                    if($result){
                        Db::commit();
                        self::ajaxReturnSuccess('充值卡使用成功');
                    }else{
                        Db::rollback();
                        self::ajaxReturnError('充值卡使用失败');
                    }
                }
            }
        }else{
            self::ajaxReturnError('充值卡不存在');
        }
    }
    public function getmoneylist(){
        $limit = input('limit');
        $type = input('type',1);
        $list = MoneyModel::make()->getMoneyHis([
            'money_uid'=>$this->uid,
            //'money_type'=>$type
        ],$limit);
        self::ajaxReturnSuccess('ok',$list);
    }
    public function Moneyds(){
        $writeValue = input('writeValue');
        //先扣豌豆,
        $result = MoneyModel::make()->addMoney(2,$writeValue/100,$this->uid,0,'豌豆兑换金币扣除');
        if($result){
            $result = MoneyModel::make()->addMoney(1,$writeValue,$this->uid,1,'豌豆兑换金币增加');
            if($result){
                self::ajaxReturnSuccess('兑换成功');
            }else{
                self::ajaxReturnError('兑换失败');
            }
        }else{
            self::ajaxReturnError('兑换失败');
        }
    }
    public function getmx(){
        $list = Db::name('money')
            ->where([
                'money_uid'=>394,
                'money_type'=>2,
                'money_make'=>1,
                'money_status'=>1
            ])->select();
        p($list);
    }
}