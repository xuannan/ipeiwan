<?php
/**
 * Created by PhpStorm.
 * User: iyahe@qq.com (天明)
 * Date: 2017/12/2/002
 * Time: 22:14
 */
namespace app\api\controller;
use app\action;
use think\Db;

class Setting extends action
{
    public function index(){
        self::ajaxReturnSuccess('api');
    }
    public function save(){
        $data = input();
        if(!$data['c_gid'] && $data['c_gid'] < 0) self::ajaxReturnError('参数有误');
        if(!$data['c_money']) self::ajaxReturnError('佣金有误,请检查');
        if(!$data['c_note']) self::ajaxReturnError('签名必须填写');
        $c_gid = intval(input('c_gid'));
        Db::startTrans();//启动事务
        $tag = intval($data['c_tag']);
        $insertData = [
            'set_tag'=>$tag,
            'set_monery'=>$data['c_money'],
            'set_leavel'=>$data['c_level'],
            'set_gid'=>$c_gid,
            'set_index'=>$data['c_index'],
            'set_note'=>$data['c_note'],
            'set_sex'=>$data['c_sex'],
            'set_uid'=>$this->uid,
            'set_status'=>$data['c_status'],
            'set_once'=>$data['c_play'],
            'set_update'=>getStrtime()
        ];
        $find = Db::name('setting')
            ->where(['set_gid'=>$data['c_gid'],'set_uid'=>$this->uid])
            ->find();
        if($find){
            $rr = Db::name('setting')
                ->where(['set_gid'=>$data['c_gid'],'set_uid'=>$this->uid])
                ->update($insertData);
        }else{
            $insertData['set_addtime'] = getStrtime();
            $rr = Db::name('setting')->insertGetId($insertData);
        }
        if($rr){
            $tagdata = [
                'ugid_tagid'=>$tag,
                'ugid_addtime'=>getStrtime(),
                'ugid_gid'=>$c_gid,
                'ugid_uid'=>$this->uid
            ];
            $where = ['ugid_gid'=>$c_gid, 'ugid_uid'=>$this->uid];
            $tags = Db::name('tag_user')->where($where)->find();
            if($tags){
                $tagres = Db::name('tag_user')->where($where)->update($tagdata);
            }else{
                $tagres = Db::name('tag_user')->insertGetId($tagdata);
            }
            if($tagres){
                Db::commit();
                self::ajaxReturnSuccess('保存成功');
            }
        }
        Db::rollback();
        self::ajaxReturnError('保存失败');
    }
    public function getdata(){
        $id = input('gid');
        $set = Db::name('setting')->where(['set_gid'=>$id,'set_uid'=>$this->uid])->find();
        $user  = Db::name('tag_user')
            ->join('p_tag','p_tag.tag_id=p_tag_user.ugid_tagid')
            ->where(['ugid_gid'=>$id,'ugid_uid'=>$this->uid])
            ->select();
        $return['set'] = $set;
        $return['user'] = $user;
        $return['game'] = Db::name('gamelist')->where(['game_id'=>$id])->find();
        self::ajaxReturnSuccess('ok',$return);
    }
}