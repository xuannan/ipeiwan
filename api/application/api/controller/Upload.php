<?php
/**
 * Created by PhpStorm.
 * User: iyahe@qq.com (天明)
 * Date: 2017/12/9/009
 * Time: 15:25
 */
namespace app\api\controller;
use app\action;
use app\common\controller;
use think\Db;

class Upload extends action
{
    public function formData(){
        $file = $_FILES['file'];
        if(isset($file)){
            $path = explode('/',$file['type']);
            $name = '/public/images/'.sha1(time()).'.'.$path[1];
            $path = $_SERVER['DOCUMENT_ROOT'].$name;
            $move = move_uploaded_file($file['tmp_name'],$path);
            if($move) {
                $result = controller\Upload::run()->upload($path,'play');
                self::ajaxReturnSuccess('ok',$result);
            }
        }
    }
}