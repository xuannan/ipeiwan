<?php
/**
 * Created by PhpStorm.
 * User: iyahe@qq.com (天明)
 * Date: 2018/1/3
 * Time: 11:16
 */
namespace app\api\controller;
use app\action;
use app\api\model\ChatlogModel;
use app\api\model\UserModel;
use think\Cache;
use think\Db;

class Chat extends action
{
    public function history(){
        $uid = $this->uid;
        $resid = input('uuid');
        $type = input('type',0);
        $field = 'user_nickname,user_avatar';
        $myinfo = UserModel::make()->getUserInfo($uid,$field);
        $userinfo = UserModel::make()->getUserInfo($resid,$field);
        $list = ChatlogModel::make()->getHistory($uid,$resid,$type);
        $returnData = [];
        $returnData['list'] = $list;
        $returnData['myinfo'] = $myinfo;
        $returnData['userinfo'] = $userinfo;
        self::ajaxReturnSuccess('ok',$returnData);
    }
}