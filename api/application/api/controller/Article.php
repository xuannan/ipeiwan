<?php
/**
 * Created by PhpStorm.
 * User: iyahe@qq.com (天明)
 * Date: 2017/12/21
 * Time: 13:26
 */
namespace app\api\controller;
use app\action;
use app\api\model\ArticleModel;
use think\Cache;
use think\Db;

class Article extends action
{
   public function listd(){
       $list = ArticleModel::make()->getList([]);
       self::ajaxReturnSuccess('ok',$list);
   }
   public function content(){
       $id = input('id');
       $content = Db::name('article')
           ->join('user','p_user.user_id=p_article.art_auid')
           ->where('article_id',$id)->find();
       self::ajaxReturnSuccess('ok',$content);
   }
}