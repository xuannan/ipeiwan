<?php
/**
 * Created by PhpStorm.
 * User: iyahe@qq.com (天明)
 * Date: 2018/1/26
 * Time: 16:43
 */
namespace app\api\controller;
use app\action;


class Boom extends action
{
    public function getData($name,$limit=10){
        if(!$json = cache($name)){
            $json = file_get_contents(FILEBASE.'/'.$name.'.json');
            cache($name,$json);
        }
        $jsonArr = json_decode($json,true);
        $arrs = [];
        $n7m = 0;
        if($limit==0) $limit = count($jsonArr);
        foreach($jsonArr as $k=>$v){
            $n7m++;
            if($n7m <= $limit){
                array_push($arrs,$v);
            }else{
                break;
            }
        }
        return  $arrs;
    }
    public function getHero(){
        $limit = input('limit');
        $arrs = $this->getData('hero',$limit);
        self::ajaxReturnSuccess('ok',$arrs);
   }
   public function getEqip(){
       $limit = input('limit');
       $arrs = $this->getData('equip',$limit);
       self::ajaxReturnSuccess('ok',$arrs);
   }
    public function getRune(){
        $limit = input('limit');
        $arrs = $this->getData('rune',$limit);
        self::ajaxReturnSuccess('ok',$arrs);
    }
}