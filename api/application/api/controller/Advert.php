<?php
/**
 * Created by PhpStorm.
 * User: iyahe@qq.com (天明)
 * Date: 2017/12/28
 * Time: 10:56
 */
namespace app\api\controller;
use app\action;
use app\api\model\ArticleModel;
use think\Cache;
use think\Db;

class Advert extends action
{
    public function onstart(){
        $onstart = [];
        $onstart['background'] = 'http://img.he29.com/play/35e202bfd6dd3a3c0fddb6a841c7e34f115c9ffd.jpeg';
        $onstart['title'] = '标题';
        $onstart['content'] = '内容';
        $onstart['url'] = '/pages/home/away?id=1';
        $onstart['md5'] = md5($onstart['url']);
        self::ajaxReturnSuccess('ok',$onstart);
    }
}
