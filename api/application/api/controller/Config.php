<?php
/**
 * Created by PhpStorm.
 * User: iyahe@qq.com (天明)
 * Date: 2017/12/2/002
 * Time: 22:13
 */
namespace app\api\controller;
use app\action;
use think\Cache;

class Config extends action
{
    public function index(){
        self::ajaxReturnSuccess('api');
    }
    public function banner(){
        if(!$list = cache('banner')){
            $list= self::getDB('banner');
            cache('banner',$list,3600);
        }
        self::ajaxReturnSuccess('ok',$list);
    }
    public function tab4(){
        $banner = array(
            ['id'=>1,'img'=>'http://img.he29.com/play/bde3280dfe932042849ea51f596f8defda569b26.jpeg','url'=>'','desc'=>''],
            ['id'=>2,'img'=>'http://img.he29.com/play/bde3280dfe932042849ea51f596f8defda569b26.jpeg','url'=>'','desc'=>''],
            ['id'=>3,'img'=>'http://img.he29.com/play/bde3280dfe932042849ea51f596f8defda569b26.jpeg','url'=>'','desc'=>''],
            ['id'=>4,'img'=>'http://img.he29.com/play/bde3280dfe932042849ea51f596f8defda569b26.jpeg','url'=>'','desc'=>''],
            ['id'=>5,'img'=>'http://img.he29.com/play/bde3280dfe932042849ea51f596f8defda569b26.jpeg','url'=>'','desc'=>''],
            ['id'=>6,'img'=>'http://img.he29.com/play/bde3280dfe932042849ea51f596f8defda569b26.jpeg','url'=>'','desc'=>'']
        );
        self::ajaxReturnSuccess('ok',$banner);
    }
    public function footerNav(){
        $data = cache('navfoot');
        if(!$data){
            $data = require_once DOC_ROOT.'/api/config/navfoot.php';
            Cache::set('navfoot',$data,3600);
        }
        self::ajaxReturnSuccess('ok',$data);
    }
    public function login(){
        $url = input('url');
        $type= input('type');
        if($type == 'web'){
            $tjson =[];
            $tjson['session_key'] = '+YEK+YayMxHYRmTxJfx7ww==';
            $tjson['openid'] = 'oCMcF0YMpsb7AU7ATV-oSTphG8xE';
        }else{
            $result = https_request($url);
            $tjson = json_decode($result,true);
        }

        self::ajaxReturnSuccess('ok',$tjson);
    }
}