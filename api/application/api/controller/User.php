<?php
/**
 * Created by PhpStorm.
 * User: iyahe@qq.com (天明)
 * Date: 2017/12/2/002
 * Time: 22:14
 */
namespace app\api\controller;
use app\action;
use app\api\model\BillboardModel;
use app\api\model\GameModel;
use app\api\model\UserModel;
use app\redis;
use think\Cache;
use think\Db;

class User extends action
{
    public function index(){
        self::ajaxReturnSuccess('api');
    }
    public function insert(){
        $data = [
            'my_uid'=>1,
            'my_gid'=>0,
            'my_addtime'=>getStrtime()
        ];
        echo Db::name('mygame')->insertGetId($data);
    }
    public function mygame(){
        $game = new GameModel();
        $list = $game->listd();
        self::ajaxReturnSuccess('ok',$list);
    }
    public function updateInfo(){
        $data = input();
        $data['openid'] = input('openid','');
        $insert = [
            'user_nickname'=>$data['nickName'],
            'user_avatar'=>$data['avatarUrl'],
            'user_address'=>$data['country'].','.$data['province'].','.$data['city'],
            'user_sex'=>$data['gender'],
            'user_openid'=>$data['openid'],
            'user_token'=>md5(time()),
            'user_update'=>getStrtime()
        ];
        if(!$data['openid']){
            self::ajaxReturnError('openid获取失败',$insert);
        }
        $list = Db::name('user')->where(['user_openid'=>$data['openid']])->find();
        if($list){
            $res = Db::name('user')->where(['user_openid'=>$data['openid']])->update($insert);
            $insert['uid'] = $list['user_id'];
        }else{
            $insert['user_addtime'] = getStrtime();
            $res = Db::name('user')->insertGetId($insert);
            $insert['uid'] = $res;
        }
        if($res){
            self::ajaxReturnSuccess('更新成功',$insert);
        }
        self::ajaxReturnError('更新失败',$res);
    }
    public function getUserInfo(){
        $uuid = input('uuid');
        //当前用户是否参与
        $bd = BillboardModel::make()->isBd($uuid);
        $openid = $this->openid;
        $status = 0;
        if(!empty($bd)){
           //访问的用户是否点赞
            $redis = new redis();
            $name = 'zan_'.$uuid;
            $result = $redis->sismember($name,$openid);
            if($result) $status = 1;
        }
        $list = UserModel::make()->getUserInfo($uuid);
        $list['set'] = Db::name('userset')->where('set_uid',$uuid)->find();
        $list['sign'] = Db::name('active')->where('active_uid',$uuid)
            ->order('active_id DESC')
            ->field('active_text')
            ->find();
        self::ajaxReturnSuccess($status,$list);
    }
    //用户的对战详情
    public function UserDet(){
        $uid = input('uuid');
        $order = input('order');
        $gameOrder = Db::name('finds')->where('find_order',$order)->field('find_gid')->find();
        $gameId = $gameOrder['find_gid'];
        $list = Db::name('user')
            ->join('p_setting','p_setting.set_uid=p_user.user_id')
            ->where(['user_id'=>$uid,'set_gid'=>$gameId])
            //->field('user_token,user_password,user_openid',true)
            ->find();
        self::ajaxReturnSuccess('ok',$list);
    }
    public function userNavcate(){
        $data = cache('userConfig');
        if(!$data){
            $data = require_once DOC_ROOT.'/api/config/userConfig.php';
            Cache::set('userConfig',$data,3600);
        }
        self::ajaxReturnSuccess('ok',$data);
    }
    public function userlist(){
        $list = Db::name('user')->limit(10)->order('user_addtime DESC')->select();
        self::ajaxReturnSuccess('ok',$list);
    }
    //用户自定义设置
    public function updateSet(){
        $insert = [];
        $insert['set_backgroundColor'] = input('backgroundColor');
        $insert['set_backgroundImg'] = input('backgroundImg');
        $insert['set_uid'] = $this->uid;
        $config = Db::name('userset')->where('set_uid',$this->uid)->find();
        if($config){
            $res = Db::name('userset')->where('set_uid',$this->uid)->update($insert);
            $msg = '更新成功';
        }else{
            $res = Db::name('userset')->insertGetId($insert);
            $msg = '保存成功';
        }
        self::ajaxReturnSuccess($msg);
    }
    public function myAbout(){
        $limit = input('limit');
       $list =  Db::name('user')
           ->join('leavel','p_leavel.leavel_id=p_user.user_leavel','left')
           ->order('user_update DESC')
           ->paginate($limit);
       self::ajaxReturnSuccess('ok',$list);
    }
}