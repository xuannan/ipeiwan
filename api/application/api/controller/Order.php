<?php
/**
 * Created by PhpStorm.
 * User: iyahe@qq.com (天明)
 * Date: 2017/12/2/002
 * Time: 22:13
 */
namespace app\api\controller;
use app\action;
use app\api\model\BespokeModel;
use app\api\model\MoneyModel;
use app\api\model\OrderModel;
use think\Cache;
use think\Db;

class order extends action
{
    public function index(){
        self::ajaxReturnSuccess('api');
    }
    public function create(){
        $data = input();
        foreach($data as $k=> $v){
           if($v == '不限') {
               $data[$k] = 0;
           }
        }
        $insert = array(
            'find_gid'=>input('c_gid',0),
            'find_sex'=>$data['c_sex'],
            'find_type'=>$data['c_type'],
            'find_money'=>intval($data['c_money'])*100,
            'find_level'=>$data['c_level'],
            'find_index'=>$data['c_index'],
            'find_note'=>$data['c_note'],
            'find_addtime'=>getStrtime(),
            'find_order'=>OrderModel::makeOrder(),
            'find_uid'=>$this->uid,
            'find_online'=>$data['c_online']
        );
        Db::startTrans();//启动事务
        $result  = Db::name('finds')->insertGetId($insert);
        if($result){
            $order = [];
            $order['find_id'] = $insert['find_order'];
            $order['loadingGif'] = $this->loadingGif();
            $order['wait'] = rand(100,6000);
            Db::commit();
            self::ajaxReturnSuccess('提交成功,即将进入匹配队列',$order);
        }
        Db::rollback();
        self::ajaxReturnError('订单创建失败');
    }
    //订单状态
    public function orderStatus(){
        $type = input('type');
        $status = cache('orderStatus');
        if(!$status){
            $status = self::getDB('orderStatus');
            cache('orderStatus',3600);
        }
        self::ajaxReturnSuccess('ok',$status);
    }
    //订单列表
    public function orderlist(){
        $order = new OrderModel();
        $status = input('status');
        $limit = input('limit',12);
        $type = input('type');
        $where= [
            'bes_myid'=>$this->uid,
            'bes_status'=>$status
        ];
        if($type == 'label'){
           unset($where['bes_myid']);
           $where['bes_from'] = $this->uid;
        }
        if($status == 0) unset($where['bes_status']);
        $order->timeOut($this->uid);
        $listd = $order->OrderList($where,$limit);
        self::ajaxReturnSuccess('ok',$listd);
    }
    //修改订单状态
    public function changeOrder(){
        $status = input('status'); //状态
        $label = input('label'); //来源
        $gid = input('gid'); //游戏
        $bes_id = input('bes_id'); //流水
        $where = [];
        $where['bes_myid'] = $this->uid;
        $where['bes_gid'] = $gid;

        if($label<=0){
            $where['bes_from'] = $this->uid;
            unset($where['bes_myid']);
        }
        //扣豌豆,
        if($status == 5){
            $BesInfo = BespokeModel::make()->findInfo(['bes_id'=>$bes_id]);
            $result = MoneyModel::make()->changeAccounts($this->uid,$BesInfo['bes_from'],$BesInfo['bes_minid']);
            if(!$result){
                self::ajaxReturnError('确认失败');
            }
        }
        $result = OrderModel::make()->changeOrder($where,$status);
        if($result){
            self::ajaxReturnSuccess('操作成功');
        }
        self::ajaxReturnError('操作失败');
    }
    //订单评价
    public function evaluate(){
        $imgs = OrderModel::make()->makeUploadImgs(input('img'));;
        $com_address = OrderModel::make()->makeAddress(input('address'));
        $insert = [];
        if( !input('id')){
            self::ajaxReturnError('数据异常');
        }
        if(mb_strlen(input('text')) <= 5){
            self::ajaxReturnError('评论字数不能少于5个字哦');
        }
        $insert['com_content'] = input('text');
        $insert['com_addtime'] = getStrtime();
        $insert['com_imgs'] = $imgs;
        $insert['com_address'] = $com_address;
        $insert['com_start'] = input('start');
        $insert['com_uid'] = $this->uid;
        $insert['com_gid'] = input('gid');
        $insert['com_touid'] = input('from');
        $insert['com_bespoke'] = input('id');
        $bespoke = Db::name('comment')->where(['com_bespoke'=>input('id')])->find();
        if(empty($bespoke)){
            $r = OrderModel::make()->changeOrder(['bes_id'=>input('id')],7);
            if($r){
                $res = Db::name('comment')->insertGetId($insert);
            }else{
                $res = 0;
                self::ajaxReturnError('订单状态修改失败');
            }
        }else{
            $res = Db::name('comment')->where(['com_bespoke'=>input('id')])->update($insert);
        }
        if($res){
            self::ajaxReturnSuccess('操作成功');
        }
        self::ajaxReturnError('操作失败');
    }
    //查看评价
    public function getEvaluate(){
        $look = input('look');
        $list = Db::name('comment')->where('com_bespoke',$look)->find();
        $list['com_address'] = OrderModel::make()->getAddress($list['com_address']);
        self::ajaxReturnSuccess('ok',$list);
    }
    //评价列表
    public function evaluateList(){
        $uuid = input('uuid');
        $list = Db::name('comment')
            ->join('user','p_user.user_id=p_comment.com_touid')
            ->join('gamelist','p_gamelist.game_id=p_comment.com_gid')
            ->join('bespoke','p_bespoke.bes_id=p_comment.com_bespoke')
            ->where('com_uid',$uuid)->paginate();
        self::ajaxReturnSuccess('ok',$list);
    }

    public function loadingGif(){
        $arr = [
            'http://img.he29.com/play/dacdb7cd91b3530ee48cf16f312cff684274b4ac.gif',
            'http://img.he29.com/play/44d78b3ed59c3d7822a3df81c03de620a5fa9fa7.gif',
            'http://img.he29.com/play/e9d8acd5463c4c39bb261c48e792ec60c872f5b7.gif',
            'http://img.he29.com/play/a7bd3d1ba04074a244a8557a73f2f659a4c3d866.gif',
            'http://img.he29.com/play/6091b6ab8e7dd89d9a577d67a5355211f7d0d4b5.gif',
            'http://img.he29.com/play/2a5cd300f200d90a49243f72abdf5d9b6d702b0b.gif',
            'http://img.he29.com/play/ea6e79459048992ce99c8a60b8691d057e3d4567.gif'
        ];
        return $arr[rand(0,count($arr)-1)];
    }
    //我的预约导航
    public function orderNavlist(){
        $arr = array();
        //我预约的玩伴
        $arr[0] = [
            'name'=>'我预约的玩伴',
            'url'=> 'order/index',
            'icon'=> 'http://img.he29.com/play/9d443b4446b1c115be2eacae611c3aa1bfbaa210.png',
            'rightNum'=>OrderModel::make()->total(['bes_status'=>1,'bes_myid'=>$this->uid])
        ];
        $arr[2] = [
            'name'=>'预约我的小伙伴',
            'url'=> 'order/label',
            'icon'=> 'http://img.he29.com/play/f4fea18362da21cdd4b766c0adfe1540a85dd38b.png',
            'rightNum'=>OrderModel::make()->total(['bes_status'=>1,'bes_from'=>$this->uid]),
        ];
        $arr[3] = [
            'name'=>'我关注的小伙伴',
            'url'=> 'user/about',
            'icon'=> 'http://img.he29.com/play/83bc32e73ef3d69f3a02d7f1b6e223aa4aa61b0f.png',
            'rightNum'=>'',
        ];
        $arr[4] = [
            'name'=>'关注我的小伙伴',
            'url'=> 'user/about',
            'icon'=> 'http://img.he29.com/play/f4408cc00679797ed4cd49b7e2ead0587a8b9f8d.png',
            'rightNum'=>0
        ];
        $arr[5] = [
            'name'=>'我的玩伴日常',
            'url'=> 'user/active',
            'icon'=> 'http://img.he29.com/play/498db310dec1bf8da81efc43ffd9f38ea93af972.png',
            'rightNum'=>0
        ];
        self::ajaxReturnSuccess('pk',$arr);
    }
}