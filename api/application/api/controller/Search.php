<?php
/**
 * Created by PhpStorm.
 * User: iyahe@qq.com (天明)
 * Date: 2017/12/2/002
 * Time: 22:14
 */
namespace app\api\controller;
use app\action;
use app\api\model\SearchModel;
use think\Db;

class Search extends action
{
    public function index(){
        self::ajaxReturnSuccess('api');
    }
    public function frind(){
        $find_id = input('find_id');
        $find = Db::name('finds')->where(['find_order'=>$find_id])->find();
        $where = [];
        $where['set_gid'] = $find['find_gid'];
        if($find['find_sex']) {
            $where['set_sex'] = $find['find_sex']; //性别;
        }
        $where['set_monery'] = ['>',$find['find_money']]; //金币数量大于
        $where['set_leavel'] = ['>',$find['find_level']]; //等级大于
        //部分游戏没有段位设置
        if($find['find_index']) {
            $where['set_index'] = ['like','%'.$find['find_index'].'%']; //段位
        }
        $where['user_online'] = $find['find_online'];
        $listCount = Db::name('setting')
            ->join('user','p_user.user_id=p_setting.set_uid')
            ->where($where)
            ->count();
        $cacheName = $find_id; //缓存名称换位orderid
        $return = [];
        $return['length'] = $listCount;
        if($return['length'] > 0) {
            $return['search_id'] = $cacheName;
            $return['time'] = getStrtime();
            cache($cacheName,$where,3600);
            self::ajaxReturnSuccess('匹配成功',$return);
        }else{
            self::ajaxReturnError('暂无符合条件的玩家');
        }
    }
    public function uuList(){
        $search_id= input('search_id');
        $where = cache($search_id);
        $limit = input('limit',10);
        $list = Db::name('setting')
            ->join('user','p_user.user_id=p_setting.set_uid')
            ->where($where)
            ->paginate($limit)
            ->each(function($item,$key){
                $wctypeid = $item["set_uid"];
                $list = Db::name('photowall')->where('wall_uid',$wctypeid)
                    ->limit(4)->order('wall_addtime DESC')->select(); //根据ID查询相关其他信息
                $item['photo'] = $list;
                $item['user_address'] = explode(',',$item['user_address']);
                $item['user_sex'] = $item['user_sex'] == 1?'男':'女';
                $item['photoPrv'] = json_encode($list);
                return $item;
            });
        self::ajaxReturnSuccess('ok',$list);
    }
    public function success(){
        $orderid = input('search_id');
        $choseID = input('choseID');
        $result = Db::name('finds')->where('find_order',$orderid)->update(['find_uuid'=>$choseID]);
        if($result){
            self::ajaxReturnSuccess('操作成功',$result);
        }
        self::ajaxReturnError('操作失败,请重试');
    }
    public function findResult(){
        $search_id = input('search_id');
        $find = Db::name('finds')->where('find_order',$search_id)->find();
        self::ajaxReturnSuccess('ok',$find);
    }
}