<?php
/**
 * Created by PhpStorm.
 * User: iyahe@qq.com (天明)
 * Date: 2017/12/4
 * Time: 14:17
 */
namespace app\api\controller;
use app\action;
use app\api\model\MoneyModel;
use app\api\model\OrderModel;
use app\api\model\UserModel;
use think\Db;
use \app\api\model\GameModel;

class Game extends action
{
    public function index(){}
    public function insert(){
        $data = [
            'game_name'=>'王者荣耀',
            'game_auth'=>'腾讯游戏天美工作室',
            'game_thumb'=>'http://img.he29.com/play/6c8526f06d3ff592891fb7f9677de4c09a27cde1.jpeg',
            'game_addtime'=>getStrtime()
        ];
        echo Db::name('gamelist')->insertGetId($data);
    }
    public function gameList(){
        $game = new GameModel();
        $list = $game->listd([],'left');
        $about = $game->myAbout($this->uid);
        self::ajaxReturnSuccess('ok',['list'=>$list,'about'=>$about]);
    }
    public function watch(){
        $id = input('gid');
        $insert = [
            'my_uid'=>$this->uid,
            'my_gid'=>$id
        ];
        $res = Db::name('mygame')->where($insert)->find();
        if(empty($res)){
            $insert['my_addtime'] = getStrtime();
            $result = Db::name('mygame')->insertGetId($insert);
            if($result){
                self::ajaxReturnSuccess('关注成功',$result);
            }
        }
        self::ajaxReturnError('关注失败');
    }
    public function isWatch(){
        $gid = input('gid');
        $result = Db::name('mygame')->where(['my_gid'=>$gid,'my_uid'=>$this->uid])->find();
        if(empty($result)){
            self::ajaxReturnError('关注成功');
        }else{
            self::ajaxReturnSuccess('您已经关注此游戏了',$result);
        }
    }
    public function watchAll(){
        $list = Db::name('mygame')->where(['my_uid'=>$this->uid])->field('my_gid')->find();
        self::ajaxReturnSuccess($list);
    }
    public function info(){
        $gid = input('gid');
        $list = Db::name('gamelist')->where('game_id',$gid)->find();
        self::ajaxReturnSuccess('ok',$list);
    }
    public function sendTempMessage(){
        $uuid = input('uuid');
        $orderId = input('orderId');
        $form_id = input('form_id');
        $user = UserModel::make()->getUserInfo($uuid);
        $orderInfo = OrderModel::make()->orderInfo($orderId);
        $sendData = [];
        $sendData['template_id'] = 'ppJ3E4uuwF_C1_fnE64DWU7nhPz1pWvLifB7-k22xbY';
        $sendData['openid'] = $user['user_openid'];
        $sendData['url'] = '/pages/game/chat';
        $sendData['formId'] = $form_id;
        $sendData['keyword'] = array(
            'keyword1'=>['value'=>$user['user_nickname'].'-王者荣耀','color'=>''],
            'keyword2'=>['value'=>$orderId,'color'=>''],
            'keyword3'=>['value'=>$orderInfo['find_once'],'color'=>''],
            'keyword4'=>['value'=>($orderInfo['find_money']/100),'color'=>''],
            'keyword5'=>['value'=>$user['user_nickname'],'color'=>''],
            'keyword6'=>['value'=>'立即使用','color'=>''],
            'keyword7'=>['value'=>getStrtime(),'color'=>''],
            'keyword8'=>['value'=>'','color'=>''],
            'keyword9'=>[
                'value'=>'请在5分钟内联系玩家 '.$user['user_nickname'].' ,逾期订单将被取消,如果被雇主投诉,您的信用积分将被扣除10分',
                'color'=>''
            ],
            'keyword10'=>['value'=>$orderInfo['find_note'],'color'=>''],
        );
        $kefu = new Kefu();
        self::logger($sendData,'模板消息发送的数据');
        $result = $kefu->sendTemplateMessage($sendData);
        self::logger($result,'模板消息返回的数据');
    }
    public function HomeMyGame(){
        $uuid = input('uuid');
        $list = GameModel::make()->userAboutGame(['my_uid'=>$uuid]);
        self::ajaxReturnSuccess('ok',$list);
    }
    //预约用户信息
    public function getmapinfo(){
        $uuid = input('uuid');
        $gameid = input('gid');
        $info = UserModel::make()->getUserInfo($uuid);
        $gameSet = GameModel::make()->userAboutGame(['my_uid'=>$uuid,'my_gid'=>$gameid]);
        self::ajaxReturnSuccess('ok',['info'=>$info,'set'=>$gameSet[0]]);
    }
    //创建预约信息
    public function bespoke(){
        $money = MoneyModel::make()->getMoney($this->uid,2,1);
        if($money <= 0){
            self::ajaxReturnError('亲!您的豆豆不足,请先充值后再来预约吧!','',-10);
        }else{
            Db::startTrans();//启动事务
            if($this->uid == input('uuid')){
                self::ajaxReturnError('亲!再亲一下(✿◡‿◡),不能自己预约自己哦!');
            }
            $alert_time = date('Y-m-d H:i:s',(time() + intval(input('alert'))*60));
            $startDay = input('startDay');
            $startTime = input('startTime');
            $time = strtotime($startDay.' '.$startTime)+60*30;
            if($time <= time()){
                self::ajaxReturnError('您预约的时间已经过去30分钟了,请重新选择一个时间吧');
            }
            $startTimeStatus = date('Y-m-d H:i:s',$time);
            $insert = [];
            $insert['bes_myid'] = $this->uid;
            $insert['bes_starttime'] = $startTimeStatus;
            $insert['bes_gid'] = input('gid');
            $insert['bes_number'] = input('play');
            $insert['bes_pay'] = input('playMoney');
            $insert['bes_alert_time'] = $alert_time;
            $insert['bes_addtime'] = getStrtime();
            $insert['bes_formid'] = input('formid');
            $insert['bes_note'] = input('note');
            $insert['bes_addtime'] = getStrtime();
            $insert['bes_from'] = input('uuid');
            $insert['bes_minid'] = '';
            $isBesplke = Db::name('bespoke')->where([
                'bes_gid'=>$insert['bes_gid'],
                'bes_myid'=>$insert['bes_myid']
            ])->find();
            if(!empty($isBesplke)){
                $orderStatus = OrderModel::make()->orderFind($isBesplke['bes_order']);
                if($orderStatus['order_status'] == 1){
                    self::ajaxReturnError('预约失败,您对的当前用户还有未完成的订单,请完成后继续');
                }
            }
            $insert['bes_order'] = OrderModel::make()->makeOrder();
            $bespoke = Db::name('bespoke')->insertGetId($insert);
            if($bespoke){
                $result = OrderModel::make()->makeOrderData($insert['bes_order'],2);
                if($result){
                    $money_guid = md5(time());
                    $money = MoneyModel::make()->addMoney(2,$insert['bes_pay'],$this->uid,-1,'预约玩家',$money_guid);
                    if($money > 0){
                        Db::name('bespoke')->where(['bes_id'=>$bespoke])->update(['bes_minid'=>$money_guid]);
                        Db::commit();
                        self::ajaxReturnSuccess('预约成功',$insert['bes_order']);
                    }else{
                        Db::rollback();
                        if($money == -1){
                            self::ajaxReturnError('豌豆扣除失败,余额不足');
                        }else{
                            self::ajaxReturnError('豌豆不足');
                        }
                    }
                }else{
                    Db::rollback();
                    self::ajaxReturnError('订单创建失败');
                }
            }
        }
    }
}