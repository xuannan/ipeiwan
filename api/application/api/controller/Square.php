<?php
/**
 * Created by PhpStorm.
 * User: iyahe@qq.com (天明)
 * Date: 2017/12/10/010
 * Time: 12:30
 */
namespace app\api\controller;
use app\action;
use think\Cache;
use think\Db;

class Square extends action
{
    public function getData(){
        $limit = input('limit',12);
        $tag = input('tag',1);
        $gid = input('gid');
        $list=  Db::name('tag_user')
            ->join('user','p_user.user_id=p_tag_user.ugid_uid')
            ->where(['ugid_gid'=>$gid,'ugid_tagid'=>$tag])
            ->paginate($limit)->each(function($item,$key){
                $wctypeid = $item["user_id"];
                $list = Db::name('photowall')->where('wall_uid',$wctypeid)
                    ->limit(4)->order('wall_addtime DESC')->select(); //根据ID查询相关其他信息
                $set= Db::name('setting')
                    ->where(['set_uid'=>$item['user_id'],'set_gid'=>$item['ugid_gid']])
                    ->field('set_tag,set_note')
                    ->find();
                $item['photo'] = $list;
                $item['set'] = $set;
                $item['user_address'] = explode(',',$item['user_address']);
                $item['user_sex'] = $item['user_sex'] == 1?'男':'女';
                $item['photoPrv'] = json_encode($list);
                return $item;
            });
        self::ajaxReturnSuccess('ok',$list);
    }
}