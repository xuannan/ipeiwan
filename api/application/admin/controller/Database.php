<?php
/**
 * Created by PhpStorm.
 * User: iyahe@qq.com (天明)
 * Date: 2017/10/13
 * Time: 16:05
 */
namespace app\admin\controller;
use app\common\controller\Adminbase;
use extend\database\DbManage;
use think\Db;

class Database extends Adminbase
{
    public $path = './backup/';
    public function index()
    {
        $list =Db::query('SHOW TABLE STATUS');
        $list = array_map('array_change_key_case', $list);
        $this->assign('list', $list);
        return $this->fetch();
    }
    public function init(){
        return  new DbManage(
            config("database.hostname"),
            config("database.username"),
            config("database.password"),
            config("database.database"),
            config("database.charset")
        );
    }
    //备份
    public function backup()
    {
        $db = $this->init();
        return json($db->backup());
    }
    //备份列表
    public function backlist(){
        foreach($list = scandir('./backup/') as $k=> $v){
            if(!stripos($v,'.sql')){
                unset($list[$k]);
            }
        }
        return $list;
    }
    //还原
    public function restore(){
        $db = $this->init();
        $name = input('name');
        json($db->restore ($this->path . $name));
    }
    public function recovery(){
        $list = $this->backlist();
        $path = './backup/';
        $infoArr = [];
        foreach ($list as $k=>$v){
            $infoArr[$k]['name'] = $v;
            $infoArr[$k]['info'] = filesize($path.$v)/1024;
            $infoArr[$k]['filetime'] = getStrtime(filemtime($path.$v));
        }
        $this->assign('list', $infoArr);
        return $this->fetch();
    }
}