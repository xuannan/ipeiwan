<?php
/**
 * Created by PhpStorm.
 * User: iyahe@qq.com (天明)
 * Date: 2017/10/12
 * Time: 17:14
 */
namespace app\admin\controller;
use app\common\controller\Adminbase;
use think\Db;

class Test extends Adminbase
{
    public function index(){
        return $this->fetch();
    }
}