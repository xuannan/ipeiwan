<?php
/**
 * Created by PhpStorm.
 * User: iyahe@qq.com (天明)
 * Date: 2018/1/5
 * Time: 10:16
 */
namespace app\admin\controller;
use app\common\controller\Adminbase;
use think\Db;

class Editor extends Adminbase
{
    public function index(){
        $this->view->engine->layout(false);
        $config = [];
        $config['name'] = '';
        $config['value'] = 'hello world';
        $config['language'] = 'html';
        $config['editorName'] = input('ename');
        $this->assign('config',$config);
        return view();
    }
}