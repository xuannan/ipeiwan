<?php
/**
 * Created by PhpStorm.
 * User: iyahe@qq.com (天明)
 * Date: 2018/1/4
 * Time: 10:58
 */
namespace app\admin\controller;
use app\action;
use app\api\controller\Billboard;
use app\api\controller\order;
use app\api\model\OrderModel;
use app\common\controller\Adminbase;
use think\Db;

class Play extends Adminbase
{
    public function banner(){
        $this->assign('list',[]);
        if(input('act') == 'add'){
            if($_POST){
                $value = input('value');
                $valueData = json_decode($value,true);
                if(is_array($valueData)){
                    $r = action::setDB('banner',$valueData);
                    if($r){
                        action::ajaxReturnSuccess('更改成功');
                    }else{
                        action::ajaxReturnError('没有变化或者更改失败');
                    }
                }else{
                    action::ajaxReturnError('更改失败,格式有误');
                }
            }else{
                $list = action::getDB('banner');
                $this->assign('bannerSet',json_encode($list));
                return $this->fetch('banner_Add');
            }
        }else{
            $list = action::getDB('banner');
            $this->assign('banner',$list);
            $this->assign('bannerJson',json_encode($list));
            return $this->fetch();
        }
    }
    public function Billboard(){
        if(input('act') == 'edit') {
            $data = input('value');
            $this->saveJson($data,'billboard');
        }else{
            $bill = new Billboard();
            $bind = input('b',0);
            $list = $bill->getList(true);
            $listd = $list['list'][$bind]['info'];
            $this->assign('list',$listd);
            $this->assign('listd',$list);
            $this->assign('bdconfig',json_encode($list['config']));
        }
        return $this->fetch();
    }
    public function saveJson($value,$name){
        $valueData = json_decode($value,true);
        if(is_array($valueData)){
            $r = action::setDB($name,$valueData);
            if($r){
                action::ajaxReturnSuccess('更改成功');
            }else{
                action::ajaxReturnError('没有变化或者更改失败');
            }
        }else{
            action::ajaxReturnError('更改失败,格式有误');
        }
    }
    public function game(){
        if(input('act') == 'edit') {
            $data = json_decode(input('value'),true);
            if(is_array($data)){
                $data['game_addtime'] = getStrtime();
                $r = Db::name('gamelist')->insertGetId($data);
                if($r){
                    action::ajaxReturnSuccess('添加成功');
                }else{
                    action::ajaxReturnError('添加失败');
                }
            }else{
                action::ajaxReturnError('添加失败,格式有误');
            }
        }else{
            $list = Db::name('gamelist')
                ->join('gametag','p_gametag.type_id=p_gamelist.game_type')
                ->order('game_order DESC')
                ->select();
            $this->assign('list',$list);
            $def = '{
            "game_name": "王者荣耀",
            "game_auth": "腾讯游戏天美工作室",
            "game_thumb": "http://img.he29.com/play/58c358435cad9d985da5236e9d497f2bc2ebcb0d.jpeg",
            "game_thumb64": ""
            }';
            $this->assign('gameconfig',json_encode(json_decode($def,true)));
            return $this->fetch();
        }
    }
    public function tag(){
        $list = Db::name('tag')->select();
        foreach ($list as $k=>$v){
            $list[$k]['total'] = Db::name('user')->where(['user_leavel'=>$v['tag_id']])->count();
        }
        $this->assign('list',$list);
        return $this->fetch();
    }
    //预约管理
    public function order(){
        $status = input('status','');
        $order = new OrderModel();
        if($status){
            $where = ['bes_status'=>$status];
        }else{
            $where = [];
        }
        $limit = 12;
        $listd = $order->OrderList($where,$limit);
        $pages = $listd->render();
        $status = action::getDB('orderStatus');
        $this->assign('status',$status);
        $this->assign('pages', $pages);
        $this->assign('list',$listd);
        return $this->fetch();
    }
    //订单管理
    public function orders(){
        return $this->fetch();
    }
}