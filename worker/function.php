<?php
/**
 * Created by PhpStorm.
 * User: iyahe@qq.com (天明)
 * Date: 2017/12/17/017
 * Time: 12:37
 */
define('API_HOST','https://play.he29.com/index.php?s=/api/socket/');
function setlogs($name='',$str='',$lev=3,$nameType='info'){
    $type = 'text';
    if(!is_array($str)){
        $inputData = $str;
    }else{
        $inputData = json_encode($str,JSON_UNESCAPED_UNICODE);
        $type = 'array';
    }
    $insert = '['.$name.'('.$type.')]['.date('Y-m-d H:i:s',time()).']'.PHP_EOL.'-------------------------------------'.PHP_EOL.$inputData.PHP_EOL.'-------------------------------------'.PHP_EOL;
    file_put_contents(__DIR__.'/log/socket.log',$insert,FILE_APPEND);
}
function https_request($url, $data = null)
{
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
    if(!empty($data)) {
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
    }
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    $output = curl_exec($curl);
    curl_close($curl);
    return $output;
}